//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.RaqebModon.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ENTITY_PHOTOS
    {
        public decimal ENTITY_PHOTO_ID { get; set; }
        public decimal ENTITY_PHOTO_TYPE_ID { get; set; }
        public decimal RAQEB_ENTITY_ID { get; set; }
        public string ENTITY_PHOTO_NAME { get; set; }
        public byte[] ENTITY_PHOTO_OBJECT { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public decimal MODIFIED_BY { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public decimal STATUS_RECORD { get; set; }
        public System.Guid ROWID { get; set; }
    
        public virtual ENTITY_PHOTO_TYPES ENTITY_PHOTO_TYPES { get; set; }
    }
}
