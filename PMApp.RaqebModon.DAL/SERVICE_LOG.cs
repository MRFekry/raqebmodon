//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.RaqebModon.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SERVICE_LOG
    {
        public decimal SERVICE_LOG_ID { get; set; }
        public decimal USER_ID { get; set; }
        public System.DateTime REQUEST_TIMESTAMP { get; set; }
        public string USER_NAME { get; set; }
        public string USER_PASS { get; set; }
        public string APPLICATION_VERSION { get; set; }
        public string FRAMEWORK_NAME { get; set; }
        public string FRAMEWORK_VERSION { get; set; }
        public string DEVICE_NAME { get; set; }
        public string DEVICE_PLATFORM { get; set; }
        public string DEVICE_VERSION { get; set; }
        public string DEVICE_UUID { get; set; }
        public string DEVICE_IMAC { get; set; }
        public string DEVICE_IMEI { get; set; }
        public string DEVICE_SERIAL { get; set; }
        public string DEVICE_IP { get; set; }
        public string CONNECTION_TYPE { get; set; }
        public string REQUESTED_SERVER { get; set; }
        public decimal REQUESTED_PORT { get; set; }
        public string REQUESTED_URI { get; set; }
        public string REQUESTED_METHOD { get; set; }
        public string REQUESTED_PARAMETERS { get; set; }
        public string REQUESTED_SERVICE_TYPE { get; set; }
        public Nullable<double> REQUESTED_LATITUDE { get; set; }
        public Nullable<double> REQUESTED_LONGITUDE { get; set; }
        public Nullable<double> REQUESTED_ACCURACY { get; set; }
        public System.DateTime RESPONSE_TIMESTAMP { get; set; }
        public string REQUEST_STATUS { get; set; }
        public string REQUEST_STATUS_DETAILS { get; set; }
        public System.Guid ROWID { get; set; }
    }
}
