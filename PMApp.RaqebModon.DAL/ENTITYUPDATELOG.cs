//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.RaqebModon.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ENTITYUPDATELOG
    {
        public double ENTITY_UPDATE_LOG_ID { get; set; }
        public double TABLEOBJECT { get; set; }
        public string OLD_BANNER_NAME { get; set; }
        public Nullable<double> OLD_WORKER_COUNT { get; set; }
        public Nullable<double> OLD_LATITUDE { get; set; }
        public Nullable<double> OLD_LONGITUDE { get; set; }
        public Nullable<double> OLD_ACCURACY { get; set; }
        public double MODIFIED_BY { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public string NEW_BANNER_NAME { get; set; }
        public Nullable<double> NEW_WORKER_COUNT { get; set; }
        public Nullable<double> NEW_LATITUDE { get; set; }
        public Nullable<double> NEW_LONGITUDE { get; set; }
        public Nullable<double> NEW_ACCURACY { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public double STATUS_RECORD { get; set; }
        public Nullable<double> RAQEB_ENTITY_ID { get; set; }
        public string ENTITY_VISIT_UUID { get; set; }
        public string OLD_PHONE { get; set; }
        public string NEW_PHONE { get; set; }
        public string OLD_EMAIL { get; set; }
        public string NEW_EMAIL { get; set; }
        public string OLD_WEBSITE { get; set; }
        public string NEW_WEBSITE { get; set; }
        public string OLD_MOBILE { get; set; }
        public string NEW_MOBILE { get; set; }
        public string OLD_OPENING_FROM1 { get; set; }
        public string OLD_OPENING_FROM2 { get; set; }
        public string OLD_OPENING_TO1 { get; set; }
        public string OLD_OPENING_TO2 { get; set; }
        public string NEW_OPENING_FROM1 { get; set; }
        public string NEW_OPENING_FROM2 { get; set; }
        public string NEW_OPENING_TO1 { get; set; }
        public string NEW_OPENING_TO2 { get; set; }
        public System.Guid ROWID { get; set; }
    }
}
