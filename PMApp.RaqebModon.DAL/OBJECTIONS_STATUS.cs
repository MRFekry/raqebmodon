//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.RaqebModon.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class OBJECTIONS_STATUS
    {
        public double STATUS_ID { get; set; }
        public string NAME { get; set; }
        public System.DateTime CREATE_DATE { get; set; }
        public System.DateTime MODIFIED_DATE { get; set; }
        public double MODIFIED_BY { get; set; }
        public double STATUS_RECORD { get; set; }
    }
}
