using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class OrderTypeService : BaseService, IOrderType
    {
        private const string serviceName = "OrderTypeService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region OrderTypeServices
        private const string operationName_DoOrderTypeList = "DoOrderTypeList";
        private const string operationName_DoOrderTypeDelete = "DoOrderTypeDelete";
        private const string operationName_DoOrderTypeInsert = "DoOrderTypeInsert";
        private const string operationName_DoOrderTypeUpdate = "DoOrderTypeUpdate";

        public OrderTypeResponse DoOrderTypeList(OrderTypeRequest req)
        {
            var res = new OrderTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrderTypeList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrderTypeList, false, false, (OrderTypeRequest request, OrderTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new OrderTypeRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new OrderTypeRecord
                    {

						ApplicationID = p.ApplicationID,
						CreatedBy = p.CreatedBy,
						CreationDate = p.CreationDate,
						Description = p.Description,
						IconClass = p.IconClass,
						ID = p.ID,
						IsDeleted = p.IsDeleted,
						LastUpdateDate = p.LastUpdateDate,
						ModifiedBy = p.ModifiedBy,
						Name = p.Name,
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var orderTypes = ApplyPaging(request, query, out count);

                    response.OrderTypeRecords = orderTypes.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public OrderTypeResponse DoOrderTypeDelete(OrderTypeRequest req)
        {
            var res = new OrderTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrderTypeDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrderTypeDelete, true, false, (OrderTypeRequest request, OrderTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.OrderTypeRecord != null && request.OrderTypeRecord.ID > 0)
                    {
                        var orderTypeRepository = new OrderTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var orderType = orderTypeRepository.FirstOrDefault(p => p.ID == request.OrderTypeRecord.ID);

                        if (orderType != null)
                        {
                            orderType.IsDeleted = true;
                            orderTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidOrderTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public OrderTypeResponse DoOrderTypeInsert(OrderTypeRequest req)
        {
            var res = new OrderTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrderTypeInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrderTypeInsert, true, false, (OrderTypeRequest request, OrderTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.OrderTypeRecord != null)
                    {
                        var orderTypeRepository = new OrderTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var orderType = OrderTypeServiceManager.CreateOrUpdateOrderType(request.ApplicationID, request.UserID, request.OrderTypeRecord);
                        orderTypeRepository.Add(orderType);
                        orderTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.OrderTypeRecord.ID = orderType.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidOrderTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public OrderTypeResponse DoOrderTypeUpdate(OrderTypeRequest req)
        {
            var res = new OrderTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrderTypeUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrderTypeUpdate, true, false, (OrderTypeRequest request, OrderTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.OrderTypeRecord != null)
                    {
                        var orderTypeRepository = new OrderTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var orderType = orderTypeRepository.FirstOrDefault(p => p.ID == request.OrderTypeRecord.ID);

                        if (orderType != null)
                        {
orderType = OrderTypeServiceManager.CreateOrUpdateOrderType(request.ApplicationID, request.UserID, request.OrderTypeRecord, orderType);

orderTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidOrderTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}