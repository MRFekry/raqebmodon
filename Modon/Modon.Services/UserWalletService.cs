using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.Interfaces;
using Modon.Services.Managers;
using Newtonsoft.Json.Linq;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class UserWalletService : BaseService, IUserWallet
    {
        private const string serviceName = "UserWalletService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region UserWalletServices
        public const string operationName_DoUserWalletList = "DoUserWalletList";
        public const string operationName_DoUserWalletInsert = "DoUserWalletInsert";
        public const string operationName_DoUserWalletUpdate = "DoUserWalletUpdate";
        public const string operationName_DoUserWalletDelete = "DoUserWalletDelete";

        //public const string operationName_DoTransfereOnHoldToEarning = "DoTransferFromPaidToEarning";



        public UserWalletResponse DoUserWalletList(UserWalletRequest req)
        {
            var res = new UserWalletResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserWalletList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserWalletList, false, false, (UserWalletRequest request, UserWalletResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new UserWalletRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new UserWalletRecord
                    {

                        ID = p.ID,
                        UserID = p.UserID,
                        Points = p.Points,
                        StatusID = p.StatusID,
                        Notes = p.Notes,
                        IsDeleted = p.IsDeleted,
                        CreatedBy = p.CreatedBy,
                        UserFullName = p.Common_User.FullName,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        CreationDate = p.CreationDate,
                        ApplicationID = p.ApplicationID,

                    });
                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }
                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var userWallets = ApplyPaging(request, query, out count, false);
                  
                    response.UserWalletRecords = userWallets.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }
        public UserWalletResponse DoUserWalletInsert(UserWalletRequest req)
        {
            var res = new UserWalletResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserWalletInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserWalletInsert, true, false, (UserWalletRequest request, UserWalletResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.UserWalletRecord != null)
                    {
                        var userWalletRepository = new UserWalletRepository(CurrentContextEntities, request.ApplicationID);
                        var common_User = new Common_UserRepository(CurrentContextEntities, request.ApplicationID).FirstOrDefault(p => p.ID == request.UserID);
                        var areasRepository = new AreaRepository(CurrentContextEntities, request.ApplicationID);

                        if (request.UserWalletRecord != null)
                        {
                            long userID = request.UserID;
                            if (request.UserWalletRecord.UserID != 0)
                                userID = request.UserWalletRecord.UserID;

                                var area = WalletServiceManager.CreateWalletRecord(request.ApplicationID, userID, request.UserWalletRecord);
                            var file = request.UserWalletRecord.WebDocumentFile;
                            if (file != null && file.ContentLength > 0)
                            {
                                area.Notes = ImageHelper.ImageResizeAndSave(file, ConfigManager.GetInstance(request.ApplicationID).DefaultImageWidth, ConfigManager.GetInstance(request.ApplicationID).DefaultImageHeight);
                            }
                            else
                                area.Notes = "~";
                            userWalletRepository.Add(area);

                            var userAreaID = common_User.AreaID;
                            var arearecord = areasRepository.FirstOrDefault(p => p.ID == userAreaID);
                            arearecord.Points += area.Points;
                            if (arearecord.ParentID != null)
                            {
                                var arearecord2 = areasRepository.FirstOrDefault(p => p.ID == arearecord.ParentID);
                                arearecord2.Points += area.Points;
                            }

                            userWalletRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr); isInValidObject = false;
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                            isInValidObject = false;

                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserWalletTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public UserWalletResponse DoUserWalletUpdate(UserWalletRequest req)
        {
            var res = new UserWalletResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserWalletInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserWalletInsert, true, false, (UserWalletRequest request, UserWalletResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.UserWalletRecord != null)
                    {
                        var userWalletRepository = new UserWalletRepository(CurrentContextEntities, request.ApplicationID);
                        var common_User = new Common_UserRepository(CurrentContextEntities, request.ApplicationID).FirstOrDefault(p => p.ID == request.UserWalletRecord.UserID);
                        var areasRepository = new AreaRepository(CurrentContextEntities, request.ApplicationID);
                        if (request.UserWalletRecord != null)
                        {

                            var area = userWalletRepository.FirstOrDefault(p => p.ID == request.UserWalletRecord.ID);
                            var oldpoints = area.Points;
                            area.Points = request.UserWalletRecord.Points;
                            var file = request.UserWalletRecord.WebDocumentFile;
                            if (file != null && file.ContentLength > 0)
                            {
                                area.Notes = ImageHelper.ImageResizeAndSave(file, ConfigManager.GetInstance(request.ApplicationID).DefaultImageWidth, ConfigManager.GetInstance(request.ApplicationID).DefaultImageHeight);
                            }

                            var updatedvalue = request.UserWalletRecord.Points - oldpoints;

                            var userAreaID = common_User.AreaID;
                            var arearecord = areasRepository.FirstOrDefault(p => p.ID == userAreaID);
                            if (arearecord !=null)
                            {
                                arearecord.Points += updatedvalue;
                                if (arearecord.ParentID != null)
                                {
                                    var arearecord2 = areasRepository.FirstOrDefault(p => p.ID == arearecord.ParentID);
                                    arearecord2.Points += updatedvalue;
                                }
                            }
                           



                            userWalletRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr); isInValidObject = false;
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserWalletTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        public UserWalletResponse DoUserWalletDelete(UserWalletRequest req)
        {
            var res = new UserWalletResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserWalletDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserWalletDelete, true, false, (UserWalletRequest request, UserWalletResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.UserWalletRecord.ID != 0)
                    {
                        var userWalletRepository = new UserWalletRepository(CurrentContextEntities, request.ApplicationID);
                        var common_User = new Common_UserRepository(CurrentContextEntities, request.ApplicationID).FirstOrDefault(p => p.ID == request.UserID);
                        var areasRepository = new AreaRepository(CurrentContextEntities, request.ApplicationID);

                        var area = userWalletRepository.FirstOrDefault(p => p.ID == request.UserWalletRecord.ID);
                        userWalletRepository.Delete(area);

                        var userAreaID = common_User.AreaID;
                        var arearecord = areasRepository.FirstOrDefault(p => p.ID == userAreaID);
                        arearecord.Points -= area.Points;
                        if (arearecord.ParentID != null)
                        {
                            var arearecord2 = areasRepository.FirstOrDefault(p => p.ID == arearecord.ParentID);
                            arearecord2.Points -= area.Points;
                        }
                        userWalletRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr); isInValidObject = false;
                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.WalletDeletedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserWalletTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        public static List<long?> RecursiveChildren(Area[] items, long toplevelid, bool getParent = true)
        {
            List<long?> inner = new List<long?>();
            if (getParent)
            {
                inner.Add(items.FirstOrDefault(item => item.ID == toplevelid).ID);
            }
            foreach (var t in items.Where(item => item.ParentID == toplevelid))
            {
                inner.Add(t.ID);
                inner = inner.Union(RecursiveChildren(items, t.ID)).ToList();
            }

            return inner;
        }


        #endregion
    }
}