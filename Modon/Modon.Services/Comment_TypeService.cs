using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class Comment_TypeService : BaseService, IComment_Type
    {
        private const string serviceName = "Comment_TypeService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region Comment_TypeServices
        private const string operationName_DoComment_TypeList = "DoComment_TypeList";
        private const string operationName_DoComment_TypeDelete = "DoComment_TypeDelete";
        private const string operationName_DoComment_TypeInsert = "DoComment_TypeInsert";
        private const string operationName_DoComment_TypeUpdate = "DoComment_TypeUpdate";

        public Comment_TypeResponse DoComment_TypeList(Comment_TypeRequest req)
        {
            var res = new Comment_TypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoComment_TypeList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoComment_TypeList, false, false, (Comment_TypeRequest request, Comment_TypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new Comment_TypeRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new Comment_TypeRecord
                    {

						ApplicationID = p.ApplicationID,
						CreatedBy = p.CreatedBy,
						CreationDate = p.CreationDate,
						Description = p.Description,
						IconClass = p.IconClass,
						ID = p.ID,
						IsDeleted = p.IsDeleted,
						LastUpdateDate = p.LastUpdateDate,
						ModifiedBy = p.ModifiedBy,
						Name = p.Name,
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var comment_Types = ApplyPaging(request, query, out count);

                    response.Comment_TypeRecords = comment_Types.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public Comment_TypeResponse DoComment_TypeDelete(Comment_TypeRequest req)
        {
            var res = new Comment_TypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoComment_TypeDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoComment_TypeDelete, true, false, (Comment_TypeRequest request, Comment_TypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Comment_TypeRecord != null && request.Comment_TypeRecord.ID > 0)
                    {
                        var comment_TypeRepository = new Comment_TypeRepository(CurrentContextEntities, request.ApplicationID);
                        var comment_Type = comment_TypeRepository.FirstOrDefault(p => p.ID == request.Comment_TypeRecord.ID);

                        if (comment_Type != null)
                        {
                            comment_Type.IsDeleted = true;
                            comment_TypeRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidComment_TypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Comment_TypeResponse DoComment_TypeInsert(Comment_TypeRequest req)
        {
            var res = new Comment_TypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoComment_TypeInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoComment_TypeInsert, true, false, (Comment_TypeRequest request, Comment_TypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Comment_TypeRecord != null)
                    {
                        var comment_TypeRepository = new Comment_TypeRepository(CurrentContextEntities, request.ApplicationID);
                        var comment_Type = Comment_TypeServiceManager.CreateOrUpdateComment_Type(request.ApplicationID, request.UserID, request.Comment_TypeRecord);
                        comment_TypeRepository.Add(comment_Type);
                        comment_TypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.Comment_TypeRecord.ID = comment_Type.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidComment_TypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Comment_TypeResponse DoComment_TypeUpdate(Comment_TypeRequest req)
        {
            var res = new Comment_TypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoComment_TypeUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoComment_TypeUpdate, true, false, (Comment_TypeRequest request, Comment_TypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Comment_TypeRecord != null)
                    {
                        var comment_TypeRepository = new Comment_TypeRepository(CurrentContextEntities, request.ApplicationID);
                        var comment_Type = comment_TypeRepository.FirstOrDefault(p => p.ID == request.Comment_TypeRecord.ID);

                        if (comment_Type != null)
                        {
comment_Type = Comment_TypeServiceManager.CreateOrUpdateComment_Type(request.ApplicationID, request.UserID, request.Comment_TypeRecord, comment_Type);

comment_TypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidComment_TypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}