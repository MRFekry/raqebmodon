using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class ActivityTypeService : BaseService, IActivityType
    {
        private const string serviceName = "ActivityTypeService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region ActivityTypeServices
        private const string operationName_DoActivityTypeList = "DoActivityTypeList";
        private const string operationName_DoActivityTypeDelete = "DoActivityTypeDelete";
        private const string operationName_DoActivityTypeInsert = "DoActivityTypeInsert";
        private const string operationName_DoActivityTypeUpdate = "DoActivityTypeUpdate";

        public ActivityTypeResponse DoActivityTypeList(ActivityTypeRequest req)
        {
            var res = new ActivityTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivityTypeList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivityTypeList, false, false, (ActivityTypeRequest request, ActivityTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new ActivityTypeRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new ActivityTypeRecord
                    {

						ApplicationID = p.ApplicationID,
						CreatedBy = p.CreatedBy,
						CreationDate = p.CreationDate,
						Description = p.Description,
						IconClass = p.IconClass,
						ID = p.ID,
						IsDeleted = p.IsDeleted,
						LastUpdateDate = p.LastUpdateDate,
						ModifiedBy = p.ModifiedBy,
						Name = p.Name,
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var activityTypes = ApplyPaging(request, query, out count);

                    response.ActivityTypeRecords = activityTypes.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);

                    return response;
                });
            }
            return res;
        }

        public ActivityTypeResponse DoActivityTypeDelete(ActivityTypeRequest req)
        {
            var res = new ActivityTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivityTypeDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivityTypeDelete, true, false, (ActivityTypeRequest request, ActivityTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.ActivityTypeRecord != null && request.ActivityTypeRecord.ID > 0)
                    {
                        var activityTypeRepository = new ActivityTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var activityType = activityTypeRepository.FirstOrDefault(p => p.ID == request.ActivityTypeRecord.ID);

                        if (activityType != null)
                        {
                            activityType.IsDeleted = true;
                            activityTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidActivityTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public ActivityTypeResponse DoActivityTypeInsert(ActivityTypeRequest req)
        {
            var res = new ActivityTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivityTypeInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivityTypeInsert, true, false, (ActivityTypeRequest request, ActivityTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.ActivityTypeRecord != null)
                    {
                        var activityTypeRepository = new ActivityTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var activityType = ActivityTypeServiceManager.CreateOrUpdateActivityType(request.ApplicationID, request.UserID, request.ActivityTypeRecord);
                        activityTypeRepository.Add(activityType);
                        activityTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);


                        
                        request.ActivityTypeRecord.ID = activityType.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidActivityTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public ActivityTypeResponse DoActivityTypeUpdate(ActivityTypeRequest req)
        {
            var res = new ActivityTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivityTypeUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivityTypeUpdate, true, false, (ActivityTypeRequest request, ActivityTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.ActivityTypeRecord != null)
                    {
                        var activityTypeRepository = new ActivityTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var activityType = activityTypeRepository.FirstOrDefault(p => p.ID == request.ActivityTypeRecord.ID);

                        if (activityType != null)
                        {
activityType = ActivityTypeServiceManager.CreateOrUpdateActivityType(request.ApplicationID, request.UserID, request.ActivityTypeRecord, activityType);

activityTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidActivityTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}