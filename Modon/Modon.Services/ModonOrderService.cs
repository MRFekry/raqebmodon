using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class ModonOrderService : BaseService, IModonOrder
    {
        private const string serviceName = "ModonOrderService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region ModonOrderServices
        private const string operationName_DoModonOrderList = "DoModonOrderList";
        private const string operationName_DoModonOrderDelete = "DoModonOrderDelete";
        private const string operationName_DoModonOrderInsert = "DoModonOrderInsert";
        private const string operationName_DoModonOrderUpdate = "DoModonOrderUpdate";

        public ModonOrderResponse DoModonOrderList(ModonOrderRequest req)
        {
            var res = new ModonOrderResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoModonOrderList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoModonOrderList, false, false, (ModonOrderRequest request, ModonOrderResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new ModonOrderRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new ModonOrderRecord
                    {

                        ApplicationID = p.ApplicationID,
                        BuildingID = p.BuildingID,
                        CreatedBy = p.CreatedBy,
                        CreationDate = p.CreationDate,
                        ID = p.ID,
                        IsDeleted = p.IsDeleted,
                        LastUpdateDate = p.LastUpdateDate,
                        ModifiedBy = p.ModifiedBy,
                        OrderTypeID = p.OrderTypeID,
                        OrderTypeName = p.OrderType.Description,
                        StatusNAme = p.Status.Name,
                        BuildingName = p.Building.Name,
                        StatusID = p.StatusID,
                    });

                    if (request.Filter == null)
                        request.Filter = new CommonDefinitions.Filters.ModonOrderRequestFilter();


                    if (ConfigManager.GetInstance(request.ApplicationID).InvestorRoleID == request.UserRoleID)
                        request.Filter.CreatedByList = new List<long?> { request.UserID };
                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var modonOrders = ApplyPaging(request, query, out count);

                    response.ModonOrderRecords = modonOrders.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public ModonOrderResponse DoModonOrderDelete(ModonOrderRequest req)
        {
            var res = new ModonOrderResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoModonOrderDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoModonOrderDelete, true, false, (ModonOrderRequest request, ModonOrderResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.ModonOrderRecord != null && request.ModonOrderRecord.ID > 0)
                    {
                        var modonOrderRepository = new ModonOrderRepository(CurrentContextEntities, request.ApplicationID);
                        var modonOrder = modonOrderRepository.FirstOrDefault(p => p.ID == request.ModonOrderRecord.ID);

                        if (modonOrder != null)
                        {
                            modonOrder.IsDeleted = true;
                            modonOrderRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidModonOrderTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public ModonOrderResponse DoModonOrderInsert(ModonOrderRequest req)
        {
            var res = new ModonOrderResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoModonOrderInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoModonOrderInsert, true, false, (ModonOrderRequest request, ModonOrderResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.ModonOrderRecord != null)
                    {
                        var modonOrderRepository = new ModonOrderRepository(CurrentContextEntities, request.ApplicationID);
                        var modonOrder = ModonOrderServiceManager.CreateOrUpdateModonOrder(request.ApplicationID, request.UserID, request.ModonOrderRecord);
                        modonOrderRepository.Add(modonOrder);
                        modonOrderRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.ModonOrderRecord.ID = modonOrder.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidModonOrderTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public ModonOrderResponse DoModonOrderUpdate(ModonOrderRequest req)
        {
            var res = new ModonOrderResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoModonOrderUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoModonOrderUpdate, true, false, (ModonOrderRequest request, ModonOrderResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.ModonOrderRecord != null)
                    {
                        var modonOrderRepository = new ModonOrderRepository(CurrentContextEntities, request.ApplicationID);
                        var modonOrder = modonOrderRepository.FirstOrDefault(p => p.ID == request.ModonOrderRecord.ID);

                        if (modonOrder != null)
                        {
                            modonOrder = ModonOrderServiceManager.CreateOrUpdateModonOrder(request.ApplicationID, request.UserID, request.ModonOrderRecord, modonOrder);

                            modonOrderRepository.Commit(entableTracking, ref oldValues, ref newValues);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidModonOrderTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}