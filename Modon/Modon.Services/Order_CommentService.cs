using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class Order_CommentService : BaseService, IOrder_Comment
    {
        private const string serviceName = "Order_CommentService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region Order_CommentServices
        private const string operationName_DoOrder_CommentList = "DoOrder_CommentList";
        private const string operationName_DoOrder_CommentDelete = "DoOrder_CommentDelete";
        private const string operationName_DoOrder_CommentInsert = "DoOrder_CommentInsert";
        private const string operationName_DoOrder_CommentUpdate = "DoOrder_CommentUpdate";

        public Order_CommentResponse DoOrder_CommentList(Order_CommentRequest req)
        {
            var res = new Order_CommentResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrder_CommentList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrder_CommentList, false, false, (Order_CommentRequest request, Order_CommentResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new Order_CommentRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new Order_CommentRecord
                    {

                        ApplicationID = p.ApplicationID,
                        Comment = p.Comment,
                        CreatedBy = p.CreatedBy,
                        CreationDate = p.CreationDate,
                        ID = p.ID,
                        IsDeleted = p.IsDeleted,
                        LastUpdateDate = p.LastUpdateDate,
                        ModifiedBy = p.ModifiedBy,
                        OrderID = p.OrderID,
                        BuildingID = p.ModonOrder.Building.ID,
                        UserID = p.UserID,
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var order_Comments = ApplyPaging(request, query, out count);

                    response.Order_CommentRecords = order_Comments.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public Order_CommentResponse DoOrder_CommentDelete(Order_CommentRequest req)
        {
            var res = new Order_CommentResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrder_CommentDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrder_CommentDelete, true, false, (Order_CommentRequest request, Order_CommentResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Order_CommentRecord != null && request.Order_CommentRecord.ID > 0)
                    {
                        var order_CommentRepository = new Order_CommentRepository(CurrentContextEntities, request.ApplicationID);
                        var order_Comment = order_CommentRepository.FirstOrDefault(p => p.ID == request.Order_CommentRecord.ID);

                        if (order_Comment != null)
                        {
                            order_Comment.IsDeleted = true;
                            order_CommentRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidOrder_CommentTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Order_CommentResponse DoOrder_CommentInsert(Order_CommentRequest req)
        {
            var res = new Order_CommentResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrder_CommentInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrder_CommentInsert, true, false, (Order_CommentRequest request, Order_CommentResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Order_CommentRecord != null)
                    {
                        var order_CommentRepository = new Order_CommentRepository(CurrentContextEntities, request.ApplicationID);
                        //var order_Comment = Order_CommentServiceManager.CreateOrUpdateOrder_Comment(request.ApplicationID, request.UserID, request.Order_CommentRecord);
                        order_CommentRepository.Add(new Order_Comment
                        {
                            ApplicationID = 1,
                            Comment = request.Order_CommentRecord.Comment,
                            CreatedBy = request.UserID,
                            CreationDate = DateTime.UtcNow,
                            IsDeleted = false,
                            OrderID = request.Order_CommentRecord.OrderID,
                            UserID = request.UserID
                        });
                        order_CommentRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;

                        #region notify employees

                        var modonOrderRepository = new ModonOrderRepository(CurrentContextEntities, request.ApplicationID);
                        var orderRecord = modonOrderRepository.FirstOrDefault(p => p.ID == request.Order_CommentRecord.OrderID);

                        var orderDBObjectID = ConfigManager.GetInstance(request.ApplicationID).OrderDBObjectID;
                        var employrrRoleID = ConfigManager.GetInstance(request.ApplicationID).EmployeeRoleID;

                        var common_UserRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);

                        var toUserListEmployees = common_UserRepository.All().Where(p => p.AreaID == orderRecord.Building.AreaID && !p.IsDeleted && p.RoleID == employrrRoleID).Select(p => p.ID).Distinct().ToList();

                        // 1. To  Owner
                        NotificationManager.GetInstance(request.ApplicationID).Notify(request.UserID, orderRecord.CreatedBy ?? 0, EnumNotificationType.Comment, orderDBObjectID, new ModonOrderRecord(orderRecord));

                        // 2. To Employees
                        NotificationManager.GetInstance(request.ApplicationID).Notify(request.UserID, toUserListEmployees, EnumNotificationType.Comment, orderDBObjectID, new ModonOrderRecord(orderRecord));
                        #endregion
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidOrder_CommentTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Order_CommentResponse DoOrder_CommentUpdate(Order_CommentRequest req)
        {
            var res = new Order_CommentResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoOrder_CommentUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoOrder_CommentUpdate, true, false, (Order_CommentRequest request, Order_CommentResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Order_CommentRecord != null)
                    {
                        var order_CommentRepository = new Order_CommentRepository(CurrentContextEntities, request.ApplicationID);
                        var order_Comment = order_CommentRepository.FirstOrDefault(p => p.ID == request.Order_CommentRecord.ID);

                        if (order_Comment != null)
                        {
                            order_Comment = Order_CommentServiceManager.CreateOrUpdateOrder_Comment(request.ApplicationID, request.UserID, request.Order_CommentRecord, order_Comment);

                            order_CommentRepository.Commit(entableTracking, ref oldValues, ref newValues);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidOrder_CommentTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}