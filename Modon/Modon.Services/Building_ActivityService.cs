using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class Building_ActivityService : BaseService, IBuilding_Activity
    {
        private const string serviceName = "Building_ActivityService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region Building_ActivityServices
        private const string operationName_DoBuilding_ActivityList = "DoBuilding_ActivityList";
        private const string operationName_DoBuilding_ActivityDelete = "DoBuilding_ActivityDelete";
        private const string operationName_DoBuilding_ActivityInsert = "DoBuilding_ActivityInsert";
        private const string operationName_DoBuilding_ActivityUpdate = "DoBuilding_ActivityUpdate";

        public Building_ActivityResponse DoBuilding_ActivityList(Building_ActivityRequest req)
        {
            var res = new Building_ActivityResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuilding_ActivityList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuilding_ActivityList, false, false, (Building_ActivityRequest request, Building_ActivityResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new Building_ActivityRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new Building_ActivityRecord
                    {

						ApplicationID = p.ApplicationID,
						BuildingID = p.BuildingID,
						CreatedBy = p.CreatedBy,
						CreationDate = p.CreationDate,
						Description = p.Description,
						ID = p.ID,
						IsAllowed = p.IsAllowed,
						IsDeleted = p.IsDeleted,
						LastUpdateDate = p.LastUpdateDate,
						ModifiedBy = p.ModifiedBy,
                        ActivitSubTypeID = p.ActivitSubTypeID,
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var building_Activitys = ApplyPaging(request, query, out count);

                    response.Building_ActivityRecords = building_Activitys.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public Building_ActivityResponse DoBuilding_ActivityDelete(Building_ActivityRequest req)
        {
            var res = new Building_ActivityResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuilding_ActivityDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuilding_ActivityDelete, true, false, (Building_ActivityRequest request, Building_ActivityResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Building_ActivityRecord != null && request.Building_ActivityRecord.ID > 0)
                    {
                        var building_ActivityRepository = new Building_ActivityRepository(CurrentContextEntities, request.ApplicationID);
                        var building_Activity = building_ActivityRepository.FirstOrDefault(p => p.ID == request.Building_ActivityRecord.ID);

                        if (building_Activity != null)
                        {
                            building_Activity.IsDeleted = true;
                            building_ActivityRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidBuilding_ActivityTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Building_ActivityResponse DoBuilding_ActivityInsert(Building_ActivityRequest req)
        {
            var res = new Building_ActivityResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuilding_ActivityInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuilding_ActivityInsert, true, false, (Building_ActivityRequest request, Building_ActivityResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Building_ActivityRecord != null)
                    {
                        var building_ActivityRepository = new Building_ActivityRepository(CurrentContextEntities, request.ApplicationID);
                        var building_Activity = Building_ActivityServiceManager.CreateOrUpdateBuilding_Activity(request.ApplicationID, request.UserID, request.Building_ActivityRecord);
                        building_ActivityRepository.Add(building_Activity);
                        building_ActivityRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.Building_ActivityRecord.ID = building_Activity.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidBuilding_ActivityTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Building_ActivityResponse DoBuilding_ActivityUpdate(Building_ActivityRequest req)
        {
            var res = new Building_ActivityResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuilding_ActivityUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuilding_ActivityUpdate, true, false, (Building_ActivityRequest request, Building_ActivityResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Building_ActivityRecord != null)
                    {
                        var building_ActivityRepository = new Building_ActivityRepository(CurrentContextEntities, request.ApplicationID);
                        var building_Activity = building_ActivityRepository.FirstOrDefault(p => p.ID == request.Building_ActivityRecord.ID);

                        if (building_Activity != null)
                        {
building_Activity = Building_ActivityServiceManager.CreateOrUpdateBuilding_Activity(request.ApplicationID, request.UserID, request.Building_ActivityRecord, building_Activity);

building_ActivityRepository.Commit(entableTracking, ref oldValues, ref newValues);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidBuilding_ActivityTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        public Building_ActivityResponse DoBuilding_ActivityChangeAllowence(Building_ActivityRequest req)
        {
            var res = new Building_ActivityResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuilding_ActivityUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuilding_ActivityUpdate, true, false, (Building_ActivityRequest request, Building_ActivityResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Building_ActivityRecord != null && request.Building_ActivityRecord.ID > 0)
                    {
                        var building_ActivityRepository = new Building_ActivityRepository(CurrentContextEntities, request.ApplicationID);
                        var building_Activity = building_ActivityRepository.FirstOrDefault(p => p.ID == request.Building_ActivityRecord.ID);

                        if (building_Activity != null)
                        {
                            building_Activity.IsAllowed=!building_Activity.IsAllowed;
                            building_Activity.ModifiedBy = req.UserID;
                            building_Activity.LastUpdateDate = DateTime.UtcNow;
                            building_ActivityRepository.Commit(entableTracking, ref oldValues, ref newValues);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidBuilding_ActivityTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        #endregion
    }
}