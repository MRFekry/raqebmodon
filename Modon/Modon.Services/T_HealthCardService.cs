using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;
using PMApp.Core.Helpers;
namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class T_HealthCardService : BaseService, IT_HealthCard
    {
        private const string serviceName = "T_HealthCardService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region T_HealthCardServices
        private const string operationName_DoT_HealthCardList = "DoT_HealthCardList";
        private const string operationName_DoT_HealthCardDelete = "DoT_HealthCardDelete";
        private const string operationName_DoT_HealthCardInsert = "DoT_HealthCardInsert";
        private const string operationName_DoT_HealthCardUpdate = "DoT_HealthCardUpdate";

        public T_HealthCardResponse DoT_HealthCardList(T_HealthCardRequest req)
        {
            var res = new T_HealthCardResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_HealthCardList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_HealthCardList, false, false, (T_HealthCardRequest request, T_HealthCardResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new T_HealthCardRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new T_HealthCardRecord
                    {

                        ApplicationID = p.ApplicationID,
                        BuildingID = p.BuildingID,
                        CreatedBy = p.CreatedBy,
                        CreationDate = p.CreationDate,
                        HC_AceptDate = p.HC_AceptDate,
                        HC_AceptedBy = p.HC_AceptedBy,
                        HC_AceptNote = p.HC_AceptNote,
                        HC_EndDate = p.HC_EndDate,
                        HC_NO = p.HC_NO,
                        HC_ReqNote = p.HC_ReqNote,
                        HC_RequstNO = p.HC_RequstNO,
                        HC_St_Id = p.HC_St_Id,
                        HC_StopById = p.HC_StopById,
                        HC_StopDate = p.HC_StopDate,
                        HC_StopReason = p.HC_StopReason,
                        HC_StratDate = p.HC_StratDate,
                        HC_WorkerId = p.HC_WorkerId,
                        HRep_FileURL = p.HRep_FileURL,
                        HRep_HospId = p.HRep_HospId,
                        HRepDate = p.HRepDate,
                        HRepNo = p.HRepNo,
                        ID = p.ID,
                        IsDeleted = p.IsDeleted,
                        LastUpdateDate = p.LastUpdateDate,
                        ModifiedBy = p.ModifiedBy,
                         
                        HC_Worker = p.T_Worker
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var t_HealthCards = ApplyPaging(request, query, out count);

                    response.T_HealthCardRecords = t_HealthCards.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public T_HealthCardResponse DoT_HealthCardDelete(T_HealthCardRequest req)
        {
            var res = new T_HealthCardResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_HealthCardDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_HealthCardDelete, true, false, (T_HealthCardRequest request, T_HealthCardResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.T_HealthCardRecord != null && request.T_HealthCardRecord.ID > 0)
                    {
                        var t_HealthCardRepository = new T_HealthCardRepository(CurrentContextEntities, request.ApplicationID);
                        var t_HealthCard = t_HealthCardRepository.FirstOrDefault(p => p.ID == request.T_HealthCardRecord.ID);

                        if (t_HealthCard != null)
                        {
                            t_HealthCard.IsDeleted = true;
                            t_HealthCardRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidT_HealthCardTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public T_HealthCardResponse DoT_HealthCardInsert(T_HealthCardRequest req)
        {
            var res = new T_HealthCardResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_HealthCardInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_HealthCardInsert, true, false, (T_HealthCardRequest request, T_HealthCardResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.T_HealthCardRecord != null)
                    {

                        var t_HealthCardRepository = new T_HealthCardRepository(CurrentContextEntities, request.ApplicationID);
                        var t_HealthCard = T_HealthCardServiceManager.CreateOrUpdateT_HealthCard(request.ApplicationID, request.UserID, request.T_HealthCardRecord);
                        t_HealthCardRepository.Add(t_HealthCard);

                        var targetObjectID_T_HealthCard = ConfigManager.GetInstance(request.ApplicationID).T_HealthCardDBObjectID;
                        var mediaRepository = new WebMediaRepository(CurrentContextEntities, request.ApplicationID);

                        var HRepFile = request.T_HealthCardRecord.HRepFile;
                        if (HRepFile != null && HRepFile.ContentLength > 0)
                        {
                            var imagePath = string.Format("~/UploadedImages/HCFiles/{0}.jpg", Guid.NewGuid().ToString());
                            HRepFile.SaveAs(UIHelper.MapPath(imagePath));
                            var media1 = new WebMedia
                            {
                                CreationDate = DateTime.Now,
                                Url = imagePath,
                                ObjectID = targetObjectID_T_HealthCard,
                                TargetObjectID = t_HealthCard.ID,
                                WebMediaTypeID = ConfigManager.GetInstance(request.ApplicationID).HRepFile_WebMediaTypeID,
                                IsActive = true
                            };
                            mediaRepository.Add(media1);
                        }

                        t_HealthCardRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.T_HealthCardRecord.ID = t_HealthCard.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidT_HealthCardTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public T_HealthCardResponse DoT_HealthCardUpdate(T_HealthCardRequest req)
        {
            var res = new T_HealthCardResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_HealthCardUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_HealthCardUpdate, true, false, (T_HealthCardRequest request, T_HealthCardResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.T_HealthCardRecord != null)
                    {
                        var t_HealthCardRepository = new T_HealthCardRepository(CurrentContextEntities, request.ApplicationID);
                        var t_HealthCard = t_HealthCardRepository.FirstOrDefault(p => p.ID == request.T_HealthCardRecord.ID);
                        if (t_HealthCard != null)
                        {
                            t_HealthCard = T_HealthCardServiceManager.CreateOrUpdateT_HealthCard(request.ApplicationID, request.UserID, request.T_HealthCardRecord, t_HealthCard);

                            t_HealthCardRepository.Commit(entableTracking, ref oldValues, ref newValues);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidT_HealthCardTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}