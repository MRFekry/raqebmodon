using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace  Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class NewsFeedsUserService : BaseService, INewsFeedsUser
    {
        private const string serviceName = "NewsFeedsUserService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region NewsFeedsUserServices
        private const string operationName_DoNewsFeedsUserList = "DoNewsFeedsUserList";
        private const string operationName_DoNewsFeedsUserDelete = "DoNewsFeedsUserDelete";
        private const string operationName_DoNewsFeedsUserInsert = "DoNewsFeedsUserInsert";
        private const string operationName_DoNewsFeedsUserUpdate = "DoNewsFeedsUserUpdate";

        public NewsFeedsUserResponse DoNewsFeedsUserList(NewsFeedsUserRequest req)
        {
            var res = new NewsFeedsUserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsUserList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsUserList, false, false, (NewsFeedsUserRequest request, NewsFeedsUserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new NewsFeedsUserRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new NewsFeedsUserRecord
                    {

						ID = p.ID,
						UserID = p.UserID,
						NewFeedsID = p.NewFeedsID,
						NotificationID = p.NotificationID,
						ApplicationID = p.ApplicationID,
                    });
                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }
                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var newsFeedsUsers = ApplyPaging(request, query, out count);

                    response.NewsFeedsUserRecords = newsFeedsUsers.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public NewsFeedsUserResponse DoNewsFeedsUserDelete(NewsFeedsUserRequest req)
        {
            var res = new NewsFeedsUserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsUserDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsUserDelete, true, false, (NewsFeedsUserRequest request, NewsFeedsUserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NewsFeedsUserRecord != null && request.NewsFeedsUserRecord.ID > 0)
                    {
                        var newsFeedsUserRepository = new NewsFeedsUserRepository(CurrentContextEntities, request.ApplicationID);
                        var newsFeedsUser = newsFeedsUserRepository.FirstOrDefault(p => p.ID == request.NewsFeedsUserRecord.ID);

                        if (newsFeedsUser != null)
                        {
                            //newsFeedsUser.IsDeleted = true;
                            newsFeedsUserRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNewsFeedsUserTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public NewsFeedsUserResponse DoNewsFeedsUserInsert(NewsFeedsUserRequest req)
        {
            var res = new NewsFeedsUserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsUserInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsUserInsert, true, false, (NewsFeedsUserRequest request, NewsFeedsUserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NewsFeedsUserRecord != null)
                    {
                        var newsFeedsUserRepository = new NewsFeedsUserRepository(CurrentContextEntities, request.ApplicationID);
                        var newsFeedsUser = NewsFeedsUserServiceManager.CreateOrUpdateNewsFeedsUser(request.ApplicationID, request.UserID, request.NewsFeedsUserRecord);
                        newsFeedsUserRepository.Add(newsFeedsUser);
                        newsFeedsUserRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.NewsFeedsUserRecord.ID = newsFeedsUser.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNewsFeedsUserTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public NewsFeedsUserResponse DoNewsFeedsUserUpdate(NewsFeedsUserRequest req)
        {
            var res = new NewsFeedsUserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsUserUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsUserUpdate, true, false, (NewsFeedsUserRequest request, NewsFeedsUserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NewsFeedsUserRecord != null)
                    {
                        var newsFeedsUserRepository = new NewsFeedsUserRepository(CurrentContextEntities, request.ApplicationID);
                        var newsFeedsUser = newsFeedsUserRepository.FirstOrDefault(p => p.ID == request.NewsFeedsUserRecord.ID);

                        if (newsFeedsUser != null)
                        {
newsFeedsUser = NewsFeedsUserServiceManager.CreateOrUpdateNewsFeedsUser(request.ApplicationID, request.UserID, request.NewsFeedsUserRecord, newsFeedsUser);

newsFeedsUserRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNewsFeedsUserTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}