﻿using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.EduLegend.DAL6MySql;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace EduLegend.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class BeeRequestConfirmation
    {
        public BeeRequestConfirmation()
        { }
        public string MerchantOrderId { get; set; }

        public string PGWOrderId { get; set; }

        public string OrderStatus { get; set; }

        public string OrderPaymentDateTime { get; set; }
    }

}
