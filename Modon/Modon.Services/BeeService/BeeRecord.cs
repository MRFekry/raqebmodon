﻿using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.EduLegend.DAL6MySql;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace EduLegend.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class BeeRecord
    {
        public BeeRecord()
        { }
        [JsonProperty(PropertyName = "OID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OrderId { get; set; }

        [JsonProperty(PropertyName = "PGWID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string PGWOrderId { get; set; }

        [JsonProperty(PropertyName = "OD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OrderDate { get; set; }

        [JsonProperty(PropertyName = "OA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OrderAmount { get; set; }


        [JsonProperty(PropertyName = "ED", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ExpiryDate { get; set; }

        [JsonProperty(PropertyName = "CN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CustomerName { get; set; }

        [JsonProperty(PropertyName = "CE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CustomerEmailAddress { get; set; }

        [JsonProperty(PropertyName = "CM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CustomerMobileNumbers { get; set; }

        [JsonProperty(PropertyName = "ME", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string MerchantEmailAddress { get; set; }

        [JsonProperty(PropertyName = "MM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string MerchantMobileNumbers { get; set; }

        [JsonProperty(PropertyName = "R1", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ReservedParamIn1 { get; set; }

        [JsonProperty(PropertyName = "R2", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ReservedParamIn2 { get; set; }

        [JsonProperty(PropertyName = "R3", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ReservedParamIn3 { get; set; }

    }

}
