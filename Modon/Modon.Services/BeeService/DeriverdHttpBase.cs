﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.IO;
using BaseClassNameSpace.Web.BaseServices;
using System.Xml.Linq;
using EduLegend.CommonDefinitions.DataContracts.Record;
using System.Xml;


namespace DeriveClassNameSpace.Services.Web
{

    public class HttpRequestResponse
    {
        private WebException CatchHttpExceptions(string ErrMsg)
        {
            ErrMsg = "Error During Web Interface. Error is: " + ErrMsg;
            return new WebException(ErrMsg);
        }


        public static void CreateRequest()
        {
            //Save in Config
            var URI = "https://staging.bee.com.eg:7777/xmlgw/merchant";
            var RequestMethod = "POST";

            BeeRecord beeOrder = new BeeRecord();
            var xml = GenerateXmlNewOrder(beeOrder);
            string url = URI;
            WebRequest req = (WebRequest)WebRequest.Create(url);

            byte[] requestBytes = Encoding.UTF8.GetBytes(xml);
            req.Method = RequestMethod;
            req.ContentType = "text/xml;charset=utf-8";
            req.ContentLength = requestBytes.Length;
            req.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials; Stream requestStream = req.GetRequestStream();
            requestStream.Write(requestBytes, 0, requestBytes.Length);
            requestStream.Close();
            WebResponse res = (WebResponse)req.GetResponse();
            StreamReader sr = new StreamReader(res.GetResponseStream(), System.Text.Encoding.Default);
            string backstr = sr.ReadToEnd();
            sr.Close();
            res.Close();
        }

        public static string GenerateXmlNewOrder(BeeRecord beeRecord)
        {
            Random rnd = new Random();
            int orderID = rnd.Next(100000000, 999999999);
            var UserName = "eduLegend";
            var UserPwd = "3acd0be86de7dcccdbf91b20f94a68cea535922d";
            var UserCode = "1971657708983828336822911572173815089583";
            var xmlRequest = new XDocument(new XDeclaration("1.0", "UTF-8","no"),
                             new XElement("Request",
                                 new XAttribute("action", "1"),
                                 new XAttribute("version", "2"),
                                           new XElement("Login", UserName),
                                           new XElement("Password", UserPwd),
                                           new XElement("Code", UserCode),
                                           new XElement("OrderId", 9563193740),
                                           new XElement("OrderDate", DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss")),
                                           new XElement("OrderAmount", beeRecord.OrderAmount),
                                           new XElement("OrderExpiryDate", DateTime.Now.AddMonths(1).ToString("yyyy.MM.dd hh:mm:ss")),
                                           new XElement("OrderDesc", beeRecord.ReservedParamIn1+" Points"),

                                           new XElement("CustomerData",
                                               new XElement("Name", beeRecord.CustomerName),
                                               new XElement("EmailAddress",beeRecord.CustomerEmailAddress),
                                               new XElement("MobileNumbers", beeRecord.CustomerMobileNumbers)),
                                           new XElement("MerchantUserData",
                                               new XElement("Name", "EduLegend"),
                                               new XElement("EmailAddress", "mnagy@sbs16.com"),
                                               new XElement("MobileNumbers", "01224088640")),
                                           new XElement("ReservedParamIn1", "N/A"),
                                           new XElement("ReservedParamIn2", ""),
                                           new XElement("ReservedParamIn3", "")));
            return ToXml(xmlRequest);
        }
        public static string GenerateXmlConfirmationResponse(string statusCode,string statusDesc)
        {
            var xmlRequest = new XDocument(new XDeclaration("1.0", "UTF-8", "no"),
                             new XElement("Res",
                                 new XAttribute("v", "1"),
                                 new XAttribute("f", "1"),
                                           new XElement("StatusCode", statusCode),
                                           new XElement("StatusDesc", statusDesc)));
                                           
            return ToXml(xmlRequest);
        }

        public static string ToXml(XDocument xDoc)
        {
            StringBuilder builder = new StringBuilder();
            using (TextWriter writer = new StringWriter(builder))
            {
                xDoc.Save(writer);
                return builder.ToString();
            }
        }
    }
}