using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.Helpers;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace  Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class SBSService : BaseSBSService
    {
        private const string EmptyResponseStr = "Empty Response - Operation: '{0}', Request: '{1}'";

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public override SBSResponse DoInvoke(SBSRequest sbsRequest)
        {
            long exceptionId = 0;
            var startDateTime = DateTime.Now;
            var sbsResponse = new SBSResponse();
            try
            {
                sbsResponse.ApplicationID = ApplicationManager.GetApplicationIDFromAppCode(sbsRequest.ApplicationName);

                switch (sbsRequest.TargetOperation)
                {
                    #region switchTargetOperation
                    // 1 - UserService.DoLogin - Login
                    case UserService.operationName_DoLogin:
                        DoInvokeService<LoginRequest, LoginResponse>(sbsRequest, ref sbsResponse, new UserService().DoLogin);
                        break;

                    case UserService.operationName_DoSocialLogin:
                        DoInvokeService<LoginRequest, LoginResponse>(sbsRequest, ref sbsResponse, new UserService().DoSocialLogin);
                        break;

                    // 2 - UserService.DoLogout - Logout
                    case UserService.operationName_DoLogout:
                        DoInvokeService<LogoutRequest, LogoutResponse>(sbsRequest, ref sbsResponse, new UserService().DoLogout);
                        break;

                    // 3 - UserService.DoForgetPassword - Forget Password
                    case UserService.operationName_DoForgetPassword:
                        DoInvokeService<ForgetPasswordRequest, ForgetPasswordResponse>(sbsRequest, ref sbsResponse, new UserService().DoForgetPassword);
                        break;

                    case UserService.operationName_DoForgetPasswordChoice:
                        DoInvokeService<ForgetPasswordRequest, ForgetPasswordResponse>(sbsRequest, ref sbsResponse, new UserService().DoForgetPasswordChoice);
                        break;

                    case UserService.operationName_DoForgetPasswordChange:
                        DoInvokeService<UserRequest, UserResponse>(sbsRequest, ref sbsResponse, new UserService().DoForgetPasswordChange);
                        break;

                    // 5 - UserService.DoUserUpdateOperation - User Update Operation
                    case UserService.operationName_DoUserUpdateOperation:
                        DoInvokeService<UserRequest, UserResponse>(sbsRequest, ref sbsResponse, new UserService().DoUserUpdateOperation);
                        break;

                    // 5 - UserService.DoUserInsertOperation - User Inser Operation
                    case UserService.operationName_DoUserInsertOperation:
                        DoInvokeService<UserRequest, UserResponse>(sbsRequest, ref sbsResponse, new UserService().DoUserInsertOperation);
                        break;

                    // 1 - UserService.DoUserActivate - DoUserActivate
                    case UserService.operationName_DoUserActivateOperation:
                        DoInvokeService<UserRequest, UserResponse>(sbsRequest, ref sbsResponse, new UserService().DoUserActivate);
                        break;

                    //// 7 - UserService.DoUserListInquiry - User List Inquiry
                    //case UserService.operationName_DoUserListInquiry:
                    //    DoInvokeService<UserRequest, UserResponse>(sbsRequest, ref sbsResponse, new UserService().DoUserListInquiry);
                    //    break;

                    // 9 - LookupService.DoLookupListInquiry - LookUp List Inquiry
                    case LookupService.operationName_DoLookupListInquiry:
                        DoInvokeService<LookupRequest, LookupResponse>(sbsRequest, ref sbsResponse, new LookupService().DoLookupListInquiry);
                        break;

                    // 13 - UtilityService.DoLanguageListInquiry - Language List Inquiry
                    case UtilityService.operationName_DoLanguageListInquiry:
                        DoInvokeService<LanguageRequest, LanguageResponse>(sbsRequest, ref sbsResponse, new UtilityService().DoLanguageListInquiry);
                        break;

                    // 19 - UtilityService.DoRoleListInquiry - Role List Inquiry
                    case UtilityService.operationName_DoRoleListInquiry:
                        DoInvokeService<ServiceOperationRoleAccessRequest, ServiceOperationRoleAccessResponse>(sbsRequest, ref sbsResponse, new UtilityService().DoRoleListInquiry);
                        break;

                    // 20 - UtilityService.DoLogException - Log Exception
                    case UtilityService.operationName_DoLogException:
                        DoInvokeService<LogExceptionRequest, LogExceptionResponse>(sbsRequest, ref sbsResponse, new UtilityService().DoLogException);
                        break;

                    // 29 - UtilityService.DoConfigsList -ConfigsList
                    case LookupService.operationName_DoConfigsList:
                        DoInvokeService<ConfigsRequest, ConfigsResponse>(sbsRequest, ref sbsResponse, new LookupService().DoConfigsList);
                        break;

                    // 30 - UserService.DoCacheVersionList -CacheVersionList
                    case UserService.operationName_DoCacheVersionList:
                        DoInvokeService<CacheVersionListRequest, CacheVersionListResponse>(sbsRequest, ref sbsResponse, new UserService().DoCacheVersionList);
                        break;

                    case CoreMenuService.operationName_DoCoreMenuListInquiry:
                        DoInvokeService<CoreMenuRequest, CoreMenuResponse>(sbsRequest, ref sbsResponse, new CoreMenuService().DoCoreMenuListInquiry);
                        break;

                    // 211 - SharedService.DoDBObjectList - DoDBObject List
                    case SharedService.operationName_DoDBObjectList:
                        DoInvokeService<DBObjectRequest, DBObjectResponse>(sbsRequest, ref sbsResponse, new SharedService().DoDBObjectList);
                        break;

                    // 203 - SharedService.DoStatusList - DoStatus List
                    case SharedService.operationName_DoStatusList:
                        DoInvokeService<StatusRequest, StatusResponse>(sbsRequest, ref sbsResponse, new SharedService().DoStatusList);
                        break;

                    // 207 - SharedService.DoStatusHistoryList - DoStatusHistory List
                    case SharedService.operationName_DoStatusHistoryList:
                        DoInvokeService<StatusHistoryRequest, StatusHistoryResponse>(sbsRequest, ref sbsResponse, new SharedService().DoStatusHistoryList);
                        break;

                   

                   
                        #endregion
                }
            }
            catch (System.Exception exception)
            {
                exceptionId = ExceptionLogger.GetInstance(sbsResponse.ApplicationID).Log(exception);
            }
            finally
            {
                if (sbsResponse == null || string.IsNullOrWhiteSpace(sbsResponse.EncryptedResponse))
                {
                    var msg = string.Format(EmptyResponseStr, sbsRequest.TargetOperation, sbsRequest.EncryptedRequest);

                    ExceptionLogger.GetInstance(sbsResponse.ApplicationID).Log(msg, msg);
                }
                var durationInSeconds = DateTime.Now.Subtract(startDateTime).TotalSeconds;
                if (!sbsResponse.IsSuccess /*&& durationInSeconds > ConfigManager.GetInstance(sbsResponse.ApplicationID).LogMinSeconds*/)
                {
                    Log(sbsResponse.ApplicationID, sbsRequest, sbsResponse, sbsResponse.AppCode, durationInSeconds, exceptionId, sbsRequest.TargetOperation);
                }
            }

            return sbsResponse;
        }
    }
}