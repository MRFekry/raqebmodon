using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Core.Interfaces;
using PMApp.Core.NotificationsSystem;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web.UI.WebControls;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class UserExService : BaseService, IUserEx
    {
        private const string serviceName = "UserExService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }
        #region Area
        public const string operationName_DoAreaList = "DoAreaList";
        public const string operationName_DoAreaDelete = "DoAreaDelete";
        public const string operationName_DoAreaInsert = "DoAreaInsert";
        public const string operationName_DoAreaUpdate = "DoAreaUpdate";

        public AreaResponse DoAreaList(AreaRequest req)
        {
            var res = new AreaResponse(req.Lang,req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoAreaList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoAreaList, false, false, (AreaRequest request, AreaResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new AreaRepository(CurrentContextEntities,request.ApplicationID).All().Select(p => new AreaRecord
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Description = p.Description,
                        ParentID = p.ParentID,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        IsDeleted = p.IsDeleted,
                        CreationDate = p.CreationDate,
                        CreatedBy = p.CreatedBy,
                        Points=p.Points
                    });

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var areas = ApplyPaging(request, query, out count);
                    var areasArray = areas.ToArray();
                    //show only user area
                    if (request.Filter != null && request.Filter.ShowOnlyMyArea)
                    {
                        var currentUserAreaID = new Common_UserRepository(CurrentContextEntities,request.ApplicationID).FirstOrDefault(u => u.ID == request.UserID).AreaID;
                        areasArray = UserExServiceManager.RecursiveChildren(areasArray, currentUserAreaID.Value, true).ToArray();
                    }
                    response.AreaRecords = areasArray;
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public AreaResponse DoAreaDelete(AreaRequest req)
        {
            var res = new AreaResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoAreaDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoAreaDelete, true, false, (AreaRequest request, AreaResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.AreaRecord != null && request.AreaRecord.ID > 0)
                    {
                        var areaRepository = new AreaRepository(CurrentContextEntities, req.ApplicationID);
                        var area = areaRepository.FirstOrDefault(p => p.ID == request.AreaRecord.ID);

                        if (area != null)
                        {
                            //check if area can deleted
                            var canDelete = true;
                            var areaRequest = new AreaRequest { InnerCall = true, AuthToken = request.AuthToken };
                            var areaListResponse = DoAreaList(areaRequest);
                            var areaChildren = UserExServiceManager.RecursiveChildren(areaListResponse.AreaRecords, request.AreaRecord.ID, false);

                            if (areaChildren != null && areaChildren.Count() > 0)
                            {
                                canDelete = false;
                                response.SetBaseResponse(false, ResponseStatus.Exception, MessageKey.ParentAreaCanNotBeDeleted);

                            }
                            if (canDelete)
                            {
                                area.IsDeleted = true;
                                areaRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                                response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            }
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidAreaTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public AreaResponse DoAreaInsert(AreaRequest req)
        {
            var res = new AreaResponse(req.Lang,req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoAreaInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoAreaInsert, true, false, (AreaRequest request, AreaResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.AreaRecord != null)
                    {
                        var areaRepository = new AreaRepository(CurrentContextEntities,request.ApplicationID);
                        var area = UserExServiceManager.CreateOrUpdateArea(request.UserID, request.AreaRecord);
                        areaRepository.Add(area);
                        areaRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.AreaRecord.ID = area.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidAreaTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public AreaResponse DoAreaUpdate(AreaRequest req)
        {
            var res = new AreaResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoAreaUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoAreaUpdate, true, false, (AreaRequest request, AreaResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.AreaRecord != null)
                    {
                        var areaRepository = new AreaRepository(CurrentContextEntities, request.ApplicationID);
                        var area = areaRepository.FirstOrDefault(p => p.ID == request.AreaRecord.ID);

                        if (area != null)
                        {
                            area = UserExServiceManager.CreateOrUpdateArea(request.UserID, request.AreaRecord, area);

                            areaRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidAreaTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}