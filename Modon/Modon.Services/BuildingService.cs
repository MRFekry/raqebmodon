using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;
using PMApp.Core.Helpers;
using System.Collections.Generic;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class BuildingService : BaseService, IBuilding
    {
        private const string serviceName = "BuildingService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region BuildingServices
        private const string operationName_DoBuildingList = "DoBuildingList";
        private const string operationName_DoBuildingDelete = "DoBuildingDelete";
        private const string operationName_DoBuildingInsert = "DoBuildingInsert";
        private const string operationName_DoBuildingUpdate = "DoBuildingUpdate";

        public BuildingResponse DoBuildingList(BuildingRequest req)
        {
            var res = new BuildingResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuildingList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuildingList, false, false, (BuildingRequest request, BuildingResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                   var commentsList = new Order_CommentRepository(CurrentContextEntities, request.ApplicationID).All();
                    var query = new BuildingRepository(CurrentContextEntities, request.ApplicationID).All()
                        .Select(p => new BuildingRecord
                    {

                        ActivityAllowed = p.ActivityAllowed,
                        ActivityTypeID = p.ActivityTypeID,
                        ApplicationID = p.ApplicationID,
                        AreaID = p.AreaID,
                        AreaSpace = p.AreaSpace,
                        CreatedBy = p.CreatedBy,
                        CreationDate = p.CreationDate,
                        Email = p.Email,
                        ID = p.ID,
                        IsDeleted = p.IsDeleted,
                        LastUpdateDate = p.LastUpdateDate,
                        ModifiedBy = p.ModifiedBy,
                        Name = p.Name,
                        NameAllowed = p.NameAllowed,
                        OwnerCID = p.OwnerCID,
                        OwnerName = p.OwnerName,
                        OwnerPhone = p.OwnerPhone,
                        Phone = p.Phone,
                        SpaceAllowed = p.SpaceAllowed,
                        StatusID = p.StatusID,
                        StreetAdress = p.StreetAdress,
                        ActivityTypeName = p.ActivityType.Name,
                        StatusName = p.Status.Name,
                        AreaName = p.Area.Name + "-" + p.Area.Area2.Name + "-" + p.Area.Area2.Area2.Name,
                        BuildingActivities = p.Building_Activity.Select(a => new Building_ActivityRecord
                        {
                            ID=a.ID,
                            ActivitSubTypeID = a.ActivitSubTypeID,
                            ActivitySubTypeName = a.Activity_SubType.Name,
                            Description = a.Description,
                            IsAllowed = a.IsAllowed,
                            Comment = a.Comment,
                            ApplicationID = a.ApplicationID
                        }
                        )
                    });
                    if (request.Filter == null)
                        request.Filter = new CommonDefinitions.Filters.BuildingRequestFilter();


                    if (ConfigManager.GetInstance(request.ApplicationID).InvestorRoleID == request.UserRoleID)
                        request.Filter.CreatedByList = new List<long?> { request.UserID };

                    query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var buildings = ApplyPaging(request, query, out count);

                    // Get Files
                    var buildingDBObjectID = ConfigManager.GetInstance(request.ApplicationID).BuildingDBObjectID;
                    var WebMediaRecords = new WebMediaRepository(CurrentContextEntities, request.ApplicationID).Where(p => p.ObjectID == buildingDBObjectID).ToArray();
                    for (int i = 0; i < buildings.Count(); i++)
                    {
                        var buildingID = buildings[i].ID;
                        buildings[i].WebMediaFiles = WebMediaRecords.Where(p => p.TargetObjectID == buildingID).ToArray();
                    }

                    response.BuildingRecords = buildings.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public BuildingResponse DoBuildingDelete(BuildingRequest req)
        {
            var res = new BuildingResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuildingDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuildingDelete, true, false, (BuildingRequest request, BuildingResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.BuildingRecord != null && request.BuildingRecord.ID > 0)
                    {
                        var buildingRepository = new BuildingRepository(CurrentContextEntities, request.ApplicationID);
                        var building = buildingRepository.FirstOrDefault(p => p.ID == request.BuildingRecord.ID);

                        if (building != null)
                        {
                            building.IsDeleted = true;
                            buildingRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidBuildingTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public BuildingResponse DoBuildingInsert(BuildingRequest req)
        {
            var res = new BuildingResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuildingInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuildingInsert, true, false, (BuildingRequest request, BuildingResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.BuildingRecord != null)
                    {
                        var buildingRepository = new BuildingRepository(CurrentContextEntities, request.ApplicationID);

                        #region Check if other area
                        if (request.BuildingRecord.AreaID == -1)
                        {
                            var areaRepository = new AreaRepository(CurrentContextEntities, request.ApplicationID);
                            var otherArea = new Area
                            {
                                CreatedBy = request.UserID,
                                CreationDate = DateTime.Now,
                                ApplicationID = request.ApplicationID,
                                Name = request.BuildingRecord.OtherAreaName,
                                ParentID = request.BuildingRecord.OtherCityID,
                                IsDeleted = true
                            };
                            areaRepository.Add(otherArea);
                            areaRepository.Commit(entableTracking, ref oldValues, ref newValues);
                            request.BuildingRecord.AreaID = otherArea.ID;

                        }
                        #endregion
                        var building = BuildingServiceManager.CreateOrUpdateBuilding(request.ApplicationID, request.UserID, request.BuildingRecord);
                        buildingRepository.Add(building);
                        buildingRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.BuildingRecord.ID = building.ID;
                        var buildingActivitesRepository = new Building_ActivityRepository(CurrentContextEntities, request.ApplicationID);

                        #region Add Activities
                        if (request.BuildingRecord.BuildingActivities != null && request.BuildingRecord.BuildingActivities.Count() > 0)
                        {
                            foreach (var item in request.BuildingRecord.BuildingActivities)
                            {
                                if (item.ActivitSubTypeID == -1)
                                {
                                    var activity_SubTypeRepository = new Activity_SubTypeRepository(CurrentContextEntities, request.ApplicationID);
                                    var activsub = new Activity_SubType
                                    {
                                        ActivityTypeID = request.BuildingRecord.ActivityTypeID,
                                        CreatedBy = request.UserID,
                                        Name = item.ActivitOtherSubTypeName,
                                        CreationDate = DateTime.Now,
                                        ApplicationID = request.ApplicationID,
                                    };
                                    activity_SubTypeRepository.Add(activsub);
                                    activity_SubTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
                                    item.ActivitSubTypeID = activsub.ID;

                                }
                                var activ = new Building_Activity
                                {
                                    CreationDate = DateTime.Now,
                                    ApplicationID = request.ApplicationID,
                                    BuildingID = building.ID,
                                    CreatedBy = request.UserID,
                                    Description = item.Description,
                                    IsAllowed = false,
                                    IsDeleted = false,
                                    ActivitSubTypeID = item.ActivitSubTypeID
                                };
                                buildingActivitesRepository.Add(activ);
                            }

                        }

                        #endregion
                        #region Add Order
                        var modonOrderRepository = new ModonOrderRepository(CurrentContextEntities, request.ApplicationID);
                        var orderType = (long)EnumOrderType.AddBuilding;
                        var orderStatusID = ConfigManager.GetInstance(request.ApplicationID).Order_NewStatusID;
                        var mOreder = new ModonOrder
                        {
                            CreationDate = DateTime.Now,
                            ApplicationID = request.ApplicationID,
                            BuildingID = building.ID,
                            CreatedBy = request.UserID,
                            OrderTypeID = orderType,
                            StatusID = orderStatusID,
                            IsDeleted = false
                        };
                        modonOrderRepository.Add(mOreder);
                        #endregion

                        #region add files
                        var targetObjectID_Building = ConfigManager.GetInstance(request.ApplicationID).BuildingDBObjectID;
                        var mediaRepository = new WebMediaRepository(CurrentContextEntities, request.ApplicationID);

                        var ownerIDFile = request.BuildingRecord.OwnerIDFile;
                        if (ownerIDFile != null && ownerIDFile.ContentLength > 0)
                        {
                            var imagePath = string.Format("~/UploadedImages/BuildingFiles/{0}.jpg", Guid.NewGuid().ToString());
                            ownerIDFile.SaveAs(UIHelper.MapPath(imagePath));
                            var media1 = new WebMedia
                            {
                                CreationDate = DateTime.Now,
                                Url = imagePath,
                                ObjectID = targetObjectID_Building,
                                TargetObjectID = building.ID,
                                WebMediaTypeID = ConfigManager.GetInstance(request.ApplicationID).OwnerIDFile_WebMediaTypeID,
                                IsActive = true
                            };
                            mediaRepository.Add(media1);
                        }

                        var sTFile = request.BuildingRecord.STFile;
                        if (sTFile != null && sTFile.ContentLength > 0)
                        {
                            var imagePath = string.Format("~/UploadedImages/BuildingFiles/{0}.jpg", Guid.NewGuid().ToString());
                            sTFile.SaveAs(UIHelper.MapPath(imagePath));
                            var media2 = new WebMedia
                            {
                                CreationDate = DateTime.Now,
                                Url = imagePath,
                                ObjectID = targetObjectID_Building,
                                TargetObjectID = building.ID,
                                WebMediaTypeID = ConfigManager.GetInstance(request.ApplicationID).STFileFile_WebMediaTypeID,
                                IsActive = true
                            };
                            mediaRepository.Add(media2);
                        }

                        var buildingLayoutFile = request.BuildingRecord.BuildingLayoutFile;
                        if (buildingLayoutFile != null && buildingLayoutFile.ContentLength > 0)
                        {
                            var imagePath = string.Format("~/UploadedImages/BuildingFiles/{0}.jpg", Guid.NewGuid().ToString());
                            buildingLayoutFile.SaveAs(UIHelper.MapPath(imagePath));
                            var media3 = new WebMedia
                            {
                                CreationDate = DateTime.Now,
                                Url = imagePath,
                                ObjectID = targetObjectID_Building,
                                TargetObjectID = building.ID,
                                WebMediaTypeID = ConfigManager.GetInstance(request.ApplicationID).BuildingLayoutFile_WebMediaTypeID,
                                IsActive = true
                            };
                            mediaRepository.Add(media3);
                        }

                        var otherFile = request.BuildingRecord.OtherFile;
                        if (otherFile != null && otherFile.Count() > 0)
                        {
                            foreach (var file in otherFile)
                            {
                                if (file != null && file.ContentLength > 0)
                                {
                                    var imagePath = string.Format("~/UploadedImages/BuildingFiles/{0}.jpg", Guid.NewGuid().ToString());
                                    file.SaveAs(UIHelper.MapPath(imagePath));
                                    var media4 = new WebMedia
                                    {
                                        CreationDate = DateTime.Now,
                                        Url = imagePath,
                                        ObjectID = targetObjectID_Building,
                                        TargetObjectID = building.ID,
                                        WebMediaTypeID = ConfigManager.GetInstance(request.ApplicationID).OtherFile_WebMediaTypeID,
                                        IsActive = true
                                    };
                                    mediaRepository.Add(media4);
                                }
                            }

                        }

                        #endregion
                        buildingActivitesRepository.Commit(entableTracking, ref oldValues, ref newValues);

                        #region notify employees
                        var BuildingDbObject = ConfigManager.GetInstance(request.ApplicationID).BuildingDBObjectID;
                        var employrrRoleID = ConfigManager.GetInstance(request.ApplicationID).EmployeeRoleID;

                        var common_UserRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);

                        var toUserListEmployees = common_UserRepository.All().Where(p => p.AreaID == building.AreaID && !p.IsDeleted && p.RoleID == employrrRoleID).Select(p => p.ID).Distinct().ToList();

                        // 1. To  Owner
                        NotificationManager.GetInstance(request.ApplicationID).Notify(request.UserID, building.CreatedBy ?? 0, EnumNotificationType.AddBuilding_User, BuildingDbObject, new BuildingRecord(building));

                        // 2. To Employees
                        NotificationManager.GetInstance(request.ApplicationID).Notify(request.UserID, toUserListEmployees, EnumNotificationType.AddBuilding_Employee, BuildingDbObject, new BuildingRecord(building));
                        #endregion

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidBuildingTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public BuildingResponse DoBuildingUpdate(BuildingRequest req)
        {
            var res = new BuildingResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoBuildingUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoBuildingUpdate, true, false, (BuildingRequest request, BuildingResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.BuildingRecord != null)
                    {
                        var buildingRepository = new BuildingRepository(CurrentContextEntities, request.ApplicationID);
                        var building = buildingRepository.FirstOrDefault(p => p.ID == request.BuildingRecord.ID);
                        var BuildingDbObject = ConfigManager.GetInstance(request.ApplicationID).BuildingDBObjectID;

                        if (building != null)
                        {
                            if(request.UserID==building.CreatedBy)
                            { 
                            building = BuildingServiceManager.CreateOrUpdateBuilding(request.ApplicationID, request.UserID, request.BuildingRecord, building);
                            }
                            else
                            {
                                var order_ApprovedStatusID = ConfigManager.GetInstance(request.ApplicationID).Order_ApprovedStatusID;
                                var order_RejectedStatusID = ConfigManager.GetInstance(request.ApplicationID).Order_RejectedStatusID;
                                var order_PendingForEditStatusID = ConfigManager.GetInstance(request.ApplicationID).Order_PendingForEditStatusID;
                                building.StatusID = request.BuildingRecord.StatusID;

                                if (request.BuildingRecord.StatusID== order_ApprovedStatusID)
                                  NotificationManager.GetInstance(request.ApplicationID).Notify(request.UserID, building.CreatedBy ?? 0, EnumNotificationType.ApproveBuilding, BuildingDbObject, new BuildingRecord(building));
                                else if(request.BuildingRecord.StatusID == order_RejectedStatusID)
                                    NotificationManager.GetInstance(request.ApplicationID).Notify(request.UserID, building.CreatedBy ?? 0, EnumNotificationType.RejectBuilding, BuildingDbObject, new BuildingRecord(building));

                                
                            }

                            var orderTypeID = (long)EnumOrderType.AddBuilding;
                            var modonOrderRepository = new ModonOrderRepository(CurrentContextEntities, request.ApplicationID);
                            var mOrder = modonOrderRepository.FirstOrDefault(p => p.BuildingID == request.BuildingRecord.ID && p.OrderTypeID== orderTypeID);
                            mOrder.StatusID = request.BuildingRecord.StatusID;

                            buildingRepository.Commit(entableTracking, ref oldValues, ref newValues);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidBuildingTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion

        public IEnumerable<WebMedia> WebMediaFiles { get; set; }
    }
}