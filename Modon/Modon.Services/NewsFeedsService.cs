using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using System.Linq;
using System.ServiceModel.Activation;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class NewsFeedsService : BaseService, INewsFeeds
    {
        private const string serviceName = "NewsFeedsService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region NewsFeedsServices
        private const string operationName_DoNewsFeedsList = "DoNewsFeedsList";
        private const string operationName_DoNewsFeedsDelete = "DoNewsFeedsDelete";
        private const string operationName_DoNewsFeedsInsert = "DoNewsFeedsInsert";
        private const string operationName_DoNewsFeedsUpdate = "DoNewsFeedsUpdate";

        public NewsFeedsResponse DoNewsFeedsList(NewsFeedsRequest req)
        {
            var res = new NewsFeedsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsList, false, false, (NewsFeedsRequest request, NewsFeedsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new NewsFeedRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new NewsFeedsRecord
                    {

						ID = p.ID,
						News = p.News,
						ModifiedBy = p.ModifiedBy,
						LastUpdateDate = p.LastUpdateDate,
						CreatedBy = p.CreatedBy,
						IsDeleted = p.IsDeleted,
						CreationDate = p.CreationDate,
						ApplicationID = p.ApplicationID,
                    });
                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }
                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var newsFeedss = ApplyPaging(request, query, out count);

                    response.NewsFeedsRecords = newsFeedss.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, PMApp.Core.CommonDefinitions.MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public NewsFeedsResponse DoNewsFeedsDelete(NewsFeedsRequest req)
        {
            var res = new NewsFeedsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsDelete, true, false, (NewsFeedsRequest request, NewsFeedsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NewsFeedRecord != null && request.NewsFeedRecord.ID > 0)
                    {
                        var newsFeedsRepository = new NewsFeedRepository(CurrentContextEntities, request.ApplicationID);
                        var newsFeeds = newsFeedsRepository.FirstOrDefault(p => p.ID == request.NewsFeedRecord.ID);

                        if (newsFeeds != null)
                        {
                            newsFeeds.IsDeleted = true;
                            newsFeedsRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNewsFeedsTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public NewsFeedsResponse DoNewsFeedsInsert(NewsFeedsRequest req)
        {
            var res = new NewsFeedsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsInsert, true, false, (NewsFeedsRequest request, NewsFeedsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NewsFeedRecord != null)
                    {
                        var newsFeedsRepository = new NewsFeedRepository(CurrentContextEntities, request.ApplicationID);
                        var newsFeeds = NewsFeedsServiceManager.CreateOrUpdateNewsFeeds(request.ApplicationID, request.UserID, request.NewsFeedRecord);
                        newsFeedsRepository.Add(newsFeeds);
                        newsFeedsRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.NewsFeedRecord.ID = newsFeeds.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNewsFeedsTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public NewsFeedsResponse DoNewsFeedsUpdate(NewsFeedsRequest req)
        {
            var res = new NewsFeedsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNewsFeedsUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNewsFeedsUpdate, true, false, (NewsFeedsRequest request, NewsFeedsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NewsFeedRecord != null)
                    {
                        var newsFeedsRepository = new NewsFeedRepository(CurrentContextEntities, request.ApplicationID);
                        var newsFeeds = newsFeedsRepository.FirstOrDefault(p => p.ID == request.NewsFeedRecord.ID);

                        if (newsFeeds != null)
                        {
newsFeeds = NewsFeedsServiceManager.CreateOrUpdateNewsFeeds(request.ApplicationID, request.UserID, request.NewsFeedRecord, newsFeeds);

newsFeedsRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNewsFeedsTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}