using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class T_WorkerServiceManager
    {
        public static T_Worker CreateOrUpdateT_Worker(long applicationID, long userID, T_WorkerRecord t_WorkerRecord, T_Worker t_Worker = null)
        {
            if (t_Worker == null)
            {
                t_Worker = new T_Worker();
                t_Worker.CreationDate = DateTime.Now;
                t_Worker.CreatedBy = userID;
                t_Worker.IsDeleted = false;
                 
            }
            else
            {
                t_Worker.ModifiedBy = userID;
                t_Worker.LastUpdateDate = DateTime.Now;
                t_Worker.IsDeleted = t_WorkerRecord.IsDeleted;
            }

t_Worker.ApplicationID = applicationID;

			t_Worker.BuildingID = t_WorkerRecord.BuildingID;
			t_Worker.W_BirthDate = t_WorkerRecord.W_BirthDate;
			t_Worker.W_imgURL = t_WorkerRecord.W_imgURL;
			t_Worker.W_Job = t_WorkerRecord.W_Job;
			t_Worker.W_KafeelName = t_WorkerRecord.W_KafeelName;
			t_Worker.W_Mobile = t_WorkerRecord.W_Mobile;
			t_Worker.W_Name = t_WorkerRecord.W_Name;
			t_Worker.W_Nationality = t_WorkerRecord.W_Nationality;
			t_Worker.W_NID_EndDate = t_WorkerRecord.W_NID_EndDate;
			t_Worker.W_NID_imgURL = t_WorkerRecord.W_NID_imgURL;
			t_Worker.W_NID_NO = t_WorkerRecord.W_NID_NO;
			t_Worker.W_NID_StartDate = t_WorkerRecord.W_NID_StartDate;
			t_Worker.W_Relagion = t_WorkerRecord.W_Relagion;

            return t_Worker;
        }
    }
}