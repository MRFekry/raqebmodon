using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.NotificationsSystem;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using System.Net;

namespace  Modon.Services.Managers
{
    public class WalletServiceManager
    {
        public static UserWallet CreateWalletRecord(long applicationID,long userID,UserWalletRecord record, UserWallet userWallet = null)
        {
            if (userWallet == null)
            {
                userWallet = new UserWallet();
                userWallet.CreationDate = DateTime.Now;
                userWallet.StatusID = 2;
                userWallet.ApplicationID = applicationID;
                userWallet.CreatedBy = userID;
                userWallet.IsDeleted = false;

            }
            else
            {
                userWallet.ModifiedBy = userID;
                userWallet.LastUpdateDate = DateTime.Now;


            }

            userWallet.Notes = record.Notes;
            userWallet.Points = record.Points;
            userWallet.UserID = userID;
            return userWallet;
        }

        private static UserWallet AddUserWalletRecord(ModonDBEntities CurrentContextEntities, IAbBaseRequest request, long userID, long walletStatusID, long userPoints, long dbObjectID, long targetObjectID)
        {
            var walletRepository = new UserWalletRepository(CurrentContextEntities, request.ApplicationID);

            //ToDo Send Notification and insert to wallet
            //Add awarded answers User Wallet  ToDo using WalletServiceManager
            var userWallet = new UserWallet()
            {
                UserID = userID,
                ApplicationID = request.ApplicationID,
                CreatedBy = request.UserID,
                StatusID = walletStatusID,
                Points = userPoints,
                CreationDate = DateTime.Now,
                Notes = GetUserWalletMessage(request.ApplicationID, dbObjectID, targetObjectID, walletStatusID, userPoints),
                IsDeleted = false,
            };
            walletRepository.Add(userWallet);
            return userWallet;
        }

        private static string GetUserWalletMessage(long applicationID, long dbObjectID, long targetObjectID, long walletStatusID, long userPoints)
        {
            var targetObjectIDStr = targetObjectID.ToString();
            var messageResource = ResourceManager.GetInstance(applicationID).GetResource(string.Format("UserWalletMessage-{0}-{1}", dbObjectID, walletStatusID));
          
            return messageResource.Replace("TargetObjectID", targetObjectIDStr)
                                    .Replace("UserPoints", Math.Abs(userPoints).ToString());
        }

    }
}