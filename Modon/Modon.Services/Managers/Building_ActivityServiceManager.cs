using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class Building_ActivityServiceManager
    {
        public static Building_Activity CreateOrUpdateBuilding_Activity(long applicationID, long userID, Building_ActivityRecord building_ActivityRecord, Building_Activity building_Activity = null)
        {
            if (building_Activity == null)
            {
                building_Activity = new Building_Activity();
                building_Activity.CreationDate = DateTime.Now;
                building_Activity.CreatedBy = userID;
                building_Activity.IsDeleted = false;
            }
            else
            {
                building_Activity.ModifiedBy = userID;
                building_Activity.LastUpdateDate = DateTime.Now;
                building_Activity.IsDeleted = building_ActivityRecord.IsDeleted;
            }

building_Activity.ApplicationID = applicationID;

			building_Activity.BuildingID = building_ActivityRecord.BuildingID;
			building_Activity.Description = building_ActivityRecord.Description;
			building_Activity.IsAllowed = building_ActivityRecord.IsAllowed;
            building_Activity.ActivitSubTypeID = building_ActivityRecord.ActivitSubTypeID;

            return building_Activity;
        }
    }
}