using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class Comment_TypeServiceManager
    {
        public static Comment_Type CreateOrUpdateComment_Type(long applicationID, long userID, Comment_TypeRecord comment_TypeRecord, Comment_Type comment_Type = null)
        {
            if (comment_Type == null)
            {
                comment_Type = new Comment_Type();
                comment_Type.CreationDate = DateTime.Now;
                comment_Type.CreatedBy = userID;
                comment_Type.IsDeleted = false;
            }
            else
            {
                comment_Type.ModifiedBy = userID;
                comment_Type.LastUpdateDate = DateTime.Now;
                comment_Type.IsDeleted = comment_TypeRecord.IsDeleted;
            }

comment_Type.ApplicationID = applicationID;

			comment_Type.Description = comment_TypeRecord.Description;
			comment_Type.IconClass = comment_TypeRecord.IconClass;
			comment_Type.Name = comment_TypeRecord.Name;

            return comment_Type;
        }
    }
}