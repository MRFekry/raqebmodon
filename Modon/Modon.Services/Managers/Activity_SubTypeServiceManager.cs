using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class Activity_SubTypeServiceManager
    {
        public static Activity_SubType CreateOrUpdateActivity_SubType(long applicationID, long userID, Activity_SubTypeRecord activity_SubTypeRecord, Activity_SubType activity_SubType = null)
        {
            if (activity_SubType == null)
            {
                activity_SubType = new Activity_SubType();
                activity_SubType.CreationDate = DateTime.Now;
                activity_SubType.CreatedBy = userID;
                activity_SubType.IsDeleted = false;
            }
            else
            {
                activity_SubType.ModifiedBy = userID;
                activity_SubType.LastUpdateDate = DateTime.Now;
                activity_SubType.IsDeleted = activity_SubTypeRecord.IsDeleted;
            }

activity_SubType.ApplicationID = applicationID;

			activity_SubType.ActivityTypeID = activity_SubTypeRecord.ActivityTypeID;
			activity_SubType.Description = activity_SubTypeRecord.Description;
			activity_SubType.Name = activity_SubTypeRecord.Name;

            return activity_SubType;
        }
    }
}