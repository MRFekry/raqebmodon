using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class ModonOrderServiceManager
    {
        public static ModonOrder CreateOrUpdateModonOrder(long applicationID, long userID, ModonOrderRecord modonOrderRecord, ModonOrder modonOrder = null)
        {
            if (modonOrder == null)
            {
                modonOrder = new ModonOrder();
                modonOrder.CreationDate = DateTime.Now;
                modonOrder.CreatedBy = userID;
                modonOrder.IsDeleted = false;
            }
            else
            {
                modonOrder.ModifiedBy = userID;
                modonOrder.LastUpdateDate = DateTime.Now;
                modonOrder.IsDeleted = modonOrderRecord.IsDeleted;
            }

modonOrder.ApplicationID = applicationID;

			modonOrder.BuildingID = modonOrderRecord.BuildingID;
			modonOrder.OrderTypeID = modonOrderRecord.OrderTypeID;
			modonOrder.StatusID = modonOrderRecord.StatusID;

            return modonOrder;
        }
    }
}