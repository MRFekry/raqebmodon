using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class OrderTypeServiceManager
    {
        public static OrderType CreateOrUpdateOrderType(long applicationID, long userID, OrderTypeRecord orderTypeRecord, OrderType orderType = null)
        {
            if (orderType == null)
            {
                orderType = new OrderType();
                orderType.CreationDate = DateTime.Now;
                orderType.CreatedBy = userID;
                orderType.IsDeleted = false;
            }
            else
            {
                orderType.ModifiedBy = userID;
                orderType.LastUpdateDate = DateTime.Now;
                orderType.IsDeleted = orderTypeRecord.IsDeleted;
            }

orderType.ApplicationID = applicationID;

			orderType.Description = orderTypeRecord.Description;
			orderType.IconClass = orderTypeRecord.IconClass;
			orderType.Name = orderTypeRecord.Name;

            return orderType;
        }
    }
}