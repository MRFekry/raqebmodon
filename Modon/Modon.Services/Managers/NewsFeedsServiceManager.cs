using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace  Modon.Services.Managers
{
    public class NewsFeedsServiceManager
    {
        public static NewsFeed CreateOrUpdateNewsFeeds(long applicationID, long userID, NewsFeedsRecord newsFeedsRecord, NewsFeed newsFeeds = null)
        {
            if (newsFeeds == null)
            {
                newsFeeds = new NewsFeed();
                newsFeeds.CreationDate = DateTime.Now;
                newsFeeds.CreatedBy = userID;
                newsFeeds.IsDeleted = false;
            }
            else
            {
                newsFeeds.ModifiedBy = userID;
                newsFeeds.LastUpdateDate = DateTime.Now;
                newsFeeds.IsDeleted = newsFeedsRecord.IsDeleted;
            }

newsFeeds.ApplicationID = applicationID;

			newsFeeds.News = newsFeedsRecord.News;

            return newsFeeds;
        }
    }
}