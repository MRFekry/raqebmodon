﻿using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Modon.Services.Managers
{
    public class UserExServiceManager
    {
        public static Area CreateOrUpdateArea(long userID, AreaRecord areaRecord, Area area = null)
        {
            if (area == null)
            {
                area = new Area();
                area.CreationDate = DateTime.Now;
                area.CreatedBy = userID;
                area.IsDeleted = false;
            }
            else
            {
                area.ModifiedBy = userID;
                area.LastUpdateDate = DateTime.Now;
                area.IsDeleted = areaRecord.IsDeleted;
            }
            area.ID = areaRecord.ID;
            area.Name = areaRecord.Name;
            area.Description = areaRecord.Description;
            area.ParentID = areaRecord.ParentID;
            area.Points = areaRecord.Points;


            return area;
        }


        public static NewsFeedsUser CreateOrUpdateNewsFeedsUser(long userID, NewsFeedsUserRecord newsFeedsUserRecord, NewsFeedsUser newsFeedsUser = null)
        {
            if (newsFeedsUser == null)
            {
                newsFeedsUser = new NewsFeedsUser();

            }
            else
            {

            }
            newsFeedsUser.ID = newsFeedsUserRecord.ID;
            newsFeedsUser.UserID = newsFeedsUserRecord.UserID;
            newsFeedsUser.NewFeedsID = newsFeedsUserRecord.NewFeedsID;
            newsFeedsUser.NotificationID = newsFeedsUserRecord.NotificationID;

            return newsFeedsUser;
        }

        public static NewsFeed CreateOrUpdateNewsFeed(long userID, NewsFeedsRecord newsFeedRecord, NewsFeed newsFeed = null)
        {
            if (newsFeed == null)
            {
                newsFeed = new NewsFeed();
                newsFeed.CreatedBy = userID;
                newsFeed.CreationDate = DateTime.Now;
                newsFeed.IsDeleted = false;
            }
            else
            {
                newsFeed.ModifiedBy = userID;
                newsFeed.LastUpdateDate = DateTime.Now;
                newsFeed.IsDeleted = newsFeedRecord.IsDeleted;
            }
            newsFeed.ID = newsFeedRecord.ID;
            newsFeed.News = newsFeedRecord.News;

            return newsFeed;
        }

        public static IEnumerable<AreaRecord> RecursiveChildren(AreaRecord[] items, long toplevelid, bool getParent = true)
        {
            List<AreaRecord> inner = new List<AreaRecord>();
            if (getParent)
            {
                inner.Add(items.FirstOrDefault(item => item.ID == toplevelid));
            }
            foreach (var t in items.Where(item => item.ParentID == toplevelid))
            {
                inner.Add(t);
                inner = inner.Union(RecursiveChildren(items, t.ID)).ToList();
            }

            return inner;
        }
    }
}