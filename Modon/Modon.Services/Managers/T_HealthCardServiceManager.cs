using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class T_HealthCardServiceManager
    {
        public static T_HealthCard CreateOrUpdateT_HealthCard(long applicationID, long userID, T_HealthCardRecord t_HealthCardRecord, T_HealthCard t_HealthCard = null)
        {
            if (t_HealthCard == null)
            {
                t_HealthCard = new T_HealthCard();
                t_HealthCard.CreationDate = DateTime.Now;
                t_HealthCard.CreatedBy = userID;
                t_HealthCard.IsDeleted = false;
            }
            else
            {
               
                t_HealthCard.ModifiedBy = userID;
                t_HealthCard.LastUpdateDate = DateTime.Now;
                t_HealthCard.IsDeleted = t_HealthCardRecord.IsDeleted;
            }

t_HealthCard.ApplicationID = applicationID;

			t_HealthCard.BuildingID = t_HealthCardRecord.BuildingID;
			t_HealthCard.HC_AceptDate = t_HealthCardRecord.HC_AceptDate;
			t_HealthCard.HC_AceptedBy = t_HealthCardRecord.HC_AceptedBy;
			t_HealthCard.HC_AceptNote = t_HealthCardRecord.HC_AceptNote;
			t_HealthCard.HC_EndDate = t_HealthCardRecord.HC_EndDate;
			t_HealthCard.HC_NO = t_HealthCardRecord.HC_NO;
			t_HealthCard.HC_ReqNote = t_HealthCardRecord.HC_ReqNote;
			t_HealthCard.HC_RequstNO = t_HealthCardRecord.HC_RequstNO;
			t_HealthCard.HC_St_Id = t_HealthCardRecord.HC_St_Id;
			t_HealthCard.HC_StopById = t_HealthCardRecord.HC_StopById;
			t_HealthCard.HC_StopDate = t_HealthCardRecord.HC_StopDate;
			t_HealthCard.HC_StopReason = t_HealthCardRecord.HC_StopReason;
			t_HealthCard.HC_StratDate = t_HealthCardRecord.HC_StratDate;
			t_HealthCard.HC_WorkerId = t_HealthCardRecord.HC_WorkerId;
			t_HealthCard.HRep_FileURL = t_HealthCardRecord.HRep_FileURL;
			t_HealthCard.HRep_HospId = t_HealthCardRecord.HRep_HospId;
			t_HealthCard.HRepDate = t_HealthCardRecord.HRepDate;
			t_HealthCard.HRepNo = t_HealthCardRecord.HRepNo;

            return t_HealthCard;
        }
    }
}