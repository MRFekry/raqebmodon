using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace  Modon.Services.Managers
{
    public class NewsFeedsUserServiceManager
    {
        public static NewsFeedsUser CreateOrUpdateNewsFeedsUser(long applicationID, long userID, NewsFeedsUserRecord newsFeedsUserRecord, NewsFeedsUser newsFeedsUser = null)
        {
            if (newsFeedsUser == null)
            {
                newsFeedsUser = new NewsFeedsUser();
                //newsFeedsUser.CreationDate = DateTime.Now;
                //newsFeedsUser.CreatedBy = userID;
                //newsFeedsUser.IsDeleted = false;
            }
            else
            {
                //newsFeedsUser.ModifiedBy = userID;
                //newsFeedsUser.LastUpdateDate = DateTime.Now;
                //newsFeedsUser.IsDeleted = newsFeedsUserRecord.IsDeleted;
            }

newsFeedsUser.ApplicationID = applicationID;

			newsFeedsUser.UserID = newsFeedsUserRecord.UserID;
			newsFeedsUser.NewFeedsID = newsFeedsUserRecord.NewFeedsID;
			newsFeedsUser.NotificationID = newsFeedsUserRecord.NotificationID;

            return newsFeedsUser;
        }
    }
}