using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class ActivityTypeServiceManager
    {
        public static ActivityType CreateOrUpdateActivityType(long applicationID, long userID, ActivityTypeRecord activityTypeRecord, ActivityType activityType = null)
        {
            if (activityType == null)
            {
                activityType = new ActivityType();
                activityType.CreationDate = DateTime.Now;
                activityType.CreatedBy = userID;
                activityType.IsDeleted = false;
            }
            else
            {
                activityType.ModifiedBy = userID;
                activityType.LastUpdateDate = DateTime.Now;
                activityType.IsDeleted = activityTypeRecord.IsDeleted;
            }

activityType.ApplicationID = applicationID;

			activityType.Description = activityTypeRecord.Description;
			activityType.IconClass = activityTypeRecord.IconClass;
			activityType.Name = activityTypeRecord.Name;

            return activityType;
        }
    }
}