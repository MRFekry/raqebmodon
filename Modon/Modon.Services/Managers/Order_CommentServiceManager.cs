using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class Order_CommentServiceManager
    {
        public static Order_Comment CreateOrUpdateOrder_Comment(long applicationID, long userID, Order_CommentRecord order_CommentRecord, Order_Comment order_Comment = null)
        {
            if (order_Comment == null)
            {
                order_Comment = new Order_Comment();
                order_Comment.CreationDate = DateTime.Now;
                order_Comment.CreatedBy = userID;
                order_Comment.IsDeleted = false;
            }
            else
            {
                order_Comment.ModifiedBy = userID;
                order_Comment.LastUpdateDate = DateTime.Now;
                order_Comment.IsDeleted = order_CommentRecord.IsDeleted;
            }

order_Comment.ApplicationID = applicationID;

			order_Comment.Comment = order_CommentRecord.Comment;
			order_Comment.OrderID = order_CommentRecord.OrderID;
			order_Comment.UserID = order_CommentRecord.UserID;

            return order_Comment;
        }
    }
}