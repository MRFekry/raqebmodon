using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Modon.Services.Managers
{
    public class BuildingServiceManager
    {
        public static Building CreateOrUpdateBuilding(long applicationID, long userID, BuildingRecord buildingRecord, Building building = null)
        {
            if (building == null)
            {
                building = new Building();
                building.CreationDate = DateTime.Now;
                building.CreatedBy = userID;
                building.IsDeleted = false;

                building.ActivityAllowed = false;
                building.NameAllowed = false;
            }
            else
            {
                building.ModifiedBy = userID;
                building.LastUpdateDate = DateTime.Now;
                building.IsDeleted = buildingRecord.IsDeleted;

                building.ActivityAllowed = buildingRecord.ActivityAllowed;
                building.NameAllowed = buildingRecord.NameAllowed;

            }

            building.ApplicationID = applicationID;

			building.ActivityTypeID = buildingRecord.ActivityTypeID;
			building.AreaID = buildingRecord.AreaID;
			building.AreaSpace = buildingRecord.AreaSpace;
			building.Email = buildingRecord.Email;
			building.Name = buildingRecord.Name;
			building.OwnerCID = buildingRecord.OwnerCID;
			building.OwnerName = buildingRecord.OwnerName;
			building.OwnerPhone = buildingRecord.OwnerPhone;
			building.Phone = buildingRecord.Phone;
			building.SpaceAllowed = buildingRecord.SpaceAllowed;
			building.StatusID = buildingRecord.StatusID;
			building.StreetAdress = buildingRecord.StreetAdress;

            return building;
        }
    }
}