using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class T_WorkerService : BaseService, IT_Worker
    {
        private const string serviceName = "T_WorkerService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region T_WorkerServices
        private const string operationName_DoT_WorkerList = "DoT_WorkerList";
        private const string operationName_DoT_WorkerDelete = "DoT_WorkerDelete";
        private const string operationName_DoT_WorkerInsert = "DoT_WorkerInsert";
        private const string operationName_DoT_WorkerUpdate = "DoT_WorkerUpdate";

        public T_WorkerResponse DoT_WorkerList(T_WorkerRequest req)
        {
            var res = new T_WorkerResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_WorkerList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_WorkerList, false, false, (T_WorkerRequest request, T_WorkerResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new T_WorkerRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new T_WorkerRecord
                    {

						ApplicationID = p.ApplicationID,
						BuildingID = p.BuildingID,
						CreatedBy = p.CreatedBy,
						CreationDate = p.CreationDate,
						ID = p.ID,
						IsDeleted = p.IsDeleted,
						LastUpdateDate = p.LastUpdateDate,
						ModifiedBy = p.ModifiedBy,
						W_BirthDate = p.W_BirthDate,
						W_imgURL = p.W_imgURL,
						W_Job = p.W_Job,
						W_KafeelName = p.W_KafeelName,
						W_Mobile = p.W_Mobile,
						W_Name = p.W_Name,
						W_Nationality = p.W_Nationality,
						W_NID_EndDate = p.W_NID_EndDate,
						W_NID_imgURL = p.W_NID_imgURL,
						W_NID_NO = p.W_NID_NO,
						W_NID_StartDate = p.W_NID_StartDate,
						W_Relagion = p.W_Relagion,
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var t_Workers = ApplyPaging(request, query, out count);

                    response.T_WorkerRecords = t_Workers.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public T_WorkerResponse DoT_WorkerDelete(T_WorkerRequest req)
        {
            var res = new T_WorkerResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_WorkerDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_WorkerDelete, true, false, (T_WorkerRequest request, T_WorkerResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.T_WorkerRecord != null && request.T_WorkerRecord.ID > 0)
                    {
                        var t_WorkerRepository = new T_WorkerRepository(CurrentContextEntities, request.ApplicationID);
                        var t_Worker = t_WorkerRepository.FirstOrDefault(p => p.ID == request.T_WorkerRecord.ID);

                        if (t_Worker != null)
                        {
                            t_Worker.IsDeleted = true;
                            t_WorkerRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidT_WorkerTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public T_WorkerResponse DoT_WorkerInsert(T_WorkerRequest req)
        {
            var res = new T_WorkerResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_WorkerInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_WorkerInsert, true, false, (T_WorkerRequest request, T_WorkerResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.T_WorkerRecord != null)
                    {
                        var t_WorkerRepository = new T_WorkerRepository(CurrentContextEntities, request.ApplicationID);
                        var t_Worker = T_WorkerServiceManager.CreateOrUpdateT_Worker(request.ApplicationID, request.UserID, request.T_WorkerRecord);
                        t_WorkerRepository.Add(t_Worker);
                        t_WorkerRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.T_WorkerRecord.ID = t_Worker.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidT_WorkerTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public T_WorkerResponse DoT_WorkerUpdate(T_WorkerRequest req)
        {
            var res = new T_WorkerResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoT_WorkerUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoT_WorkerUpdate, true, false, (T_WorkerRequest request, T_WorkerResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.T_WorkerRecord != null)
                    {
                        var t_WorkerRepository = new T_WorkerRepository(CurrentContextEntities, request.ApplicationID);
                        var t_Worker = t_WorkerRepository.FirstOrDefault(p => p.ID == request.T_WorkerRecord.ID);

                        if (t_Worker != null)
                        {
t_Worker = T_WorkerServiceManager.CreateOrUpdateT_Worker(request.ApplicationID, request.UserID, request.T_WorkerRecord, t_Worker);

t_WorkerRepository.Commit(entableTracking, ref oldValues, ref newValues);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidT_WorkerTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}