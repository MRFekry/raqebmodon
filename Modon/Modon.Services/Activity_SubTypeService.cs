using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions;
using Modon.CommonDefinitions.DataContracts;
using Modon.Interfaces;
using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using PMApp.Core.Interfaces;
using PMApp.Core.Services;
using System;
using System.Linq;
using System.ServiceModel.Activation;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.NotificationsSystem;
using Newtonsoft.Json;

namespace Modon.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class Activity_SubTypeService : BaseService, IActivity_SubType
    {
        private const string serviceName = "Activity_SubTypeService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region Activity_SubTypeServices
        private const string operationName_DoActivity_SubTypeList = "DoActivity_SubTypeList";
        private const string operationName_DoActivity_SubTypeDelete = "DoActivity_SubTypeDelete";
        private const string operationName_DoActivity_SubTypeInsert = "DoActivity_SubTypeInsert";
        private const string operationName_DoActivity_SubTypeUpdate = "DoActivity_SubTypeUpdate";

        public Activity_SubTypeResponse DoActivity_SubTypeList(Activity_SubTypeRequest req)
        {
            var res = new Activity_SubTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivity_SubTypeList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivity_SubTypeList, false, false, (Activity_SubTypeRequest request, Activity_SubTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    var query = new Activity_SubTypeRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new Activity_SubTypeRecord
                    {

						ActivityTypeID = p.ActivityTypeID,
						ApplicationID = p.ApplicationID,
						CreatedBy = p.CreatedBy,
						CreationDate = p.CreationDate,
						Description = p.Description,
						ID = p.ID,
						IsDeleted = p.IsDeleted,
						LastUpdateDate = p.LastUpdateDate,
						ModifiedBy = p.ModifiedBy,
						Name = p.Name,
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var activity_SubTypes = ApplyPaging(request, query, out count);

                    response.Activity_SubTypeRecords = activity_SubTypes.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public Activity_SubTypeResponse DoActivity_SubTypeDelete(Activity_SubTypeRequest req)
        {
            var res = new Activity_SubTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivity_SubTypeDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivity_SubTypeDelete, true, false, (Activity_SubTypeRequest request, Activity_SubTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Activity_SubTypeRecord != null && request.Activity_SubTypeRecord.ID > 0)
                    {
                        var activity_SubTypeRepository = new Activity_SubTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var activity_SubType = activity_SubTypeRepository.FirstOrDefault(p => p.ID == request.Activity_SubTypeRecord.ID);

                        if (activity_SubType != null)
                        {
                            activity_SubType.IsDeleted = true;
                            activity_SubTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidActivity_SubTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Activity_SubTypeResponse DoActivity_SubTypeInsert(Activity_SubTypeRequest req)
        {
            var res = new Activity_SubTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivity_SubTypeInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivity_SubTypeInsert, true, false, (Activity_SubTypeRequest request, Activity_SubTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Activity_SubTypeRecord != null)
                    {
                        var activity_SubTypeRepository = new Activity_SubTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var activity_SubType = Activity_SubTypeServiceManager.CreateOrUpdateActivity_SubType(request.ApplicationID, request.UserID, request.Activity_SubTypeRecord);
                        activity_SubTypeRepository.Add(activity_SubType);
                        activity_SubTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
                        request.Activity_SubTypeRecord.ID = activity_SubType.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidActivity_SubTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public Activity_SubTypeResponse DoActivity_SubTypeUpdate(Activity_SubTypeRequest req)
        {
            var res = new Activity_SubTypeResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoActivity_SubTypeUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoActivity_SubTypeUpdate, true, false, (Activity_SubTypeRequest request, Activity_SubTypeResponse response, bool entableTracking, ref string oldValues, ref string newValues) =>
                {
                    bool isInValidObject = true;

                    if (request.Activity_SubTypeRecord != null)
                    {
                        var activity_SubTypeRepository = new Activity_SubTypeRepository(CurrentContextEntities, request.ApplicationID);
                        var activity_SubType = activity_SubTypeRepository.FirstOrDefault(p => p.ID == request.Activity_SubTypeRecord.ID);

                        if (activity_SubType != null)
                        {
activity_SubType = Activity_SubTypeServiceManager.CreateOrUpdateActivity_SubType(request.ApplicationID, request.UserID, request.Activity_SubTypeRecord, activity_SubType);

activity_SubTypeRepository.Commit(entableTracking, ref oldValues, ref newValues);
            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKeyEx.InvalidActivity_SubTypeTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}