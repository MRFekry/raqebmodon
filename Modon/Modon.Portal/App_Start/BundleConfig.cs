using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Optimization;

namespace  Modon.Portal
{
    public class BundleConfig
    {
        private static string currentVersionNp = string.Empty;
        public static string CurrentVersionNo
        {
            get
            {
                if (currentVersionNp == string.Empty)
                {
                    var fileCreatedDate = File.GetCreationTime(HostingEnvironment.MapPath("~/bin/Modon.Portal.dll"));
                    return fileCreatedDate.ToString("ddMMyyyyHHmmss") + "-12";
                }
                return currentVersionNp;
            }
        }

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            var bundlesList = new List<Bundle>();

            bundlesList.Add(new StyleBundle("~/bundles/css/s1").Include(
                "~/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css"
                , "~/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css"
                , "~/assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css"
                , "~/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css"
                , "~/assets/css/animate.min.css"
            ));

            bundlesList.Add(new StyleBundle("~/bundles/css/sen").Include(
                "~/assets/css/style.css"
                , "~/assets/css/style-responsive.css"
            ));

            bundlesList.Add(new StyleBundle("~/bundles/css/sar").Include(
                "~/assets/css/style-ar.css"
                , "~/assets/css/style-responsive-ar.css"
            ));

            bundlesList.Add(new StyleBundle("~/bundles/css/s2").Include(
                "~/assets/css/theme/default.css"
                , "~/assets/plugins/morris/morris.css"
                , "~/assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css"
                , "~/assets/plugins/gritter/css/jquery.gritter.css"
                , "~/assets/plugins/switchery/switchery.min.css"
                , "~/assets/plugins/powerange/powerange.min.css"
                , "~/assets/plugins/bootstrap3-editable/css/bootstrap-editable.css"
                , "~/assets/plugins/bootstrap3-editable/inputs-ext/address/address.css"
                , "~/assets/plugins/bootstrap3-editable/inputs-ext/typeaheadjs/lib/typeahead.css"
                , "~/Content/Site.css"
                , "~/assets/plugins/bootstrap-wysihtml5/lib/css/wysiwyg-color.css"
                , "~/assets/plugins/isotope/isotope.css"
                , "~/assets/plugins/select2/dist/css/select2.css"
                , "~/Content/number-polyfill.css"
                , "~/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css"
                , "~/assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css"
                , "~/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css"
                , "~/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css"
                , "~/Content/font-awesome-animation.min.css"));


            bundlesList.Add(new ScriptBundle("~/ScriptBundles/SBS").Include("~/Scripts/SBS.js"));

            bundlesList.Add(new ScriptBundle("~/bundles/layoutscripts").Include(
                "~/assets/plugins/jquery/jquery-1.9.1.min.js"
                , "~/Scripts/jquery.validate.min.js"
                , "~/Scripts/jquery.validate.unobtrusive.min.js"
                , "~/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"
                , "~/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"
                , "~/assets/plugins/bootstrap/js/bootstrap.min.js"
                , "~/Scripts/SBS.js"
                , "~/assets/plugins/slimscroll/jquery.slimscroll.min.js"
                , "~/assets/plugins/jquery-cookie/jquery.cookie.js"
                , "~/assets/plugins/flot/jquery.flot.min.js"
                , "~/assets/plugins/flot/jquery.flot.time.min.js"
                , "~/assets/plugins/flot/jquery.flot.resize.min.js"
                , "~/assets/plugins/flot/jquery.flot.pie.min.js"
                , "~/assets/plugins/gritter/js/jquery.gritter.js"
                , "~/assets/plugins/morris/raphael.min.js"
                , "~/assets/plugins/switchery/switchery.min.js"
                , "~/assets/plugins/morris/morris.js"
                , "~/assets/js/gallery.demo.min.js"
                , "~/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"
                , "~/assets/plugins/password-indicator/js/password-indicator.js"
                , "~/assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js"
                , "~/assets/plugins/bootstrap-select/bootstrap-select.min.js"
                , "~/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js"
                , "~/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js"
                , "~/assets/plugins/jquery-tag-it/js/tag-it.min.js"
                , "~/assets/plugins/bootstrap-daterangepicker/moment.js"
                , "~/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"
                , "~/assets/plugins/select2/dist/js/select2.min.js"
                , "~/assets/js/skitch.js"
                , "~/assets/plugins/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"
                , "~/assets/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"
                , "~/assets/js/email-compose.demo.min.js"
                , "~/assets/plugins/superbox/js/superbox.js"
                , "~/assets/js/gallery-v2.demo.min.js"
                , "~/assets/plugins/parsley/dist/parsley.js"
                , "~/assets/plugins/lightbox/js/lightbox-2.6.min.js"
                , "~/assets/plugins/mockjax/jquery.mockjax.js"
                , "~/assets/plugins/moment/moment.min.js"
                , "~/assets/js/form-editable.js"
                , "~/assets/plugins/powerange/powerange.min.js"
                , "~/assets/js/form-slider-switcher.demo.min.js"
                , "~/assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js"
                , "~/assets/plugins/bootstrap3-editable/inputs-ext/address/address.js"
                , "~/assets/plugins/bootstrap3-editable/inputs-ext/typeaheadjs/lib/typeahead.js"
                , "~/assets/plugins/bootstrap3-editable/inputs-ext/typeaheadjs/typeaheadjs.js"
                , "~/assets/plugins/bootstrap3-editable/inputs-ext/wysihtml5/wysihtml5.js"
                , "~/assets/plugins/morris/raphael.min.js"
                , "~/assets/plugins/morris/morris.js"
                , "~/assets/js/chart-morris.demo.min.js"
                , "~/assets/js/apps.min.js"
                , "~/assets/js/apps.js"
                , "~/Content/starrr.js"
                , "~/Content/starrr2.js"
                , "~/Content/persianumber.min.js"
                  , "~/assets/plugins/DataTables/media/js/jquery.dataTables.js"
                   , "~/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"
                   , "~/assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"
                   , "~/assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"
                   , "~/assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"
                   , "~/assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js"
                   , "~/assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"
                   , "~/assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"
                   //, "~/assets/plugins/DataTables/extensions/Buttons/js/buttons.print.js"
                   , "~/assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.js"
                   , "~/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"
                , "~/Content/number-polyfill.js"
            ));

            var path = HostingEnvironment.MapPath("~/Scripts");
            if (!string.IsNullOrWhiteSpace(path) && Directory.Exists(path))
            {
                var scriptDirs = Directory.GetDirectories(path);
                for (int d = 0; d < scriptDirs.Length; d++)
                {
                    var scripts = Directory.GetFiles(scriptDirs[d], "*.js");
                    for (int jsf = 0; jsf < scripts.Length; jsf++)
                    {
                        var jsFilePath = string.Format("~/Scripts/{0}/{1}", Path.GetFileNameWithoutExtension(scriptDirs[d]), Path.GetFileName(scripts[jsf]));
                        var jsFilePathBundles = string.Format("~/ScriptBundles/{0}/{1}", Path.GetFileNameWithoutExtension(scriptDirs[d]), Path.GetFileNameWithoutExtension(scripts[jsf]));
                        bundlesList.Add(new ScriptBundle(jsFilePathBundles).Include(jsFilePath));
                    }
                }
            }

            for (int i = 0; i < bundlesList.Count; i++)
            {
                bundlesList[i].Transforms.Add(new LastModifiedBundleTransform());
                bundles.Add(bundlesList[i]);
            }
#if DEBUG
            System.Web.Optimization.BundleTable.EnableOptimizations = false;

            foreach (var bundle in bundles)
            {
                bundle.Transforms.Clear();
            }
#endif

        }


        public class LastModifiedBundleTransform : IBundleTransform
        {
            public void Process(BundleContext context, BundleResponse response)
            {
                foreach (var file in response.Files)
                {
                    file.IncludedVirtualPath = string.Concat(file.IncludedVirtualPath, "?v=", BundleConfig.CurrentVersionNo);
                }
            }
        }



        public static IHtmlString RenderStyle(params string[] paths)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                paths[i] += string.Format("?v={0}", BundleConfig.CurrentVersionNo);
            }
            return System.Web.Optimization.Styles.Render(paths);
        }


        public static IHtmlString RenderScript(params string[] paths)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                paths[i] += string.Format("?v={0}", BundleConfig.CurrentVersionNo);
            }
            return System.Web.Optimization.Scripts.Render(paths);
        }

    }
}