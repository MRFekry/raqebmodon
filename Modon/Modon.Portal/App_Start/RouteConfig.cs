using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace  Modon.Portal
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.js/{*pathInfo}");
            routes.IgnoreRoute("{resource}.css/{*pathInfo}");
            routes.IgnoreRoute("{resource}.svc/{*pathInfo}");
            routes.IgnoreRoute("{resource}.woff");
            routes.IgnoreRoute("{resource}.woff2");
            routes.IgnoreRoute("{resource}.ttf");

            var routeEntryList = new List<RouteEntry>();
            routeEntryList.Add(new RouteEntry("Attachment/AddAttachment", "Attachment", "AddAttachment"));
            routeEntryList.Add(new RouteEntry("Attachment/Download", "Attachment", "Download"));
            routeEntryList.Add(new RouteEntry("User/Login", "User", "Login"));
            routeEntryList.Add(new RouteEntry("Home/LandingPage", "Home", "LandingPage"));
            routeEntryList.Add(new RouteEntry("Home/PartialRanking", "Home", "PartialRanking"));
            routeEntryList.Add(new RouteEntry("Home/ContactUs", "Home", "ContactUs"));
            routeEntryList.Add(new RouteEntry("Home/ListQuestion", "Home", "ListQuestion"));
            routeEntryList.Add(new RouteEntry("Schedule/{id}", "Schedule", "Index"));
            routeEntryList.Add(new RouteEntry("Question/ListQuestionLanding", "Question", "ListQuestionLanding"));
            routeEntryList.Add(new RouteEntry("Home/ListPromoCode", "Home", "ListPromoCode"));

            routeEntryList.Add(new RouteEntry("UIMembership/LandingMembership", "UIMembership", "LandingMembership"));
            routeEntryList.Add(new RouteEntry("User/ForgetPassword", "User", "ForgetPassword"));
            routeEntryList.Add(new RouteEntry("User/ForgetPasswordChoice", "User", "ForgetPasswordChoice"));
            routeEntryList.Add(new RouteEntry("User/ActivateAccount", "User", "ActivateAccount"));
            routeEntryList.Add(new RouteEntry("User/ChangePassword", "User", "ChangePassword"));
            routeEntryList.Add(new RouteEntry("User/AddUserRegisteration", "User", "AddUserRegisteration"));
            routeEntryList.Add(new RouteEntry("DefaultCaptcha/Generate", "DefaultCaptcha", "Generate"));
            routeEntryList.Add(new RouteEntry("DefaultCaptcha/refresh", "DefaultCaptcha", "refresh"));

            
            for (int i = 0; i < routeEntryList.Count; i++)
            {
                routes.MapRoute(
                  name: routeEntryList[i].Name,
                  url: routeEntryList[i].Url,
                  defaults: routeEntryList[i].Defaults,
                  namespaces: new[] { "Modon.Portal.Controllers" }

                 );
            }
            routes.MapRoute(
                name: "Default",
                url: "{application}/{controller}/{action}/{id}",
                defaults: new { controller = "User", action = "Login", id = UrlParameter.Optional },
                                  namespaces: new[] { "Modon.Portal.Controllers" }

            );




        }
    }



    class RouteEntry
    {
        public object Defaults { get; set; }

        public string Name { get; set; }
        public string Url { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public RouteEntry(string url, string controller, string action)
        {
            this.Url = url.ToLower();
            this.Controller = controller;
            this.Action = action;
            this.Name = url.Replace("{", "_").Replace("}", "_").Replace("/", "_");

            if (url.ToLower().Contains("{title}") && url.ToLower().Contains("{id}"))
                this.Defaults = new { Controller = controller, Action = action, title = UrlParameter.Optional, id = UrlParameter.Optional };
            else if (url.ToLower().Contains("{title}"))
                this.Defaults = new { Controller = controller, Action = action, title = UrlParameter.Optional };
            else if (url.ToLower().Contains("{id}"))
                this.Defaults = new { Controller = controller, Action = action, id = UrlParameter.Optional };
            else
                this.Defaults = new { Controller = controller, Action = action };
        }
    }
}
