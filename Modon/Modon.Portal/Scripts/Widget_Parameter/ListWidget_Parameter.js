


function DeleteWidget_ParameterCheck(DeleteTargetName, DeleteTargetID) {
    SBSAjax(  {
        url: ResolveUrl("~/Home/DeleteCheck"),
        type: "post",
        data: {
            DeleteTarget: DeleteTargetName
        },
        success: function (response) {
            $("#div-delete-check").html(response);
            $("#div-delete-check").show();

            $("#btn-delete-cancel").click(function () {
                $("#div-delete-check").hide();
            });

            $("#btn-delete-ok").click(function () {
                $("#div-delete-check").hide();
                SBSAjax(  {
                    url: ResolveUrl("~/Widget_Parameter/DeleteWidget_Parameter"),
                    data: {
                        widget_ParameterID: DeleteTargetID
                    },
                    success: function (response) {
                        location.reload();
                    }
                });
            });
        }
    });
}

