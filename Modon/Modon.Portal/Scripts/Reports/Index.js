﻿
function loadBarChart(widgetRecord, model, e) {
    var labels = widgetRecord.YKeysLabelList;
    var xdata = [];
    //var colors = widgetRecord.ColorsList;
    var xkeys = widgetRecord.XKeysList;
    var ykeys = widgetRecord.YKeysList;

    for (var i = 0; i < model.length; i++) {
        var xrecord = {};
        for (var d = 0; d < model[i].length; d++) {
            xrecord[model[i][d].Name] = model[i][d].Value;
        }
        xdata.push(xrecord);
    }

    var chart = Morris.Bar({
        element: 'chartdiv_' + widgetRecord.ID,
        data: xdata,
        xkey: xkeys,
        hideHover: 'auto',
        //barColors: colors,
        ykeys: ykeys,
        labels: labels,
        xLabelAngle: 60,
        resize: true,
        stacked: e,
        grid: true,
        gridTextSize: 15,
        gridTextWeight: 'bold',
    });
   
    chart.options.labels.forEach(function (labels, i) {
        var legendlabel = $('<span style="display: inline-block;font-weight:bold;color:black;padding-left:5px" >' + labels + '</span>')
        var legendItem = $('<div class="mbox"></div>').css('background', chart.options.barColors[i]).append(legendlabel)
        $('#legend_' + widgetRecord.ID).append(legendItem)
    });
};

function loadPieChart(widgetRecord, model) {
    var labels = widgetRecord.YKeysLabelList;
    var xdata = [];
    var colors = widgetRecord.ColorsList;
    var xkeys = widgetRecord.XKeysList;
    var ykeys = widgetRecord.YKeysList;

    for (var i = 0; i < model.length; i++) {

        for (var d = 0; d < model[i].length; d++) {
            for (var k = 0; k < ykeys.length; k++) {
                if (ykeys[k] == model[i][d].Name) {
                    xdata.push(model[i][d].Value);
                }
            }
        }
    }
    var ctx = document.getElementById('pieChart_' +widgetRecord.ID);
    var data = {
        datasets: [{
            data: xdata,
            backgroundColor: colors,
            label: labels // for legend
        }],
        labels: labels
    };

    var pieChart = new Chart(ctx, {
        data: data,
        type: 'pie',
        otpions: {
            legend: false
        }
    });
};

function loadLineChart(widgetRecord, model) {
    var labels = widgetRecord.YKeysLabelList;
    var xdata = [];
    var colors = widgetRecord.ColorsList;
    var xkeys = widgetRecord.XKeysList;
    var ykeys = widgetRecord.YKeysList;

    for (var i = 0; i < model.length; i++) {
        var xrecord = {};
        for (var d = 0; d < model[i].length; d++) {
            xrecord[model[i][d].Name] = model[i][d].Value;
        }
        xdata.push(xrecord);
    }
    var chart = Morris.Line({
        element: 'chartdiv_' + widgetRecord.ID,
        xkey: xkeys,
        ykeys: ykeys,
        labels: labels,
        hideHover: 'auto',
        lineColors: colors,
        xLabelAngle: 60,
        data: xdata,
        resize: true
    });
};

function loadAreaChart(widgetRecord, model) {
    var labels = widgetRecord.YKeysLabelList;
    var xdata = [];
    var colors = widgetRecord.ColorsList;
    var xkeys = widgetRecord.XKeysList;
    var ykeys = widgetRecord.YKeysList;

    for (var i = 0; i < model.length; i++) {
        var xrecord = {};
        for (var d = 0; d < model[i].length; d++) {
            xrecord[model[i][d].Name] = model[i][d].Value;
        }
        xdata.push(xrecord);
    }
    var chart = Morris.Area({
        element: 'chartdiv_' + widgetRecord.ID,
        data: xdata,
        xkey: xkeys,
        ykeys: ykeys,
        lineColors: colors,
        labels: labels,
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
};

function LoadDonutChart(widgetRecord, model) {
    var labels = widgetRecord.YKeysLabelList;
    var xdata = [];
    var colors = widgetRecord.ColorsList;
    var xkeys = widgetRecord.XKeysList;
    var ykeys = widgetRecord.YKeysList;
    for (var i = 0; i < model.length; i++) {
        var xrecord = {};
        xrecord.label = model[i][0].Value;
        xrecord.value = model[i][1].Value;
        xdata.push(xrecord);
    }


    Morris.Donut({
        element: 'chartdiv_' + widgetRecord.ID,
        data: xdata,
        colors: colors,
        formatter: function (y) {
            return y + "%";
        },
        resize: true
    });
};


function GenerateReport(WidgetID) {
    var prms = $('.rptFilterParameter');
    var str = '';

    for (var i = 0; i < prms.length; i++) {
        var values = [];
        var allvalues = [];
        var name = $(prms[i]).attr('name');
        var isallinp = false;
        if ($(prms[i]).is('fieldset')) {
            for (var k = 0; k < prms[i].childElementCount; k++) {
                var ck = $(prms[i]).children().eq(k)[0];
                var inp = ck.getElementsByTagName('input');

                if (inp[0].checked) {
                    values.push(inp[0].value);
                }
                else {
                    allvalues.push(inp[0].value);
                }
            }
        }
        else {
            values.push(prms[i].value);
        }

        if (str != '')
            str += ',';
        if (values.length == 0) {
            str += "{ 'Name':'" + name + "', 'Value':'" + allvalues + "', 'ControlType':'" + prms[i].getAttribute('data-controltype') + "'}";
        } else {
            str += "{ 'Name':'" + name + "', 'Value':'" + values + "', 'ControlType':'" + prms[i].getAttribute('data-controltype') + "'}";
        }

    }
    str = '[' + str + ']';
    SBSAjax(  {
        url: ResolveUrl("~/Reports/DisplayReport"),
        data: {
            widgetID: WidgetID,
            param: str
        },
        success: function (response) {
            $("#filtersControls2").html(response);
        }
    });
};
$(function () {
    $('.button-checkbox').each(function () {
        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {
            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
    $('.default-select2').select2();


});

