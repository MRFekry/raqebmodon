
$(document).ready(function () {
    LoadRankedUsers();
    $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
function LoadRankedUsers() {
    RankedUsersAjax(0, "#life-time");
    RankedUsersAjax(__rankSecondTabDays, "#RankSecondTabDuration");
    RankedUsersAjax(__rankThirdTabDays, "#RankThirdTabDuration");
}

function RankedUsersAjax(days, divID) {
    var levelID = $("[name='LevelID']").val();
    var countryID = $(".rank-country-id").val();
    var gradeID = $("[name='GradeID']").val();
    var subjectID = $("[name='SubjectID']").val();
    var calledCurrentUserRank = false;
    if ($("[name='isProfileDisplay']").prop("checked") == true) {
        calledCurrentUserRank = true;
    }
    var isDetails = true;
    if ($(".more-details-btn")[0]) {
        isDetails = false;
    }
    if ($("[name='isDetails']").prop("checked") == true) {
        isDetails = true;
    }
    else
    {
        isDetails = false;
    }
    SBSAjax(  {
        url: ResolveUrl("~/Home/LoadRankedUsers"),
        data: {
            days: days, levelID: levelID, countryID: countryID
            , gradeID: gradeID, subjectID: subjectID, isDetails: isDetails, calledCurrentUserRank: calledCurrentUserRank
        },
        type: "post",
        success: function (response) {
            $(divID).html(response);
        }
    });
}