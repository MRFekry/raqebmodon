﻿$(document).ready(function () {
    FormSliderSwitcher.init();
    FormEditable.init();
    $('[data-toggle=tooltip]').tooltip();
});
function showSaveInfo() {
    $("#btnsaveinfo").show();

}
function showSaveSetting() {
    $("#btnsavesetting").show();
}
function SubmitProfile() {
    $("#hdfullname").val($('#username').text());
    $("#hdnotes").val($('#jobdesc').text());
    $("#hdmobile").val($('#mobile').text());
    $("#hdemail").val($('#emaill').text());
    $("#hdnickname").val($('#nickname').text());
    $("#hdsecondemail").val($('#secondemail').text());

    $('#formprfile').submit();
}
$(".default-select2").select2({
});
$("#btnsaveinfo").hide();
$("#btnsavesetting").hide();

var getMonthName = function (number) {
    var month = ["1", "2", "3", "4"];

    return month[number];
};

var getDate = function (date) {
    var currentDate = new Date(date);
    var dd = currentDate.getDate();
    var mm = currentDate.getMonth() + 1;
    var yyyy = currentDate.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    currentDate = yyyy + '-' + mm + '-' + dd;

    return currentDate;
};




function handleVisitorsDonutChart(value1, value2, line1, line2,color1,color2) {
    var green = color1;
    var blue = color2;
    Morris.Donut({
        element: 'visitors-donut-chart',
        data: [
            { label: line1, value: value1 },
            { label: line2, value: value2 }
        ],
        colors: [green, blue],
        labelFamily: 'Roboto',
        labelColor: 'rgba(255,255,255,0.5)',
        labelTextSize: '11px',
        backgroundColor: '#000'
    });
}
function handleVisitorsLineChart2(data, line1, line2, color1, color2) {
    var green = color1;
    var greenLight = color1;
    var blue = color2;
    var blueLight = color2;
    var blackTransparent = 'rgba(0,0,0,0.5)';
    var whiteTransparent = 'rgba(255,255,255,0.5)';


    Morris.Bar({
        element: 'visitors-line-chart2',
        data: data,
        xkey: 'x',
        ykeys: ['z'],
        labels: [line2],
        barColors: [blue],
        pointFillColors: [blueLight],
        lineWidth: '4px',
        pointStrokeColors: [whiteTransparent],
        resize: true,
        xLabelAngle: 60,
        hideHover: 'auto',
        gridTextFamily: 'Roboto',
        gridTextColor: whiteTransparent,
        gridTextWeight: 'bold',
        gridTextSize: '15px',
        gridLineColor: 'rgba(255,255,255,0.5)',
    });
}
function handleVisitorsLineChart(data, line1, line2, color1, color2) {
    var green = color1;
    var greenLight = color1;
    var blue = color2;
    var blueLight = color2;
    var blackTransparent = 'rgba(0,0,0,0.5)';
    var whiteTransparent = 'rgba(255,255,255,0.5)';


    Morris.Bar({
        element: 'visitors-line-chart',
        data: data,
        xkey: 'x',
        ykeys: ['y'],
        labels: [line1],
        barColors: [green],
        pointFillColors: [greenLight],
        lineWidth: '4px',
        pointStrokeColors: [whiteTransparent],
        resize: true,
        xLabelAngle: 60,
        hideHover: 'auto',
        gridTextFamily: 'Roboto',
        gridTextColor: whiteTransparent,
        gridTextWeight: 'bold',
        gridTextSize: '15px',
        gridLineColor: 'rgba(255,255,255,0.5)',
    });
}

