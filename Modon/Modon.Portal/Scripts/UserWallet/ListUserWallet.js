﻿

function DeleteUserWalletCheck(DeleteTargetName, DeleteTargetID) {
    var tr = $(event.target).closest('tr');
    CheckModal('هل أنت متأكد من حذف', DeleteTargetName, function (response) {
        SBSAjax({
            url: ResolveUrl("~/UserWallet/DeleteUserWallet"),
            data: {
                walletID: DeleteTargetID
            },
            success: function (response) {
                if (response.toLocaleLowerCase().indexOf('success') > 0)
                    tr.remove();

                $('#deleteModal').modal('hide');
                window.reload();
            },
            error: function () {
                $('#deleteModal').modal('hide');

            }
        });
    });
};


