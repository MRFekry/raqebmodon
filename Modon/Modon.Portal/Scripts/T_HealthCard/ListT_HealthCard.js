


function DeleteT_HealthCardCheck(DeleteTargetName, DeleteTargetID) {
    SBSAjax({
        url: ResolveUrl("~/Home/DeleteCheck"),
        type: "post",
        data: {
            DeleteTarget: DeleteTargetName
        },
        success: function (response) {
            $("#div-delete-check").html(response);
            $("#div-delete-check").show();

            $("#btn-delete-cancel").click(function () {
                $("#div-delete-check").hide();
            });

            $("#btn-delete-ok").click(function () {
                $("#div-delete-check").hide();
                SBSAjax({
                    url: ResolveUrl("~/T_HealthCard/DeleteT_HealthCard"),
                    data: {
                        t_HealthCardID: DeleteTargetID
                    },
                    success: function (response) {
                        location.reload();
                    }
                });
            });
        }
    });
}

