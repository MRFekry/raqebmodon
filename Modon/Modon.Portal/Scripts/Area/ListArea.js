﻿function DeleteUserCheck(DeleteTargetName, DeleteTargetID) {
    var tr = $(event.target).closest('tr');
    CheckModal('هل أنت متأكد من حذف', DeleteTargetName, function (response) {
        SBSAjax({
            url: ResolveUrl("~/Area/DeleteArea"),
            data: {
                userID: DeleteTargetID
            },
            success: function (response) {
                if (response.toLocaleLowerCase().indexOf('success') > 0)
                    tr.remove();

                $('#deleteModal').modal('hide');
                window.reload();
            },
            error: function () {
                $('#deleteModal').modal('hide');

            }
        });
    });
};


function AreasClicked() {
    if ($("#btn-edit").prop("disabled") == true) {

        $("#btn-edit").prop("disabled", false);
        $("#btn-delete").prop("disabled", false);

    }
}

function EditClicked() {
    $('li').each(function (i, obj) {
        if ($(obj).attr("aria-selected") == 'true') {
            location.href = ResolveUrl("~/Area/EditArea?areaID=" + $(obj).attr("id"));
        }
    });
}

function DeleteClicked() {
    $('li').each(function (i, obj) {
        if ($(obj).attr("aria-selected") == 'true') {
            DeleteAreaCheck($(obj).find("a").text(), $(obj).attr("id"));
        }
    });
}

