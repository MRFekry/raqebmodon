﻿__LikeType = 1;
__DisLikeType = 2;
__CommentType = 3;
__FavouriteType = 4;
__BaseRootURL = '';
__BaseURL = '';
__WebSiteRootPath = window.location.pathname.substring(0, window.location.pathname.substring(1).indexOf('/') + 2);//'/Modon.Portal/';
__WebSitePath = __WebSiteRootPath + window.__CurrentApplication;

function ResolveRootUrl(url) {
    try {
        if (__BaseRootURL == '') {
            __BaseRootURL = document.location.origin;
            __BaseRootURL += __WebSiteRootPath;
        }
        return url.replace('~', __BaseRootURL);
    }
    catch (arg) {
        return null;
    }
};
function initTableWithMoreButton() {
    TableManageButtons.init({
        responsive: {
            columnDefs: [{
                visible: false
            }]
        },
    });
};
function getFiltersForExportExcel() {
    var filtersDic = [];
    var filters = $('.filterrow');
    for (var i = 0; i < filters.length; i++) {
        filtersDic.push({
            name: $(filters[i]).find('.filterTitle').first().text(),
            value: $(filters[i]).find('.filterValues').first().text()
        });
    }
    return filtersDic;
};
function CheckModal(msg, target, onSuccess, onCancel) {
    $('#deleteModal span[name=target]').html(target);
    $('#deleteModal span[name=msg]').html(msg);
    $('#deleteModal input[name=btnOk]').unbind("click");
    $('#deleteModal input[name=btnOk]').click(function () {
        if ($.isFunction(onSuccess))
            onSuccess();
    });
    $('#deleteModal input[name=btnCancel]').unbind("click");
    $('#deleteModal input[name=btnCancel]').click(function () {
        if ($.isFunction(onCancel))
            onCancel();
        $('#deleteModal').modal('hide');
    });
    $('#deleteModal').modal('show');
};
function CheckAndConfirm(headmsg,msg, onSuccess) {
    swal({
        title: headmsg,
        text: msg,
        type: "success",
        showCancelButton: true,
        confirmButtonClass: 'btn-success',
    }, function (isConfirm) {
        if (isConfirm)
            {
        if ($.isFunction(onSuccess))
            onSuccess();
        }
    }
       );
};

function ResolveUrl(url) {
    try {
        if (__BaseURL == '') {
            __BaseURL = document.location.origin;
            __BaseURL += __WebSitePath;
        }
        return url.replace('~', __BaseURL);
    }
    catch (arg) {
        return null;
    }
};
showLoader = function (callerObj) {
    $('#page-loader').addClass('show');
};
hideLoader = function (callerObj) {
    $('#page-loader').removeClass('show');
    //$('#page-container').addClass('in');
};

SBSAjax = function (ajaxObject, callerObj) {

    showLoader(callerObj);
    $.ajax({
        url: ResolveUrl(ajaxObject.url),
        type: ajaxObject.type,
        data: ajaxObject.data,
        success: function (response) {
            hideLoader(callerObj);
            if (ajaxObject.success)
                ajaxObject.success(response);
        },
        error: function (response) {
            hideLoader(callerObj);
            if (ajaxObject.error)
                ajaxObject.error(response);
        }
    });
};



function formateDate(date) {
    if (date[0] == "/")
        date = eval(date.substr(1, date.length - 2));
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [month, day, year].join('/');
};

function formateDateTime(date) {
    if (date[0] == "/")
        date = eval('new ' + date.substr(1, date.length - 2)) || Date.now();
    var month = '' + (date.getMonth() + 1),
        day = '' + date.getDate(),
        year = date.getFullYear();

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;

    return [day, month, year].join('/') + "  " + strTime;
};

function imagesUploaded(ctr, targetDivID, descriptionAttribute, placeHolder) {
    if (!ctr.files || !window.FileReader) return;

    var files = ctr.files;
    var filesArr = Array.prototype.slice.call(files);
    $(targetDivID).html('');
    filesArr.forEach(function (f) {
        if (!f.type.match("image.*")) {
            return;
        }

        var reader = new FileReader();
        reader.targetDivID = targetDivID;
        reader.onload = function (e) {
            var html = $(reader.targetDivID).html() + "<div class='previewImageDiv'><div class='previewImage'><a onclick=\"ClearElementDiv(this)\" class='deletepic'><p>x</p></a><img class='img-tag'  src='" + e.target.result + "'><div class='clear'></div><p>" + f.name + "</p></div><div class='clear'></div><textarea class=' wide-textarea textareaNS short-textarea' name='" + descriptionAttribute + "' placeholder='" + placeHolder + "' /></div>";
            $(reader.targetDivID).html(html);
        }
        reader.readAsDataURL(f);
    });

};

function ClearElementDiv(element) {
    $(element).parent().parent().html("");

}
function NotAuthUser() {
    SBSAjax(  {
        url: "~/Landing/RequestForLogin",
        success: function (response) {
            //$("#popup3").html(response);
            showPopup(response);
        }
    });
}
function getQueryString(key) {
    function parseParams() {
        var params = {},
            e,
            a = /\+/g,  // Regex for replacing addition symbol with a space
            r = /([^&=]+)=?([^&]*)/g,
            d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
            q = window.location.search.substring(1);

        while (e = r.exec(q))
            params[d(e[1])] = d(e[2]);

        return params;
    }

    if (!this.queryStringParams)
        this.queryStringParams = parseParams();

    return this.queryStringParams[key];
};

function fbshareCurrentPage(ctr, url, title) {
    return share('fb', ctr, url, title);
}

function tweetCurrentPage(ctr, url, title) {
    return share('t', ctr, url, title);
}

function googleplusbtn(ctr, url, title) {
    return share('gp', ctr, url, title);
}

function linkedinbtn(ctr, url, title) {
    return share('ln', ctr, url, title);
}

function share(type, ctr, url, title) {
    if (url == null || url == undefined || url == '')
        url = window.location.href;
    url = url.toLocaleLowerCase();
    if (url.indexOf('~') == -1 && url.indexOf('http') == -1)
        url = '~/' + url;
    url = ResolveUrl(url);
    url = url.toLocaleLowerCase();
    url = escape(url.replace('localhost/bridereview', 'www.bride-review.com'));
    if (title == null || title == undefined)
        title = document.title;

    if (type == 'fb') {
        window.open("https://www.facebook.com/sharer/sharer.php?u=" + url + "&t=" + title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    } else if (type == 't') {
        window.open("https://twitter.com/share?url=" + url + "&t=" + title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    } else if (type == 'ln') {
        window.open("https://www.linkedin.com/in/share?url=" + url + "&t=" + title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    } else if (type == 'gp') {
        window.open("https://plus.google.com/share?url=" + url + "&t=" + title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    }
    return false;
}

function showPopup(html) {
    $("#popup3").html(html);
    $('#popup3').css('opacity', '1');
    $('#popup3').css('pointer-events', 'auto');
}
function hidePopup() {
    $("#popup3").html('');
    $('#popup3').css('opacity', '0');
    $('#popup3').css('pointer-events', 'none');
}

function closepopup() {
    hidePopup();
}


$(document).ready(function () {
    var images = $('img');
    for (var i = 0; i < images.length; i++) {
        if ($(images[i]).attr('alt'))
            $(images[i]).attr('title', $(images[i]).attr('alt'));
    }

    $(".success-message").delay(3000).fadeOut();
    $(".fail-message").delay(3000).fadeOut();
    if ($('.datepicker').datepicker) {
        $('.datepicker').datepicker({
            singleDatePicker: true,
            calender_style: "picker_3",
            format: window.__DateFormat,
            maxDate: new Date(2013, 1,18),
            dateFormat: window.__DateFormat,
            locale: {
                format: window.__DateFormat,
                dateFormat: window.__DateFormat
            },
        }, function (start, end, label) {
        });
    }

});
function Notify(text, type) {
    $.gritter.add({
        title: text,
        text: type,
        image: '../../../assets/img/logo.png',
        sticky: true,
        time: '',
        class_name: 'my-sticky-class'
    });
};

function unbindForm() {
    if ($('form').valid()) {
        showLoader('show');
        $(window).unbind('beforeunload');
    }
};

$(document).ready(function () {
    var els = $('.validatediscard');
    if (els != null && els.length > 0) {
        $(window).bind('beforeunload', function () {
            if (NotifyDiscardChanges())
                return true;
            return;
        });
        $('form').submit(unbindForm);
    }
});

function NotifyDiscardChanges() {
    var els = $('.validatediscard');
    if (els != null && els.length > 0) {
        for (var i = 0; i < els.length; i++) {
            if ($(els[i]).val() != '')
                return true;
        }
    }
    return false;
};

function showConfirmationDialog(title, msg, onclick_function) {
    $('#modal-dialog-confirmation-messageTitle').text(title);
    $('#modal-dialog-confirmation-messageText').text(msg);
    $('#modal-dialog-confirmation-aConfirm').unbind("click");
    $('#modal-dialog-confirmation-aConfirm').click(function () {
        $('#modal-dialog-confirmation').modal('hide');
        onclick_function();
    });
    $('#modal-dialog-confirmation').modal('show');
}

function correlateQuestinsTime() {
    var els = $('[name=qeditedon]');
    if (els.length > 0) {
        var timeZone = -1 * (new Date().getTimezoneOffset() / 60);
        for (var el in els) {
            el = $(els[el]);
            var d = moment(el.text(), 'DD-MMM-YYYY hh:mm:ss A').add(timeZone, 'h').format('DD-MMM-YYYY hh:mm:ss A');
            el.text(d);
        }
    }
}