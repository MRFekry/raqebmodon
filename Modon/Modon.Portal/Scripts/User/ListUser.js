﻿function DeleteUserCheck(DeleteTargetName, DeleteTargetID) {
    var tr = $(event.target).closest('tr');
    CheckModal('هل أنت متأكد من حذف', DeleteTargetName, function (response) {
        SBSAjax({
            url: ResolveUrl("~/User/DeleteUser"),
            data: {
                userID: DeleteTargetID
            },
            success: function (response) {
                if (response.toLocaleLowerCase().indexOf('success') > 0)
                    tr.remove();

                $('#deleteModal').modal('hide');
                window.reload();
            },
            error: function () {
                $('#deleteModal').modal('hide');

            }
        });
    });
};
function loadDistricts() {
    var areaID = $('#ddlarea option:selected').val();

    if (areaID != "0") {
        SBSAjax({
            url: ResolveUrl("~/User/FillDistrict"),
            type: "Post",
            data: {
                parentID: areaID
            },
            success: function (response) {
                $('#ddlDistrict').empty();
                for (var i = 0; i < response.length; i++) {
                    var selected = (i == 0) ? 'selected' : '';
                    $('#ddlDistrict').append('<option ' + selected + ' value="' + response[i].ID + '">' + response[i].Name + '</option>')
                }
            }
        });
    }
}