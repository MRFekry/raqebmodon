


function DeleteComment_TypeCheck(DeleteTargetName, DeleteTargetID) {
    SBSAjax({
        url: ResolveUrl("~/Home/DeleteCheck"),
        type: "post",
        data: {
            DeleteTarget: DeleteTargetName
        },
        success: function (response) {
            $("#div-delete-check").html(response);
            $("#div-delete-check").show();

            $("#btn-delete-cancel").click(function () {
                $("#div-delete-check").hide();
            });

            $("#btn-delete-ok").click(function () {
                $("#div-delete-check").hide();
                SBSAjax({
                    url: ResolveUrl("~/Comment_Type/DeleteComment_Type"),
                    data: {
                        comment_TypeID: DeleteTargetID
                    },
                    success: function (response) {
                        location.reload();
                    }
                });
            });
        }
    });
}

