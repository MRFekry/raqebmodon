


function DeleteT_WorkerCheck(DeleteTargetName, DeleteTargetID) {
    SBSAjax({
        url: ResolveUrl("~/Home/DeleteCheck"),
        type: "post",
        data: {
            DeleteTarget: DeleteTargetName
        },
        success: function (response) {
            $("#div-delete-check").html(response);
            $("#div-delete-check").show();

            $("#btn-delete-cancel").click(function () {
                $("#div-delete-check").hide();
            });

            $("#btn-delete-ok").click(function () {
                $("#div-delete-check").hide();
                SBSAjax({
                    url: ResolveUrl("~/T_Worker/DeleteT_Worker"),
                    data: {
                        t_WorkerID: DeleteTargetID
                    },
                    success: function (response) {
                        location.reload();
                    }
                });
            });
        }
    });
}

