﻿/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7 & Bootstrap 4.0.0-Alpha 6
Version: 3.0.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v3.0/admin/html/
*/

var handleDataTableButtons = function() {
	"use strict";
    
    if ($('.data-table').length !== 0) {
        $('.data-table').DataTable({
            
            responsive: true,
            "language": {
                "lengthMenu": "عرض _MENU_ بيان لكل صفحة",
                "zeroRecords": "لا يوجد بيانات",
                "info": "عرض صفحة _PAGE_ من _PAGES_",
                "infoEmpty": "لا يوجد بيانات للعرض",
                "search": "بحث",
                "paginate": {
                    "previous": "السابق",
                    "next": "التالى",
                }
            },

            "lengthMenu": [[10, 25, 50,100, -1], [" 10", " 25", " 50","100", "الكل"]]
        });
    }
};

var TableManageButtons = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleDataTableButtons();
        }
    };
}();