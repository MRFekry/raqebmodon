﻿using PMApp.Modon.DAL6;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Modon.Portal.Helpers
{
    public class AuthorizeAdmin : AuthorizeAttribute
    {

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                // The user is not authenticated
                return false;
            }
            var User = httpContext.User;
            if (long.Parse(AuthHelper.GetClaimValue("UserRoleID")) != ConfigManager.GetUIInstance().AdminRoleID)
            {
                return false;
            }
            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "AccessDenied", action = "Index" }));
            }
        }

    }
}