﻿using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web;
using System.Web.Security;

namespace Modon.Portal.Helpers
{
    public class AuthHelper
    {
        public static void ApplyAuthUser()
        {
            long userID = UIHelper.CurrentUserID;
            FormsIdentity formsIdentity = null;

            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                formsIdentity = new FormsIdentity(ticket);

                if (userID == 0)
                {
                    userID = SecurityHelper.GetUserIDFromAuthenticationToken(formsIdentity.Name);
                }
            }

            if (userID != 0)
            {
                var authToken = string.Empty;
                if (formsIdentity != null)
                    authToken = formsIdentity.Name;

                var userService = new UserService();
                var request = new UserRequest()
                {
                    InnerCall = true,
                    AuthToken = authToken,
                    
                    Filter = new UserRequestFilter
                    {
                        UserIDList = new List<long>() { userID },
                    }
                };

                var response = userService.DoUserListInquiry(request);
                UserRecord userObj = null;
                if (response.Success == true && response.UserRecords.Length > 0)
                {
                    userObj = response.UserRecords[0];
                    UIHelper.CurrentUserID = userID;
                }

                if (formsIdentity != null && userObj != null)
                {
                    var claimsIdentity = new ClaimsIdentity(formsIdentity);
                    claimsIdentity.AddClaim(new Claim("UserID", userObj.ID.ToString()));
                    claimsIdentity.AddClaim(new Claim("UserName", userObj.NickName ?? userObj.FullName));
                    claimsIdentity.AddClaim(new Claim("UserEmail", userObj.Email));
                    claimsIdentity.AddClaim(new Claim("UserRoleID", userObj.RoleID.ToString()));
                    claimsIdentity.AddClaim(new Claim("UserRoleName", userObj.RoleName));
                    if (userObj.ProfilePictureUrl != null)
                    {
                        claimsIdentity.AddClaim(new Claim("UserPicture", userObj.ProfilePictureUrl));
                    }
                    else
                    {
                        var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                        var defaultPicUrl = ConfigManager.GetInstance(applicationID).UserDefaultPictureUrl;
                        claimsIdentity.AddClaim(new Claim("UserPicture", UIHelper.ResolveServerUrl(defaultPicUrl)));
                    }
                    ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                    HttpContext.Current.User = claimsPrincipal;
                }
            }
        }

        public static string GetClaimValue(string type)
        {
            var identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            var claim = identity.FindFirst(c => c.Type == type);
            if (claim != null)
                return claim.Value;
            //if (type != null && type.ToLower().EndsWith("id"))
            //    return "0";
            return null;
        }

        //public static string GetIPHelper()
        //{
        // return HttpContext.Current.Request.UserHostAddress;
        //}
    }
}