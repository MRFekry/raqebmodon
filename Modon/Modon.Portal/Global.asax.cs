using Modon.Portal.Helpers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using PMApp.Core.Helpers;
using PMApp.Core.Helpers.MVCBinders;
using PMApp.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CaptchaMvc.Infrastructure;

namespace  Modon.Portal
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // ModelBinders
            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeBinder());
            //ModelBinders.Binders.Add(typeof(Int64), new EncryptDataBinder<Int64>());
            //ModelBinders.Binders.Add(typeof(Int64?), new EncryptDataBinder<Int64?>());
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current != null)
            {
                var url = HttpContext.Current.Request.Url.ToString().ToLower();
                if (!url.Contains("sbsservice.svc"))
                {
                    UIHelper.CheckLoggedUser();
                    AuthHelper.ApplyAuthUser();
                    //AuthHelper.AuthenticateUrl(Request, Response); 
                }
            }
            SessionHelper.ClearPerRequest();
            UIHelper.SetThreadDateTimeFormat(DateTimeHelper.dateFormatedd_MM_yyyy, DateTimeHelper.dateFormatedd_MM_yyyy);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            if (applicationID != 0)
            {
                var exception = Server.GetLastError();
                var errorID = ExceptionLogger.GetInstance(applicationID).Log(exception);
                var cryptoEx = exception as CryptographicException;
                if (cryptoEx != null)
                {
                    Server.ClearError();
                }
            }
        }
    }
}
