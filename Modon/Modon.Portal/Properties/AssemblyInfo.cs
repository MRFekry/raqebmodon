using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Modon.Portal")]
[assembly: AssemblyProduct("Modon.Portal")]
[assembly: AssemblyDescription("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyConfiguration("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyCompany("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyCopyright("SBS © 2016")]
[assembly: AssemblyTrademark("http://www.sbs16.com")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2a95a1ab-d464-4d22-9c62-3f34b21f6b61")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
