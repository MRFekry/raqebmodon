﻿using Modon.Portal.Controllers;
using System.Web.Mvc;

namespace LiveRep.Portal.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        //
        // GET: /Dashboard/
        public ActionResult Welcome()
        {

            return View();
        }

        

        public ActionResult Index()
        {
            return View();
        }
    }
}
