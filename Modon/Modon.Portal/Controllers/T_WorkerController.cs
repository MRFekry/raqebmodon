using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class T_WorkerController : Controller
    {

        public ActionResult ListT_Worker(long buildingID=0)
        {
            var t_WorkerService = new T_WorkerService();
            var t_WorkerRequest = RequestBuilder.CreateRequest<T_WorkerRequest>(User.Identity.Name);
            if (buildingID != 0)
                t_WorkerRequest.Filter = new T_WorkerRequestFilter { BuildingIDList = new List<long?> { buildingID } };
            var t_WorkerResponse = t_WorkerService.DoT_WorkerList(t_WorkerRequest);
            return PartialView(t_WorkerResponse.T_WorkerRecords.Where(p => p.CreatedBy == t_WorkerRequest.UserID).ToArray());
        }

        public ActionResult DeleteT_Worker(long t_WorkerID)
        {
            var t_WorkerService = new T_WorkerService();
            var t_WorkerRequest = RequestBuilder.CreateRequest<T_WorkerRequest>(User.Identity.Name);
            t_WorkerRequest.T_WorkerRecord = new T_WorkerRecord
            {
                ID = t_WorkerID
            };
            var t_WorkerResponse = t_WorkerService.DoT_WorkerDelete(t_WorkerRequest);
            if (t_WorkerResponse.Success.HasValue && t_WorkerResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("T_WorkerDeletedSuccessfuly"));
            else
                SessionHelper.Set("FailMessage", t_WorkerResponse.Message);
            return null;
        }

        public ActionResult AddT_Worker()
        {
            FillAddT_WorkerFields();
            return View();
        }

        [HttpPost]
        public ActionResult AddT_Worker(T_WorkerRecord model)
        {
            var t_WorkerService = new T_WorkerService();
            var t_WorkerRequest = RequestBuilder.CreateRequest<T_WorkerRequest>(User.Identity.Name);
            t_WorkerRequest.T_WorkerRecord = model;
            var t_WorkerResponse = t_WorkerService.DoT_WorkerInsert(t_WorkerRequest);
            if (t_WorkerResponse.Success.HasValue && t_WorkerResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("T_WorkerAddedSuccessfuly"));

                return RedirectToAction("ListT_Worker");
            }
            SessionHelper.Set("FailMessage", t_WorkerResponse.Message);
            FillAddT_WorkerFields();
            return View();
        }


        public ActionResult EditT_Worker(long t_WorkerID)
        {
            var t_WorkerService = new T_WorkerService();
            var t_WorkerRequest = RequestBuilder.CreateRequest<T_WorkerRequest>(User.Identity.Name);
            t_WorkerRequest.Filter = new T_WorkerRequestFilter
            {
                IDList = new List<long>() { t_WorkerID }
            };
            var t_WorkerResponse = t_WorkerService.DoT_WorkerList(t_WorkerRequest);
            var record = t_WorkerResponse.T_WorkerRecords[0];
            FillAddT_WorkerFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditT_Worker(T_WorkerRecord model)
        {
            var t_WorkerService = new T_WorkerService();
            var t_WorkerRequest = RequestBuilder.CreateRequest<T_WorkerRequest>(User.Identity.Name);
            t_WorkerRequest.T_WorkerRecord = model;
            var t_WorkerResponse = t_WorkerService.DoT_WorkerUpdate(t_WorkerRequest);
            if (t_WorkerResponse.Success.HasValue && t_WorkerResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("T_WorkerEditedSuccessfuly"));

                return RedirectToAction("ListT_Worker");
            }
            SessionHelper.Set("FailMessage", t_WorkerResponse.Message);

            FillAddT_WorkerFields(model);
            return View(model);
        }

        private void FillAddT_WorkerFields(T_WorkerRecord t_WorkerRecord = null)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest, BuildingRequestFilter>(User.Identity.Name,
                new BuildingRequestFilter
                {
                    //ShowOnlyMyArea = true
                });
            var buildingResponse = buildingService.DoBuildingList(buildingRequest);
            var buildingList = buildingResponse.BuildingRecords.Where(p => p.CreatedBy == buildingRequest.UserID && p.StatusName == "Approved").ToArray();

            ViewBag.buildingList = buildingList;



        }
    }
}