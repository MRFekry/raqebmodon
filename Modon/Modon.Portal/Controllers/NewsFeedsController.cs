using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using Modon.Services;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.Helpers;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Web.Mvc;


namespace Modon.Portal.Controllers
{
    [Authorize]
    public class NewsFeedsController : BaseController
    {

        public ActionResult ListNewsFeeds()
        {
            var newsFeedsService = new NewsFeedsService();
            var newsFeedsRequest = RequestBuilder.CreateRequest<NewsFeedsRequest>(User.Identity.Name);
            var newsFeedsResponse = newsFeedsService.DoNewsFeedsList(newsFeedsRequest);
            return View(newsFeedsResponse.NewsFeedsRecords);
        }

        public ActionResult DeleteNewsFeeds(long newsFeedsID)
        {
            var newsFeedsService = new NewsFeedsService();
            var newsFeedsRequest = RequestBuilder.CreateRequest<NewsFeedsRequest>(User.Identity.Name);
            newsFeedsRequest.NewsFeedRecord = new NewsFeedsRecord
                {
                    ID = newsFeedsID
                };
            var newsFeedsResponse = newsFeedsService.DoNewsFeedsDelete(newsFeedsRequest);
            if (newsFeedsResponse.Success.HasValue && newsFeedsResponse.Success.Value)
                SessionHelper.SetSuccessMessage( ResourceManager.UIGetResource("NewsFeedsDeletedSuccessfuly"));
            else
                SessionHelper.SetFailMessage( newsFeedsResponse.Message);
            return null;
        }

        public ActionResult AddNewsFeeds()
        {
            FillAddNewsFeedsFields();
            return View();
        }

        [HttpPost]
        public ActionResult AddNewsFeeds(NewsFeedsRecord model)
        {
            var newsFeedsService = new NewsFeedsService();
            var newsFeedsRequest = RequestBuilder.CreateRequest<NewsFeedsRequest>(User.Identity.Name);
            newsFeedsRequest.NewsFeedRecord = model;
            var newsFeedsResponse = newsFeedsService.DoNewsFeedsInsert(newsFeedsRequest);
            if (newsFeedsResponse.Success.HasValue && newsFeedsResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage( ResourceManager.UIGetResource("NewsFeedsAddedSuccessfuly"));

                return RedirectToAction("ListNewsFeeds");
            }
            SessionHelper.SetFailMessage( newsFeedsResponse.Message);
            FillAddNewsFeedsFields();
            return View();
        }


        public ActionResult EditNewsFeeds(long newsFeedsID)
        {

            var newsFeedsService = new NewsFeedsService();
            var newsFeedsRequest = RequestBuilder.CreateRequest<NewsFeedsRequest>(User.Identity.Name);
            newsFeedsRequest.Filter = new NewsFeedsRequestFilter
                {
                    IDList = new List<long>() { newsFeedsID }
                };
            var newsFeedsResponse = newsFeedsService.DoNewsFeedsList(newsFeedsRequest);
            var record = newsFeedsResponse.NewsFeedsRecords[0];
            FillAddNewsFeedsFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditNewsFeeds(NewsFeedsRecord model)
        {
            var newsFeedsService = new NewsFeedsService();
            var newsFeedsRequest = RequestBuilder.CreateRequest<NewsFeedsRequest>(User.Identity.Name);
            newsFeedsRequest.NewsFeedRecord = model;
            var newsFeedsResponse = newsFeedsService.DoNewsFeedsUpdate(newsFeedsRequest);
            if (newsFeedsResponse.Success.HasValue && newsFeedsResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage( ResourceManager.UIGetResource("NewsFeedsEditedSuccessfuly"));

                return RedirectToAction("ListNewsFeeds");
            }
            SessionHelper.SetFailMessage( newsFeedsResponse.Message);

            FillAddNewsFeedsFields(model);
            return View(model);
        }

        private void FillAddNewsFeedsFields(NewsFeedsRecord newsFeedsRecord = null)
        {

            var common_UserService = new UserService();


            var common_UserRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            var common_UserResponse = common_UserService.DoUserListInquiry(common_UserRequest);
            ViewBag.common_User = common_UserResponse.UserRecords;

        }
    }
}