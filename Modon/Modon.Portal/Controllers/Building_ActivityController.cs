using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class Building_ActivityController : Controller
    {

        public ActionResult ListBuilding_Activity()
        {
            var building_ActivityService = new Building_ActivityService();
            var building_ActivityRequest = RequestBuilder.CreateRequest<Building_ActivityRequest>(User.Identity.Name);
            var building_ActivityResponse = building_ActivityService.DoBuilding_ActivityList(building_ActivityRequest);
            return View(building_ActivityResponse.Building_ActivityRecords);
        }

           public ActionResult DeleteBuilding_Activity(long building_ActivityID)
        {
            var building_ActivityService = new Building_ActivityService();
            var building_ActivityRequest = RequestBuilder.CreateRequest<Building_ActivityRequest>(User.Identity.Name);
             building_ActivityRequest.Building_ActivityRecord = new Building_ActivityRecord
            {
                ID = building_ActivityID
            };
            var building_ActivityResponse = building_ActivityService.DoBuilding_ActivityDelete(building_ActivityRequest);
            if (building_ActivityResponse.Success.HasValue && building_ActivityResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Building_ActivityDeletedSuccessfuly"));
            else
                 SessionHelper.Set("FailMessage", building_ActivityResponse.Message);
            return null;
        }

          public ActionResult AddBuilding_Activity()
        {
            FillAddBuilding_ActivityFields();
            return View();
        }

            [HttpPost]
        public ActionResult AddBuilding_Activity(Building_ActivityRecord model)
        {
            var building_ActivityService = new Building_ActivityService();
            var building_ActivityRequest = RequestBuilder.CreateRequest<Building_ActivityRequest>(User.Identity.Name);
            building_ActivityRequest.Building_ActivityRecord = model;
            var building_ActivityResponse = building_ActivityService.DoBuilding_ActivityInsert(building_ActivityRequest);
            if (building_ActivityResponse.Success.HasValue && building_ActivityResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Building_ActivityAddedSuccessfuly"));

                return RedirectToAction("ListBuilding_Activity");
            }
            SessionHelper.Set("FailMessage", building_ActivityResponse.Message);
            FillAddBuilding_ActivityFields();
            return View();
        }

        
        public ActionResult EditBuilding_Activity(long building_ActivityID)
        {
            var building_ActivityService = new Building_ActivityService();
            var building_ActivityRequest = RequestBuilder.CreateRequest<Building_ActivityRequest>(User.Identity.Name);
             building_ActivityRequest.Filter = new Building_ActivityRequestFilter
            {
                IDList = new List<long>() { building_ActivityID }
            };
            var building_ActivityResponse = building_ActivityService.DoBuilding_ActivityList(building_ActivityRequest);
            var record = building_ActivityResponse.Building_ActivityRecords[0];
            FillAddBuilding_ActivityFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditBuilding_Activity(Building_ActivityRecord model)
        {
            var building_ActivityService = new Building_ActivityService();
            var building_ActivityRequest = RequestBuilder.CreateRequest<Building_ActivityRequest>(User.Identity.Name);
             building_ActivityRequest.Building_ActivityRecord = model;
            var building_ActivityResponse = building_ActivityService.DoBuilding_ActivityUpdate(building_ActivityRequest);
            if (building_ActivityResponse.Success.HasValue && building_ActivityResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Building_ActivityEditedSuccessfuly"));

                return RedirectToAction("ListBuilding_Activity");
            }
            SessionHelper.Set("FailMessage", building_ActivityResponse.Message);

            FillAddBuilding_ActivityFields(model);
            return View(model);
        }

           private void FillAddBuilding_ActivityFields(Building_ActivityRecord building_ActivityRecord = null)
        {
            

            
        }
    }
}