using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6.EntitiesExtention;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using Modon.Services.Managers;


namespace  Modon.Portal.Controllers
{
    [AuthorizeAdmin]
    public class ResourcesController : Controller
    {

        public ActionResult ListResources()
        {
            var ResourceService = new LookupService();
            var ResourceRequest = RequestBuilder.CreateRequest<ResourceRequest>(User.Identity.Name);
            var ResourceResponse = ResourceService.DoResourceList(ResourceRequest);
            return View(ResourceResponse.ResourceRecords);
        }

        public ActionResult DeleteResource(long resourcesID)
        {
            var ResourceService = new LookupService();
            var ResourceRequest = RequestBuilder.CreateRequest<ResourceRequest>(User.Identity.Name);
            ResourceRequest.ResourceRecord = new ResourceRecord
            {
                ID = resourcesID
            };
            var ResourceResponse = ResourceService.DoResourceDelete(ResourceRequest);
            if (ResourceResponse.Success.HasValue && ResourceResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ResourceDeletedSuccessfuly"));
            else
                SessionHelper.Set("FailMessage", ResourceResponse.Message);
            return null;
        }

        public ActionResult AddResources()
        {
            FillAddResourceFields();
            return View();
        }

        [HttpPost]
        public ActionResult AddResources(ResourceRecord model)
        {
            var ResourceService = new LookupService();
            var ResourceRequest = RequestBuilder.CreateRequest<ResourceRequest>(User.Identity.Name);
            ResourceRequest.ResourceRecord = model;
            var ResourceResponse = ResourceService.DoResourceInsert(ResourceRequest);
            if (ResourceResponse.Success.HasValue && ResourceResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.UIGetResource("ResourceAddedSuccessfuly"));

                return RedirectToAction("ListResources");
            }
            var msg = ResourceResponse.Message;
            if (msg.ToLower().Contains("uniq"))
                msg = ResourceManager.UIGetResource("ThisCodeAlreadyExist");

            SessionHelper.SetFailMessage(msg);
            FillAddResourceFields();
            return View(model);
        }


        public ActionResult EditResources(long resourcesID)
        {
            var ResourceService = new LookupService();
            var ResourceRequest = RequestBuilder.CreateRequest<ResourceRequest>(User.Identity.Name);
            ResourceRequest.Filter = new ResourceRequestFilter
            {
               IDList  =PMExtentions.ItemsToList(resourcesID)
            };
            var ResourceResponse = ResourceService.DoResourceList(ResourceRequest);
            var record = ResourceResponse.ResourceRecords[0];
            FillAddResourceFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditResources(ResourceRecord model)
        {
            var ResourceService = new LookupService();
            var ResourceRequest = RequestBuilder.CreateRequest<ResourceRequest>(User.Identity.Name);
            ResourceRequest.ResourceRecord = model;
            var ResourceResponse = ResourceService.DoResourceUpdate(ResourceRequest);
            if (ResourceResponse.Success.HasValue && ResourceResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.UIGetResource("ResourceEditedSuccessfuly"));
                ResourceManager.GetUIInstance().ClearResourcesCache();
                return RedirectToAction("ListResources");
            }
            var msg = ResourceResponse.Message;
            if (msg.ToLower().Contains("uniq"))
                msg = ResourceManager.UIGetResource("ThisCodeAlreadyExist");

            SessionHelper.SetFailMessage(msg);

            FillAddResourceFields(model);
            return View(model);
        }

        private void FillAddResourceFields(ResourceRecord ResourceRecord = null)
        {

        }
    }
}