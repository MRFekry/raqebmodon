﻿using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using Modon.Services;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Web.Mvc;

namespace LiveRep.Portal.Controllers
{
    [Authorize]
    public class AreaController : Controller
    {
        //
        // GET: /AdminArea/

        public ActionResult ListArea()
        {
            var userID = SecurityHelper.GetUserIDFromAuthenticationToken(User.Identity.Name);
            ViewBag.userID = userID;
            var userEXService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(User.Identity.Name);
            var areaResponse = userEXService.DoAreaList(areaRequest);
            //ViewBag.Areas = areaResponse.Records;
            var areaList = new List<object>();
            var areaRecs = areaResponse.AreaRecords;
            for (int i = 0; i < areaRecs.Length; i++)
            {
                var isRootArea = (areaRecs[i].ParentID == null);
                areaList.Add(new { id = areaRecs[i].ID.ToString(), parent = isRootArea ? "#" : areaRecs[i].ParentID.ToString(), text = areaRecs[i].Name });
            }
            ViewBag.Areas = Json(areaList);

            return View();
        }

        public ActionResult AddArea()
        {
            FillAddAreaFields();
            return View();
        }

        [HttpPost]
        public ActionResult AddArea(AreaRecord model)
        {
            var userExService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(User.Identity.Name);
            areaRequest.AreaRecord = model;
            var areaResponse = userExService.DoAreaInsert(areaRequest);
            if (areaResponse.Success.HasValue && areaResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.GetInstance(areaRequest.ApplicationID).GetResource("AreaAddedSuccessfuly"));

                return RedirectToAction("ListArea");
            }
            SessionHelper.SetFailMessage(areaResponse.Message);
            FillAddAreaFields();
            return RedirectToAction("ListArea");
        }


        public ActionResult EditArea(long areaID)
        {

            var userExService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(User.Identity.Name);
            areaRequest.Filter = new AreaRequestFilter
            {
                IDList = new List<long>() { areaID }
            };

            var areaResponse = userExService.DoAreaList(areaRequest);
            var record = areaResponse.AreaRecords[0];
            FillAddAreaFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditArea(AreaRecord model)
        {
            var userExService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(User.Identity.Name);
            areaRequest.AreaRecord = model;
            var areaResponse = userExService.DoAreaUpdate(areaRequest);
            if (areaResponse.Success.HasValue && areaResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.GetInstance(areaRequest.ApplicationID).GetResource("AreaEditedSuccessfuly"));

                return RedirectToAction("ListArea");
            }
            else
            SessionHelper.SetFailMessage(areaResponse.Message);

            FillAddAreaFields(model);
            return RedirectToAction("ListArea");
        }

        public string DeleteArea(long areaID)
        {
            var userExService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(User.Identity.Name);
            areaRequest.AreaRecord = new AreaRecord
            {
                ID = areaID
            };
            var areaResponse = userExService.DoAreaDelete(areaRequest);

            return areaResponse.Message;
        }

        private void FillAddAreaFields(AreaRecord areaRecord = null)
        {
            var userExService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(User.Identity.Name);
            var areaResponse = userExService.DoAreaList(areaRequest);
            ViewBag.area = areaResponse.AreaRecords;
        }

    }
}
