using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using Modon.Services;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class ActivityTypeController : Controller
    {

        public ActionResult ListActivityType()
        {
            var activityTypeService = new ActivityTypeService();
            var activityTypeRequest = RequestBuilder.CreateRequest<ActivityTypeRequest>(User.Identity.Name);
            var activityTypeResponse = activityTypeService.DoActivityTypeList(activityTypeRequest);
            return View(activityTypeResponse.ActivityTypeRecords);
        }

           public ActionResult DeleteActivityType(long activityTypeID)
        {
            var activityTypeService = new ActivityTypeService();
            var activityTypeRequest = RequestBuilder.CreateRequest<ActivityTypeRequest>(User.Identity.Name);
            activityTypeRequest.ActivityTypeRecord = new ActivityTypeRecord
            {
                ID = activityTypeID
            };
            var activityTypeResponse = activityTypeService.DoActivityTypeDelete(activityTypeRequest);
            if (activityTypeResponse.Success.HasValue && activityTypeResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ActivityTypeDeletedSuccessfuly"));
            else
                 SessionHelper.Set("FailMessage", activityTypeResponse.Message);
            return null;
        }

          public ActionResult AddActivityType()
        {
            FillAddActivityTypeFields();
            return View();
        }

            [HttpPost]
        public ActionResult AddActivityType(ActivityTypeRecord model)
        {
            var activityTypeService = new ActivityTypeService();
            var activityTypeRequest = RequestBuilder.CreateRequest<ActivityTypeRequest>(User.Identity.Name);
            activityTypeRequest.ActivityTypeRecord = model;
            var activityTypeResponse = activityTypeService.DoActivityTypeInsert(activityTypeRequest);
            if (activityTypeResponse.Success.HasValue && activityTypeResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ActivityTypeAddedSuccessfuly"));

                return RedirectToAction("ListActivityType");
            }
            SessionHelper.Set("FailMessage", activityTypeResponse.Message);
            FillAddActivityTypeFields();
            return View();
        }

        
        public ActionResult EditActivityType(long activityTypeID)
        {
            var activityTypeService = new ActivityTypeService();
            var activityTypeRequest = RequestBuilder.CreateRequest<ActivityTypeRequest>(User.Identity.Name);
             activityTypeRequest.Filter = new ActivityTypeRequestFilter
            {
                IDList = new List<long>() { activityTypeID }
            };
            var activityTypeResponse = activityTypeService.DoActivityTypeList(activityTypeRequest);
            var record = activityTypeResponse.ActivityTypeRecords[0];
            FillAddActivityTypeFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditActivityType(ActivityTypeRecord model)
        {
            var activityTypeService = new ActivityTypeService();
            var activityTypeRequest = RequestBuilder.CreateRequest<ActivityTypeRequest>(User.Identity.Name);
             activityTypeRequest.ActivityTypeRecord = model;
            var activityTypeResponse = activityTypeService.DoActivityTypeUpdate(activityTypeRequest);
            if (activityTypeResponse.Success.HasValue && activityTypeResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ActivityTypeEditedSuccessfuly"));

                return RedirectToAction("ListActivityType");
            }
            SessionHelper.Set("FailMessage", activityTypeResponse.Message);

            FillAddActivityTypeFields(model);
            return View(model);
        }

           private void FillAddActivityTypeFields(ActivityTypeRecord activityTypeRecord = null)
        {
            

            
        }
    }
}