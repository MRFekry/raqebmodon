using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class Order_CommentController : Controller
    {

        public ActionResult ListOrder_Comment()
        {
            var order_CommentService = new Order_CommentService();
            var order_CommentRequest = RequestBuilder.CreateRequest<Order_CommentRequest>(User.Identity.Name);
            var order_CommentResponse = order_CommentService.DoOrder_CommentList(order_CommentRequest);
            return View(order_CommentResponse.Order_CommentRecords);
        }

           public ActionResult DeleteOrder_Comment(long order_CommentID)
        {
            var order_CommentService = new Order_CommentService();
            var order_CommentRequest = RequestBuilder.CreateRequest<Order_CommentRequest>(User.Identity.Name);
             order_CommentRequest.Order_CommentRecord = new Order_CommentRecord
            {
                ID = order_CommentID
            };
            var order_CommentResponse = order_CommentService.DoOrder_CommentDelete(order_CommentRequest);
            if (order_CommentResponse.Success.HasValue && order_CommentResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Order_CommentDeletedSuccessfuly"));
            else
                 SessionHelper.Set("FailMessage", order_CommentResponse.Message);
            return null;
        }

      

          public void AddOrder_Comment(long orderID,string comment)
        {
            var order_CommentService = new Order_CommentService();
            var order_CommentRequest = RequestBuilder.CreateRequest<Order_CommentRequest>(User.Identity.Name);
            order_CommentRequest.Order_CommentRecord = new Order_CommentRecord();
            order_CommentRequest.Order_CommentRecord.OrderID = orderID;
            order_CommentRequest.Order_CommentRecord.Comment = comment;
            var order_CommentResponse = order_CommentService.DoOrder_CommentInsert(order_CommentRequest);
        }

        
        public ActionResult EditOrder_Comment(long order_CommentID)
        {
            var order_CommentService = new Order_CommentService();
            var order_CommentRequest = RequestBuilder.CreateRequest<Order_CommentRequest>(User.Identity.Name);
             order_CommentRequest.Filter = new Order_CommentRequestFilter
            {
                IDList = new List<long>() { order_CommentID }
            };
            var order_CommentResponse = order_CommentService.DoOrder_CommentList(order_CommentRequest);
            var record = order_CommentResponse.Order_CommentRecords[0];
            FillAddOrder_CommentFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditOrder_Comment(Order_CommentRecord model)
        {
            var order_CommentService = new Order_CommentService();
            var order_CommentRequest = RequestBuilder.CreateRequest<Order_CommentRequest>(User.Identity.Name);
             order_CommentRequest.Order_CommentRecord = model;
            var order_CommentResponse = order_CommentService.DoOrder_CommentUpdate(order_CommentRequest);
            if (order_CommentResponse.Success.HasValue && order_CommentResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Order_CommentEditedSuccessfuly"));

                return RedirectToAction("ListOrder_Comment");
            }
            SessionHelper.Set("FailMessage", order_CommentResponse.Message);

            FillAddOrder_CommentFields(model);
            return View(model);
        }

           private void FillAddOrder_CommentFields(Order_CommentRecord order_CommentRecord = null)
        {
            

            
        }
    }
}