using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class ModonOrderController : Controller
    {

        public ActionResult ListModonOrder(long buildingID = 0, bool isPatrial = false)
        {
            ViewBag.isPatrial = isPatrial;
            var modonOrderService = new ModonOrderService();
            var modonOrderRequest = RequestBuilder.CreateRequest<ModonOrderRequest>(User.Identity.Name);
            if (buildingID > 0)
            {
                modonOrderRequest.Filter = new ModonOrderRequestFilter();
                modonOrderRequest.Filter.BuildingIDList = new List<long> { buildingID };
            }
            var modonOrderResponse = modonOrderService.DoModonOrderList(modonOrderRequest);
            if (isPatrial)
                return PartialView(modonOrderResponse.ModonOrderRecords);
            else
                return View(modonOrderResponse.ModonOrderRecords);
        }
        public ActionResult EListModonOrder(long buildingID = 0, bool isPatrial = false)
        {
            ViewBag.isPatrial = isPatrial;
            var modonOrderService = new ModonOrderService();
            var modonOrderRequest = RequestBuilder.CreateRequest<ModonOrderRequest>(User.Identity.Name);
            if (buildingID > 0)
            {
                modonOrderRequest.Filter = new ModonOrderRequestFilter();
                modonOrderRequest.Filter.BuildingIDList = new List<long> { buildingID };
            }
            var modonOrderResponse = modonOrderService.DoModonOrderList(modonOrderRequest);
            if (isPatrial)
                return PartialView(modonOrderResponse.ModonOrderRecords);
            else
                return View(modonOrderResponse.ModonOrderRecords);
        }
        public PartialViewResult EPartialModonOrder(ModonOrderRecord[] model)
        {
            return PartialView(model);
        }
        public PartialViewResult PartialModonOrder(ModonOrderRecord[] model)
        {
            return PartialView(model);
        }

        public ActionResult DeleteModonOrder(long modonOrderID)
        {
            var modonOrderService = new ModonOrderService();
            var modonOrderRequest = RequestBuilder.CreateRequest<ModonOrderRequest>(User.Identity.Name);
            modonOrderRequest.ModonOrderRecord = new ModonOrderRecord
           {
               ID = modonOrderID
           };
            var modonOrderResponse = modonOrderService.DoModonOrderDelete(modonOrderRequest);
            if (modonOrderResponse.Success.HasValue && modonOrderResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ModonOrderDeletedSuccessfuly"));
            else
                SessionHelper.Set("FailMessage", modonOrderResponse.Message);
            return null;
        }

        public ActionResult AddModonOrder()
        {
            FillAddModonOrderFields();
            return View();
        }

        [HttpPost]
        public ActionResult AddModonOrder(ModonOrderRecord model)
        {
            var modonOrderService = new ModonOrderService();
            var modonOrderRequest = RequestBuilder.CreateRequest<ModonOrderRequest>(User.Identity.Name);
            modonOrderRequest.ModonOrderRecord = model;
            var modonOrderResponse = modonOrderService.DoModonOrderInsert(modonOrderRequest);
            if (modonOrderResponse.Success.HasValue && modonOrderResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ModonOrderAddedSuccessfuly"));

                return RedirectToAction("ListModonOrder");
            }
            SessionHelper.Set("FailMessage", modonOrderResponse.Message);
            FillAddModonOrderFields();
            return View();
        }


        public ActionResult EditModonOrder(long modonOrderID)
        {
            var modonOrderService = new ModonOrderService();
            var modonOrderRequest = RequestBuilder.CreateRequest<ModonOrderRequest>(User.Identity.Name);
            modonOrderRequest.Filter = new ModonOrderRequestFilter
           {
               IDList = new List<long>() { modonOrderID }
           };
            var modonOrderResponse = modonOrderService.DoModonOrderList(modonOrderRequest);
            var record = modonOrderResponse.ModonOrderRecords[0];
            FillAddModonOrderFields(record);
            return View(record);
        }

        [HttpPost]
        public ActionResult EditModonOrder(ModonOrderRecord model)
        {
            var modonOrderService = new ModonOrderService();
            var modonOrderRequest = RequestBuilder.CreateRequest<ModonOrderRequest>(User.Identity.Name);
            modonOrderRequest.ModonOrderRecord = model;
            var modonOrderResponse = modonOrderService.DoModonOrderUpdate(modonOrderRequest);
            if (modonOrderResponse.Success.HasValue && modonOrderResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ModonOrderEditedSuccessfuly"));

                return RedirectToAction("ListModonOrder");
            }
            SessionHelper.Set("FailMessage", modonOrderResponse.Message);

            FillAddModonOrderFields(model);
            return View(model);
        }

        private void FillAddModonOrderFields(ModonOrderRecord modonOrderRecord = null)
        {



        }

        public ActionResult BuildingLicence(long buildingID)
        {
            var record = GetBuildingDetails(buildingID);
            return View(record);
        }
        private BuildingRecord GetBuildingDetails(long buildingID)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.Filter = new BuildingRequestFilter
            {
                IDList = new List<long>() { buildingID }
            };
            var buildingResponse = buildingService.DoBuildingList(buildingRequest);
            return buildingResponse.BuildingRecords[0];
        }


    }
}