﻿using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6.EntitiesExtention;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using Modon.Services.Managers;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class T_HealthCardController : Controller
    {

        public ActionResult ListT_HealthCard()
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            return View(t_HealthCardResponse.T_HealthCardRecords.Where(p => p.CreatedBy == t_HealthCardRequest.UserID).ToArray());
        }

        public ActionResult IPartialHCList()
        {
            return PartialView();
        }

        public ActionResult EdListT_HealthCard()
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            return View(t_HealthCardResponse.T_HealthCardRecords.Where(p => p.HC_St_Id == 1).ToArray());
        }


        public ActionResult DetialsT_HealthCard(long HCID)
        {

            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            var t_HealthCardRecord = t_HealthCardResponse.T_HealthCardRecords.Where(p => p.ID == HCID).ToArray();


            var HostpitalsService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest, UserRequestFilter>(User.Identity.Name,
                new UserRequestFilter
                {
                    
                });
            var userResponse = HostpitalsService.DoUserListInquiry(userRequest);
            var HostpitalName = userResponse.UserRecords.Where(p => p.ID == t_HealthCardRecord[0].HRep_HospId).ToArray()[0].FullName;

            ViewBag.HostpitalName = HostpitalName;
             
            return View(t_HealthCardRecord[0]);
        }

        public ActionResult DeleteT_HealthCard(long t_HealthCardID)
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            t_HealthCardRequest.T_HealthCardRecord = new T_HealthCardRecord
            {
                ID = t_HealthCardID
            };
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardDelete(t_HealthCardRequest);
            if (t_HealthCardResponse.Success.HasValue && t_HealthCardResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("T_HealthCardDeletedSuccessfuly"));
            else
                SessionHelper.Set("FailMessage", t_HealthCardResponse.Message);
            return null;
        }

        public ActionResult AddT_HealthCard(long workerID)
        {
            FillAddT_HealthCardFields(workerID);
            return View();
        }


        public string ChangeHCStatus(long hcID, long statusID)
        {
            string msg = "";
            var HCService = new T_HealthCardService();
            var HCRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            HCRequest.T_HealthCardRecord = new T_HealthCardRecord();
            HCRequest.T_HealthCardRecord.HC_St_Id = statusID;
            HCRequest.T_HealthCardRecord.ID = hcID;

            
            if (statusID == 3)
            {

                var hcResponse = HCService.DoT_HealthCardList(HCRequest);
                var hc_no = hcResponse.T_HealthCardRecords.Select(p => long.Parse(p.HC_NO)).DefaultIfEmpty(0).Max() + 1 ;
                HCRequest.T_HealthCardRecord.HC_NO = hc_no.ToString();
                HCRequest.T_HealthCardRecord.HC_AceptedBy = HCRequest.UserID;
                HCRequest.T_HealthCardRecord.HC_AceptDate = System.DateTime.Now;
                HCRequest.T_HealthCardRecord.HC_StratDate = System.DateTime.Now;
                HCRequest.T_HealthCardRecord.HC_EndDate = System.DateTime.Now.AddYears(1);
            }
            else if (statusID == 5)
            {
                HCRequest.T_HealthCardRecord.HC_StopById = HCRequest.UserID;
                HCRequest.T_HealthCardRecord.HC_StopDate = System.DateTime.Now;
                
            }
            var HCResponse = HCService.DoT_HealthCardUpdate(HCRequest);
            if (HCResponse.Success.HasValue && HCResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(msg);
            }
            else
            {
                SessionHelper.SetFailMessage(HCResponse.Message);
                msg = HCResponse.Message;
            }



            return msg;
        }

        [HttpPost]
        public ActionResult AddT_HealthCard(T_HealthCardRecord model)
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            t_HealthCardRequest.T_HealthCardRecord = model;
            t_HealthCardRequest.T_HealthCardRecord.HC_WorkerId = ViewBag.W_ID;
            t_HealthCardRequest.T_HealthCardRecord.HC_St_Id = 1;
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardInsert(t_HealthCardRequest);
            if (t_HealthCardResponse.Success.HasValue && t_HealthCardResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("T_HealthCardAddedSuccessfuly"));

                return RedirectToAction("ListT_HealthCard");
            }
            SessionHelper.Set("FailMessage", t_HealthCardResponse.Message);
            FillAddT_HealthCardFields();
            return View();
        }

        public ActionResult EListHC()
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            return View(t_HealthCardResponse.T_HealthCardRecords.Where(p => p.HC_St_Id == 3 ||  p.HC_St_Id == 4 || p.HC_St_Id == 5).ToArray());
           
        }

        public ActionResult EPartialHClist()
        {
            return PartialView();
        }
    
        public ActionResult HCListStop()
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            return View(t_HealthCardResponse.T_HealthCardRecords.Where(p => p.HC_St_Id == 3).ToArray());

        }

        public ActionResult StopDetialsHC(long HCID)
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            var t_HealthCardRecord = t_HealthCardResponse.T_HealthCardRecords.Where(p => p.ID == HCID).ToArray();


            var HostpitalsService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest, UserRequestFilter>(User.Identity.Name,
                new UserRequestFilter
                {
                   
                });
            var userResponse = HostpitalsService.DoUserListInquiry(userRequest);
            var HostpitalName = userResponse.UserRecords.Where(p => p.ID == t_HealthCardRecord[0].HRep_HospId).ToArray()[0].FullName;

            ViewBag.HostpitalName = HostpitalName;

            return View(t_HealthCardRecord[0]);
        }


        public ActionResult HAdd_HCReq()
        {
            return View();
        }
        public ActionResult HList_HRep()
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            return View(t_HealthCardResponse.T_HealthCardRecords.Where(p => p.CreatedBy == t_HealthCardRequest.UserID && p.HC_St_Id == 1).ToArray());
        }

        public string HCheckWorker(string WID)
        {
            string msg = "";

            var WorkerService = new T_WorkerService();
            var WorkerRequest = RequestBuilder.CreateRequest<T_WorkerRequest, T_WorkerRequestFilter>(User.Identity.Name,
                new T_WorkerRequestFilter
                {

                });
            var WorkerResponse = WorkerService.DoT_WorkerList(WorkerRequest);
            var Worker = WorkerResponse.T_WorkerRecords.Where(p => p.W_NID_NO == WID).ToArray();

            if (Worker.Count() > 0)
            {
                SessionHelper.SetSuccessMessage(msg);
                ViewBag.workerID = Worker[0].ID;
            }
            else
            {
                msg = "لا يوجد عامل بهذا الرقم"; 
            }
             

            return msg;
        }

        /*
        public ActionResult EditT_HealthCard(long t_HealthCardID)
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
              t_HealthCardRequest.Filter = new T_HealthCardRequestFilter
            {
                IDList = new List<long>() { t_HealthCardID }
            };
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardList(t_HealthCardRequest);
            var record = t_HealthCardResponse.T_HealthCardRecords[0];
            FillAddT_HealthCardFields(record);
            return View(record);
        }



        
        [HttpPost]
        public ActionResult EditT_HealthCard(T_HealthCardRecord model)
        {
            var t_HealthCardService = new T_HealthCardService();
            var t_HealthCardRequest = RequestBuilder.CreateRequest<T_HealthCardRequest>(User.Identity.Name);
              t_HealthCardRequest.T_HealthCardRecord = model;
            var t_HealthCardResponse = t_HealthCardService.DoT_HealthCardUpdate(t_HealthCardRequest);
            if (t_HealthCardResponse.Success.HasValue && t_HealthCardResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("T_HealthCardEditedSuccessfuly"));

                return RedirectToAction("ListT_HealthCard");
            }
            SessionHelper.Set("FailMessage", t_HealthCardResponse.Message);

            FillAddT_HealthCardFields(model);
            return View(model);
        }
        */
        private void FillAddT_HealthCardFields(T_HealthCardRecord t_HealthCardRecord = null)
        {

            var HostpitalsService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest, UserRequestFilter>(User.Identity.Name,
                new UserRequestFilter
                {
                 
                });
            var userResponse = HostpitalsService.DoUserListInquiry(userRequest);
            var UserList = userResponse.UserRecords.Where(p => p.RoleID == 6).ToArray();

            ViewBag.UserList = UserList;


        }

        private void FillAddT_HealthCardFields(long workerID, T_HealthCardRecord t_HealthCardRecord = null)
        {

            var HostpitalsService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest, UserRequestFilter>(User.Identity.Name,
                new UserRequestFilter
                {
                     
                });
            var userResponse = HostpitalsService.DoUserListInquiry(userRequest);
            var UserList = userResponse.UserRecords.Where(p => p.RoleID == 6).ToArray();

            ViewBag.UserList = UserList;

            var T_WorkerService = new T_WorkerService();
            var T_WorkerRequest = RequestBuilder.CreateRequest<T_WorkerRequest>(User.Identity.Name);
            T_WorkerRequest.Filter = new T_WorkerRequestFilter
            {
                IDList = new List<long>() { workerID }
            };
            var T_WorkerResponse = T_WorkerService.DoT_WorkerList(T_WorkerRequest);
            if (T_WorkerResponse.T_WorkerRecords.Count() > 0)
            {
                ViewBag.WorkerName = T_WorkerResponse.T_WorkerRecords[0].W_Name;
                ViewBag.WNID = T_WorkerResponse.T_WorkerRecords[0].W_NID_NO;
                ViewBag.W_Job = T_WorkerResponse.T_WorkerRecords[0].W_Job;
                ViewBag.W_Mobile = T_WorkerResponse.T_WorkerRecords[0].W_Mobile;
                ViewBag.W_ID = T_WorkerResponse.T_WorkerRecords[0].ID;
            }
            else
            {
                ViewBag.WorkerName = null;
                ViewBag.WNID = null;
                ViewBag.W_Job = null;
                ViewBag.W_Mobile = null;
                ViewBag.W_ID = null;

                SessionHelper.Set("FailMessage", T_WorkerResponse.Message);
            }







        }

    }
}