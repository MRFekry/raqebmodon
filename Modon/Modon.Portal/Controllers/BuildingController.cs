using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class BuildingController : Controller
    {

        public ActionResult ListBuilding()
        {
            var buildingRecords = GetBuildingList();
            return View(buildingRecords);

        }

        public ActionResult EListBuilding()
        {
            var buildingRecords = GetBuildingList();
            return View(buildingRecords);
        }


        public ActionResult DeleteBuilding(long buildingID)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.BuildingRecord = new BuildingRecord
           {
               ID = buildingID
           };
            var buildingResponse = buildingService.DoBuildingDelete(buildingRequest);
            if (buildingResponse.Success.HasValue && buildingResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("BuildingDeletedSuccessfuly"));
            else
                SessionHelper.Set("FailMessage", buildingResponse.Message);
            return null;
        }

        public ActionResult AddBuilding()
        {
            FillAddBuildingFields();
            return View();
        }

        [HttpPost]
        public ActionResult AddBuilding(BuildingRecord model)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.BuildingRecord = model;
            var buildingResponse = buildingService.DoBuildingInsert(buildingRequest);
            if (buildingResponse.Success.HasValue && buildingResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("BuildingAddedSuccessfuly"));

                return RedirectToAction("ListBuilding");
            }
            SessionHelper.Set("FailMessage", buildingResponse.Message);
            FillAddBuildingFields();
            return View();
        }


        public ActionResult EditBuilding(long buildingID)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.Filter = new BuildingRequestFilter
           {
               IDList = new List<long>() { buildingID }
           };
            var buildingResponse = buildingService.DoBuildingList(buildingRequest);
            var record = buildingResponse.BuildingRecords[0];
            FillAddBuildingFields(record);
            return View(record);
        }


        public ActionResult BuildingDetails(long buildingID)
        {
            var record = GetBuildingDetails(buildingID);
         ViewBag.comments= GetAllComments(buildingID);
            return View(record);
        }


        public ActionResult EBuildingDetails(long buildingID, long orderID=0)
        {
            var record = GetBuildingDetails(buildingID);
            ViewBag.comments = GetAllComments(buildingID);
            ViewBag.orderID = orderID;
            return View(record);
        }
        public ActionResult EBuildingReview(long buildingID,long orderID)
        {
            var record = GetBuildingDetails(buildingID);
            long orderIDRef=0;
            ViewBag.order = GetOrderDetails(orderID, buildingID, out orderIDRef);
            ViewBag.comments = GetAllComments(orderIDRef, false);

            return View(record);
        }



        public ActionResult MService(long buildingID)
        {
            ViewBag.buildingID = buildingID;
            return PartialView();
        }


        public ActionResult AddBuildingActivity(long pIndex = 0)
        {
            ViewBag.pIndex = pIndex;
            return PartialView();
        }

        [HttpPost]
        public ActionResult EditBuilding(BuildingRecord model)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.BuildingRecord = model;
            var buildingResponse = buildingService.DoBuildingUpdate(buildingRequest);
            if (buildingResponse.Success.HasValue && buildingResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("BuildingEditedSuccessfuly"));

                return RedirectToAction("ListBuilding");
            }
            SessionHelper.Set("FailMessage", buildingResponse.Message);

            FillAddBuildingFields(model);
            return View(model);
        }

        public string ChangeBuildingStatus(long buildingID, long statusID)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.BuildingRecord = new BuildingRecord();
            buildingRequest.BuildingRecord.StatusID = statusID;
            buildingRequest.BuildingRecord.ID = buildingID;
            var buildingResponse = buildingService.DoBuildingUpdate(buildingRequest);
            var msg = ResourceManager.UIGetResource("StatusChangedSuccessfully");
            if (buildingResponse.Success.HasValue && buildingResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(msg);
            }
            else
            {
                SessionHelper.SetFailMessage( buildingResponse.Message);
                msg = buildingResponse.Message;
            }

            return msg;
        }


        private void FillAddBuildingFields(BuildingRecord buildingRecord = null)
        {
            var userEXService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest, AreaRequestFilter>(User.Identity.Name,
                new AreaRequestFilter
                {
                    //ShowOnlyMyArea = true
                });
            var areaResponse = userEXService.DoAreaList(areaRequest);
            var areasList = areaResponse.AreaRecords.Where(p => p.ParentID == null).ToArray();
            ViewBag.areas = areasList;

            var activityTypeService = new ActivityTypeService();
            var activityTypeRequest = RequestBuilder.CreateRequest<ActivityTypeRequest, ActivityTypeRequestFilter>(User.Identity.Name,
                new ActivityTypeRequestFilter
                {
                    //ShowOnlyMyArea = true
                });
            var activityTypeResponse = activityTypeService.DoActivityTypeList(activityTypeRequest);
            ViewBag.activityType = activityTypeResponse.ActivityTypeRecords.ToArray();


        }

        public JsonResult FillSubActivity(long parentID)
        {
            var activityTypeService = new Activity_SubTypeService();
            var activityTypeRequest = RequestBuilder.CreateRequest<Activity_SubTypeRequest, Activity_SubTypeRequestFilter>(User.Identity.Name,
                new Activity_SubTypeRequestFilter
                {
                    ActivityTypeIDList = new List<long> { parentID }
                });
            var activityTypeResponse = activityTypeService.DoActivity_SubTypeList(activityTypeRequest);
            return Json(activityTypeResponse.Activity_SubTypeRecords);
        }
        private BuildingRecord[] GetBuildingList()
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);

            var buildingResponse = buildingService.DoBuildingList(buildingRequest);
            return buildingResponse.BuildingRecords;
        }
        private BuildingRecord GetBuildingDetails(long buildingID)
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.Filter = new BuildingRequestFilter
            {
                IDList = new List<long>() { buildingID }
            };
            var buildingResponse = buildingService.DoBuildingList(buildingRequest);
            return buildingResponse.BuildingRecords[0];
        }

        private Order_CommentRecord[] GetAllComments(long targetID,bool isBuilding=true)
        {
            var order_CommentService = new Order_CommentService();
            var order_CommentRequest = RequestBuilder.CreateRequest<Order_CommentRequest>(User.Identity.Name);
            if (isBuilding)
            order_CommentRequest.Filter = new Order_CommentRequestFilter { BuildingIDList = new List<long> { targetID } };
            else
                order_CommentRequest.Filter = new Order_CommentRequestFilter { OrderIDList = new List<long> { targetID } };

            var order_CommentResponse = order_CommentService.DoOrder_CommentList(order_CommentRequest);
            return order_CommentResponse.Order_CommentRecords;
        }
        private ModonOrderRecord GetOrderDetails(long modonOrderID,long buildingID,out long orderIDRef)
        {

            if(modonOrderID==0)
            {
                //Get Order

            }
            var modonOrderService = new ModonOrderService();
            var modonOrderRequest = RequestBuilder.CreateRequest<ModonOrderRequest>(User.Identity.Name);
                            modonOrderRequest.Filter = new ModonOrderRequestFilter();

            if (modonOrderID != 0)
                modonOrderRequest.Filter.IDList = new List<long>() { modonOrderID };
            else
            {
                var orderType = (long)EnumOrderType.AddBuilding;
                modonOrderRequest.Filter.BuildingIDList = new List<long>() { buildingID };
                modonOrderRequest.Filter.OrderTypeIDList = new List<long>() { orderType };
            }
            var modonOrderResponse = modonOrderService.DoModonOrderList(modonOrderRequest);
            orderIDRef = modonOrderResponse.ModonOrderRecords[0].ID;
            ViewBag.orderID = modonOrderResponse.ModonOrderRecords[0].ID;
            return modonOrderResponse.ModonOrderRecords[0];
        }


        public void ChangeActivityAllowence(long activityID)
        {
            var building_ActivityService = new Building_ActivityService();
            var building_ActivityRequest = RequestBuilder.CreateRequest<Building_ActivityRequest>(User.Identity.Name);
            building_ActivityRequest.Building_ActivityRecord = new Building_ActivityRecord();
            building_ActivityRequest.Building_ActivityRecord.ID = activityID;
            var building_ActivityResponse = building_ActivityService.DoBuilding_ActivityChangeAllowence(building_ActivityRequest);
            if (building_ActivityResponse.Success.HasValue && building_ActivityResponse.Success.Value)
            {
                //SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Building_ActivityEditedSuccessfuly"));
            }
            //SessionHelper.Set("FailMessage", building_ActivityResponse.Message);
        }


    }
}