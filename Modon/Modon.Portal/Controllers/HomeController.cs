﻿using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Modon.Portal.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult Index()
        {
         
         var buildingRecords=   GetApprovedBuildingList();
            return  View(buildingRecords);
        }

        [Authorize]
        public ActionResult EIndex()
        {
            return View();
        }

        #region Landing
        public ActionResult LandingPage()
        {
            var userEXService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(User.Identity.Name);
            var areaResponse = userEXService.DoAreaList(areaRequest);
           var areasCategory= areaResponse.AreaRecords.Where(p=>p.ParentID==null).ToArray();
            var areasSubCategory = areaResponse.AreaRecords.Where(p => p.ParentID != null).ToArray();

            ViewBag.Areas = areaResponse.AreaRecords ;
            ViewBag.Category = areasSubCategory.Count();
            ViewBag.SubCategory = areasSubCategory.Count();
            ViewBag.CategoryPoints = areasSubCategory.Sum(p=>p.Points);
            ViewBag.SubCategoryPoints = areasSubCategory.Sum(p => p.Points);
            ViewBag.TopCategory = areasCategory.OrderByDescending(p => p.Points).Take(5).ToArray();
            ViewBag.TopSubCategory = areasSubCategory.OrderByDescending(p=>p.Points).Take(5).ToArray();

            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            var userResponse = userService.DoUserListInquiry(userRequest);
            var users = userResponse.UserRecords.Where(p => !p.IsDeleted&&p.IsActive).ToArray();
            //var studentRoleID= ConfigManager.GetUIInstance().StudentRoleID;
            //var teacherRoleID = ConfigManager.GetUIInstance().TeacherRoleID;

            //ViewBag.Students = users!=null? users.Where(p => p.RoleID == studentRoleID).Count():0;
            //ViewBag.Teachers = users != null ? users.Where(p => p.RoleID == teacherRoleID).Count():0;
            //ViewBag.TopStudents = users.Where(p => p.RoleID == studentRoleID).OrderByDescending(p => p.phone1).Take(5).ToArray();
            //ViewBag.TopTeachers = users.Where(p => p.RoleID == teacherRoleID).OrderByDescending(p => p.Balance).Take(5).ToArray();

            return View();
        }

        public ActionResult ChangeCookieLanguage(string lan = "ar")
        {
            var cookieLang = UIHelper.GetCookieLanguage();
            if (lan != cookieLang)
                UIHelper.SetThreadLanguage(lan);

            return RedirectToAction("Login","User");
        }

        #endregion


        #region Profile
        [Authorize]
        public ActionResult Profile(long editableUserID = 0)
        {
            var userID = long.Parse(AuthHelper.GetClaimValue("UserID"));
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<ProfileRequest>(User.Identity.Name);
            if (editableUserID > 0 && long.Parse(AuthHelper.GetClaimValue("UserRoleID")) == ConfigManager.GetUIInstance().AdminRoleID)
            {
                userID = editableUserID;
                userRequest.UserID = userID;
            }
            var userResponse = userService.DoUserProfile(userRequest);

            
            //Get last walltet activities
            var userWalletService = new UserWalletService();
            var userWalletRequest = RequestBuilder.CreateRequest<UserWalletRequest>(User.Identity.Name);
            userWalletRequest.PageIndex = 0;
            userWalletRequest.pageSize = 10;
            userWalletRequest.Filter = new UserWalletRequestFilter
            {
                UserIDList = new List<long> { long.Parse(AuthHelper.GetClaimValue("UserID")) },
            };
            var userWalletResponse = userWalletService.DoUserWalletList(userWalletRequest);
            ViewBag.wallet = userWalletResponse.UserWalletRecords;
            if (userWalletResponse.UserWalletRecords.Count() > 0)
            {
                ViewBag.lastActivityDate = TimeZoneHelper.ToCountryDateTime(userWalletResponse.UserWalletRecords[0].CreationDate);
            }

            if (userResponse != null)
            {
                FillLookups(userResponse.ApplicationID, userID);
                return View(userResponse.UserRecord);
            }
            else
                return null;
        }
        [Authorize]
        public ActionResult ProfileDisplay(long userID = 0)
        {
            if (userID != 0)
            {
                if (userID == long.Parse(AuthHelper.GetClaimValue("UserID")))
                    return RedirectToAction("Profile", "Home");

                var userService = new UserService();
                var userRequest = RequestBuilder.CreateRequest<ProfileRequest>(User.Identity.Name);
                userRequest.ProfileUserID = userID;
                var userResponse = userService.DoUserProfile(userRequest);
                if (userResponse != null && userResponse.UserRecord != null)
                {
                    ViewBag.IsBlockerUser = userResponse.IsBlockerUser;
                    ViewBag.IsBlockedUser = userResponse.IsBlockedUser;

                    return View(userResponse.UserRecord);
                }
                else
                    return View("~/Views/Shared/Error.cshtml", new Error
                    {
                        Message = userResponse.Message
                    });
            }
            else
                return null;
        }

        [Authorize]
        [HttpPost]
        public ActionResult Profile(UserRecord model)
        {
            var userlang = model.UserLanguage;
            var cookieLang = UIHelper.GetCookieLanguage();
            if (userlang != cookieLang)
                UIHelper.SetThreadLanguage(userlang);

            //var userID = long.Parse(AuthHelper.GetClaimValue("UserID"));
            var userID = model.ID;

            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<ProfileRequest>(User.Identity.Name);
            userRequest.UserRecord = model;
            userRequest.UserRecord.IsActive = true;

            var userResponse = userService.DoUserProfileUpdate(userRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(userResponse.Message);

            }
            else
                SessionHelper.SetFailMessage(userResponse.Message);

            FillLookups(userResponse.ApplicationID, userID);

            return RedirectToAction("Profile", new { editableUserID = model.ID });
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordRequest model)
        {
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<ChangePasswordRequest>(User.Identity.Name);
            userRequest.OldPassword = model.OldPassword;
            userRequest.NewPassword = model.NewPassword;
            var changePasswordResponse = userService.DoChangePassword(userRequest);
            if (changePasswordResponse.Success.HasValue && changePasswordResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(changePasswordResponse.Message);
                return RedirectToAction("Index");
            }
            else
            {
                SessionHelper.SetFailMessage(changePasswordResponse.Message);
                return View(model);
            }
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }
        #endregion

        #region Rank
        [Authorize]
        public ActionResult RankingDetails()
        {
            return View();
        }

        public ActionResult PartialRanking(bool isDetails = false, bool calledCurrentUserRank = false, bool isSearch = false, long levelID = 0, long countryID = 0, long gradeID = 0, long subjectID = 0)
        {
            if (isDetails)
                ViewBag.hideDetailsBtn = true;
            ViewBag.calledCurrentUserRank = calledCurrentUserRank;
            //if (isSearch)
            //{
            ViewBag.isSearch = isSearch;
            ViewBag.levelID = levelID;
            ViewBag.countryID = countryID;
            ViewBag.gradeID = gradeID;
            ViewBag.subjectID = subjectID;
            //}

            return PartialView();
        }

        public ActionResult LoadRankedUsers(int days = 0)
        {
            
            return PartialView();
        }
        #endregion

        # region General fns
        private void FillLookups(long applicationID, long userID)
        {
            var tasks = new[]
                             {
                          Task.Factory.StartNew(() => FillLookupRole(applicationID))
                             };

            Task.WaitAll(tasks);

            var utilityService = new UtilityService();
            var roleRequest = RequestBuilder.CreateRequest<ServiceOperationRoleAccessRequest>(string.Empty);
            roleRequest.SetApplicationID(applicationID);
            //roleRequest.Filter = new ServiceOperationRoleAccessRequestFilter { IDList = new List<long> { ConfigManager.GetUIInstance().StudentRoleID, ConfigManager.GetUIInstance().TeacherRoleID } };
            var roleResponse = utilityService.DoRoleListInquiry(roleRequest);
            ViewBag.role = roleResponse.ApplicationRoles;
        }

        private void FillLookupRole(long applicationID)
        {
            var utilityService = new UtilityService();
            var roleRequest = RequestBuilder.CreateRequest<ServiceOperationRoleAccessRequest>(string.Empty);
            roleRequest.SetApplicationID(applicationID);
            //roleRequest.Filter = new ServiceOperationRoleAccessRequestFilter { IDList = new List<long> { ConfigManager.GetUIInstance().StudentRoleID, ConfigManager.GetUIInstance().TeacherRoleID } };
            var roleResponse = utilityService.DoRoleListInquiry(roleRequest);
            ViewBag.role = roleResponse.ApplicationRoles;
        }



        public ActionResult ContactUs(ContactUsRecord model)
        {
            var mailSender = new MailSender(ConfigManager.GetUIInstance().GetConfig("SendMail.From"),
                            ConfigManager.GetUIInstance().GetConfig("SendMail.Password"),
                            ConfigManager.GetUIInstance().GetConfig("SendMail.Host"),
                            ConfigManager.GetUIInstance().GetConfig("SendMail.Port"),
                            ConfigManager.GetUIInstance().GetConfig("SendMail.EnableSsl"),
                            ConfigManager.GetUIInstance().GetConfig("SendMail.UseDefaultCredentials"));

            if (EmailLogger.TrySendMail(1, ConfigManager.GetUIInstance().SupportEmail, ResourceManager.GetUIInstance().GetResource("SendMail.ContactUs.Subject"),
                string.Format(ResourceManager.UIGetResource("SendMail.ContactUs.Body"), model.Name, model.Email, model.Message)))
            {
                SessionHelper.SetSuccessMessage(ResourceManager.UIGetResource("YourMessageReceived"));
            }
            else
            {
                SessionHelper.SetFailMessage(ResourceManager.UIGetResource("SendingEmailFailed"));
            }

            return Redirect(Request.UrlReferrer.ToString() + model.DivID);
        }

        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DeleteCheck(string DeleteTarget, bool isUnblockUser = false)
        {
            ViewBag.IsUnblockUser = isUnblockUser;
            ViewBag.DeleteTarget = DeleteTarget;
            return PartialView();
        }
        #endregion

        private BuildingRecord[] GetApprovedBuildingList()
        {
            var buildingService = new BuildingService();
            var buildingRequest = RequestBuilder.CreateRequest<BuildingRequest>(User.Identity.Name);
            buildingRequest.Filter = new BuildingRequestFilter {StatusIDList=new List<long> { ConfigManager.GetUIInstance().Order_ApprovedStatusID } };
            var buildingResponse = buildingService.DoBuildingList(buildingRequest);
            return buildingResponse.BuildingRecords;
        }


    }
}