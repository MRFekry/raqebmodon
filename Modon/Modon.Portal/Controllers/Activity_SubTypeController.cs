using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Modon.DAL6;
using PMApp.Core.CommonDefinitions;


namespace Modon.Portal.Controllers
{
    [Authorize]
    public class Activity_SubTypeController : Controller
    {

        public ActionResult ListActivity_SubType()
        {
            var activity_SubTypeService = new Activity_SubTypeService();
            var activity_SubTypeRequest = RequestBuilder.CreateRequest<Activity_SubTypeRequest>(User.Identity.Name);
            var activity_SubTypeResponse = activity_SubTypeService.DoActivity_SubTypeList(activity_SubTypeRequest);
            return View(activity_SubTypeResponse.Activity_SubTypeRecords);
        }

           public ActionResult DeleteActivity_SubType(long activity_SubTypeID)
        {
            var activity_SubTypeService = new Activity_SubTypeService();
            var activity_SubTypeRequest = RequestBuilder.CreateRequest<Activity_SubTypeRequest>(User.Identity.Name);
             activity_SubTypeRequest.Activity_SubTypeRecord = new Activity_SubTypeRecord
            {
                ID = activity_SubTypeID
            };
            var activity_SubTypeResponse = activity_SubTypeService.DoActivity_SubTypeDelete(activity_SubTypeRequest);
            if (activity_SubTypeResponse.Success.HasValue && activity_SubTypeResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Activity_SubTypeDeletedSuccessfuly"));
            else
                 SessionHelper.Set("FailMessage", activity_SubTypeResponse.Message);
            return null;
        }

          public ActionResult AddActivity_SubType()
        {
            FillAddActivity_SubTypeFields();
            return View();
        }

            [HttpPost]
        public ActionResult AddActivity_SubType(Activity_SubTypeRecord model)
        {
            var activity_SubTypeService = new Activity_SubTypeService();
            var activity_SubTypeRequest = RequestBuilder.CreateRequest<Activity_SubTypeRequest>(User.Identity.Name);
            activity_SubTypeRequest.Activity_SubTypeRecord = model;
            var activity_SubTypeResponse = activity_SubTypeService.DoActivity_SubTypeInsert(activity_SubTypeRequest);
            if (activity_SubTypeResponse.Success.HasValue && activity_SubTypeResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Activity_SubTypeAddedSuccessfuly"));

                return RedirectToAction("ListActivity_SubType");
            }
            SessionHelper.Set("FailMessage", activity_SubTypeResponse.Message);
            FillAddActivity_SubTypeFields();
            return View();
        }

        
        public ActionResult EditActivity_SubType(long activity_SubTypeID)
        {
            var activity_SubTypeService = new Activity_SubTypeService();
            var activity_SubTypeRequest = RequestBuilder.CreateRequest<Activity_SubTypeRequest>(User.Identity.Name);
             activity_SubTypeRequest.Filter = new Activity_SubTypeRequestFilter
            {
                IDList = new List<long>() { activity_SubTypeID }
            };
            var activity_SubTypeResponse = activity_SubTypeService.DoActivity_SubTypeList(activity_SubTypeRequest);
            var record = activity_SubTypeResponse.Activity_SubTypeRecords[0];
            FillAddActivity_SubTypeFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditActivity_SubType(Activity_SubTypeRecord model)
        {
            var activity_SubTypeService = new Activity_SubTypeService();
            var activity_SubTypeRequest = RequestBuilder.CreateRequest<Activity_SubTypeRequest>(User.Identity.Name);
             activity_SubTypeRequest.Activity_SubTypeRecord = model;
            var activity_SubTypeResponse = activity_SubTypeService.DoActivity_SubTypeUpdate(activity_SubTypeRequest);
            if (activity_SubTypeResponse.Success.HasValue && activity_SubTypeResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("Activity_SubTypeEditedSuccessfuly"));

                return RedirectToAction("ListActivity_SubType");
            }
            SessionHelper.Set("FailMessage", activity_SubTypeResponse.Message);

            FillAddActivity_SubTypeFields(model);
            return View(model);
        }

           private void FillAddActivity_SubTypeFields(Activity_SubTypeRecord activity_SubTypeRecord = null)
        {
            

            
        }
    }
}