﻿using Modon.Portal.Helpers;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using System.Web.Mvc;

namespace  Modon.Portal.Controllers
{
    public class BaseController : Controller
    {
        protected ActionResult IsAdmin()
        {
            if (!User.Identity.IsAuthenticated || long.Parse(AuthHelper.GetClaimValue("UserRoleID")) != ConfigManager.GetUIInstance().AdminRoleID)
            {
                return View("Error", new Error
                {
                    Code = "",
                    Message = ResourceManager.UIGetResource("NotAccessable")
                });
            }
            return null;
        }
    }
}