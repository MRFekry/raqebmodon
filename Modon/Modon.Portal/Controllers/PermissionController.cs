﻿using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using System.Linq;
using System.Web.Mvc;

namespace Modon.Portal.Controllers
{
    public class PermissionController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DrawMenu()
        {
            var coreMenuService = new CoreMenuService();
            var coreMenuRequest = new CoreMenuRequest
            {

                InnerCall = true,
                AuthToken = User.Identity.Name,
            };
            var response = coreMenuService.DoCoreMenuListInquiry(coreMenuRequest);
            return PartialView(response.CoreMenuRecords);
        }
         public ActionResult ListMenu(long roleID)
        {
            var coreMenuService = new CoreMenuService();
            var coreMenuRequest = new CoreMenuRequest
            {

                InnerCall = true,
                AuthToken = User.Identity.Name,
                RoleID = roleID
            };
            coreMenuRequest.LoadAll = true;
            var response = coreMenuService.DoCoreMenuListInquiry(coreMenuRequest);
            return PartialView(response.CoreMenuRecords);
        }
        public ActionResult MenuPermission()
        {
            var roleService = new UtilityService();
            var roleRequest = new ServiceOperationRoleAccessRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
                Filter = new ServiceOperationRoleAccessRequestFilter
                {
                }
            };
            var response = roleService.DoRoleListInquiry(roleRequest);
            ViewBag.roles = response.ApplicationRoles.Where(p => p.ID != 1).ToArray();

            return View();
        }

        public string UpdateRoleMenuAccess(long menuID, long roleID, bool isAllowed)
        {
            var coreMenuService = new CoreMenuService();
            var coreMenuRequest = new CoreMenuRequest
            {

                InnerCall = true,
                AuthToken = User.Identity.Name,
                CoreMenuRecord = new CoreMenuRecord
                {
                    ID = menuID,
                    RoleId = roleID,
                    IsAllowed = isAllowed
                }
            };
            var response = coreMenuService.DoUpdateRoleMenu(coreMenuRequest);
            if (response.Success.HasValue && response.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.GetInstance(coreMenuRequest.ApplicationID).GetResource("PermissionsUpdatedSuccessfuly"));
            else
                SessionHelper.Set("FailMessage", response.Message);
            return response.Message;
        }
        public ActionResult OperationPermission()
        {
            var roleService = new UtilityService();
            var roleRequest = new ServiceOperationRoleAccessRequest() {AuthToken= User.Identity.Name };
            var response = roleService.DoRoleListInquiry(roleRequest);
            ViewBag.roles = response.ApplicationRoles.Where(p=>p.ID!=1).ToArray();

            return View();
        }

        public ActionResult ListOperation(long roleID)
        {
            var utilityService = new UtilityService();
            var serviceOperationRoleAccessRequest = new ServiceOperationRoleAccessRequest() { AuthToken = User.Identity.Name };
            serviceOperationRoleAccessRequest.Filter = new ServiceOperationRoleAccessRequestFilter
            {

                FilterRoleID = roleID
            };
            var response = utilityService.DoServiceRoleAccessListInquiry(serviceOperationRoleAccessRequest);
            return PartialView(response.ApplicationRoles[0].ServiceOperationRoleAccesses.Where(p => p.ObjectID != null).ToArray());
        }
        public string UpdateRoleOperationAccess(long rowID, bool isAllowed)
        {
            var utilityService = new UtilityService();
            var serviceOperationRoleAccessRequest =new ServiceOperationRoleAccessRequest() { AuthToken = User.Identity.Name };
            serviceOperationRoleAccessRequest.ServiceOperationRoleAccessRecord = new ServiceOperationRoleAccessRecord
            {
                ID = rowID,
                IsAllowed = isAllowed

            };
            var response = utilityService.DoServiceRoleAccessUpdateOperation(serviceOperationRoleAccessRequest);
            if (response.Success.HasValue && response.Success.Value)
                return "تم تعديل الصلاحية بنجاح";
            else
                return response.Message;
        }

    }
}