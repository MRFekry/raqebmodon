﻿using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace  Modon.Portal.Controllers
{
    public class AccessDeniedController : Controller
    {
        // GET: AccessDenied
        public ActionResult Index()
        {
            return View("Error", new Error
            {
                Code = "",
                Message = ResourceManager.UIGetResource("NotAccessable")
            });
        }
    }
}