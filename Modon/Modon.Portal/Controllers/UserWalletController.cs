using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;
using PMApp.Modon.DAL6;


namespace  Modon.Portal.Controllers
{
    [Authorize]
    public class UserWalletController : BaseController
    {
        public ActionResult ListUserWallet()
        {
            var userWalletService = new UserWalletService();
            var userWalletRequest = RequestBuilder.CreateRequest<UserWalletRequest>(User.Identity.Name);

            userWalletRequest.Filter = new UserWalletRequestFilter
            {
                //UserIDList = new List<long> { long.Parse(AuthHelper.GetClaimValue("UserID")) },
            
                    ShowMyManagers = true
            };
           
            var userWalletResponse = userWalletService.DoUserWalletList(userWalletRequest);
            if (Request.IsAjaxRequest())
                    return PartialView("ListUserWalletPartial", userWalletResponse.UserWalletRecords);
            else
                return View(userWalletResponse.UserWalletRecords);

        }
        public ActionResult ListUserWallet2()
        {
            var userWalletService = new UserWalletService();
            var userWalletRequest = RequestBuilder.CreateRequest<UserWalletRequest>(User.Identity.Name);
            userWalletRequest.Filter = new UserWalletRequestFilter
            {
                ShowMyManagers = true
            };
            var userWalletResponse = userWalletService.DoUserWalletList(userWalletRequest);
                return View(userWalletResponse.UserWalletRecords);

        }

        public ActionResult AddUserWallet()
        {
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            var userResponse = userService.DoUserListInquiry(userRequest);
            ViewBag.users = userResponse.UserRecords;
            return View();
        }

        [HttpPost]
        public ActionResult AddUserWallet(UserWalletRecord model)
        {
            var userWalletService = new UserWalletService();
            var userWalletRequest = RequestBuilder.CreateRequest<UserWalletRequest>(User.Identity.Name);
            userWalletRequest.UserWalletRecord = model;
          var  userResponse = userWalletService.DoUserWalletInsert(userWalletRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(userResponse.Message);

                return RedirectToAction("ListUserWallet");
            }
            else
                SessionHelper.SetFailMessage(userResponse.Message);
            return RedirectToAction("ListUserWallet2");
        }

        [HttpPost]
        public ActionResult EditUserWallet(UserWalletRecord model)
        {
            var userWalletService = new UserWalletService();
            var userWalletRequest = RequestBuilder.CreateRequest<UserWalletRequest>(User.Identity.Name);
            userWalletRequest.UserWalletRecord = model;
            var userResponse = userWalletService.DoUserWalletUpdate(userWalletRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(userResponse.Message);
                return RedirectToAction("ListUserWallet2");
            }
            else
                SessionHelper.SetFailMessage(userResponse.Message);
            return RedirectToAction("ListUserWallet2");
        }
        public ActionResult EditUserWallet(long walletID)
        {
            var userWalletService = new UserWalletService();
            var userWalletRequest = RequestBuilder.CreateRequest<UserWalletRequest>(User.Identity.Name);
            userWalletRequest.Filter = new UserWalletRequestFilter { IDList=new List<long> { walletID } };
            var userResponse = userWalletService.DoUserWalletList(userWalletRequest);

            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            var userResponse2 = userService.DoUserListInquiry(userRequest);
            ViewBag.users = userResponse2.UserRecords;
            return View(userResponse.UserWalletRecords[0]);
        }

        [Authorize]
        public ActionResult DeleteUserWallet(long walletID)
        {
            var userWalletService = new UserWalletService();
            var userWalletRequest = RequestBuilder.CreateRequest<UserWalletRequest>(User.Identity.Name);
            userWalletRequest.UserWalletRecord = new UserWalletRecord { ID = walletID };
            var userResponse = userWalletService.DoUserWalletDelete(userWalletRequest);

            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.GetInstance(userWalletRequest.ApplicationID).GetResource("WalletDeletedSuccessfuly"));

                return RedirectToAction("ListUserWallet2");
            }
            else
            SessionHelper.SetFailMessage(userResponse.Message);
            return RedirectToAction("ListUserWallet2");
        }
    }
}