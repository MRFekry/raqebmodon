using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;

namespace Modon.Portal.Controllers
{
    [Authorize]
    public class OrderTypeController : Controller
    {

        public ActionResult ListOrderType()
        {
            var orderTypeService = new OrderTypeService();
            var orderTypeRequest = RequestBuilder.CreateRequest<OrderTypeRequest>(User.Identity.Name);
            var orderTypeResponse = orderTypeService.DoOrderTypeList(orderTypeRequest);
            return View(orderTypeResponse.OrderTypeRecords);
        }

           public ActionResult DeleteOrderType(long orderTypeID)
        {
            var orderTypeService = new OrderTypeService();
            var orderTypeRequest = RequestBuilder.CreateRequest<OrderTypeRequest>(User.Identity.Name);
             orderTypeRequest.OrderTypeRecord = new OrderTypeRecord
            {
                ID = orderTypeID
            };
            var orderTypeResponse = orderTypeService.DoOrderTypeDelete(orderTypeRequest);
            if (orderTypeResponse.Success.HasValue && orderTypeResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("OrderTypeDeletedSuccessfuly"));
            else
                 SessionHelper.Set("FailMessage", orderTypeResponse.Message);
            return null;
        }

          public ActionResult AddOrderType()
        {
            FillAddOrderTypeFields();
            return View();
        }

            [HttpPost]
        public ActionResult AddOrderType(OrderTypeRecord model)
        {
            var orderTypeService = new OrderTypeService();
            var orderTypeRequest = RequestBuilder.CreateRequest<OrderTypeRequest>(User.Identity.Name);
            orderTypeRequest.OrderTypeRecord = model;
            var orderTypeResponse = orderTypeService.DoOrderTypeInsert(orderTypeRequest);
            if (orderTypeResponse.Success.HasValue && orderTypeResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("OrderTypeAddedSuccessfuly"));

                return RedirectToAction("ListOrderType");
            }
            SessionHelper.Set("FailMessage", orderTypeResponse.Message);
            FillAddOrderTypeFields();
            return View();
        }

        
        public ActionResult EditOrderType(long orderTypeID)
        {
            var orderTypeService = new OrderTypeService();
            var orderTypeRequest = RequestBuilder.CreateRequest<OrderTypeRequest>(User.Identity.Name);
             orderTypeRequest.Filter = new OrderTypeRequestFilter
            {
                IDList = new List<long>() { orderTypeID }
            };
            var orderTypeResponse = orderTypeService.DoOrderTypeList(orderTypeRequest);
            var record = orderTypeResponse.OrderTypeRecords[0];
            FillAddOrderTypeFields(record);
            return View(record);
        }




        [HttpPost]
        public ActionResult EditOrderType(OrderTypeRecord model)
        {
            var orderTypeService = new OrderTypeService();
            var orderTypeRequest = RequestBuilder.CreateRequest<OrderTypeRequest>(User.Identity.Name);
             orderTypeRequest.OrderTypeRecord = model;
            var orderTypeResponse = orderTypeService.DoOrderTypeUpdate(orderTypeRequest);
            if (orderTypeResponse.Success.HasValue && orderTypeResponse.Success.Value)
            {
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("OrderTypeEditedSuccessfuly"));

                return RedirectToAction("ListOrderType");
            }
            SessionHelper.Set("FailMessage", orderTypeResponse.Message);

            FillAddOrderTypeFields(model);
            return View(model);
        }

           private void FillAddOrderTypeFields(OrderTypeRecord orderTypeRecord = null)
        {
            

            
        }
    }
}