﻿using Modon.Portal.Helpers;
using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.Filters;
using Modon.Services;
using System.Linq;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.Security;


namespace Modon.Portal.Controllers
{
    public class UserController : BaseController
    {

        [HttpPost]
        public ActionResult SocialLogin(UserRecord user)
        {
            var applicationID = ApplicationManager.GetApplicationIDFromAppCode(ConfigManager.DefaultApplicationCode);

            var userService = new UserService();
            //user.RememberMe = true;
            var loginRequest = RequestBuilder.CreateRequest<LoginRequest>("");

            loginRequest.LoginChannelName = ConfigManager.GetInstance(applicationID).WebChannelName;
            loginRequest.DeviceRecord = new DeviceRecord
            {
                DeviceName = ConfigManager.GetInstance(applicationID).WebChannelName,
                //DeviceType = ConfigManager.GetInstance(applicationID).type
            };
            if (string.IsNullOrWhiteSpace(user.Email))
                user.Email = ConfigManager.GetInstance(applicationID).DefaultEmail;
            loginRequest.Email = user.Email.ToLower();
            loginRequest.FacebookUserID = user.FacebookUserID;
            loginRequest.SetApplicationID(applicationID);
            var loginResponse = userService.DoSocialLogin(loginRequest);
            if (loginResponse.Success.HasValue && loginResponse.Success.Value)
            {
                var ApplicationCode = ApplicationManager.GetApplicationFromApplicationID(applicationID);
                var authToken = loginResponse.AuthToken;
                FormsAuthentication.SetAuthCookie(authToken, user.RememberMe);
                UIHelper.CurrentUserID = loginResponse.UserId;
                return null;
            }

            if (loginResponse.Status == ResponseStatus.Inactive.ToString())
            {
                SessionHelper.SetFailMessage(loginResponse.Message);
                return Json(loginResponse.Message);
            }
            FillLookups(applicationID);
            var userRecord = new UserRecord
            {
                Email = user.Email,
                FullName = user.FullName,
                FacebookUserID = user.FacebookUserID,
                RememberMe = user.RememberMe,
                Password = user.Password
            };
            return View("AddUserRegisteration", userRecord);

        }

        public ActionResult Login()
        {
            SessionHelper.ClearCurrentSession();
            SessionHelper.ClearCurrentCache();
            ViewBag.returnUrl = Request.QueryString["ReturnUrl"];
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserRecord model)
        {
            long applicationID = 0;
            if (model.ApplicationID > 0)
            {
                applicationID = model.ApplicationID;
                model.ApplicationCode = ApplicationManager.GetApplicationFromApplicationID(applicationID);
            }
            else
                applicationID = ApplicationManager.GetApplicationIDFromAppCode(model.ApplicationCode);
            if (applicationID > 0)
            {
                var userService = new UserService();
                var loginRequest = RequestBuilder.CreateRequest<LoginRequest>("");

                loginRequest.ApplicationCode = model.ApplicationCode;
                loginRequest.LoginChannelName = ConfigManager.GetInstance(applicationID).WebChannelName;
                loginRequest.DeviceRecord = new DeviceRecord
                {
                    DeviceName = ConfigManager.GetInstance(applicationID).WebChannelName,
                    //DeviceType = ConfigManager.GetInstance(applicationID).type
                };

                loginRequest.Email = model.Email;
                loginRequest.FacebookUserID = model.FacebookUserID;
                loginRequest.Password = model.Password;
                loginRequest.SetApplicationID(applicationID);
                var loginResponse = userService.DoLogin(loginRequest);

                if (loginResponse.Success.HasValue && loginResponse.Success.Value)
                {
                    var authToken = loginResponse.AuthToken;
                    FormsAuthentication.SetAuthCookie(authToken, model.RememberMe);
                    UIHelper.CurrentUserID = loginResponse.UserId;

                    if (string.IsNullOrWhiteSpace(model.ReturnUrl))
                    {
                        if (ConfigManager.GetInstance(applicationID).InvestorRoleID == loginResponse.RoleId)
                            return RedirectToAction("Index", "Home", new { application = model.ApplicationCode });
                        else
                            return RedirectToAction("EIndex", "Home", new { application = model.ApplicationCode });

                    }
                    else
                        return Redirect(model.ReturnUrl);
                }
                else
                {
                    ModelState.AddModelError("CredentialError", loginResponse.Message);
                    SessionHelper.SetFailMessage(loginResponse.Message);


                }
            }
            else
            {
                ModelState.AddModelError("CredentialError", ResourceManager.UIGetResource("ErrorInOrganizationCode"));
            }
            return View();
        }

        [Authorize]
        public ActionResult Logout()
        {
            var userID = SecurityHelper.GetUserIDFromAuthenticationToken(User.Identity.Name);
            var userService = new UserService();
            var logoutRequest = RequestBuilder.CreateRequest<LogoutRequest>(User.Identity.Name);

            logoutRequest.UserID = userID;
            var logoutResponse = userService.DoLogout(logoutRequest);
            SessionHelper.ClearCurrentSession();
            SessionHelper.ClearCurrentCache();
            UIHelper.CurrentUserID = 0;
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "User");
        }

        public ActionResult ForgetPassword(string email)
        {
            var userRecord = new UserRecord();
            return View(userRecord);
        }

        [HttpPost]
        public ActionResult ForgetPassword(UserRecord model)
        {
            var userService = new UserService();
            var forgetPasswordRequest = RequestBuilder.CreateRequest<ForgetPasswordRequest>("");
            forgetPasswordRequest.Email = model.Email;
            var forgetPasswordResponse = userService.DoForgetPassword(forgetPasswordRequest);
            if (forgetPasswordResponse.Success.HasValue && forgetPasswordResponse.Success.Value)
            {
                ViewBag.fullEmail = forgetPasswordResponse.OriginalEmail;
                if (!string.IsNullOrWhiteSpace(forgetPasswordResponse.Email))
                    ViewBag.Email = forgetPasswordResponse.Email;
                if (!string.IsNullOrWhiteSpace(forgetPasswordResponse.SecondEmail))
                    ViewBag.SecondEmail = forgetPasswordResponse.SecondEmail;
                if (!string.IsNullOrWhiteSpace(forgetPasswordResponse.Mobile))
                    ViewBag.Mobile = forgetPasswordResponse.Mobile;

            }
            else
                ViewBag.RegisterationFaild = forgetPasswordResponse.Message;
            return View();

        }

        public ActionResult ForgetPasswordChoice()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetPasswordChoice(UserRecord model)
        {
            var userService = new UserService();
            var forgetPasswordRequest = RequestBuilder.CreateRequest<ForgetPasswordRequest>("");
            forgetPasswordRequest.Email = model.Email;
            //forgetPasswordRequest.ForgetPasswordChoice = model.ForgetPasswordChoice;
            var forgetPasswordResponse = userService.DoForgetPasswordChoice(forgetPasswordRequest);
            if (forgetPasswordResponse.Success.HasValue && forgetPasswordResponse.Success.Value)
            {
                ViewBag.RegisterationSuccess = forgetPasswordResponse.Message;
            }
            else
                ViewBag.RegisterationFaild = forgetPasswordResponse.Message;
            return RedirectToAction("Login");

        }

        #region Backend Users
        [Authorize]
        public ActionResult AddUser()
        {
            FillLookups(1);

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddUser(UserRecord model)
        {
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            userRequest.UserRecord = model;
            var userResponse = userService.DoUserInsertOperation(userRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(userResponse.Message);

                return RedirectToAction("ListUserRegisteration");
            }
            else
                SessionHelper.SetFailMessage(userResponse.Message);
            FillLookups(1, model);
            return View();
        }

        [Authorize]
        public ActionResult ListUser(int pageIndex = 0, int pageSize = 10, string userName = "")
        {
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            //userRequest.PageIndex = pageIndex;
            //userRequest.pageSize = pageSize;
            userRequest.ActiveSelectOption = ActiveSelect.All;
            if (!string.IsNullOrEmpty(userName))
            {
                userRequest.Filter = new UserRequestFilter
                {
                    FullName = userName
                };
            }
            var userResponse = userService.DoUserListInquiry(userRequest);
            ViewBag.HasMore = userResponse.TotalCount >= ((pageIndex + 1) * pageSize) ? true : false;

            return View(userResponse.UserRecords);
        }


        [Authorize]
        public ActionResult DeleteUser(long userID)
        {
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            userRequest.UserRecord = new UserRecord
            {
                ID = userID
            };
            var userResponse = userService.DoUserDeleteOperation(userRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.GetInstance(userRequest.ApplicationID).GetResource("UserDeletedSuccessfuly"));

                return RedirectToAction("ListUserRegisteration");
            }
            SessionHelper.SetFailMessage(userResponse.Message);
            return RedirectToAction("ListUserRegisteration");
        }


        [Authorize]
        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        [Authorize]
        public ActionResult EditUser(long userID)
        {

            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            userRequest.Filter = new UserRequestFilter
            {
                UserIDList = new List<long>() { userID }
            };
            userRequest.ActiveSelectOption = ActiveSelect.All;
            var userResponse = userService.DoUserListInquiry(userRequest);
            var record = userResponse.UserRecords[0];
            FillLookups(1, record);

            return View(record);
        }

        [Authorize]
        public ActionResult ActiveUser(long userID)
        {
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            userRequest.Filter = new UserRequestFilter
            {
                UserIDList = new List<long>() { userID }
            };
            userRequest.ActiveSelectOption = ActiveSelect.All;
            var userResponse = userService.DoUserListInquiry(userRequest);
            var record = userResponse.UserRecords[0];

            return View(record);
        }

        [Authorize]
        public string ChangeUserStatus(long userID, long statusID)
        {
            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(User.Identity.Name);
            userRequest.Filter = new UserRequestFilter
            {
                UserIDList = new List<long>() { userID }
            };
            userRequest.ActiveSelectOption = ActiveSelect.All;
            var userResponse = userService.DoUserListInquiry(userRequest);
            if (userResponse.Success == true)
            {
                userService = new UserService();
                userRequest.UserRecord = userResponse.UserRecords[0];
                userRequest.UserRecord.StatusID = statusID;
                userResponse = userService.DoUserUpdateOperation(userRequest);
            }
            if (userResponse.Success == true)
                return string.Empty;
            return userResponse.Message;
        }

        #endregion
        #region FronEnd Registeration
        public ActionResult AddUserRegisteration(long applicationID = 1)
        {
            //var applicationID = ApplicationManager.GetApplicationIDFromAppCode(ApplicationCode);
            if (applicationID > 0)
            {
                FillLookups(applicationID);
            }
            return View();
        }
        private void FillLookups(long applicationID = 1, UserRecord userRecord = null)
        {

            ViewBag.ApplicationID = applicationID;

            var utilityService = new UtilityService();
            var roleRequest = RequestBuilder.CreateRequest<ServiceOperationRoleAccessRequest>(string.Empty);
            roleRequest.SetApplicationID(applicationID);
            roleRequest.Filter = new ServiceOperationRoleAccessRequestFilter { IDList = new List<long> { 2, 3, 4 } };
            var roleResponse = utilityService.DoRoleListInquiry(roleRequest);
            ViewBag.role = roleResponse.ApplicationRoles;

            var userEXService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest, AreaRequestFilter>(User.Identity.Name,
                new AreaRequestFilter
                {
                    //ShowOnlyMyArea = true
                });
            var areaResponse = userEXService.DoAreaList(areaRequest);
            ViewBag.Category = areaResponse.AreaRecords.Where(p => p.ParentID == null).ToArray();
            var areaList = new List<object>();
            var areaRecs = areaResponse.AreaRecords;
            for (int i = 0; i < areaRecs.Count(); i++)
            {
                var isRootArea = areaRecs[i].ParentID == null;
                var isSelected = (areaRecs != null && userRecord != null && userRecord.AreaID == areaRecs[i].ID);

                areaList.Add(new
                {
                    id = areaRecs[i].ID.ToString(),
                    parent = isRootArea ? "#" : areaRecs[i].ParentID.ToString(),
                    text = areaRecs[i].Name,
                    state = new
                    {
                        selected = isSelected
                    }
                });
            }
            ViewBag.Areas = Json(areaList);



        }

        [HttpPost]
        public ActionResult AddUserRegisteration(UserRecord model)
        {

            var userService = new UserService();
            var userRequest = RequestBuilder.CreateRequest<UserRequest>(string.Empty);
            userRequest.UserRecord = model;
            userRequest.SetApplicationID(model.ApplicationID);
            var userResponse = userService.DoUserInsertOperation(userRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                ViewBag.RegisterationSuccess = userResponse.Message;
            }
            else
                ViewBag.RegisterationFaild = userResponse.Message;

            FillLookups(model.ApplicationID);
            return View(model);
        }

        [Authorize]
        public ActionResult ListUserRegisteration()
        {
            var userService = new UserService();
            var userRequest = new UserRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
                ActiveSelectOption = ActiveSelect.All,
                Filter = new UserRequestFilter
                {
                    ShowMyManagers = true
                }

            };
            var userResponse = userService.DoUserListInquiry(userRequest);
            var users = userResponse.UserRecords.Where(p => p.RoleID != 1).ToArray();
            return View(users);
        }

        [Authorize]
        public ActionResult DeleteUserRegisteration(string userEmail)
        {
            var userService = new UserService();
            var userRequest = new UserRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
                UserRecord = new UserRecord
                {
                    Email = userEmail
                }
            };
            var userResponse = userService.DoUserDeleteOperation(userRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.GetInstance(userRequest.ApplicationID).GetResource("UserDeletedSuccessfuly"));

                return RedirectToAction("ListUserRegisteration");
            }
            SessionHelper.SetFailMessage(userResponse.Message);
            return RedirectToAction("ListUserRegisteration");
        }

        [Authorize]
        public ActionResult EditUserRegisteration(long userID)
        {

            var userService = new UserService();
            var userRequest = new UserRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
                Filter = new UserRequestFilter
                {
                    UserIDList = new List<long>() { userID }
                },
                ActiveSelectOption = ActiveSelect.All
            };
            FillLookups(1);
            var userResponse = userService.DoUserListInquiry(userRequest);
            var record = userResponse.UserRecords[0];

            return View(record);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditUser(UserRecord model)
        {
            var userService = new UserService();
            var userRequest = new UserRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
                UserRecord = model
            };
            var userResponse = userService.DoUserUpdateOperation(userRequest);
            if (userResponse.Success.HasValue && userResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.GetInstance(userRequest.ApplicationID).GetResource("UserEditedSuccessfuly"));

            }
            else
                SessionHelper.SetFailMessage(userResponse.Message);


            return RedirectToAction("ListUserRegisteration");
        }
        #endregion
        #region Activate Accounts
        public ActionResult ActivateAccount(string ac = "")
        {
            if (!string.IsNullOrWhiteSpace(ac))
            {
                ac = ac.Replace(" ", "+");
                int mod4 = ac.Length % 4;
                if (mod4 > 0)
                    ac += new string('=', 4 - mod4);
                try
                {
                    var activationCodeStr = SecurityHelper.DecryptActivationCode(ac);
                    var activationCodeArr = activationCodeStr.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                    var userRecord = new UserRecord() { Code = activationCodeArr[0], ID = long.Parse(activationCodeArr[1]) };
                    return ActivateAccount(userRecord);
                }
                catch (Exception ex)
                {
                    ExceptionLogger.GetUIInstance().Log(ex);
                    return RedirectToAction("Login");
                }
            }
            else
            {
                return View();

            }
        }

        [HttpPost]
        public ActionResult ActivateAccount(UserRecord model)
        {
            try
            {
                var userService = new UserService();
                var userRequest = RequestBuilder.CreateRequest<UserRequest>("");
                userRequest.ActiveSelectOption = ActiveSelect.IsNotActive;
                userRequest.Filter = new UserRequestFilter
                {
                    EmployeeCode = model.Code,
                    UserIDList = new List<long> { model.ID },
                    Email = model.Email
                };
                var defaultApplicationID = ApplicationManager.GetApplicationIDFromAppCode(ConfigManager.DefaultApplicationCode);
                userRequest.SetApplicationID(defaultApplicationID);
                var activeResponse = userService.DoUserActivate(userRequest);
                if (activeResponse.Success.HasValue && activeResponse.Success.Value)
                {
                    Login(activeResponse.UserRecords[0]);
                    ViewBag.ActivationSuccess = activeResponse.Message;
                    return View();
                }

                //return Login(activeResponse.UserRecords[0]);
                else
                {
                    ViewBag.ActivationFaild = activeResponse.Message;
                    return RedirectToAction("Login");
                }
            }
            catch
            {
                ViewBag.RegisterationFaild = "";
                return RedirectToAction("Login");
            }
        }
        #endregion
        #region ChangePassword
        public ActionResult ChangePassword(string ac = "")
        {
            if (!string.IsNullOrWhiteSpace(ac))
            {
                ac = ac.Replace(" ", "+");
                int mod4 = ac.Length % 4;
                if (mod4 > 0)
                    ac += new string('=', 4 - mod4);
                try
                {
                    var activationCodeStr = SecurityHelper.DecryptActivationCode(ac);
                    var activationCodeArr = activationCodeStr.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                    var userRecord = new UserRecord() { Code = activationCodeArr[0], ID = long.Parse(activationCodeArr[1]), Notes=ac };
                    return View(userRecord);

                }
                catch (Exception ex)
                {
                    ExceptionLogger.GetUIInstance().Log(ex);
                    return RedirectToAction("Login");
                }
            }
            else
                return View("Login");
        }

        [HttpPost]
        public ActionResult ChangePassword(UserRecord model)
        {
            try
            {
                var userService = new UserService();
                var userRequest = RequestBuilder.CreateRequest<UserRequest>("");
                userRequest.ActiveSelectOption = ActiveSelect.IsNotActive;
                userRequest.UserRecord = new UserRecord
                {
                    Code = model.Code,
                    ID = model.ID,
                    Password = model.Password
                };
                var defaultApplicationID = ApplicationManager.GetApplicationIDFromAppCode(ConfigManager.DefaultApplicationCode);
                userRequest.SetApplicationID(defaultApplicationID);
                var changePasswordResponse = userService.DoForgetPasswordChange(userRequest);
                if (changePasswordResponse.Success.HasValue && changePasswordResponse.Success.Value)
                {
                    SessionHelper.SetSuccessMessage(changePasswordResponse.Message);
                return View("Login");


                }
                else
                {
                    ViewBag.RegisterationFaild = changePasswordResponse.Message;
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetUIInstance().Log(ex);

                return View("Login");
            }
        }


        [HttpPost]
        public JsonResult FillDistrict(long parentID, long applicationID = 1)
        {
            var userEXService = new UserExService();
            var areaRequest = RequestBuilder.CreateRequest<AreaRequest>(string.Empty);
            areaRequest.SetApplicationID(applicationID);

            areaRequest.Filter = new AreaRequestFilter
            {
                ParentIDList = new List<long>() { parentID }
            };
            var areaResponse = userEXService.DoAreaList(areaRequest);
            return Json(areaResponse.AreaRecords);
        }
        #endregion
    }
}