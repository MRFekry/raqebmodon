using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.Filters;
using Modon.Portal.Helpers;
using Modon.Services;
using PMApp.Core.Services;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PMApp.Core.CommonDefinitions;


namespace  Modon.Portal.Controllers
{
    [Authorize]
    public class NotificationsController : BaseController
    {

        public ActionResult Index()
        {
            var notificationService = new NotificationService();
            var notificationRequest = RequestBuilder.CreateRequest<NotificationRequest>(User.Identity.Name);
            notificationRequest.MakeAsSeen = true;
            var notificationResponse = notificationService.DoNotificationList(notificationRequest);
            return View(notificationResponse.NotificationRecords);
        }
        public ActionResult PartialNotification(int pageSize = 10)
        {
            var NotificationRecords = GetNotinficationList();
            return PartialView(NotificationRecords);
        }
        public ActionResult EPartialNotification(int pageSize = 10)
        {
            var NotificationRecords = GetNotinficationList();
            return PartialView(NotificationRecords);
        }


        public void DoNotificationSeen(int notificationID=0)
        {
            if (notificationID > 0)
            {
                var notificationService = new NotificationService();
                var notificationRequest = RequestBuilder.CreateRequest<NotificationRequest>(User.Identity.Name);
                notificationRequest.NotificationRecord = new NotificationRecord { ID = notificationID };
                var notificationResponse = notificationService.DoNotificationSeen(notificationRequest);
                ViewBag.count = notificationResponse.TotalCount;  
            }
        }

        private NotificationRecord[] GetNotinficationList(int pageSize = 10)
        {
            var notificationService = new NotificationService();
            var notificationRequest = RequestBuilder.CreateRequest<NotificationRequest>(User.Identity.Name);
            notificationRequest.PageSize = pageSize;
            notificationRequest.Filter = new NotificationRequestFilter { IsSeen = false };

            var notificationResponse = notificationService.DoNotificationList(notificationRequest);
            ViewBag.count = notificationResponse.TotalCount;
            return notificationResponse.NotificationRecords;
        }
    }
}