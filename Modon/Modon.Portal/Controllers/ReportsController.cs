using Newtonsoft.Json.Linq;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LiveRep.Portal.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        public ActionResult ListReports(bool isUI = false, long categoryID = 0)
        {
            var widgetService = new WidgetService();
            var widgetRequest = new WidgetRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name
            };

            var widgetResponse = widgetService.DoWidgetList(widgetRequest);


            var widgetCategoryService = new PMCore_WidgetCategoryService();

            var widgetCategoryRequest = new PMCore_WidgetCategoryRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
            };

            var widgetCategoryResponse = widgetCategoryService.DoPMCore_WidgetCategoryList(widgetCategoryRequest);
            ViewBag.Categories = widgetCategoryResponse.PMCore_WidgetCategoryRecords;


            var model = widgetResponse.WidgetRecords;
            if (categoryID != 0)
                model = model.Where(t => t.CategoryID == categoryID).ToArray();


            if (isUI)
                return View("ListReports", "UILayout", model);
            else
                return View(model);
        }

        public ActionResult ListReportsPerCategory(long categoryID = 0)
        {
            var widgetService = new WidgetService();
            var widgetRequest = new WidgetRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name
            };

            var widgetResponse = widgetService.DoWidgetList(widgetRequest);

            var widgetCategoryService = new PMCore_WidgetCategoryService();

            var widgetCategoryRequest = new PMCore_WidgetCategoryRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
            };

            var widgetCategoryResponse = widgetCategoryService.DoPMCore_WidgetCategoryList(widgetCategoryRequest);
            ViewBag.Categories = widgetCategoryResponse.PMCore_WidgetCategoryRecords;


            var model = widgetResponse.WidgetRecords;
            if (categoryID != 0)
            {
                ViewBag.CategoryName = widgetCategoryResponse.PMCore_WidgetCategoryRecords.Where(p => p.ID == categoryID).FirstOrDefault().Name;
                model = model.Where(t => t.CategoryID == categoryID).ToArray();
            }
            return PartialView(model);
        }

        public ActionResult InitReportFilters(long? widegeID)
        {
            var wID = widegeID;

            if (wID.HasValue && wID > 0)
            {
                var widgetService = new WidgetService();
                var widgetRequest = new WidgetRequest
                {
                    InnerCall = true,
                    AuthToken = User.Identity.Name,
                    WidgetID = wID
                };
                var widgetResponse = widgetService.DoWidgetList(widgetRequest);
                if (widgetResponse.WidgetRecords != null)
                {
                    var widgetRecord = widgetResponse.WidgetRecords.FirstOrDefault();
                    ViewBag.WidgetID = wID;
                    ViewBag.WidgetRecord = widgetRecord;
                    ViewBag.ReportName = ResourceManager.GetInstance(widgetRequest.ApplicationID).GetResource(widgetRecord.Name);
                    ViewBag.ReportDescription = widgetRecord.Description;
                   return PartialView(widgetRecord.Params);
                }
            }

            return View();
        }

        public ActionResult DisplayReport(long widgetID, string param)
        {
            var reportPrms = new List<ReportParam>();

            JArray paramJs = JArray.Parse(param);
            for (int i = 0; i < paramJs.Count; i++)
            {
                reportPrms.Add(new ReportParam(paramJs[i]["Name"].ToString(), paramJs[i]["Value"].ToString(), paramJs[i]["ControlType"].ToString()));
            }

            var widgetService = new WidgetService();

            var widgetRequest = new WidgetRequest
            {
                InnerCall = true,
                AuthToken = User.Identity.Name,
                WidgetID = widgetID,

                Filter = new WidgetRequestFilter
                {
                    Params = reportPrms.ToArray()

                }
            };

            var widgetResponse = widgetService.DoWidgetInquiry(widgetRequest);
            if (widgetResponse.Success == true)
            {
                var widgetRecord = widgetResponse.WidgetRecords.FirstOrDefault(); ;
                ViewBag.WidgetRecord = widgetRecord;
                ViewBag.ReportName = ResourceManager.GetInstance(widgetRequest.ApplicationID).GetResource(widgetRecord.Name);
                ViewBag.ReportDescription = widgetRecord.Description;
                ViewBag.ChartType = widgetRecord.ChartType;



                if (widgetResponse.WidgetDataRecords.Count() == 1)
                    ViewBag.IsPie = "1";
                else
                    ViewBag.IsPie = "0";
                ViewBag.WidgetID = widgetRecord.ID;
                return PartialView(widgetResponse.WidgetDataRecords);
            }
            return PartialView();
        }


        //public ActionResult PdfReport(long widgetID, string param)
        //{
        //    return new ActionAsPdf(
        //                   "DisplayReport",
        //                   new
        //                   {
        //                       widgetID = widgetID,
        //                       param = param
        //                   })
        //    { FileName = "Report.pdf" };

        //}


        public static string GetReportValue(WidgetDataRecord widgetDataRecord, ref int i)
        {
            if (widgetDataRecord != null && widgetDataRecord.Data != null && widgetDataRecord.Data.Count > i)
            {
                var columnName = widgetDataRecord.Data[i].Name.ToLower();
                if (!IsReportValue(columnName))
                {
                    if (widgetDataRecord.Data[i].Value != null && ReportValueDic.ContainsKey(columnName))
                    {
                        var applicationCode = ApplicationManager.GetApplicationFromRouteData();
                        var url = UIHelper.ResolveServerUrl(string.Format(ReportValueDic[columnName], applicationCode, widgetDataRecord.Data[i].Value.ToString()));

                        if (widgetDataRecord.Data.Count > i + 1)
                            return string.Format("<a target=\"_blank\" href=\"{0}\"><b class=\"underline text-success italic\">{1}</b></a>", url, widgetDataRecord.Data[++i].Value);

                        return string.Format("<a target=\"_blank\" href=\"{0}\"><b class=\"underline text-success italic\">{1}</b></a>", url, widgetDataRecord.Data[i].Value);
                    }
                }
                if (widgetDataRecord.Data[i].DataType.Contains("DateTime"))
                    return TimeZoneHelper.ToCountryDateTime(widgetDataRecord.Data[i].Value as DateTime?);
                return widgetDataRecord.Data[i].Value.ToString();
            }
            return "";

        }

        private static Dictionary<string, string> ReportValueDic = null;
        public static bool IsReportValue(string columnName)
        {
            if (columnName != null)
            {
                if (ReportValueDic == null)
                {
                    ReportValueDic = new Dictionary<string, string>();
                    ReportValueDic.Add("question", "~/{0}/Question/QuestionDetails?questionid={1}");
                    ReportValueDic.Add("user", "~/{0}/Home/ProfileDisplay?userID={1}");
                }
                columnName = columnName.ToLower();

                if (ReportValueDic.ContainsKey(columnName))
                    return false;
            }
            return true;
        }
    }

}

