using Modon.Portal.Helpers;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Core.Services;
using PMApp.Modon.DAL6;
using System.Web.Mvc;


namespace Modon.Portal.Controllers
{
    [AuthorizeAdmin]
    public class ConfigsController : Controller
    {

        public ActionResult ListConfigs()
        {
            var configsService = new LookupService();
            var configsRequest = RequestBuilder.CreateRequest<ConfigsRequest>(User.Identity.Name);
            configsRequest.Filter = new ConfigsRequestFilter { IsUIEditable = true };
            var configsResponse = configsService.DoConfigsList(configsRequest);
            return View(configsResponse.ConfigsRecords);
        }

        public ActionResult DeleteConfigs(long configsID)
        {
            var configsService = new LookupService();
            var configsRequest = RequestBuilder.CreateRequest<ConfigsRequest>(User.Identity.Name);
            configsRequest.ConfigsRecord = new ConfigRecord
           {
               ID = configsID
           };
            var configsResponse = configsService.DoConfigsDelete(configsRequest);
            if (configsResponse.Success.HasValue && configsResponse.Success.Value)
                SessionHelper.Set("SuccessMessage", ResourceManager.UIGetResource("ConfigsDeletedSuccessfuly"));
            else
                SessionHelper.Set("FailMessage", configsResponse.Message);
            return null;
        }

        public ActionResult AddConfigs()
        {
            FillAddConfigsFields();
            return View();
        }

        [HttpPost]
        public ActionResult AddConfigs(ConfigRecord model)
        {
            var configsService = new LookupService();
            var configsRequest = RequestBuilder.CreateRequest<ConfigsRequest>(User.Identity.Name);
            configsRequest.ConfigsRecord = model;
            var configsResponse = configsService.DoConfigsInsert(configsRequest);
            if (configsResponse.Success.HasValue && configsResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.UIGetResource("ConfigsAddedSuccessfuly"));

                return RedirectToAction("ListConfigs");
            }
            var msg = configsResponse.Message;
            if (msg.ToLower().Contains("uniq"))
                msg = ResourceManager.UIGetResource("ThisCodeAlreadyExist");

            SessionHelper.SetFailMessage(msg);
            FillAddConfigsFields();
            return View(model);
        }


        public ActionResult EditConfigs(long configsID)
        {
            var configsService = new LookupService();
            var configsRequest = RequestBuilder.CreateRequest<ConfigsRequest>(User.Identity.Name);
            configsRequest.Filter = new ConfigsRequestFilter
           {
               ID = configsID
           };
            var configsResponse = configsService.DoConfigsList(configsRequest);
            var record = configsResponse.ConfigsRecords[0];
            FillAddConfigsFields(record);
            return View(record);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditConfigs(ConfigRecord model)
        {
            var configsService = new LookupService();
            var configsRequest = RequestBuilder.CreateRequest<ConfigsRequest>(User.Identity.Name);
            configsRequest.ConfigsRecord = model;
            var configsResponse = configsService.DoConfigsUpdate(configsRequest);
            if (configsResponse.Success.HasValue && configsResponse.Success.Value)
            {
                SessionHelper.SetSuccessMessage(ResourceManager.UIGetResource("ConfigsEditedSuccessfuly"));
                ConfigManager.GetUIInstance().ClearConfigsCache();
                return RedirectToAction("ListConfigs");
            }
            var msg = configsResponse.Message;
            if (msg.ToLower().Contains("uniq"))
                msg = ResourceManager.UIGetResource("ThisCodeAlreadyExist");

            SessionHelper.SetFailMessage(msg);

            FillAddConfigsFields(model);
            return View(model);
        }

        private void FillAddConfigsFields(ConfigRecord configsRecord = null)
        {

        }
    }
}