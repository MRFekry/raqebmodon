using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IOrder_Comment
    {
        #region Order_Comment Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Order_CommentList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Order_CommentResponse DoOrder_CommentList(Order_CommentRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/Order_CommentDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Order_CommentResponse DoOrder_CommentDelete(Order_CommentRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Order_CommentInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Order_CommentResponse DoOrder_CommentInsert(Order_CommentRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Order_CommentUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Order_CommentResponse DoOrder_CommentUpdate(Order_CommentRequest req);
        #endregion
    }
}