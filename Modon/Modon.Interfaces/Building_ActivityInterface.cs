using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IBuilding_Activity
    {
        #region Building_Activity Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Building_ActivityList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Building_ActivityResponse DoBuilding_ActivityList(Building_ActivityRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/Building_ActivityDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Building_ActivityResponse DoBuilding_ActivityDelete(Building_ActivityRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Building_ActivityInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Building_ActivityResponse DoBuilding_ActivityInsert(Building_ActivityRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Building_ActivityUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Building_ActivityResponse DoBuilding_ActivityUpdate(Building_ActivityRequest req);
        #endregion
    }
}