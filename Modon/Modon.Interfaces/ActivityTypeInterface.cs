using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IActivityType
    {
        #region ActivityType Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ActivityTypeList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ActivityTypeResponse DoActivityTypeList(ActivityTypeRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/ActivityTypeDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ActivityTypeResponse DoActivityTypeDelete(ActivityTypeRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ActivityTypeInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ActivityTypeResponse DoActivityTypeInsert(ActivityTypeRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ActivityTypeUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ActivityTypeResponse DoActivityTypeUpdate(ActivityTypeRequest req);
        #endregion
    }
}