using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IOrderType
    {
        #region OrderType Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/OrderTypeList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        OrderTypeResponse DoOrderTypeList(OrderTypeRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/OrderTypeDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        OrderTypeResponse DoOrderTypeDelete(OrderTypeRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/OrderTypeInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        OrderTypeResponse DoOrderTypeInsert(OrderTypeRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/OrderTypeUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        OrderTypeResponse DoOrderTypeUpdate(OrderTypeRequest req);
        #endregion
    }
}