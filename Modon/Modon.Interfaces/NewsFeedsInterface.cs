using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;

using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface INewsFeeds
    {
        #region NewsFeeds Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsResponse DoNewsFeedsList(NewsFeedsRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsResponse DoNewsFeedsDelete(NewsFeedsRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsResponse DoNewsFeedsInsert(NewsFeedsRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsResponse DoNewsFeedsUpdate(NewsFeedsRequest req);
        #endregion
    }
}