using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IT_Worker
    {
        #region T_Worker Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/T_WorkerList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_WorkerResponse DoT_WorkerList(T_WorkerRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/T_WorkerDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_WorkerResponse DoT_WorkerDelete(T_WorkerRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/T_WorkerInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_WorkerResponse DoT_WorkerInsert(T_WorkerRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/T_WorkerUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_WorkerResponse DoT_WorkerUpdate(T_WorkerRequest req);
        #endregion
    }
}