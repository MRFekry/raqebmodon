using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IActivity_SubType
    {
        #region Activity_SubType Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Activity_SubTypeList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Activity_SubTypeResponse DoActivity_SubTypeList(Activity_SubTypeRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/Activity_SubTypeDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Activity_SubTypeResponse DoActivity_SubTypeDelete(Activity_SubTypeRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Activity_SubTypeInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Activity_SubTypeResponse DoActivity_SubTypeInsert(Activity_SubTypeRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Activity_SubTypeUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Activity_SubTypeResponse DoActivity_SubTypeUpdate(Activity_SubTypeRequest req);
        #endregion
    }
}