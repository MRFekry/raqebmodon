using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IComment_Type
    {
        #region Comment_Type Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Comment_TypeList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Comment_TypeResponse DoComment_TypeList(Comment_TypeRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/Comment_TypeDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Comment_TypeResponse DoComment_TypeDelete(Comment_TypeRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Comment_TypeInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Comment_TypeResponse DoComment_TypeInsert(Comment_TypeRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/Comment_TypeUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        Comment_TypeResponse DoComment_TypeUpdate(Comment_TypeRequest req);
        #endregion
    }
}