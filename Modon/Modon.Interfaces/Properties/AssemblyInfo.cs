using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Modon.Interfaces")]
[assembly: AssemblyProduct("Modon.Interfaces")]
[assembly: AssemblyDescription("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyConfiguration("SBS | Simple Brilliant Solutions")] 
[assembly: AssemblyCompany("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyCopyright("SBS © 2016")]
[assembly: AssemblyTrademark("http://www.sbs16.com")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b00dbc9f-7801-4221-803b-3ac092f9c5a3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
