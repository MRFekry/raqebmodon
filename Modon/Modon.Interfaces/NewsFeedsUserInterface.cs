using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;

using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface INewsFeedsUser
    {
        #region NewsFeedsUser Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsUserList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsUserResponse DoNewsFeedsUserList(NewsFeedsUserRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsUserDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsUserResponse DoNewsFeedsUserDelete(NewsFeedsUserRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsUserInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsUserResponse DoNewsFeedsUserInsert(NewsFeedsUserRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/NewsFeedsUserUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NewsFeedsUserResponse DoNewsFeedsUserUpdate(NewsFeedsUserRequest req);
        #endregion
    }
}