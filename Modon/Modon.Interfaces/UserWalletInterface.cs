using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IUserWallet
    {
        #region UserWallet Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/UserWalletList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        UserWalletResponse DoUserWalletList(UserWalletRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DoUserWalletInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        UserWalletResponse DoUserWalletInsert(UserWalletRequest req);

        #endregion
    }
}