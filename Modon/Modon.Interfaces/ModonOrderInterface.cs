using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IModonOrder
    {
        #region ModonOrder Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ModonOrderList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ModonOrderResponse DoModonOrderList(ModonOrderRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/ModonOrderDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ModonOrderResponse DoModonOrderDelete(ModonOrderRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ModonOrderInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ModonOrderResponse DoModonOrderInsert(ModonOrderRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/ModonOrderUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ModonOrderResponse DoModonOrderUpdate(ModonOrderRequest req);



        #endregion
    }
}