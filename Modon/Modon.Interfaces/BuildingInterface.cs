using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IBuilding
    {
        #region Building Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/BuildingList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        BuildingResponse DoBuildingList(BuildingRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/BuildingDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        BuildingResponse DoBuildingDelete(BuildingRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/BuildingInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        BuildingResponse DoBuildingInsert(BuildingRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/BuildingUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        BuildingResponse DoBuildingUpdate(BuildingRequest req);
        #endregion
    }
}