using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;


namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IT_HealthCard
    {
        #region T_HealthCard Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/T_HealthCardList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_HealthCardResponse DoT_HealthCardList(T_HealthCardRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/T_HealthCardDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_HealthCardResponse DoT_HealthCardDelete(T_HealthCardRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/T_HealthCardInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_HealthCardResponse DoT_HealthCardInsert(T_HealthCardRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/T_HealthCardUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        T_HealthCardResponse DoT_HealthCardUpdate(T_HealthCardRequest req);
        #endregion
    }
}