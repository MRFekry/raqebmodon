using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts;
using PMApp.Core.Interfaces;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Modon.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IUserEx
    {
        #region Area
        [OperationContract]
        [WebInvoke(UriTemplate = "/AreaList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        AreaResponse DoAreaList(AreaRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AreaDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        AreaResponse DoAreaDelete(AreaRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AreaInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        AreaResponse DoAreaInsert(AreaRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/AreaUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        AreaResponse DoAreaUpdate(AreaRequest request);
        #endregion
    }
}