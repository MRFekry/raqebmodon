using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Modon.CommonDefinitions")]
[assembly: AssemblyProduct("Modon.CommonDefinitions")]
[assembly: AssemblyDescription("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyConfiguration("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyCompany("SBS | Simple Brilliant Solutions")]
[assembly: AssemblyCopyright("SBS © 2016")]
[assembly: AssemblyTrademark("http://www.sbs16.com")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("90c0397d-7241-43fa-abf5-16708217a5db")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
