using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class NewsFeedsRecord : BaseRecord
    {
        public NewsFeedsRecord()
        { }

        public NewsFeedsRecord(NewsFeed newsFeeds)
        {
            if (newsFeeds == null)
                return;

            this.ID = newsFeeds.ID;
            this.News = newsFeeds.News;
            this.ModifiedBy = newsFeeds.ModifiedBy;
            this.LastUpdateDate = newsFeeds.LastUpdateDate;
            this.CreatedBy = newsFeeds.CreatedBy;
            this.IsDeleted = newsFeeds.IsDeleted;
            this.CreationDate = newsFeeds.CreationDate;
            this.ApplicationID = newsFeeds.ApplicationID;
        }




        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string News { get; set; }

    }

}
