using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class Order_CommentRecord : BaseRecord
    {
        public Order_CommentRecord()
        { }

        public Order_CommentRecord(Order_Comment order_Comment)
        {
            if (order_Comment == null)
                return;

            this.Comment = order_Comment.Comment;
            this.CreatedBy = order_Comment.CreatedBy;
            this.CreationDate = order_Comment.CreationDate;
            this.ID = order_Comment.ID;
            this.IsDeleted = order_Comment.IsDeleted;
            this.LastUpdateDate = order_Comment.LastUpdateDate;
            this.ModifiedBy = order_Comment.ModifiedBy;
            this.OrderID = order_Comment.OrderID;
            this.UserID = order_Comment.UserID;
        }



        [JsonProperty(PropertyName = "C", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Comment { get; set; }

        [JsonProperty(PropertyName = "OI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long OrderID { get; set; }


        [JsonProperty(PropertyName = "UI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long UserID { get; set; }

        [JsonProperty(PropertyName = "BI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long BuildingID { get; set; }
    }

}
