using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Web;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class BuildingRecord : BaseRecord
    {
        public BuildingRecord()
        { }

        public BuildingRecord(Building building)
        {
            if (building == null)
                return;

            this.ActivityTypeID = building.ActivityTypeID;
            this.AreaID = building.AreaID;
            this.AreaSpace = building.AreaSpace;
            this.CreatedBy = building.CreatedBy;
            this.CreationDate = building.CreationDate;
            this.Email = building.Email;
            this.ID = building.ID;
            this.IsDeleted = building.IsDeleted;
            this.LastUpdateDate = building.LastUpdateDate;
            this.ModifiedBy = building.ModifiedBy;
            this.Name = building.Name;
            this.OwnerCID = building.OwnerCID;
            this.OwnerName = building.OwnerName;
            this.OwnerPhone = building.OwnerPhone;
            this.Phone = building.Phone;
            this.SpaceAllowed = building.SpaceAllowed;
            this.StatusID = building.StatusID;
            this.StreetAdress = building.StreetAdress;
        }



        [JsonProperty(PropertyName = "AA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? ActivityAllowed { get; set; }

        [SBSRequiredAttribute("EnterActivityTypeID")]
        [JsonProperty(PropertyName = "ATI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ActivityTypeID { get; set; }

        [JsonProperty(PropertyName = "AAA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AreaAllowed { get; set; }

        [SBSRequiredAttribute("EnterAreaID")]
        [JsonProperty(PropertyName = "AI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long AreaID { get; set; }

        [SBSRequiredAttribute("EnterAreaSpace")]
        [JsonProperty(PropertyName = "AS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long AreaSpace { get; set; }


        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? CreatedBy { get; set; }

        [ScriptIgnore]
        public DateTime CreationDate { get; set; }

        [JsonProperty(PropertyName = "CDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(CreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    CreationDate = dt;
            }
        }


        [SBSRequiredAttribute("EnterEmail")]
        [JsonProperty(PropertyName = "E", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Email { get; set; }


        [JsonProperty(PropertyName = "EA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string EmailAllowed { get; set; }


        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }


        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsDeleted { get; set; }

        [ScriptIgnore]
        public DateTime? LastUpdateDate { get; set; }

        [JsonProperty(PropertyName = "LUDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastUpdateDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastUpdateDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastUpdateDate = dt;
            }
        }



        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ModifiedBy { get; set; }

        [SBSRequiredAttribute("EnterName")]
        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }


        [JsonProperty(PropertyName = "NA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? NameAllowed { get; set; }


        [JsonProperty(PropertyName = "OCI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OwnerCID { get; set; }

        [SBSRequiredAttribute("EnterOwnerName")]
        [JsonProperty(PropertyName = "ON", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OwnerName { get; set; }

        [SBSRequiredAttribute("EnterOwnerName")]
        [JsonProperty(PropertyName = "OAN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OtherAreaName { get; set; }

        [SBSRequiredAttribute("EnterOwnerName")]
        [JsonProperty(PropertyName = "OCI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long OtherCityID { get; set; }




        [JsonProperty(PropertyName = "OP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OwnerPhone { get; set; }

        [SBSRequiredAttribute("EnterPhone")]
        [JsonProperty(PropertyName = "P", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Phone { get; set; }


        [JsonProperty(PropertyName = "SA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? SpaceAllowed { get; set; }


        [JsonProperty(PropertyName = "SI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long StatusID { get; set; }


        [JsonProperty(PropertyName = "SA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string StreetAdress { get; set; }


        [JsonProperty(PropertyName = "SA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? StreetAllowed { get; set; }
        public IEnumerable<Building_ActivityRecord> BuildingActivities { get; set; }

        [SBSRequiredAttribute("EnterOwnerIDFile")]
        [ScriptIgnore]
        public HttpPostedFileBase OwnerIDFile { get; set; }


        [SBSRequiredAttribute("EnterSTFile")]
        [ScriptIgnore]
        public HttpPostedFileBase STFile { get; set; }


        [SBSRequiredAttribute("EnterBuildingLayoutFile")]
        [ScriptIgnore]
        public HttpPostedFileBase BuildingLayoutFile { get; set; }


        [SBSRequiredAttribute("EnterOtherFile")]
        [ScriptIgnore]
        public HttpPostedFileBase[] OtherFile { get; set; }


       


        [JsonProperty(PropertyName = "ATN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ActivityTypeName { get; set; }


        [JsonProperty(PropertyName = "SN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string StatusName { get; set; }


        [JsonProperty(PropertyName = "AN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AreaName { get; set; }


        [JsonProperty(PropertyName = "WMF", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public WebMedia[] WebMediaFiles { get; set; }

        public IEnumerable<Order_Comment> OrderCommentsList { get; set; }
    }

}
