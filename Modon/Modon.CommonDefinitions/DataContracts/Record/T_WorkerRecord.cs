using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class T_WorkerRecord : BaseRecord
    {
        public T_WorkerRecord()
        { }

        public T_WorkerRecord(T_Worker t_Worker)
        {
            if (t_Worker == null)
                return;

			this.BuildingID = t_Worker.BuildingID;
			this.CreatedBy = t_Worker.CreatedBy;
			this.CreationDate = t_Worker.CreationDate;
			this.ID = t_Worker.ID;
			this.IsDeleted = t_Worker.IsDeleted;
			this.LastUpdateDate = t_Worker.LastUpdateDate;
			this.ModifiedBy = t_Worker.ModifiedBy;
			this.W_BirthDate = t_Worker.W_BirthDate;
			this.W_imgURL = t_Worker.W_imgURL;
			this.W_Job = t_Worker.W_Job;
			this.W_KafeelName = t_Worker.W_KafeelName;
			this.W_Mobile = t_Worker.W_Mobile;
			this.W_Name = t_Worker.W_Name;
			this.W_Nationality = t_Worker.W_Nationality;
			this.W_NID_EndDate = t_Worker.W_NID_EndDate;
			this.W_NID_imgURL = t_Worker.W_NID_imgURL;
			this.W_NID_NO = t_Worker.W_NID_NO;
			this.W_NID_StartDate = t_Worker.W_NID_StartDate;
			this.W_Relagion = t_Worker.W_Relagion;
        }


		
        [JsonProperty(PropertyName = "BI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? BuildingID { get; set; }

		
        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long CreatedBy { get; set; }
		
		[ScriptIgnore]		
        public DateTime CreationDate { get; set; }
		
       [JsonProperty(PropertyName = "CDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(CreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    CreationDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }

		
        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsDeleted { get; set; }
		
		[ScriptIgnore]		
        public DateTime? LastUpdateDate { get; set; }
		
       [JsonProperty(PropertyName = "LUDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastUpdateDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastUpdateDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastUpdateDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ModifiedBy { get; set; }
		
		[ScriptIgnore]		
        public DateTime? W_BirthDate { get; set; }
		
       [JsonProperty(PropertyName = "WBDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_BirthDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(W_BirthDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    W_BirthDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "WUR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_imgURL { get; set; }

		
        [JsonProperty(PropertyName = "WJ", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_Job { get; set; }

		
        [JsonProperty(PropertyName = "WKN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_KafeelName { get; set; }

		
        [JsonProperty(PropertyName = "WM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_Mobile { get; set; }

		
        [JsonProperty(PropertyName = "WN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_Name { get; set; }

		
        [JsonProperty(PropertyName = "WNat", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_Nationality { get; set; }
		
		[ScriptIgnore]		
        public DateTime? W_NID_EndDate { get; set; }
		
       [JsonProperty(PropertyName = "WNIDEDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_NID_EndDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(W_NID_EndDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    W_NID_EndDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "WNIDUR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_NID_imgURL { get; set; }

		
        [JsonProperty(PropertyName = "WNIDN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_NID_NO { get; set; }
		
		[ScriptIgnore]		
        public DateTime? W_NID_StartDate { get; set; }
		
       [JsonProperty(PropertyName = "WNIDSDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_NID_StartDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(W_NID_StartDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    W_NID_StartDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "WR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string W_Relagion { get; set; }

    }

}
