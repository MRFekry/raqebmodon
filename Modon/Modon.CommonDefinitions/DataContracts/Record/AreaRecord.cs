﻿using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{


    [DataContract]
    public class AreaRecord : BaseRecord
    {
        public AreaRecord()
        { }

        public AreaRecord(Area area)
        {
            if (area == null)
                return;
            ID = area.ID;
            Name = area.Name;
            Description = area.Description;
            ParentID = area.ParentID;
            ModifiedBy = area.ModifiedBy;
            LastUpdateDate = area.LastUpdateDate;
            IsDeleted = area.IsDeleted;
            CreationDate = area.CreationDate;
            CreatedBy = area.CreatedBy;
            ApplicationID = area.ApplicationID;
        }

        [DataMember]
        public long ID { get; set; }

        [DataMember]
        [SBSRequired("EnterAreaName")]
        public string Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long? ParentID { get; set; }
        public long Points { get; set; }
    }

}
