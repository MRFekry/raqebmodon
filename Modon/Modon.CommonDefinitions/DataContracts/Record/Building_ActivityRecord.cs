using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class Building_ActivityRecord : BaseRecord
    {
        public Building_ActivityRecord()
        { }

        public Building_ActivityRecord(Building_Activity building_Activity)
        {
            if (building_Activity == null)
                return;

			this.BuildingID = building_Activity.BuildingID;
			this.CreatedBy = building_Activity.CreatedBy;
			this.CreationDate = building_Activity.CreationDate;
			this.Description = building_Activity.Description;
			this.ID = building_Activity.ID;
			this.IsAllowed = building_Activity.IsAllowed;
			this.IsDeleted = building_Activity.IsDeleted;
			this.LastUpdateDate = building_Activity.LastUpdateDate;
			this.ModifiedBy = building_Activity.ModifiedBy;
            this.ActivitSubTypeID = building_Activity.ActivitSubTypeID;
        }


		
        [JsonProperty(PropertyName = "BI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long BuildingID { get; set; }


        [JsonProperty(PropertyName = "ASI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ActivitSubTypeID { get; set; }


        [JsonProperty(PropertyName = "ASTN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ActivitOtherSubTypeName { get; set; }

        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? CreatedBy { get; set; }
		
		[ScriptIgnore]		
        public DateTime CreationDate { get; set; }
		
       [JsonProperty(PropertyName = "CDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(CreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    CreationDate = dt;
            }
        }



        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Description { get; set; }

		
        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }

		
        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsAllowed { get; set; }

		
        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsDeleted { get; set; }
		
		[ScriptIgnore]		
        public DateTime? LastUpdateDate { get; set; }
		
       [JsonProperty(PropertyName = "LUDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastUpdateDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastUpdateDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastUpdateDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ModifiedBy { get; set; }

        [SBSRequiredAttribute("EnterName")]

        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }


        public string Comment { get; set; }

        public string ActivitySubTypeName { get; set; }
    }

}
