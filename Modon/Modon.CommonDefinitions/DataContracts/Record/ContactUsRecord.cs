using PMApp.Core.CommonDefinitions.DataContracts.Record;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class ContactUsRecord : BaseRecord
    {
        public ContactUsRecord()
        { }
        public string Name { get; set; }

        public string Email { get; set; }

        public string Message { get; set; }

        public string DivID { get; set; }
    }

}
