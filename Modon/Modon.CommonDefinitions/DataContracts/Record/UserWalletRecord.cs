using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class UserWalletRecord : BaseRecord
    {
        public UserWalletRecord()
        { }

        public UserWalletRecord(UserWallet userWallet)
        {
            if (userWallet == null)
                return;

            this.ID = userWallet.ID;
            this.UserID = userWallet.UserID;
            this.Points = userWallet.Points;
            this.StatusID = userWallet.StatusID;
            this.Notes = userWallet.Notes;
            this.IsDeleted = userWallet.IsDeleted;
            this.CreatedBy = userWallet.CreatedBy;
            this.ModifiedBy = userWallet.ModifiedBy;
            this.LastUpdateDate = userWallet.LastUpdateDate;
            this.CreationDate = userWallet.CreationDate;
            this.ApplicationID = userWallet.ApplicationID;

        }



        [JsonProperty(PropertyName = "UI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long UserID { get; set; }


        [JsonProperty(PropertyName = "P", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long Points { get; set; }


        [JsonProperty(PropertyName = "SI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long StatusID { get; set; }

        [JsonProperty(PropertyName = "SN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string StatusName { get; set; }


        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Notes { get; set; }


        [JsonProperty(PropertyName = "UFN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string UserFullName { get; set; }

        public string userimageUrl { get; set; }

        [JsonProperty(PropertyName = "IMG", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ImageUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(userimageUrl))
                    return userimageUrl;
                return UIHelper.ResolveServerUrl(userimageUrl);

            }
            set { userimageUrl = value; }
        }


        public string PromoCode { get; set; }

        [JsonProperty(PropertyName = "C", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double Cost { get; set; }

        [JsonProperty(PropertyName = "FE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double Fees { get; set; }

        [JsonProperty(PropertyName = "STC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double SubTotalCost { get; set; }

        [JsonProperty(PropertyName = "STP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double SubTotalPoints { get; set; }

        [JsonProperty(PropertyName = "PMI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long PaymentMethodID { get; set; }

        [JsonProperty(PropertyName = "PMN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string PaymentMethodName { get; set; }

        [JsonProperty(PropertyName = "RQO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string RequestObject { get; set; }

        [JsonProperty(PropertyName = "RSO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ResponseObject { get; set; }

        [JsonProperty(PropertyName = "OI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OrderID { get; set; }

        [JsonProperty(PropertyName = "UBLC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string UserBuyPointsLocalCost { get; set; }

        [JsonProperty(PropertyName = "LC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LocalCurrency { get; set; }

        public System.Web.HttpPostedFileBase WebDocumentFile { get; set; }
}

}
