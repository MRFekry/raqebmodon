using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class Comment_TypeRecord : BaseRecord
    {
        public Comment_TypeRecord()
        { }

        public Comment_TypeRecord(Comment_Type comment_Type)
        {
            if (comment_Type == null)
                return;

			this.CreatedBy = comment_Type.CreatedBy;
			this.CreationDate = comment_Type.CreationDate;
			this.Description = comment_Type.Description;
			this.IconClass = comment_Type.IconClass;
			this.ID = comment_Type.ID;
			this.IsDeleted = comment_Type.IsDeleted;
			this.LastUpdateDate = comment_Type.LastUpdateDate;
			this.ModifiedBy = comment_Type.ModifiedBy;
			this.Name = comment_Type.Name;
        }


		
        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? CreatedBy { get; set; }
		
		[ScriptIgnore]		
        public DateTime CreationDate { get; set; }
		
       [JsonProperty(PropertyName = "CDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(CreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    CreationDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Description { get; set; }

		
        [JsonProperty(PropertyName = "IC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string IconClass { get; set; }

		
        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }

		
        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsDeleted { get; set; }
		
		[ScriptIgnore]		
        public DateTime? LastUpdateDate { get; set; }
		
       [JsonProperty(PropertyName = "LUDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastUpdateDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastUpdateDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastUpdateDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ModifiedBy { get; set; }

		
        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }

    }

}
