using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Web;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class ModonOrderRecord : BaseRecord
    {
        public ModonOrderRecord()
        { }

        public ModonOrderRecord(ModonOrder modonOrder)
        {
            if (modonOrder == null)
                return;

			this.BuildingID = modonOrder.BuildingID;
			this.CreatedBy = modonOrder.CreatedBy;
			this.CreationDate = modonOrder.CreationDate;
			this.ID = modonOrder.ID;
			this.IsDeleted = modonOrder.IsDeleted;
			this.LastUpdateDate = modonOrder.LastUpdateDate;
			this.ModifiedBy = modonOrder.ModifiedBy;
			this.OrderTypeID = modonOrder.OrderTypeID;
			this.StatusID = modonOrder.StatusID;
        }


		
        [JsonProperty(PropertyName = "BI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long BuildingID { get; set; }

		
        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? CreatedBy { get; set; }
		
		[ScriptIgnore]		
        public DateTime CreationDate { get; set; }
		
       [JsonProperty(PropertyName = "CDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(CreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    CreationDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }

		
        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsDeleted { get; set; }
		
		[ScriptIgnore]		
        public DateTime? LastUpdateDate { get; set; }
		
       [JsonProperty(PropertyName = "LUDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastUpdateDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastUpdateDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastUpdateDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ModifiedBy { get; set; }

		
        [JsonProperty(PropertyName = "OTI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long OrderTypeID { get; set; }

		
        [JsonProperty(PropertyName = "SI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long StatusID { get; set; }


        public string OrderTypeName { get; set; }

        public string StatusNAme { get; set; }

        public string BuildingName { get; set; }

        [SBSRequiredAttribute("EnterOwnerRentContractFile")]
        [ScriptIgnore]
        public HttpPostedFileBase OwnerRentContractFile { get; set; }


        [SBSRequiredAttribute("EnterOwnerIDLicenceFile")]
        [ScriptIgnore]
        public HttpPostedFileBase OwnerIDLicenceFile { get; set; }


        [SBSRequiredAttribute("EnterBannerReciptFile")]
        [ScriptIgnore]
        public HttpPostedFileBase BannerReciptFile { get; set; }


        [SBSRequiredAttribute("EnterOtherLicenceFile")]
        [ScriptIgnore]
        public HttpPostedFileBase[] OtherLicenceFile { get; set; }
    }

}
