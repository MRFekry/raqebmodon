using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Web;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class T_HealthCardRecord : BaseRecord
    {
        public T_HealthCardRecord()
        { }

        public T_HealthCardRecord(T_HealthCard t_HealthCard)
        {
            if (t_HealthCard == null)
                return;

			this.BuildingID = t_HealthCard.BuildingID;
			this.CreatedBy = t_HealthCard.CreatedBy;
			this.CreationDate = t_HealthCard.CreationDate;
			this.HC_AceptDate = t_HealthCard.HC_AceptDate;
			this.HC_AceptedBy = t_HealthCard.HC_AceptedBy;
			this.HC_AceptNote = t_HealthCard.HC_AceptNote;
			this.HC_EndDate = t_HealthCard.HC_EndDate;
			this.HC_NO = t_HealthCard.HC_NO;
			this.HC_ReqNote = t_HealthCard.HC_ReqNote;
			this.HC_RequstNO = t_HealthCard.HC_RequstNO;
			this.HC_St_Id = t_HealthCard.HC_St_Id;
			this.HC_StopById = t_HealthCard.HC_StopById;
			this.HC_StopDate = t_HealthCard.HC_StopDate;
			this.HC_StopReason = t_HealthCard.HC_StopReason;
			this.HC_StratDate = t_HealthCard.HC_StratDate;
			this.HC_WorkerId = t_HealthCard.HC_WorkerId;
			this.HRep_FileURL = t_HealthCard.HRep_FileURL;
			this.HRep_HospId = t_HealthCard.HRep_HospId;
			this.HRepDate = t_HealthCard.HRepDate;
			this.HRepNo = t_HealthCard.HRepNo;
			this.ID = t_HealthCard.ID;
			this.IsDeleted = t_HealthCard.IsDeleted;
			this.LastUpdateDate = t_HealthCard.LastUpdateDate;
			this.ModifiedBy = t_HealthCard.ModifiedBy;
        }


		
        [JsonProperty(PropertyName = "BI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long BuildingID { get; set; }

		
        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long CreatedBy { get; set; }
		
		[ScriptIgnore]		
        public DateTime CreationDate { get; set; }
		
       [JsonProperty(PropertyName = "CDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(CreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    CreationDate = dt;
            }
        }

		
		[ScriptIgnore]		
        public DateTime? HC_AceptDate { get; set; }
		
       [JsonProperty(PropertyName = "HCADS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_AceptDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(HC_AceptDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    HC_AceptDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "HCAB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? HC_AceptedBy { get; set; }

		
        [JsonProperty(PropertyName = "HCAN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_AceptNote { get; set; }
		
		[ScriptIgnore]		
        public DateTime? HC_EndDate { get; set; }
		
       [JsonProperty(PropertyName = "HCEDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_EndDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(HC_EndDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    HC_EndDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "HCN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_NO { get; set; }

		
        [JsonProperty(PropertyName = "HCRN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_ReqNote { get; set; }

		
        [JsonProperty(PropertyName = "HCRN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_RequstNO { get; set; }

		
        [JsonProperty(PropertyName = "HCSI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? HC_St_Id { get; set; }

		
        [JsonProperty(PropertyName = "HCSBI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? HC_StopById { get; set; }
		
		[ScriptIgnore]		
        public DateTime? HC_StopDate { get; set; }
		
       [JsonProperty(PropertyName = "HCSDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_StopDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(HC_StopDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    HC_StopDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "HCSR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_StopReason { get; set; }
		
		[ScriptIgnore]		
        public DateTime? HC_StratDate { get; set; }
		
       [JsonProperty(PropertyName = "HCSDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HC_StratDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(HC_StratDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    HC_StratDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "HCWI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? HC_WorkerId { get; set; }

		
        [JsonProperty(PropertyName = "HRFUR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HRep_FileURL { get; set; }

		
        [JsonProperty(PropertyName = "HRHI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? HRep_HospId { get; set; }
		
		[ScriptIgnore]		
        public DateTime? HRepDate { get; set; }
		
       [JsonProperty(PropertyName = "HRDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HRepDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(HRepDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    HRepDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "HRN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string HRepNo { get; set; }

		
        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }

		
        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsDeleted { get; set; }
		
		[ScriptIgnore]		
        public DateTime? LastUpdateDate { get; set; }
		
       [JsonProperty(PropertyName = "LUDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastUpdateDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastUpdateDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastUpdateDate = dt;
            }
        }


		
        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ModifiedBy { get; set; }

        [SBSRequiredAttribute("EnterHRF")]
        [ScriptIgnore]
        public HttpPostedFileBase HRepFile { get; set; }

        public string WorkerName { get; set; }
        public string W_NID_NO { get; set; }
        public T_Worker  HC_Worker { get; set; }

    }

}
