using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts.Record
{
    [DataContract]
    public class NewsFeedsUserRecord : BaseRecord
    {
        public NewsFeedsUserRecord()
        { }

        public NewsFeedsUserRecord(NewsFeedsUser newsFeedsUser)
        {
            if (newsFeedsUser == null)
                return;

			this.ID = newsFeedsUser.ID;
			this.UserID = newsFeedsUser.UserID;
			this.NewFeedsID = newsFeedsUser.NewFeedsID;
			this.NotificationID = newsFeedsUser.NotificationID;
			this.ApplicationID = newsFeedsUser.ApplicationID;
        }


		
        
        

		
        [JsonProperty(PropertyName = "UI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long UserID { get; set; }

		
        [JsonProperty(PropertyName = "NFI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long NewFeedsID { get; set; }

		
        [JsonProperty(PropertyName = "NI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long NotificationID { get; set; }

    }

}
