using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class UserWalletRequest : BaseRequest<UserWalletResponse, UserWalletRequestFilter>
    {
        [JsonProperty(PropertyName = "UWR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public UserWalletRecord UserWalletRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (UserWalletRecord != null)
            return UserWalletRecord.ID;
        return 0;
    }
}
}