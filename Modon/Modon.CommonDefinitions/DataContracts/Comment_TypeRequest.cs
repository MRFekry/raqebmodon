using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class Comment_TypeRequest : BaseRequest<Comment_TypeResponse, Comment_TypeRequestFilter>
    {
        [JsonProperty(PropertyName = "CTR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Comment_TypeRecord Comment_TypeRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (Comment_TypeRecord != null)
            return Comment_TypeRecord.ID;
        return 0;
    }
}
}