using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class ActivityTypeRequest : BaseRequest<ActivityTypeResponse, ActivityTypeRequestFilter>
    {
        [JsonProperty(PropertyName = "ATR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ActivityTypeRecord ActivityTypeRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (ActivityTypeRecord != null)
            return ActivityTypeRecord.ID;
        return 0;
    }
}
}