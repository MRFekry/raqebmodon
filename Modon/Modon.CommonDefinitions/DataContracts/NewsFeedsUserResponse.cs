using Modon.CommonDefinitions.DataContracts.Record;
using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class NewsFeedsUserResponse : BaseResponse
    {

        public NewsFeedsUserResponse(Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "NFURs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public NewsFeedsUserRecord[] NewsFeedsUserRecords { get; set; }
    }
}