using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class BuildingRequest : BaseRequest<BuildingResponse, BuildingRequestFilter>
    {
        [JsonProperty(PropertyName = "BR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public BuildingRecord BuildingRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (BuildingRecord != null)
            return BuildingRecord.ID;
        return 0;
    }
}
}