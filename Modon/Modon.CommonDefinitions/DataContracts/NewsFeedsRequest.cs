using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class NewsFeedsRequest : BaseRequest<NewsFeedsResponse, NewsFeedsRequestFilter>
    {
        [JsonProperty(PropertyName = "NFR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public NewsFeedsRecord NewsFeedRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (NewsFeedRecord != null)
                return NewsFeedRecord.ID;
            return 0;
        }
    }
}