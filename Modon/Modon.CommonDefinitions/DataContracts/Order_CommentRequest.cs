using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class Order_CommentRequest : BaseRequest<Order_CommentResponse, Order_CommentRequestFilter>
    {
        [JsonProperty(PropertyName = "OCR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Order_CommentRecord Order_CommentRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (Order_CommentRecord != null)
            return Order_CommentRecord.ID;
        return 0;
    }
}
}