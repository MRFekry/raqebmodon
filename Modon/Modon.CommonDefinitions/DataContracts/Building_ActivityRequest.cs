using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class Building_ActivityRequest : BaseRequest<Building_ActivityResponse, Building_ActivityRequestFilter>
    {
        [JsonProperty(PropertyName = "BAR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Building_ActivityRecord Building_ActivityRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (Building_ActivityRecord != null)
            return Building_ActivityRecord.ID;
        return 0;
    }
}
}