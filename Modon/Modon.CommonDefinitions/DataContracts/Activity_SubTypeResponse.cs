using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class Activity_SubTypeResponse : BaseResponse
    {

        public Activity_SubTypeResponse(Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "ASTRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Activity_SubTypeRecord[] Activity_SubTypeRecords { get; set; }
    }
}