using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class T_WorkerRequest : BaseRequest<T_WorkerResponse, T_WorkerRequestFilter>
    {
        [JsonProperty(PropertyName = "TWR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public T_WorkerRecord T_WorkerRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (T_WorkerRecord != null)
            return T_WorkerRecord.ID;
        return 0;
    }
}
}