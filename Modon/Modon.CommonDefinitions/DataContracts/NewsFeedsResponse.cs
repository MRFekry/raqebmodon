using Modon.CommonDefinitions.DataContracts.Record;
using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class NewsFeedsResponse : BaseResponse
    {

        public NewsFeedsResponse(Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "NFRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public NewsFeedsRecord[] NewsFeedsRecords { get; set; }
    }
}