using PMApp.Core.CommonDefinitions;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class AreaRequest : BaseRequest<AreaResponse, AreaRequestFilter>
    {
        [DataMember]
        public AreaRecord AreaRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (AreaRecord != null)
                return AreaRecord.ID;
            return base.GetCurrentRecordID();
        }
    }
}