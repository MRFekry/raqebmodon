using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class Activity_SubTypeRequest : BaseRequest<Activity_SubTypeResponse, Activity_SubTypeRequestFilter>
    {
        [JsonProperty(PropertyName = "ASTR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Activity_SubTypeRecord Activity_SubTypeRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (Activity_SubTypeRecord != null )
            return Activity_SubTypeRecord.ID;
        return 0;
    }
}
}