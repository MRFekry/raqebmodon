using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class AreaResponse : BaseResponse
    {

        public AreaResponse(Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [DataMember]
        public AreaRecord[] AreaRecords { get; set; }
    }
}