using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class OrderTypeRequest : BaseRequest<OrderTypeResponse, OrderTypeRequestFilter>
    {
        [JsonProperty(PropertyName = "OTR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public OrderTypeRecord OrderTypeRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (OrderTypeRecord != null)
            return OrderTypeRecord.ID;
        return 0;
    }
}
}