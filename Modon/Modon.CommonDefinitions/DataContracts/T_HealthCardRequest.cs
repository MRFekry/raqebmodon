using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class T_HealthCardRequest : BaseRequest<T_HealthCardResponse, T_HealthCardRequestFilter>
    {
        [JsonProperty(PropertyName = "THCR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public T_HealthCardRecord T_HealthCardRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (T_HealthCardRecord != null)
            return T_HealthCardRecord.ID;
        return 0;
    }
}
}