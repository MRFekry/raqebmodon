using Modon.CommonDefinitions.DataContracts.Record;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class NewsFeedsUserRequest : BaseRequest<NewsFeedsUserResponse, NewsFeedsUserRequestFilter>
    {
        [JsonProperty(PropertyName = "NFUR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public NewsFeedsUserRecord NewsFeedsUserRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (NewsFeedsUserRecord != null)
                return NewsFeedsUserRecord.ID;
            return 0;
        }
    }
}