using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Runtime.Serialization;
using Modon.CommonDefinitions.Filters;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.DataContracts
{
    [DataContract]
    public class ModonOrderRequest : BaseRequest<ModonOrderResponse, ModonOrderRequestFilter>
    {
        [JsonProperty(PropertyName = "MOR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ModonOrderRecord ModonOrderRecord { get; set; }

        public override long GetCurrentRecordID()
    {
        if (ModonOrderRecord != null)
            return ModonOrderRecord.ID;
        return 0;
    }
}
}