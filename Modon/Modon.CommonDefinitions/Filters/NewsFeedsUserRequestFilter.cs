using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class NewsFeedsUserRequestFilter : AbRequestFilter
    {

       [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> IDList { get; set; }

       [JsonProperty(PropertyName = "UIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> UserIDList { get; set; }

       [JsonProperty(PropertyName = "NFIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> NewFeedsIDList { get; set; }

       [JsonProperty(PropertyName = "NIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> NotificationIDList { get; set; }

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ApplicationIDList { get; set; }



        public IQueryable<NewsFeedsUserRecord> ApplyFilter(long applicationID, IQueryable<NewsFeedsUserRecord> query)
        {

            if (IDList != null && IDList.Count > 0)
            {
				query = query.Where(p => IDList.Contains(p.ID));
            }

            if (UserIDList != null && UserIDList.Count > 0)
            {
				query = query.Where(p => UserIDList.Contains(p.UserID));
            }

            if (NewFeedsIDList != null && NewFeedsIDList.Count > 0)
            {
				query = query.Where(p => NewFeedsIDList.Contains(p.NewFeedsID));
            }

            if (NotificationIDList != null && NotificationIDList.Count > 0)
            {
				query = query.Where(p => NotificationIDList.Contains(p.NotificationID));
            }

            if (ApplicationIDList != null && ApplicationIDList.Count > 0)
            {
				query = query.Where(p => ApplicationIDList.Contains(p.ApplicationID));
            }

            return query;
        }
    }
}