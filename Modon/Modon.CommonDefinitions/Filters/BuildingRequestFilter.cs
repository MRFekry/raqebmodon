using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class BuildingRequestFilter : AbRequestFilter
    {

       [JsonProperty(PropertyName = "AA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public bool? ActivityAllowed { get; set; }

       [JsonProperty(PropertyName = "ATIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ActivityTypeIDList { get; set; }

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ApplicationIDList { get; set; }

       [JsonProperty(PropertyName = "AA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string AreaAllowed { get; set; }

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> AreaIDList { get; set; }

       [JsonProperty(PropertyName = "ASL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> AreaSpaceList { get; set; }

       [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> CreatedByList { get; set; }

       [JsonProperty(PropertyName = "CD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? CreationDate { get; set; }

       [JsonProperty(PropertyName = "E", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string Email { get; set; }

       [JsonProperty(PropertyName = "EA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string EmailAllowed { get; set; }

       [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> IDList { get; set; }

       [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public bool? IsDeleted { get; set; }

       [JsonProperty(PropertyName = "LUD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? LastUpdateDate { get; set; }

       [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> ModifiedByList { get; set; }

       [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string Name { get; set; }

       [JsonProperty(PropertyName = "NA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public bool? NameAllowed { get; set; }

       [JsonProperty(PropertyName = "OCIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> OwnerCIDList { get; set; }

       [JsonProperty(PropertyName = "ON", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string OwnerName { get; set; }

       [JsonProperty(PropertyName = "OPL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> OwnerPhoneList { get; set; }

       [JsonProperty(PropertyName = "P", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string Phone { get; set; }

       [JsonProperty(PropertyName = "SA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string SpaceAllowed { get; set; }

       [JsonProperty(PropertyName = "SIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> StatusIDList { get; set; }

       [JsonProperty(PropertyName = "SA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string StreetAdress { get; set; }

       [JsonProperty(PropertyName = "SA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string StreetAllowed { get; set; }



        public IQueryable<BuildingRecord> ApplyFilter(long applicationID, IQueryable<BuildingRecord> query)
        {

            if (ApplicationIDList != null && ApplicationIDList.Count > 0)
            {
                query = query.Where(p => ApplicationIDList.Contains(p.ApplicationID));
            }

           

            if (CreatedByList != null && CreatedByList.Count > 0)
            {
                query = query.Where(p => p.CreatedBy.HasValue && CreatedByList.Contains(p.CreatedBy.Value));
            }

            if (CreationDate.HasValue)
            {
                query = query.Where(p => p.CreationDate == CreationDate);
            }
            if (IDList != null && IDList.Count > 0)
            {
                query = query.Where(p => IDList.Contains(p.ID));
            }

            if (IsDeleted.HasValue)
            {
                query = query.Where(p => p.IsDeleted == IsDeleted);
            }
            if (LastUpdateDate.HasValue)
            {
                query = query.Where(p => p.LastUpdateDate == LastUpdateDate);
            }
            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
                query = query.Where(p => p.ModifiedBy.HasValue && ModifiedByList.Contains(p.ModifiedBy.Value));
            }

           

            if (StatusIDList != null && StatusIDList.Count > 0)
            {
                query = query.Where(p => StatusIDList.Contains(p.StatusID));
            }
            return query;
        }
    }
}