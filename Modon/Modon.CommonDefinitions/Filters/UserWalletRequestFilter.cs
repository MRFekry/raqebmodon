using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class UserWalletRequestFilter : AbRequestFilter
    {

       [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> IDList { get; set; }

       [JsonProperty(PropertyName = "UIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> UserIDList { get; set; }

       [JsonProperty(PropertyName = "PL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> PointsList { get; set; }

       [JsonProperty(PropertyName = "SIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> StatusIDList { get; set; }

       [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string Notes { get; set; }

       [JsonProperty(PropertyName = "DBOIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> DBObjectIDList { get; set; }

       [JsonProperty(PropertyName = "TOIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> TargetObjectIDList { get; set; }

       [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public bool? IsDeleted { get; set; }

       [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> CreatedByList { get; set; }

       [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> ModifiedByList { get; set; }

       [JsonProperty(PropertyName = "LUD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? LastUpdateDate { get; set; }

       [JsonProperty(PropertyName = "CD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? CreationDate { get; set; }

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ApplicationIDList { get; set; }
        public bool ShowMyManagers { get; set; }

        public IQueryable<UserWalletRecord> ApplyFilter(long applicationID, IQueryable<UserWalletRecord> query)
        {

            if (IDList != null && IDList.Count > 0)
            {
				query = query.Where(p => IDList.Contains(p.ID));
            }

            if (UserIDList != null && UserIDList.Count > 0)
            {
				query = query.Where(p => UserIDList.Contains(p.UserID));
            }

            if (PointsList != null && PointsList.Count > 0)
            {
				query = query.Where(p => PointsList.Contains(p.Points));
            }

            if (StatusIDList != null && StatusIDList.Count > 0)
            {
				query = query.Where(p => StatusIDList.Contains(p.StatusID));
            }

            if (!string.IsNullOrWhiteSpace(this.Notes))
            {
                query = query.Where(p => p.Notes != null && p.Notes.Contains(this.Notes));
            }
         

            if (IsDeleted.HasValue)
            {
                query = query.Where(p => p.IsDeleted == IsDeleted);
            }
            if (CreatedByList != null && CreatedByList.Count > 0)
            {
				query = query.Where(p => p.CreatedBy.HasValue && CreatedByList.Contains(p.CreatedBy.Value));
            }

            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
				query = query.Where(p => p.ModifiedBy.HasValue && ModifiedByList.Contains(p.ModifiedBy.Value));
            }

            if (LastUpdateDate.HasValue)
            {
                query = query.Where(p => p.LastUpdateDate == LastUpdateDate);
            }
            if (CreationDate.HasValue)
            {
                query = query.Where(p => p.CreationDate == CreationDate);
            }
            if (ApplicationIDList != null && ApplicationIDList.Count > 0)
            {
				query = query.Where(p => ApplicationIDList.Contains(p.ApplicationID));
            }

            return query;
        }
    }
}