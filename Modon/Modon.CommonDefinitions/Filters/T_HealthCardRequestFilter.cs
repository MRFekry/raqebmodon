using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class T_HealthCardRequestFilter : AbRequestFilter
    {

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ApplicationIDList { get; set; }

       [JsonProperty(PropertyName = "BIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> BuildingIDList { get; set; }

       [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> CreatedByList { get; set; }

       [JsonProperty(PropertyName = "CD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? CreationDate { get; set; }

       [JsonProperty(PropertyName = "HCAD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? HC_AceptDate { get; set; }

       [JsonProperty(PropertyName = "HCABL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> HC_AceptedByList { get; set; }

       [JsonProperty(PropertyName = "HCAN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string HC_AceptNote { get; set; }

       [JsonProperty(PropertyName = "HCED", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? HC_EndDate { get; set; }

       [JsonProperty(PropertyName = "HCN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string HC_NO { get; set; }

       [JsonProperty(PropertyName = "HCRN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string HC_ReqNote { get; set; }

       [JsonProperty(PropertyName = "HCRN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string HC_RequstNO { get; set; }

       [JsonProperty(PropertyName = "HCSIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> HC_St_IdList { get; set; }

       [JsonProperty(PropertyName = "HCSBIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> HC_StopByIdList { get; set; }

       [JsonProperty(PropertyName = "HCSD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? HC_StopDate { get; set; }

       [JsonProperty(PropertyName = "HCSR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string HC_StopReason { get; set; }

       [JsonProperty(PropertyName = "HCSD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? HC_StratDate { get; set; }

       [JsonProperty(PropertyName = "HCWIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> HC_WorkerIdList { get; set; }

       [JsonProperty(PropertyName = "HRFUR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string HRep_FileURL { get; set; }

       [JsonProperty(PropertyName = "HRHIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> HRep_HospIdList { get; set; }

       [JsonProperty(PropertyName = "HRD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? HRepDate { get; set; }

       [JsonProperty(PropertyName = "HRN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string HRepNo { get; set; }

       [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> IDList { get; set; }

       [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public bool? IsDeleted { get; set; }

       [JsonProperty(PropertyName = "LUD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? LastUpdateDate { get; set; }

       [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> ModifiedByList { get; set; }



        public IQueryable<T_HealthCardRecord> ApplyFilter(long applicationID, IQueryable<T_HealthCardRecord> query)
        {

            if (ApplicationIDList != null && ApplicationIDList.Count > 0)
            {
				query = query.Where(p => ApplicationIDList.Contains(p.ApplicationID));
            }

            if (BuildingIDList != null && BuildingIDList.Count > 0)
            {
				query = query.Where(p => BuildingIDList.Contains(p.BuildingID));
            }

            if (CreatedByList != null && CreatedByList.Count > 0)
            {
				query = query.Where(p => CreatedByList.Contains(p.CreatedBy));
            }

            if (CreationDate.HasValue)
            {
                query = query.Where(p => p.CreationDate == CreationDate);
            }
            if (HC_AceptDate.HasValue)
            {
                query = query.Where(p => p.HC_AceptDate == HC_AceptDate);
            }
            if (HC_AceptedByList != null && HC_AceptedByList.Count > 0)
            {
				query = query.Where(p => p.HC_AceptedBy.HasValue && HC_AceptedByList.Contains(p.HC_AceptedBy.Value));
            }

            if (!string.IsNullOrWhiteSpace(this.HC_AceptNote))
            {
                query = query.Where(p => p.HC_AceptNote != null && p.HC_AceptNote.Contains(this.HC_AceptNote));
            }
            if (HC_EndDate.HasValue)
            {
                query = query.Where(p => p.HC_EndDate == HC_EndDate);
            }
            if (!string.IsNullOrWhiteSpace(this.HC_NO))
            {
                query = query.Where(p => p.HC_NO != null && p.HC_NO.Contains(this.HC_NO));
            }
            if (!string.IsNullOrWhiteSpace(this.HC_ReqNote))
            {
                query = query.Where(p => p.HC_ReqNote != null && p.HC_ReqNote.Contains(this.HC_ReqNote));
            }
            if (!string.IsNullOrWhiteSpace(this.HC_RequstNO))
            {
                query = query.Where(p => p.HC_RequstNO != null && p.HC_RequstNO.Contains(this.HC_RequstNO));
            }
            if (HC_St_IdList != null && HC_St_IdList.Count > 0)
            {
				query = query.Where(p => p.HC_St_Id.HasValue && HC_St_IdList.Contains(p.HC_St_Id.Value));
            }

            if (HC_StopByIdList != null && HC_StopByIdList.Count > 0)
            {
				query = query.Where(p => p.HC_StopById.HasValue && HC_StopByIdList.Contains(p.HC_StopById.Value));
            }

            if (HC_StopDate.HasValue)
            {
                query = query.Where(p => p.HC_StopDate == HC_StopDate);
            }
            if (!string.IsNullOrWhiteSpace(this.HC_StopReason))
            {
                query = query.Where(p => p.HC_StopReason != null && p.HC_StopReason.Contains(this.HC_StopReason));
            }
            if (HC_StratDate.HasValue)
            {
                query = query.Where(p => p.HC_StratDate == HC_StratDate);
            }
            if (HC_WorkerIdList != null && HC_WorkerIdList.Count > 0)
            {
				query = query.Where(p => p.HC_WorkerId.HasValue && HC_WorkerIdList.Contains(p.HC_WorkerId.Value));
            }

            if (!string.IsNullOrWhiteSpace(this.HRep_FileURL))
            {
                query = query.Where(p => p.HRep_FileURL != null && p.HRep_FileURL.Contains(this.HRep_FileURL));
            }
            if (HRep_HospIdList != null && HRep_HospIdList.Count > 0)
            {
				query = query.Where(p => p.HRep_HospId.HasValue && HRep_HospIdList.Contains(p.HRep_HospId.Value));
            }

            if (HRepDate.HasValue)
            {
                query = query.Where(p => p.HRepDate == HRepDate);
            }
            if (!string.IsNullOrWhiteSpace(this.HRepNo))
            {
                query = query.Where(p => p.HRepNo != null && p.HRepNo.Contains(this.HRepNo));
            }
            if (IDList != null && IDList.Count > 0)
            {
				query = query.Where(p => IDList.Contains(p.ID));
            }

            if (IsDeleted.HasValue)
            {
                query = query.Where(p => p.IsDeleted == IsDeleted);
            }
            if (LastUpdateDate.HasValue)
            {
                query = query.Where(p => p.LastUpdateDate == LastUpdateDate);
            }
            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
				query = query.Where(p => p.ModifiedBy.HasValue && ModifiedByList.Contains(p.ModifiedBy.Value));
            }

            return query;
        }
    }
}