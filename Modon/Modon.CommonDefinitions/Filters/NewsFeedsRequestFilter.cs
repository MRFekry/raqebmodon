using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class NewsFeedsRequestFilter : AbRequestFilter
    {

       [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> IDList { get; set; }

       [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string News { get; set; }

       [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> ModifiedByList { get; set; }

       [JsonProperty(PropertyName = "LUD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? LastUpdateDate { get; set; }

       [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> CreatedByList { get; set; }

       [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public bool? IsDeleted { get; set; }

       [JsonProperty(PropertyName = "CD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? CreationDate { get; set; }

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ApplicationIDList { get; set; }



        public IQueryable<NewsFeedsRecord> ApplyFilter(long applicationID, IQueryable<NewsFeedsRecord> query)
        {

            if (IDList != null && IDList.Count > 0)
            {
				query = query.Where(p => IDList.Contains(p.ID));
            }

            if (!string.IsNullOrWhiteSpace(this.News))
            {
                query = query.Where(p => p.News != null && p.News.Contains(this.News));
            }
            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
				query = query.Where(p => p.ModifiedBy.HasValue && ModifiedByList.Contains(p.ModifiedBy.Value));
            }

            if (LastUpdateDate.HasValue)
            {
                query = query.Where(p => p.LastUpdateDate == LastUpdateDate);
            }
            if (CreatedByList != null && CreatedByList.Count > 0)
            {
				query = query.Where(p => p.CreatedBy.HasValue && CreatedByList.Contains(p.CreatedBy.Value));
            }

            if (IsDeleted.HasValue)
            {
                query = query.Where(p => p.IsDeleted == IsDeleted);
            }
            if (CreationDate.HasValue)
            {
                query = query.Where(p => p.CreationDate == CreationDate);
            }
            if (ApplicationIDList != null && ApplicationIDList.Count > 0)
            {
				query = query.Where(p => ApplicationIDList.Contains(p.ApplicationID));
            }

            return query;
        }
    }
}