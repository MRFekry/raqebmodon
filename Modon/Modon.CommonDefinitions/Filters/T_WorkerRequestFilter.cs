using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class T_WorkerRequestFilter : AbRequestFilter
    {

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ApplicationIDList { get; set; }

       [JsonProperty(PropertyName = "BIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> BuildingIDList { get; set; }

       [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> CreatedByList { get; set; }

       [JsonProperty(PropertyName = "CD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? CreationDate { get; set; }

       [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> IDList { get; set; }

       [JsonProperty(PropertyName = "IDL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? IsDeleted { get; set; }

        [JsonProperty(PropertyName = "LUD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? LastUpdateDate { get; set; }

       [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> ModifiedByList { get; set; }

       [JsonProperty(PropertyName = "WBD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? W_BirthDate { get; set; }

       [JsonProperty(PropertyName = "WUR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_imgURL { get; set; }

       [JsonProperty(PropertyName = "WJ", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_Job { get; set; }

       [JsonProperty(PropertyName = "WKN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_KafeelName { get; set; }

       [JsonProperty(PropertyName = "WM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_Mobile { get; set; }

       [JsonProperty(PropertyName = "WN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_Name { get; set; }

       [JsonProperty(PropertyName = "WN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_Nationality { get; set; }

       [JsonProperty(PropertyName = "WNIDED", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? W_NID_EndDate { get; set; }

       [JsonProperty(PropertyName = "WNIDUR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_NID_imgURL { get; set; }

       [JsonProperty(PropertyName = "WNIDN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_NID_NO { get; set; }

       [JsonProperty(PropertyName = "WNIDSD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? W_NID_StartDate { get; set; }

       [JsonProperty(PropertyName = "WR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string W_Relagion { get; set; }



        public IQueryable<T_WorkerRecord> ApplyFilter(long applicationID, IQueryable<T_WorkerRecord> query)
        {

            if (ApplicationIDList != null && ApplicationIDList.Count > 0)
            {
				query = query.Where(p => ApplicationIDList.Contains(p.ApplicationID));
            }

            if (BuildingIDList != null && BuildingIDList.Count > 0)
            {
				query = query.Where(p => p.BuildingID.HasValue && BuildingIDList.Contains(p.BuildingID.Value));
            }

            if (CreatedByList != null && CreatedByList.Count > 0)
            {
				query = query.Where(p => CreatedByList.Contains(p.CreatedBy));
            }

            if (CreationDate.HasValue)
            {
                query = query.Where(p => p.CreationDate == CreationDate);
            }
            if (IDList != null && IDList.Count > 0)
            {
				query = query.Where(p => IDList.Contains(p.ID));
            }

            if (IsDeleted.HasValue)
            {
                query = query.Where(p => p.IsDeleted == IsDeleted);
            }

            if (LastUpdateDate.HasValue)
            {
                query = query.Where(p => p.LastUpdateDate == LastUpdateDate);
            }
            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
				query = query.Where(p => p.ModifiedBy.HasValue && ModifiedByList.Contains(p.ModifiedBy.Value));
            }

            if (W_BirthDate.HasValue)
            {
                query = query.Where(p => p.W_BirthDate == W_BirthDate);
            }
            if (!string.IsNullOrWhiteSpace(this.W_imgURL))
            {
                query = query.Where(p => p.W_imgURL != null && p.W_imgURL.Contains(this.W_imgURL));
            }
            if (!string.IsNullOrWhiteSpace(this.W_Job))
            {
                query = query.Where(p => p.W_Job != null && p.W_Job.Contains(this.W_Job));
            }
            if (!string.IsNullOrWhiteSpace(this.W_KafeelName))
            {
                query = query.Where(p => p.W_KafeelName != null && p.W_KafeelName.Contains(this.W_KafeelName));
            }
            if (!string.IsNullOrWhiteSpace(this.W_Mobile))
            {
                query = query.Where(p => p.W_Mobile != null && p.W_Mobile.Contains(this.W_Mobile));
            }
            if (!string.IsNullOrWhiteSpace(this.W_Name))
            {
                query = query.Where(p => p.W_Name != null && p.W_Name.Contains(this.W_Name));
            }
            if (!string.IsNullOrWhiteSpace(this.W_Nationality))
            {
                query = query.Where(p => p.W_Nationality != null && p.W_Nationality.Contains(this.W_Nationality));
            }
            if (W_NID_EndDate.HasValue)
            {
                query = query.Where(p => p.W_NID_EndDate == W_NID_EndDate);
            }
            if (!string.IsNullOrWhiteSpace(this.W_NID_imgURL))
            {
                query = query.Where(p => p.W_NID_imgURL != null && p.W_NID_imgURL.Contains(this.W_NID_imgURL));
            }
            if (!string.IsNullOrWhiteSpace(this.W_NID_NO))
            {
                query = query.Where(p => p.W_NID_NO != null && p.W_NID_NO.Contains(this.W_NID_NO));
            }
            if (W_NID_StartDate.HasValue)
            {
                query = query.Where(p => p.W_NID_StartDate == W_NID_StartDate);
            }
            if (!string.IsNullOrWhiteSpace(this.W_Relagion))
            {
                query = query.Where(p => p.W_Relagion != null && p.W_Relagion.Contains(this.W_Relagion));
            }
            return query;
        }
    }
}