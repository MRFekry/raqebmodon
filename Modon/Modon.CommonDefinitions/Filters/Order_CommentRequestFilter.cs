using Modon.CommonDefinitions.DataContracts;
using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class Order_CommentRequestFilter : AbRequestFilter
    {

       [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> ApplicationIDList { get; set; }

       [JsonProperty(PropertyName = "C", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public string Comment { get; set; }

       [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> CreatedByList { get; set; }

       [JsonProperty(PropertyName = "CD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? CreationDate { get; set; }

       [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> IDList { get; set; }

       [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public bool? IsDeleted { get; set; }

       [JsonProperty(PropertyName = "LUD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public DateTime? LastUpdateDate { get; set; }

       [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long?> ModifiedByList { get; set; }

       [JsonProperty(PropertyName = "OIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> OrderIDList { get; set; }

       [JsonProperty(PropertyName = "UIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> UserIDList { get; set; }

               [JsonProperty(PropertyName = "BIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
       public List<long> BuildingIDList { get; set; }


        public IQueryable<Order_CommentRecord> ApplyFilter(long applicationID, IQueryable<Order_CommentRecord> query)
        {

            if (ApplicationIDList != null && ApplicationIDList.Count > 0)
            {
				query = query.Where(p => ApplicationIDList.Contains(p.ApplicationID));
            }
            if (BuildingIDList != null && BuildingIDList.Count > 0)
            {
                query = query.Where(p => BuildingIDList.Contains(p.BuildingID));
            }
            if (!string.IsNullOrWhiteSpace(this.Comment))
            {
                query = query.Where(p => p.Comment != null && p.Comment.Contains(this.Comment));
            }
            if (CreatedByList != null && CreatedByList.Count > 0)
            {
				query = query.Where(p => p.CreatedBy.HasValue && CreatedByList.Contains(p.CreatedBy.Value));
            }

            if (CreationDate.HasValue)
            {
                query = query.Where(p => p.CreationDate == CreationDate);
            }
            if (IDList != null && IDList.Count > 0)
            {
				query = query.Where(p => IDList.Contains(p.ID));
            }

            if (IsDeleted.HasValue)
            {
                query = query.Where(p => p.IsDeleted == IsDeleted);
            }
            if (LastUpdateDate.HasValue)
            {
                query = query.Where(p => p.LastUpdateDate == LastUpdateDate);
            }
            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
				query = query.Where(p => p.ModifiedBy.HasValue && ModifiedByList.Contains(p.ModifiedBy.Value));
            }

            if (OrderIDList != null && OrderIDList.Count > 0)
            {
				query = query.Where(p => OrderIDList.Contains(p.OrderID));
            }

            if (UserIDList != null && UserIDList.Count > 0)
            {
				query = query.Where(p => UserIDList.Contains(p.UserID));
            }

            return query;
        }

    }
}