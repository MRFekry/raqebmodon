using Modon.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Modon.CommonDefinitions.Filters
{
    [DataContract]
    public class AreaRequestFilter : AbRequestFilter
    {
        [DataMember(EmitDefaultValue = false)]
        public List<long> IDList { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<long> ParentIDList { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<long> ModifiedByList { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<long> CreatedByList { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool ShowOnlyMyArea { get; set; }



        public IQueryable<AreaRecord> ApplyFilter(IQueryable<AreaRecord> query)
        {
            if (IDList != null && IDList.Count > 0)
            {
                query = query.Where(p => IDList.Contains(p.ID));
            }

            if (!string.IsNullOrWhiteSpace(Name))
            {
                query = query.Where(p => p.Name != null && p.Name.Contains(Name));
            }

            if (ParentIDList != null && ParentIDList.Count > 0)
            {
                query = query.Where(p => ParentIDList.Contains(p.ParentID ?? 0));
            }

            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
                query = query.Where(p => ModifiedByList.Contains(p.ModifiedBy ?? 0));
            }

            if (CreatedByList != null && CreatedByList.Count > 0)
            {
                query = query.Where(p => CreatedByList.Contains(p.CreatedBy ?? 0));
            }


            return query;
        }


    }
}