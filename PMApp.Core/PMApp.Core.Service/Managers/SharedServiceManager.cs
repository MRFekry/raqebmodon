﻿using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Linq;

namespace  PMApp.Core.Services
{
    public class SharedServiceManager
    {
        public static void CreateStatusHistory(ModonDBEntities contextEntities, long applicationID, long userID, long dbObjectID, long targetObjectID, long? fromStatusID, long toStatusID, bool commit = false)
        {
            var configManager = ConfigManager.GetInstance(applicationID);
            StatusHistoryRecord statusHistoryRecord = new StatusHistoryRecord
            {
                FromStatusID = fromStatusID,
                ToStatusID = toStatusID,
                TargetObjectID = targetObjectID,
                ObjectID = dbObjectID
            };
            var statusHistory = CreateOrUpdateStatusHistory(userID, applicationID, statusHistoryRecord);
            var statusHistoryRepository = new StatusHistoryRepository(contextEntities, applicationID);
            statusHistoryRepository.Add(statusHistory);
            if (commit)
            {
                statusHistoryRepository.Commit();
            }
        }

        public static StatusHistory CreateOrUpdateStatusHistory(long userID, long applicationID, StatusHistoryRecord statusHistoryRecord, StatusHistory statusHistory = null)
        {
            if (statusHistory == null)
            {
                statusHistory = new StatusHistory();
                statusHistory.CreatedBy = userID;
                statusHistory.CreationDate = DateTime.Now;
                statusHistory.IsDeleted = false;
            }
            else
            {
                statusHistory.ModifiedBy = userID;
                statusHistory.LastUpdateDate = DateTime.Now;
                statusHistory.IsDeleted = statusHistoryRecord.IsDeleted;
            }
            statusHistory.ID = statusHistoryRecord.ID;
            statusHistory.FromStatusID = statusHistoryRecord.FromStatusID;
            statusHistory.ToStatusID = statusHistoryRecord.ToStatusID;
            statusHistory.TargetObjectID = statusHistoryRecord.TargetObjectID;
            statusHistory.ObjectID = statusHistoryRecord.ObjectID;
            statusHistory.ApplicationID = applicationID;
            return statusHistory;
        }

        public static Status CreateOrUpdateStatus(long userID, long applicationID, StatusRecord statusRecord, Status status = null)
        {
            if (status == null)
            {
                status = new Status();
                status.CreatedBy = userID;
                status.CreationDate = DateTime.Now;
                status.IsDeleted = false;
            }
            else
            {
                status.ModifiedBy = userID;
                status.LastUpdateDate = DateTime.Now;
                status.IsDeleted = statusRecord.IsDeleted;
            }
            status.ID = statusRecord.ID;
            status.Name = statusRecord.Name;
            status.Description = statusRecord.Description;
            status.ObjectID = statusRecord.ObjectID;
            status.ApplicationID = applicationID;
            return status;
        }

        public static DBObject CreateOrUpdateDBObject(long userID, long applicationID, DBObjectRecord dBObjectRecord, DBObject dBObject = null)
        {
            if (dBObject == null)
            {
                dBObject = new DBObject();
                dBObject.CreationDate = DateTime.Now;
                dBObject.CreatedBy = userID;
                dBObject.IsDeleted = false;
            }
            else
            {
                dBObject.ModifiedBy = userID;
                dBObject.LastUpdateDate = DateTime.Now;
                dBObject.IsDeleted = dBObjectRecord.IsDeleted;
            }
            dBObject.ID = dBObjectRecord.ID;
            dBObject.Name = dBObjectRecord.Name;
            dBObject.ApplicationID = applicationID;
            return dBObject;
        }

        private static Dictionary<long, long> DefaultStatusIDs = new Dictionary<long, long>();

        public static long GetDetfaultStatusID(ModonDBEntities contextEntities, long applicationID, long dbObjectID)
        {
            if (!DefaultStatusIDs.ContainsKey(dbObjectID))
            {
                var configManager = ConfigManager.GetInstance(applicationID);
                var statusRepository = new StatusRepository(contextEntities, applicationID);
                var defaultStatusID = statusRepository.All().Where(p => p.ObjectID == dbObjectID && p.IsDefault == true).Select(p => p.ID).FirstOrDefault();

                lock (statusRepository)
                {
                    if (!DefaultStatusIDs.ContainsKey(dbObjectID))
                        DefaultStatusIDs.Add(dbObjectID, defaultStatusID);
                }
            }
            if (DefaultStatusIDs.ContainsKey(dbObjectID))
                return DefaultStatusIDs[dbObjectID];
            return 0;
        }

        public static string GetStatusName(ModonDBEntities contextEntities, long applicationID, long statusID)
        {
            if (contextEntities == null)
                contextEntities = EFUnitOfWork.NewUnitOfWork("GetStatusName");
            var statusRepository = new StatusRepository(contextEntities, applicationID);
            var statusName = statusRepository.All().Where(p => p.ID == statusID).Select(p => p.Name).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(statusName))
                return statusName;
            return string.Format("Status{0}", statusID);
        }
    }
}