using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace  PMApp.Core.Services.Managers
{
    public class UserServiceManager
    {
        public static UserRecord[] ConvertFromCommonUser(List<Common_User> commonUsers, Language language)
        {
            UserRecord userRecord;
            var userRecords = new List<UserRecord>();
            foreach (var commonUser in commonUsers)
            {
                userRecord = new UserRecord(commonUser);
                userRecords.Add(userRecord);
            }
            return userRecords.ToArray();
        }

        public static Common_User ConvertToCommonUserRecord(long applicationID, UserRecord userRecord)
        {
            var commonUser = new Common_User();
            commonUser.RegisterDate = DateTime.Now;
            commonUser.Email = userRecord.Email.Trim();
            commonUser.FullName = userRecord.FullName.Trim();
            commonUser.Password = userRecord.Password;
            commonUser.Birthdate = userRecord.Birthdate;
            commonUser.Phone1 = userRecord.phone1;
            commonUser.RoleID = userRecord.RoleID;
            commonUser.IsActive = userRecord.IsActive;
            commonUser.IsDeleted = userRecord.IsDeleted;
            commonUser.Notes = userRecord.Notes;
            commonUser.AreaID = userRecord.AreaID;
                        return commonUser;
        }



        public static IEnumerable<UserRecord> RecursiveChildren(UserRecord[] items, long toplevelid, bool getParent = true)
        {
            List<UserRecord> inner = new List<UserRecord>();
            if (getParent)
            {
                inner.Add(items.FirstOrDefault(item => item.ID == toplevelid));
            }
            foreach (var t in items.Where(item => item.ParentAreaID == toplevelid))
            {
                inner.Add(t);
                inner = inner.Union(RecursiveChildren(items, t.ID)).ToList();
            }

            return inner;
        }

        public static bool IsValidAuthenticationToken(string authToken, out long userID)
        {
            userID = 0;
            var context = EFUnitOfWork.NewUnitOfWork("IsValidAuthenticationToken");
            var applicationID = ApplicationManager.GetApplicationIDFromAuthToken(authToken);
            var userDevice = new Common_UserDeviceRepository(context, applicationID).FirstOrDefault(p => p.AuthToken == authToken && p.IsLoggedIn && p.Common_User.IsActive);
            context.Dispose();
            if (userDevice != null)
            {
                userID = userDevice.Common_UserID;
                return true;
            }
            return false;
        }
    }
}