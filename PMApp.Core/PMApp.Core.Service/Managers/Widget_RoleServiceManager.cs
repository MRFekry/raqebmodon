using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System;

namespace  PMApp.Core.Services.Managers
{
    public class PMCore_Widget_RoleServiceManager
    {
        public static PMCore_Widget_Role CreateOrUpdatePMCore_Widget_Role(long userID, PMCore_Widget_RoleRecord pMCore_Widget_RoleRecord, PMCore_Widget_Role pMCore_Widget_Role = null)
        {
            if (pMCore_Widget_Role == null)
            {
                pMCore_Widget_Role = new PMCore_Widget_Role();
                pMCore_Widget_Role.CreationDate = DateTime.Now;
                pMCore_Widget_Role.CreatedBy = userID;
                pMCore_Widget_Role.IsDeleted = false;
            }
            else
            {
                pMCore_Widget_Role.ModifiedBy = userID;
                pMCore_Widget_Role.LastUpdateDate = DateTime.Now;
                pMCore_Widget_Role.IsDeleted = pMCore_Widget_RoleRecord.IsDeleted;
            }


            pMCore_Widget_Role.RoleID = pMCore_Widget_RoleRecord.RoleID;
            pMCore_Widget_Role.WidgetID = pMCore_Widget_RoleRecord.WidgetID;
            pMCore_Widget_Role.IsAllowed = pMCore_Widget_RoleRecord.IsAllowed;

            return pMCore_Widget_Role;
        }
    }
}