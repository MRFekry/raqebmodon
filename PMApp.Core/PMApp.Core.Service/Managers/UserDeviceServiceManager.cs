using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System;

namespace  Modon.Services.Managers
{
    public class UserDeviceServiceManager
    {

        public static Common_UserDevice CreateOrUpdateCommon_UserDevice(long applicationID, long userID, string lang, string authToken, DeviceRecord userDeviceRecord, Common_UserDevice common_UserDevice = null)
        {
            var configManager = ConfigManager.GetInstance(applicationID);
            if (common_UserDevice == null)
            {
                common_UserDevice = new Common_UserDevice();
                common_UserDevice.CreationDate = DateTime.Now;
                common_UserDevice.CreatedBy = userID;
                common_UserDevice.IsDeleted = false;
                common_UserDevice.ApplicationID = applicationID;
            }
            else
            {
                common_UserDevice.ModifiedBy = userID;
                common_UserDevice.LastUpdateDate = DateTime.Now;
                common_UserDevice.IsDeleted = userDeviceRecord.IsDeleted;
            }

            common_UserDevice.Common_UserID = userID;
            common_UserDevice.Lang = lang;

            //common_UserDevice.ID = userDeviceRecord.ID;
            common_UserDevice.DeviceName = userDeviceRecord.DeviceName;
            common_UserDevice.DeviceType = "";
            common_UserDevice.DeviceOSVersion = userDeviceRecord.DeviceOSVersion;
            common_UserDevice.DeviceIMEI = userDeviceRecord.DeviceIMEI;
            common_UserDevice.DeviceToken = userDeviceRecord.DeviceToken;
            common_UserDevice.DeviceEmail = userDeviceRecord.DeviceEmail;
            common_UserDevice.DeviceMobileNumber = userDeviceRecord.DeviceMobileNumber;

            common_UserDevice.AuthToken = authToken;
            common_UserDevice.AuthCreationDate = DateTime.Now;
            common_UserDevice.AuthExpirationDate = DateTime.Now.AddDays(configManager.AuthExpirationPeriodInDays);

            common_UserDevice.LastActiveDate = DateTime.Now;
            common_UserDevice.IsLoggedIn = true;
            

            return common_UserDevice;
        }
    }
}