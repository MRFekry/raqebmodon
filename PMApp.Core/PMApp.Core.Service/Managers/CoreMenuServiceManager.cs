using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;

namespace  PMApp.Core.Services.Managers
{
    public class CoreMenuServiceManager
    {
        public static List<CoreMenuRecord> ConvertFromCoreMenu(List<PMCore_Menu> coreMenuRecords, PMApp.Core.Helpers.Language language, long roleID)
        {
            CoreMenuRecord menuRecord;
            var coreMenus = new List<CoreMenuRecord>();
            foreach (var coreMenu in coreMenuRecords)
            {
                menuRecord = new CoreMenuRecord();
                menuRecord.ID = coreMenu.ID;
                menuRecord.Name = coreMenu.Name;
                menuRecord.Tip = coreMenu.Tip;
                menuRecord.IsAllowed = coreMenu.IsDeleted;
                menuRecord.ParentId = coreMenu.ParentID;
                menuRecord.RoleId = roleID;
                menuRecord.InitialValues = coreMenu.InitialValues;
                menuRecord.SenchaMenuId = coreMenu.SenchaMenuId;
                menuRecord.IconURL = coreMenu.IconURL;
                menuRecord.UrlRoute = coreMenu.UrlRoute;
                menuRecord.IconClass = coreMenu.IconClass;
                menuRecord.OrderID = coreMenu.OrderID ?? 0;
                menuRecord.ApplicationID = coreMenu.ApplicationID;
                coreMenus.Add(menuRecord);
            }
            return coreMenus;
        }


        public static List<CoreMenuRecord> RecursiveChildren(List<CoreMenuRecord> items, long? topLevelID)
        {
            var inner = new List<CoreMenuRecord>();
            var grps = items.Where(item => (item.ParentId == topLevelID) || (!topLevelID.HasValue && !item.ParentId.HasValue));
            if (grps == null || grps.Count() == 0)
                return null;
            foreach (var itm in grps)
            {
                var childItms = RecursiveChildren(items, itm.ID);
                if (childItms != null && childItms.Count > 0)
                {
                    itm.InnerCoreMenuRecordList = childItms;
                }
                inner.Add(itm);
            }
            return inner.OrderBy(p => p.OrderID).ToList();
        }

        public static void InjectReportsMenu(ModonDBEntities contextEntities, CoreMenuRequest request, List<PMCore_Menu> coreMenuRecords)
        {
            var reportsMenuID = coreMenuRecords.Where(c => c.Name.ToLower() == "reports").Select(c => c.ID).FirstOrDefault();
            if (reportsMenuID > 0)
            {
                var maxMenuID = coreMenuRecords.Max(c => c.ID);
                var widgets = new PMCore_WidgetRepository(contextEntities, request.ApplicationID).Where(c => !c.IsDeleted).ToList();
                for (int i = 0; i < widgets.Count; i++)
                {
                    coreMenuRecords.Add(new PMCore_Menu
                    {
                        ID = ++maxMenuID,
                        Enable = true,
                        Name = widgets[i].Name,
                        ParentID = reportsMenuID,
                        IconClass = "antiFa",
                        UrlRoute = string.Format(ConfigManager.GetInstance(request.ApplicationID).MenuWidgetReportUrl, widgets[i].ID)
                    });
                }
            }
        }
    }
}
