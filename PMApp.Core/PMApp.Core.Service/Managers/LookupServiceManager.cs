using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;

namespace  PMApp.Core.Services.Managers
{
    public class LookupServiceManager
    {
        public static void ConvertToPOSLookup(ref PMCore_Lookup Lookup, long nextID, LookupRecord LookupRecord)
        {
            Lookup.LookupText = LookupRecord.LookupText;

            if (nextID != 0)
            {
                Lookup.LookupName = LookupRecord.LookupName;
                Lookup.Enable = true;
                Lookup.LookupKey = nextID.ToString();
            }
        }

        public static LookupRecord[] ConvertFromPOSLookup(bool showLSText, List<PMCore_Lookup> posLookupRecords, PMApp.Core.Helpers.Language language)
        {
            LookupRecord LookupRecord;
            var Lookups = new List<LookupRecord>();
            foreach (var posLookupRecord in posLookupRecords)
            {
                LookupRecord = new LookupRecord();
                LookupRecord.ID = posLookupRecord.ID;
                LookupRecord.LookupKey = posLookupRecord.LookupKey;
                LookupRecord.LookupName = posLookupRecord.LookupName;
                LookupRecord.ApplicationID = posLookupRecord.ApplicationID;
                if (showLSText)
                {
                    LookupRecord.LookupText = posLookupRecord.LookupText;
                }
                else
                {
                    LookupRecord.LookupText = posLookupRecord.LookupText;
                }
                Lookups.Add(LookupRecord);
            }
            return Lookups.ToArray();
        }

        public static Common_Resource CreateOrUpdateCommon_Resource(long userID, ResourceRecord resourceRecord, Common_Resource common_Resource = null)
        {
            if (common_Resource == null)
            {
                common_Resource.CreatedBy = userID;
                common_Resource.CreationDate = DateTime.Now;
            }
            else
            {
                common_Resource.ModifiedBy = userID;
                common_Resource.LastUpdateDate = DateTime.Now;
            }
            common_Resource.ResourceValue = resourceRecord.ResourceValue;
            common_Resource.ResourceLang = resourceRecord.ResourceLang;
            common_Resource.IsDeleted = resourceRecord.IsDeleted;

            return common_Resource;
        }

        public static PMCore_Configs CreateOrUpdatePMCore_Configs(long userID, ConfigRecord configsRecord, PMCore_Configs pMCore_Configs = null)
        {
            if (pMCore_Configs == null)
            {
                pMCore_Configs.CreationDate = DateTime.Now;
                pMCore_Configs.CreatedBy = userID;
            }
            else
            {
                pMCore_Configs.ModifiedBy = userID;
                pMCore_Configs.LastUpdateDate = DateTime.Now;
            }
            pMCore_Configs.ConfValue = configsRecord.ConfValue;
            pMCore_Configs.Decription = configsRecord.Decription;
            pMCore_Configs.IsDeleted = configsRecord.IsDeleted;

            return pMCore_Configs;
        }
    }
}
