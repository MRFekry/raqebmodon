using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;

namespace  PMApp.Core.Services.Managers
{
    public class UtilityServiceManager
    {
        private const string DATEFORMATE_ddMMyyyy = "dd/mm/yyyy";
        private const int SECOND = 1;
        private const int MINUTE = 60 * SECOND;
        private const int HOUR = 60 * MINUTE;
        private const int DAY = 24 * HOUR;
        private const int MONTH = 30 * DAY;


        public static string UIToRelativeDate(DateTime dt, string lang = "")
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return ToRelativeDate(applicationID, dt, lang);
        }

        public static string ToRelativeDate(long applicationID, DateTime dt, string lang = "")
        {
            string suffix = "Ago";
            if (dt > DateTime.Now)
                suffix = "Comming";
            var ts = new TimeSpan(Math.Abs(DateTime.UtcNow.Ticks - dt.Ticks));
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * MINUTE)
                return ResourceManager.GetInstance(applicationID).GetResource("JustNow", lang);

            if (delta < 2 * MINUTE)
                return ResourceManager.GetInstance(applicationID).GetResource("AMinute" + suffix, lang);

            if (delta < 45 * MINUTE)
                return string.Format(ResourceManager.GetInstance(applicationID).GetResource("XMinutes" + suffix, lang), ts.Minutes);

            if (delta < 90 * MINUTE)
                return ResourceManager.GetInstance(applicationID).GetResource("AnHour" + suffix, lang);

            if (delta < 24 * HOUR)
                return string.Format(ResourceManager.GetInstance(applicationID).GetResource("XHours" + suffix, lang), ts.Hours);

            if (delta < 48 * HOUR)
            {
                if (dt > DateTime.Now)
                    return ResourceManager.GetInstance(applicationID).GetResource("Tomorrow", lang);
                else
                    return ResourceManager.GetInstance(applicationID).GetResource("Yesterday", lang);
            }

            if (delta < 30 * DAY)
                return string.Format(ResourceManager.GetInstance(applicationID).GetResource("XDays" + suffix, lang), ts.Days);

            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return string.Format(ResourceManager.GetInstance(applicationID).GetResource("XMonths" + suffix, lang), months);
                //months <= 1 ? ResourceManager.GetInstance(applicationID).GetResource("Amonth" + suffix, lang) : 
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return  string.Format(ResourceManager.GetInstance(applicationID).GetResource("XYears" + suffix, lang), years);
                //years <= 1 ? ResourceManager.GetInstance(applicationID).GetResource("AYear" + suffix, lang) :
            }
        }

        public static ServiceOperationRoleAccessRecord[] ConvertFromServiceOperationRoleAccess(long applicationID, List<PMCore_ServiceOperationRoleAccess> pmCoreServiceOperationRoleAccess)
        {
            var unitOfWork = EFUnitOfWork.NewUnitOfWork("ConvertFromServiceOperationRoleAccess");
            var lst = new List<ServiceOperationRoleAccessRecord>();
            var dBObjectLst = new DBObjectRepository(unitOfWork, applicationID).All().ToList();
            for (int i = 0; i < pmCoreServiceOperationRoleAccess.Count(); i++)
            {
                var serviceOperationRoleAccess = new ServiceOperationRoleAccessRecord();
                serviceOperationRoleAccess.ID = pmCoreServiceOperationRoleAccess[i].ID;
                serviceOperationRoleAccess.IsAllowed = pmCoreServiceOperationRoleAccess[i].IsAllowed;
                serviceOperationRoleAccess.RoleID = pmCoreServiceOperationRoleAccess[i].RoleId;
                serviceOperationRoleAccess.ApplicationID = pmCoreServiceOperationRoleAccess[i].ApplicationID;
                serviceOperationRoleAccess.ServiceOperationID = pmCoreServiceOperationRoleAccess[i].ServiceOperationId;
                var serviceOperation = ServiceOperationManager.GetInstance(applicationID).GetServiceOperation(pmCoreServiceOperationRoleAccess[i].ServiceOperationId);
                if (serviceOperation != null)
                {
                    serviceOperationRoleAccess.ServiceOperationName = serviceOperation.OperationName;
                    serviceOperationRoleAccess.ServiceName = serviceOperation.ServiceName;
                    serviceOperationRoleAccess.ServiceOperationTitle = serviceOperation.OperationTitle;
                    serviceOperationRoleAccess.ObjectID = serviceOperation.ObjectID;
                    if (serviceOperation.ObjectID != null)
                    {
                        serviceOperationRoleAccess.DBObjectName = dBObjectLst.Where(p => p.ID == serviceOperation.ObjectID).Select(p => p.Name).FirstOrDefault();
                    }
                }
                lst.Add(serviceOperationRoleAccess);
            }
            var result = lst.OrderBy(p => p.ServiceName).ToArray();
            unitOfWork.Dispose();
            return result;
        }

        public static PMCore_ServiceOperationRoleAccess[] ConvertToPMCoreServiceOperationRoleAccess(ApplicationRoleRecord applicationRole)
        {
            var pmCoreServiceOperationRoleAccesses = new List<PMCore_ServiceOperationRoleAccess>();
            PMCore_ServiceOperationRoleAccess pmCoreServiceOperationRoleAccess;
            if (applicationRole.ServiceOperationRoleAccesses.Any())
            {
                foreach (var coreServiceOperationRoleAccess in applicationRole.ServiceOperationRoleAccesses)
                {
                    pmCoreServiceOperationRoleAccess = new PMCore_ServiceOperationRoleAccess();
                    pmCoreServiceOperationRoleAccess.IsAllowed = coreServiceOperationRoleAccess.IsAllowed;
                    pmCoreServiceOperationRoleAccess.ServiceOperationId = coreServiceOperationRoleAccess.ServiceOperationID;

                    pmCoreServiceOperationRoleAccesses.Add(pmCoreServiceOperationRoleAccess);
                }
            }
            return pmCoreServiceOperationRoleAccesses.ToArray();
        }

        public static PMCore_Role ConvertToPMCoreRoleRecord(ApplicationRoleRecord applicationRole)
        {
            var pmCoreRole = new PMCore_Role();
            pmCoreRole.Name = applicationRole.Name;
            pmCoreRole.Notes = applicationRole.Notes;
            pmCoreRole.ParentID = applicationRole.ParentId;
            pmCoreRole.IsAdmin = applicationRole.IsAdmin;
            pmCoreRole.IsActive = applicationRole.IsActive;
            pmCoreRole.IsDeleted = false;

            return pmCoreRole;
        }

        public static List<ServiceOperationRecord> ConvertFromPMCoreServiceOperation(List<PMCore_ServiceOperation> seviceOperations, Language language)
        {
            ServiceOperationRecord serviceOperation;
            var serviceOperations = new List<ServiceOperationRecord>();
            foreach (var pmCoreServiceOperation in seviceOperations)
            {
                serviceOperation = new ServiceOperationRecord();
                serviceOperation.ID = pmCoreServiceOperation.ID;
                serviceOperation.ApplicationID = pmCoreServiceOperation.ApplicationID;
                serviceOperation.IsAnonymous = pmCoreServiceOperation.IsAnonymous;
                serviceOperation.OperationName = pmCoreServiceOperation.OperationName;
                serviceOperation.ServiceName = pmCoreServiceOperation.ServiceName;
                serviceOperation.OperationTitle = pmCoreServiceOperation.OperationTitle;
                serviceOperations.Add(serviceOperation);
            }
            return serviceOperations;
        }

        public static CoreMenuRecord[] ConvertFromPMCoreMenuToCoreMenu(List<PMCore_Role_Menu> coreRole_MenuList, Language language)
        {
            var roleMenuList = new List<CoreMenuRecord>();
            for (int i = 0; i < coreRole_MenuList.Count; i++)
            {
                roleMenuList.Add(new CoreMenuRecord()
                {
                    ID = coreRole_MenuList[i].MenuID,
                    IsAllowed = coreRole_MenuList[i].IsAllowed,
                    ApplicationID = coreRole_MenuList[i].ApplicationID
                });
            }
            return roleMenuList.ToArray();
        }

        public static UserLocation CreateOrUpdateUserLocation(long userID, string channelName, UserLocationRecord userLocationRecord, UserLocation userLocation = null)
        {
            if (userLocation == null)
            {
                userLocation = new UserLocation();
                userLocation.CreationDateTime = DateTime.Now;
                userLocation.SubmitDateTime = DateTime.Now;
                userLocation.UserID = userID;
                userLocation.IsOnline = true;
            }
            else
            {
                userLocation.SubmitDateTime = DateTime.Now;
            }

            userLocation.AquirityDistance = userLocationRecord.AquirityDistance;
            userLocation.BataryStatus = userLocationRecord.BataryStatus;
            userLocation.DeviceID = userLocationRecord.DeviceID;
            userLocation.DirectionAngle = userLocationRecord.DirectionAngle;
            userLocation.Latitude = userLocationRecord.Latitude;
            userLocation.Longitude = userLocationRecord.Longitude;
            userLocation.NetworkStatus = userLocationRecord.NetworkStatus;
            userLocation.Speed = userLocationRecord.Speed;
            userLocation.ChannelID = ((Channel)Enum.Parse(typeof(Channel), channelName)).GetHashCode();

            userLocation.Address = userLocationRecord.Address;

            return userLocation;
        }

    }
}
