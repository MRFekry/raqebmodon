using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System;

namespace  Modon.Services.Managers
{
    public class NotificationTypeServiceManager
    {
        public static NotificationType CreateOrUpdateNotificationType(long userID, NotificationTypeRecord notificationTypeRecord, NotificationType notificationType = null)
        {
            if (notificationType == null)
            {
                notificationType.CreatedBy = userID;
                notificationType.CreationDate = DateTime.Now;

            }
            else
            {
                notificationType.ModifiedBy = userID;
                notificationType.LastUpdateDate = DateTime.Now;

            }
            notificationType.ID = notificationTypeRecord.ID;
            notificationType.DBObjectID = notificationTypeRecord.DBObjectID;
            notificationType.Name = notificationTypeRecord.Name;
            notificationType.Description = notificationTypeRecord.Description;
            notificationType.IsDeleted = notificationTypeRecord.IsDeleted;

            return notificationType;
        }
    }
}