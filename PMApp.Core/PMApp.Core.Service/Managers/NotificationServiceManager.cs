using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System;

namespace  PMApp.Core.Services.Managers
{
    public class NotificationServiceManager
    {
        public static Notification CreateOrUpdateNotification(long applicationID, long userID, NotificationRecord notificationRecord, Notification notification = null)
        {
            if (notification == null)
            {
                notification = new Notification();
                notification.CreationDate = DateTime.Now;
                notification.CreatedBy = userID;

            }
            else
            {
                notification.ModifiedBy = userID;

            }
            notification.ID = notificationRecord.ID;
            notification.FromUserID = notificationRecord.FromUserID;
            notification.ToUserID = notificationRecord.ToUserID;
            notification.IsSent = notificationRecord.IsSent;
            notification.IsReceived = notificationRecord.IsReceived;
            notification.IsSeen = notificationRecord.IsSeen;
            notification.NotificationMessage = notificationRecord.NotificationMessage;
            notification.NotificationData = notificationRecord.NotificationData;
            notification.NotificationTypeID = notificationRecord.NotificationTypeID;
            notification.LastUpdateDate = notificationRecord.LastUpdateDate;
            notification.IsDeleted = notificationRecord.IsDeleted;
            notification.ApplicationID = applicationID;

            return notification;
        }
    }
}