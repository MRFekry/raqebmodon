using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

namespace  PMApp.Core.Services.Managers
{
    public class WidgetServiceManager
    {

        public static Parameter[] GetSPParameters(long userID, string spName, ReportParam[] filterParams)
        {
            var spParameters = new Parameter[filterParams.Length];
            for (int i = 0; i < filterParams.Length; i++)
            {
                spParameters[i] = new Parameter(filterParams[i].Name);
                spParameters[i].DefaultValue = filterParams[i].Value.ToString();
            }
            return spParameters;
        }

        public static DataTable ExecuteFilterParameterProcedure(ModonDBEntities context, long applicationID, long userID, string filterSPName)
        {
            try
            {
                var spFilterParamsDT = DBHelper.GetSPParameters(context.Database.Connection.ConnectionString, filterSPName, applicationID);

                var parmsList = new List<Parameter>();
                for (int i = 0; i < spFilterParamsDT.Rows.Count; i++)
                {
                    var val = GetSPParameterValue(context, userID, applicationID, spFilterParamsDT.Rows[i]);
                    parmsList.Add(new Parameter
                    {
                        Name = spFilterParamsDT.Rows[i]["PARAMETER_NAME"].ToString(),
                        DefaultValue = val
                    });
                }
                return DBHelper.ExecuteProcedure(context.Database.Connection.ConnectionString, filterSPName, parmsList.ToArray());
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
                return null;
            }
        }

        private static string GetSPParameterValue(ModonDBEntities context, long userID, long applicationID, DataRow spParamsDTRow)
        {
            return GetParameterValue(context, userID, applicationID, Convert.ToString(spParamsDTRow["StaticValue"]), Convert.ToString(spParamsDTRow["StaticValue"]), Convert.ToString(spParamsDTRow["QueryValue"]));
        }

        public static string GetParameterValue(ModonDBEntities context, long userID, long applicationID, string staticValue, string sessionValue, string queryValue)
        {
            // try value as Static
            if (!string.IsNullOrWhiteSpace(staticValue))
            {
                switch (staticValue)
                {
                    case "YESTERDAY_YMD":
                        return DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
                    case "MONTHAGO_YMD":
                        return DateTime.Today.AddMonths(-1).ToString("yyyy-MM-dd");
                    case "WEEKAGO_YMD":
                        return DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd");
                    case "TOMORROW_YMD":
                        return DateTime.Today.AddDays(1).ToString("yyyy-MM-dd");
                    case "CURRENT_APPLICATIONID":
                        return applicationID.ToString();
                    case "CURRENT_USER":
                        return userID.ToString();
                }
                return staticValue;
            }
            else if (!string.IsNullOrWhiteSpace(queryValue))
            {
                var parmsList = new List<Parameter>();
                if (queryValue.ToLower().Contains("puserid"))
                {
                    parmsList.Add(new Parameter
                    {
                        Name = "puserid",
                        DefaultValue = userID.ToString()
                    });
                }
                return Convert.ToString(DBHelper.ExecuteScaler(context.Database.Connection.ConnectionString, queryValue, parmsList.ToArray()));
            }
            return string.Empty;
        }

        public static void FillWidgetDataRecord(ModonDBEntities context, PMCore_Widget widget, long applicationID, List<WidgetDataRecord> widgetDataRecords, SBSResponse sbsResponse)
        {
            //TODO
            throw new NotImplementedException();
        }

        public static void FillWidgetDataRecord(ModonDBEntities context, long userID, long applicationID, PMCore_Widget widget, List<WidgetDataRecord> widgetDataRecords, System.Data.DataTable widgetDT)
        {
            for (int r = 0; r < widgetDT.Rows.Count; r++)
            {
                var recordData = new List<ReportField>(widgetDT.Columns.Count);
                for (int i = 0; i < widgetDT.Columns.Count; i++)
                {
                    var columName = widgetDT.Columns[i].ColumnName;
                    if (widgetDT.Columns.Contains(columName))
                    {
                        recordData.Add(new ReportField(columName, widgetDT.Rows[r][columName], widgetDT.Columns[columName].DataType.Name));
                    }
                }
                var record = new WidgetDataRecord(recordData, applicationID);
                record.ApplicationID = applicationID;
                widgetDataRecords.Add(record);
            }
        }
    }
}