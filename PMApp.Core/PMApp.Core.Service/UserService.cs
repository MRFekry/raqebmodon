using Modon.Services.Managers;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Core.Interfaces;
using PMApp.Core.NotificationsSystem;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace PMApp.Core.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class UserService : BaseService, IUser
    {
        private const string serviceName = "UserService";
        public const string operationName_DoLogin = "DoLogin";
        public const string operationName_DoSocialLogin = "DoSocialLogin";
        public const string operationName_DoLogout = "DoLogout";
        public const string operationName_DoForgetPassword = "DoForgetPassword";
        public const string operationName_DoChangePassword = "DoChangePassword";
        public const string operationName_DoUserInsertOperation = "DoUserInsertOperation";
        public const string operationName_DoUserUpdateOperation = "DoUserUpdateOperation";
        public const string operationName_DoUserActivateOperation = "DoUserActivateOperation";
        public const string operationName_DoUserDeleteOperation = "DoUserDeleteOperation";
        public const string operationName_DoUserListInquiry = "DoUserListInquiry";
        public const string operationName_DoCacheVersionList = "DoCacheVersionList";
        public const string operationName_DoForgetPasswordChange = "DoForgetPasswordChange";
        public const string operationName_DoUserProfile = "DoUserProfile";
        public const string operationName_DoUserProfileUpdate = "DoUserProfileUpdate";
        public const string operationName_DoForgetPasswordChoice = "DoForgetPasswordChoice";
        private int hideStringPercentage = 50;

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        public LoginResponse DoLogin(LoginRequest req)
        {
            var res = new LoginResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLogin, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLogin, false, false, (LoginRequest request, LoginResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                    request.Email = request.Email.ToLower().Trim(); // Lower Case the UserName


                    var user = repository.FirstOrDefault(p => !p.IsDeleted &&
                        (
                            ( (p.Email == request.Email ))                        ));
                    if (user == null || request.Password != user.Password
                        //|| !PasswordHelper.ValidatePassword(request.Password, user.Password)
                        )
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserNameOrPassword);
                    }
                    else
                    {
                        request.UserRoleID = user.RoleID;
                        response.UserId = user.ID;

                        if (!user.IsActive)
                        {
                            response.SetBaseResponse(false, ResponseStatus.Inactive, MessageKey.UserInactive);
                            return response;
                        }
                        else
                        {
                                request.ApplicationCode = request.AppCode;
                                request.UserRoleID = user.RoleID;
                                response.UserId = user.ID;
                                var userDeviceRepository = new Common_UserDeviceRepository(CurrentContextEntities, request.ApplicationID);

                                // Generate Authentication Token
                                var userDeviceList = userDeviceRepository.All().Where(p => p.Common_UserID == user.ID).ToList();

                                var userDevice = userDeviceList.FirstOrDefault(p => p.DeviceIMEI == request.DeviceRecord.DeviceIMEI);

                                var isNewDevice = userDevice == null;

                                if (user.PMCore_Role == null)
                                    user.PMCore_Role = new PMCore_RoleRepository(CurrentContextEntities, request.ApplicationID).FirstOrDefault(p => p.ID == user.RoleID);

                                if (!user.PMCore_Role.AllowMultiLogin)
                                {
                                    for (int i = 0; i < userDeviceList.Count; i++)
                                    {
                                        if (userDeviceList[i].DeviceIMEI != request.DeviceRecord.DeviceIMEI)
                                            userDeviceList[i].IsLoggedIn = false;
                                    }
                                }

                                userDevice = UserDeviceServiceManager.CreateOrUpdateCommon_UserDevice(request.ApplicationID, user.ID, request.Lang.ToString(), string.Empty, request.DeviceRecord, userDevice);
                                if (isNewDevice)
                                {
                                    userDeviceRepository.Add(userDevice);
                                    userDeviceRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                                }

                                // Generate Authentication Token
                                var authToken = SecurityHelper.GenerateAuthenticationToken(user.ID, request.IP, request.LoginChannelName, userDevice.ID, user.RoleID, request.AppCode);
                                userDevice.IsLoggedIn = true;
                                userDevice.AuthToken = authToken;
                                userDevice.AuthExpirationDate = DateTime.Now.AddDays(ConfigManager.GetInstance(request.ApplicationID).AuthExpirationPeriodInDays);
                                userDeviceRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                                repository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                                // fill the response
                                response.Email = user.Email;
                                response.FullName = user.FullName;
                                response.Notes = user.Notes;
                                response.RoleId = user.RoleID;
                                response.RoleName = user.PMCore_Role.Name;
                                response.AuthToken = authToken;
                                response.SetBaseResponse(true, ResponseStatus.Active, MessageKey.LoginSuccessfully);
                        }
                    }
                    return response;
                });
            }
            return res;
        }

        public LogoutResponse DoLogout(LogoutRequest req)
        {
            var res = new LogoutResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLogout, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLogout, true, false, (LogoutRequest request, LogoutResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    //var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                    //var user = repository.FirstOrDefault(p => p.ID == request.UserID);
                    //user.AuthenticationIP = null;
                    //user.AuthenticationToken = null;

                    var userDeviceRepository = new Common_UserDeviceRepository(CurrentContextEntities, request.ApplicationID);

                    Common_UserDevice userDevice = null;

                    // if request from web portal
                    if (request.InnerCall)
                    {
                        var webChannelName = ConfigManager.GetInstance(request.ApplicationID).WebChannelName;
                        var deviceName = ConfigManager.GetInstance(request.ApplicationID).WebChannelName;
                        userDevice = userDeviceRepository.FirstOrDefault(p => p.Common_UserID == request.UserID
                        && p.DeviceType == webChannelName && p.DeviceName == deviceName);

                    }
                    else
                    {
                        userDevice = userDeviceRepository.FirstOrDefault(p => p.ID == request.DeviceID);

                    }

                    if (userDevice != null)
                    {
                        userDevice.IsLoggedIn = false;
                        userDevice.AuthIP = null;
                        userDevice.AuthToken = null;
                        userDevice.ModifiedBy = request.UserID;
                        userDevice.LastUpdateDate = DateTime.Now;
                    }


                    userDeviceRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                    //repository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.LogoutSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public LoginResponse DoSocialLogin(LoginRequest req)
        {
            var res = new LoginResponse(req.Lang, req.ApplicationID);
            //set customer email and social login password to pass login request validation
            if (string.IsNullOrWhiteSpace(req.Email))
                req.Email = ConfigManager.GetInstance(req.ApplicationID).DefaultEmail;
            req.Email = req.Email.ToLower();
            req.Password = ConfigManager.GetInstance(req.ApplicationID).DefaultSocialPassword;

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoSocialLogin, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoSocialLogin, false, false, (LoginRequest request, LoginResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
            {
                var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                var query = repository.Where(p => !p.IsDeleted);
                Common_User user = null;

                //if (!string.IsNullOrWhiteSpace(request.FacebookUserID))
                //{
                //    user = query.FirstOrDefault(p => p.FacebookUserID == request.FacebookUserID);
                //    if (user == null && request.Email != ConfigManager.GetInstance(request.ApplicationID).DefaultEmail)
                //    {
                //        user = query.FirstOrDefault(p => p.Email == request.Email && (p.FacebookUserID == null || p.FacebookUserID.Length == 0));
                //        if (user != null)
                //        {
                //            user.FacebookUserID = request.FacebookUserID;
                //            repository.Commit();
                //        }
                //    }
                //}

                //if (user == null)
                //{
                //    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.FacebookUserIDNotExisted);
                //}
                //else
                //{
                //    request.Email = user.Email;
                //    request.Password = user.Password;
                //    var userService = new UserService();
                //    var loginResponse = userService.DoLogin(request);
                //    response = loginResponse;

                //}
                return response;
            });
            }
            return res;
        }

        public ForgetPasswordResponse DoForgetPassword(ForgetPasswordRequest req)
        {
            var res = new ForgetPasswordResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoForgetPassword, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoForgetPassword, true, false, (ForgetPasswordRequest request, ForgetPasswordResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                    request.Email = request.Email.ToLower();

                    var user = repository.FirstOrDefault(p => p.Email == request.Email);
                    if (user == null)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidEmail);
                    }
                    else if (user.IsDeleted || !user.IsActive)
                    {
                        response.SetBaseResponse(false, ResponseStatus.Inactive, MessageKey.UserInactive);
                    }
                    else
                    {
                        response.SetBaseResponse(true, ResponseStatus.Active, MessageKey.ForgetPasswordSuccessfully);
                        response.Email = response.IsPhone ? UIHelper.HideStringPart(user.Email, hideStringPercentage) : user.Email;
                        response.OriginalEmail = user.Email;
                        response.Success = true;
                    }
                    return response;
                });
            }
            return res;
        }

        public ForgetPasswordResponse DoForgetPasswordChoice(ForgetPasswordRequest req)
        {
            var res = new ForgetPasswordResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoForgetPasswordChoice, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoForgetPasswordChoice, true, false, (ForgetPasswordRequest request, ForgetPasswordResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);

                    var user = repository.FirstOrDefault(p => p.Email == request.Email);
                    if (user == null)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidEmail);
                    }
                    else if (user.IsDeleted || !user.IsActive)
                    {
                        response.SetBaseResponse(false, ResponseStatus.Inactive, MessageKey.UserInactive);
                    }

                    else
                    {
                        //Generate new code
                        Random generator = new Random();
                        user.Notes = generator.Next(0, 1000000).ToString("D6");


                        //Url For Activation
                        var changePasswordCode = string.Format("{0}-{1}", user.Notes, user.ID);
                        var encryptedchangePasswordCode = SecurityHelper.EncryptedActivationCode(changePasswordCode);
                        var url = UIHelper.ResolveServerUrl("~/User/ChangePassword?ac=" + encryptedchangePasswordCode);

                        //var newPassword = PasswordHelper.GeneratePassword(3, 3, 3);
                        // generate a password then hash it
                        var systemUserID = ConfigManager.GetInstance(request.ApplicationID).SystemUserID;

                        var subjectDic = PMExtentions.ToDictionary(request.Lang.ToString(), ResourceManager.GetInstance(request.ApplicationID).GetResource("SendMail.ForgetPassword.Subject", request.Lang.ToString()));
                        var bodyDic = PMExtentions.ToDictionary(request.Lang.ToString(), string.Format(ResourceManager.GetInstance(request.ApplicationID).GetResource("SendMail.ForgetPassword.Body", request.Lang.ToString()), user.FullName, url));
                        var bodyPushDic = PMExtentions.ToDictionary(request.Lang.ToString(), string.Format(ResourceManager.GetInstance(request.ApplicationID).GetResource("SendMail.ForgetPassword.BodyPush", request.Lang.ToString()), user.FullName, changePasswordCode));

                        var enumNotificationType = EnumNotificationType.EmailOnly;
                        var secondEmail = string.Empty;


                        NotificationManager.GetInstance(request.ApplicationID).Notify(systemUserID, user.ID, enumNotificationType, subjectDic, bodyDic, bodyPushDic, 0, null, null, secondEmail, false);

                        response.SetBaseResponse(true, ResponseStatus.Active, MessageKey.ForgetPasswordSuccessfully);

                        repository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                        response.Success = true;
                    }
                    return response;
                });
            }
            return res;
        }

        public UserResponse DoUserInsertOperation(UserRequest req)
        {
            var res = new UserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserInsertOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserInsertOperation, true, false, (UserRequest request, UserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.UserRecord != null)
                    {
                        var userRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                        request.UserRecord.Email = request.UserRecord.Email.ToLower().Trim();

                        if (request.UserRecord.phone1 !=0)
                        {
                            request.UserRecord.phone1 = request.UserRecord.phone1;
                        }

                        var user = userRepository.FirstOrDefault(p =>
                            (p.Email == request.UserRecord.Email));
                        if (user != null)
                        {
                            if (request.UserRecord.phone1 == user.Phone1)
                            {
                                response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.MobileAlreadyRegistered);

                            }
                            else if (user.Email == request.UserRecord.Email)
                            {
                                response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.EmailAlreadyRegistered);

                            }
                            else
                            {
                                response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.UserAlreadyRegistered);
                            }
                            return response;
                        }
                        {
                            #region Insert User to DB
                            var commonUser = UserServiceManager.ConvertToCommonUserRecord(request.ApplicationID, request.UserRecord);
                            //GenerateCode
                            Random generator = new Random();
                            commonUser.EmployeeCode = generator.Next(0, 1000000).ToString("D6");
                            commonUser.RegisterDate = DateTime.Now;
                            commonUser.CreationDate = DateTime.Now;
                            //add to config
                            commonUser.IsActive = false;// user is inactive by default
                            userRepository.Add(commonUser);
                           
                            var file = request.UserRecord.ImageFile;

                            if (file != null && file.ContentLength > 0)
                            {
                                var mediaRepository = new WebMediaRepository(CurrentContextEntities, request.ApplicationID);
                                var targetObjectID_User = ConfigManager.GetInstance(request.ApplicationID).UserDBObjectID;

                                
                                var imagePath = string.Format("~/UploadedImages/UserIDs/{0}-{1}.jpg", commonUser.ID, Guid.NewGuid().ToString());
                                file.SaveAs(UIHelper.MapPath(imagePath));
                                var media = new WebMedia
                                {
                                    CreationDate = DateTime.Now,
                                    Url = imagePath,
                                    ObjectID = targetObjectID_User,
                                    TargetObjectID = commonUser.ID,
                                    WebMediaTypeID = ConfigManager.GetInstance(request.ApplicationID).Picture_WebMediaTypeID,
                                    IsActive = true
                                };
                                mediaRepository.Add(media);
                            }

                            userRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            #endregion
                            request.UserRecord.ID = commonUser.ID;

                            //Url For Activation
                            var activationCode = string.Format("{0}-{1}", commonUser.EmployeeCode, commonUser.ID);
                            var encryptedActivationCode = SecurityHelper.EncryptedActivationCode(activationCode);
                            var url = UIHelper.ResolveServerUrl("~/User/ActivateAccount?ac=" + encryptedActivationCode);


                            if (EmailLogger.TrySendMail(request.ApplicationID, commonUser.Email, ResourceManager.GetInstance(request.ApplicationID).GetResource("SendMail.UserInsert.Subject", request.Lang.ToString()), string.Format(ResourceManager.GetInstance(request.ApplicationID).GetResource("SendMail.UserInsert.Body", request.Lang.ToString()), commonUser.EmployeeCode, url)))
                            {
                                response.SetBaseResponse(true, ResponseStatus.Active, MessageKey.UserAddedSuccessfuly);
                                response.Success = true;
                            }
                            else
                            {
                                //Rollback User
                                commonUser.IsDeleted = true;
                                commonUser.Email += "-Deleted";
                                commonUser.Phone1 += 200;
                                userRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                                throw new Exception(string.Format("Faild to send mail to user id '{0}'", commonUser.Email));
                            }

                            response.Success = true;
                        }
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserObject);
                    }
                    return response;
                });
            }
            return res;
        }

        public UserResponse DoUserUpdateOperation(UserRequest req)
        {
            var res = new UserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserUpdateOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserUpdateOperation, true, false, (UserRequest request, UserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {

                    if (request.UserRecord != null)
                    {
                        var userObject = ConfigManager.GetInstance(request.ApplicationID).UserDBObjectID;
                        var userRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                        var commonUser = userRepository.FirstOrDefault(p => p.ID == request.UserRecord.ID);

                        // UserName can't be changed
                        if (commonUser.Email != request.UserRecord.Email)
                        {
                            response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.UserNameCantBeChanged);
                        }
                        else
                        {
                            
              
                            commonUser.IsActive = request.UserRecord.IsActive;
                            commonUser.FullName = request.UserRecord.FullName;
                            if (!string.IsNullOrWhiteSpace(request.UserRecord.Password))
                                commonUser.Password = request.UserRecord.Password;// PasswordHelper.CreateHash(request.UserRecord.Password);

                            if (request.UserRecord.Birthdate != null)
                                commonUser.Birthdate = request.UserRecord.Birthdate;

                            if (request.UserRecord.RoleID > 0)
                                commonUser.RoleID = request.UserRecord.RoleID;

                            if (request.UserRecord.CountryID > 0)
                                commonUser.AreaID = request.UserRecord.AreaID;



                            var file = request.UserRecord.ImageFile;

                            if (file != null && file.ContentLength > 0)
                            {
                                var mediaRepository = new WebMediaRepository(CurrentContextEntities, request.ApplicationID);
                                var targetObjectID_User = ConfigManager.GetInstance(request.ApplicationID).UserDBObjectID;

                                var UserImages = mediaRepository.All()
                                   .Where(p => p.ObjectID == targetObjectID_User && p.TargetObjectID == commonUser.ID && !p.IsDeleted && p.IsActive).ToList();
                                for (int i = 0; i < UserImages.Count; i++)
                                {
                                    UserImages[i].IsActive = false;
                                }
                                var imagePath = string.Format("~/UploadedImages/UserIDs/{0}-{1}.jpg", commonUser.ID, Guid.NewGuid().ToString());
                                file.SaveAs(UIHelper.MapPath(imagePath));
                                var media = new WebMedia
                                {
                                    CreationDate = DateTime.Now,
                                    Url = imagePath,
                                    ObjectID = targetObjectID_User,
                                    TargetObjectID = commonUser.ID,
                                    WebMediaTypeID = ConfigManager.GetInstance(request.ApplicationID).Picture_WebMediaTypeID,
                                    IsActive = true
                                };
                                mediaRepository.Add(media);
                            }

                            userRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            UserRepository.RemoveUserFromCache(commonUser.ID);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);
                        }
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserObject);
                    }
                    return response;
                });
            }
            return res;
        }

        public UserResponse DoUserActivate(UserRequest req)
        {
            var res = new UserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserActivateOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserActivateOperation, true, false, (UserRequest request, UserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.Filter.EmployeeCode != null)
                    {
                        var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);

                        var userUserIDList = request.Filter.UserIDList == null || request.Filter.UserIDList.Count == 0 || request.Filter.UserIDList[0] == 0;

                        var user = repository.FirstOrDefault(p => ((userUserIDList || request.Filter.UserIDList.Contains(p.ID))
                                && (request.Filter.Email == null || request.Filter.Email == p.Email)));
                        if (user != null)
                        {
                                user.IsActive = true;
                                user.Notes = string.Empty;
                                #region Fill Response with data
                                var userList = new List<UserRecord>();
                                userList.Add(new UserRecord
                                {
                                    ID = user.ID,
                                    IsActive = user.IsActive,
                                    IsDeleted = user.IsDeleted,
                                    FullName = user.FullName,
                                    Birthdate = user.Birthdate,
                                    RegisterDate = user.RegisterDate,
                                    Email = user.Email,
                                    RoleID = user.RoleID,
                                    DBRole = user.PMCore_Role,
                                    RoleName = user.PMCore_Role.Name,
                                    Notes = user.Notes,
                                    Password = user.Password,
                                    ApplicationID = user.ApplicationID
                                });
                                response.UserRecords = userList.ToArray();
                                #endregion

                                repository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                                response.SetBaseResponse(true, ResponseStatus.Active, MessageKey.ActivatedSuccessfully);
                        }
                        else
                        {
                            response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidActivationCode);
                        }
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidActivationCodeOrEmail);
                    }
                    return response;
                });
            }
            return res;
        }

        public UserResponse DoForgetPasswordChange(UserRequest req) 
        {
            var res = new UserResponse(req.Lang, req.ApplicationID);
            if (req.UserRecord != null)
            {
                req.UserRecord.Email = ConfigManager.GetInstance(req.ApplicationID).DefaultEmail;
            }
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoForgetPasswordChange, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoForgetPasswordChange, true, false, (UserRequest request, UserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.UserRecord != null)
                    {
                        var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                        var user = repository.FirstOrDefault(p =>  p.ID == request.UserRecord.ID);
                        if (user != null)
                        {
                            user.IsActive = true;
                            user.Notes = string.Empty;
                            user.Password = request.UserRecord.Password;
                            repository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            var userList = new List<UserRecord>();
                            userList.Add(new UserRecord
                            {
                                ID = user.ID,
                                IsActive = user.IsActive,
                                IsDeleted = user.IsDeleted,
                                FullName = user.FullName,
                                Birthdate = user.Birthdate,
                                RegisterDate = user.RegisterDate,
                                Email = user.Email,
                                RoleID = user.RoleID,
                                DBRole = user.PMCore_Role,
                                RoleName = user.PMCore_Role.Name,
                                Notes = user.Notes,
                                Password = user.Password,
                                ApplicationID = user.ApplicationID
                            });
                            response.UserRecords = userList.ToArray();
                            response.SetBaseResponse(true, ResponseStatus.Active, MessageKey.PasswordChangedSuccessfuly);
                        }
                        else
                        {
                            response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidCodeUrl);
                        }
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidCodeUrl);
                    }
                    return response;
                });
            }
            return res;
        }

        public UserResponse DoUserDeleteOperation(UserRequest req)
        {
            var res = new UserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserDeleteOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserDeleteOperation, true, false, (UserRequest request, UserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.UserRecord != null && request.UserRecord.ID != 0)
                    {
                        var userRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                        var commonUser = userRepository.FirstOrDefault(p => p.ID == request.UserRecord.ID);

                        commonUser.IsDeleted = true;
                        commonUser.Email = commonUser.Email + "-deleted";
                        userRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        UserRepository.RemoveUserFromCache(commonUser.ID);
                        response.Success = true;
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserObject);
                    }
                    return response;
                });
            }
            return res;
        }

        public UserResponse DoUserListInquiry(UserRequest req)
        {
            var res = new UserResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserListInquiry, false, false, (UserRequest request, UserResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var userObj = ConfigManager.GetInstance(request.ApplicationID).UserDBObjectID;
                    var query = new Common_UserRepository(CurrentContextEntities, request.ApplicationID).All()
                                                            .Select(u => new UserRecord
                                                            {
                                                                ID = u.ID,
                                                                IsActive = u.IsActive,
                                                                IsDeleted = u.IsDeleted,
                                                                FullName = u.FullName,
                                                                Birthdate = u.Birthdate,
                                                                RegisterDate = u.RegisterDate,
                                                                Email = u.Email,
                                                                RoleID = u.RoleID,
                                                                DBRole = u.PMCore_Role,
                                                                RoleName = u.PMCore_Role.Notes,
                                                                Notes = u.Notes,
                                                                AreaID = u.AreaID,
                                                                AreaName = u.Area.Name,
                                                                ParentAreaID=u.Area.ParentID,
                                                                ParentAreaName = u.Area.Name,
                                                                ApplicationID = u.ApplicationID,
                                                                phone1 = u.Phone1
                                                            });

                    switch (request.ActiveSelectOption)
                    {
                        case ActiveSelect.IsActive:

                            query = query.Where(p => p.IsActive);
                            break;
                        case ActiveSelect.IsNotActive:
                            query = query.Where(p => !p.IsActive);
                            break;
                    }

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.UserID, request.UserRoleID, CurrentContextEntities, query);




                    int count = 0;
                    var userRecordList = ApplyPaging(request, query, out count);
                    response.UserRecords = userRecordList.ToArray();
                    response.TotalCount = count;
                    response.Success = true;
                    return response;
                });
            }
            return res;
        }

        public ChangePasswordResponse DoChangePassword(ChangePasswordRequest req)
        {
            var res = new ChangePasswordResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoChangePassword, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoChangePassword, true, false, (ChangePasswordRequest request, ChangePasswordResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var userRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                    var commonUser = userRepository.FirstOrDefault(p => p.ID == request.UserID);
                    if (commonUser != null)
                    {
                        if (commonUser.Password == request.OldPassword)
                        {
                            if (!string.IsNullOrWhiteSpace(request.NewPassword))
                            {
                                if (commonUser.Password == request.NewPassword)
                                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.TheNewPasswordIsTheSame);

                                else
                                {
                                    commonUser.Password = request.NewPassword;
                                    userRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.PasswordChangedSuccessfuly);
                                }
                            }
                            else
                            {
                                response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.PasswordEmpty);

                            }
                        }
                        else
                        {
                            response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidOldPassword);

                        }
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserId);

                    }

                    return response;
                });
            }
            return res;
        }

        public CacheVersionListResponse DoCacheVersionList(CacheVersionListRequest req)
        {
            var res = new CacheVersionListResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoCacheVersionList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoCacheVersionList, true, false, (CacheVersionListRequest request, CacheVersionListResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    // get Cache List
                    response.CacheVersionRecords = new PMCore_ServiceOperationRepository(CurrentContextEntities, request.ApplicationID).Where(p => !p.IsDeleted && p.IsCacheable == true && p.CacheVersion > 0).Select(p => new CacheVersionRecord
                    {
                        CacheVersion = p.CacheVersion.Value,
                        OperationName = p.OperationName
                    }).ToArray();

                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);

                    return response;
                });
            }
            return res;
        }

        public ProfileResponse DoUserProfile(ProfileRequest req)
        {
            var res = new ProfileResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserProfile, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserProfile, true, false, (ProfileRequest request, ProfileResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
            {
                var repository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);

                var userID = req.UserID;
                if (request.ProfileUserID.HasValue && request.ProfileUserID != 0)
                    userID = request.ProfileUserID ?? 0;

                if (response.IsBlockerUser || response.IsBlockedUser)
                {
                    response.SetBaseResponse(false, ResponseStatus.Exception, ResourceManager.GetInstance(request.ApplicationID).GetResource("UserIsBlocked"));
                    return response;
                }
                else
                {
                    var user = repository.FirstOrDefault(p => p.ID == userID && p.IsActive && !p.IsDeleted);
                    request.UserRoleID = user.RoleID;
                    if (!user.IsActive)
                    {
                        response.SetBaseResponse(false, ResponseStatus.Inactive, MessageKey.UserInactive);
                    }
                    else
                    {
                        var userRecord = new UserRecord();
                        userRecord.ID = user.ID;
                        userRecord.IsDeleted = user.IsDeleted;
                        userRecord.Birthdate = user.Birthdate;
                        userRecord.FullName = user.FullName;
                        userRecord.RegisterDate = user.RegisterDate;
                        userRecord.Email = user.Email;
                        userRecord.RoleID = user.RoleID;
                        userRecord.DBRole = user.PMCore_Role;
                        userRecord.RoleName = user.PMCore_Role.Notes;
                        userRecord.Notes = user.Notes;
                        userRecord.Birthdate = user.Birthdate;
                        userRecord.ApplicationID = request.ApplicationID;

                        response.UserRecord = userRecord;
                        response.SetBaseResponse(true, ResponseStatus.Active, MessageKey.ProfileListSuccessfully);
                    }
                }
                return response;
            });
            }
            return res;
        }

        public ProfileResponse DoUserProfileUpdate(ProfileRequest req)
        {
            var res = new ProfileResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUserProfileUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUserProfileUpdate, true, false, (ProfileRequest request, ProfileResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.UserRecord != null)
                    {
                        var userObject = ConfigManager.GetInstance(request.ApplicationID).UserDBObjectID;
                        var userRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                        var commonUser = userRepository.FirstOrDefault(p => p.ID == request.UserRecord.ID);

                        if (commonUser != null)
                        {
                            // UserName can't be changed
                            if (commonUser.Email != request.UserRecord.Email)
                            {
                                response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.EmailCantBeChanged);
                                return response;
                            }

                            // Eamil and second email can't be the same
                            if (commonUser.Email == request.UserRecord.SecondEmail)
                            {
                                response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.SecondEmailCanNotBeLikeEmail);
                                return response;
                            }

                            if (!string.IsNullOrWhiteSpace(request.UserRecord.Mobile))
                            {
                                request.UserRecord.Mobile = request.UserRecord.Mobile.Trim();
                                request.UserRecord.Mobile = request.UserRecord.Mobile;
                                //var user = userRepository.FirstOrDefault(p => p.Mobile == request.UserRecord.Mobile && p.ID != request.UserRecord.ID);
                                //if (user != null)
                                //{
                                //    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.MobileAlreadyRegistered);
                                //    return response;
                                //}
                            }
                            //force logout if user was active and been not active
                            if ((commonUser.IsActive && !request.UserRecord.IsActive) || (req.UserRoleID == ConfigManager.GetInstance(req.ApplicationID).AdminRoleID && request.UserRecord.StatusID == ConfigManager.GetInstance(req.ApplicationID).User_AccountDeactivateByAdminStatusID))
                            {
                                var userDeviceRepository = new Common_UserDeviceRepository(CurrentContextEntities, request.ApplicationID);
                                var userDeviceList = userDeviceRepository.All().Where(p => p.Common_UserID == commonUser.ID).ToList();

                                for (int i = 0; i < userDeviceList.Count; i++)
                                {
                                    userDeviceList[i].IsLoggedIn = false;
                                    userDeviceList[i].AuthIP = null;
                                    userDeviceList[i].AuthToken = null;
                                    userDeviceList[i].ModifiedBy = request.UserID;
                                    userDeviceList[i].LastUpdateDate = DateTime.Now;
                                }
                                userDeviceRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            }
                            //commonUser.IsActive = request.UserRecord.IsActive;
                            //commonUser.Email = request.UserRecord.Email;
                            commonUser.FullName = request.UserRecord.FullName;
                            if (!string.IsNullOrWhiteSpace(request.UserRecord.Password))
                            {
                                commonUser.Password = request.UserRecord.Password;// PasswordHelper.CreateHash(request.UserRecord.Password);

                            }
                            if (request.UserRecord.Birthdate != null)
                            {
                                commonUser.Birthdate = request.UserRecord.Birthdate;

                            }
                            //commonUser.RegisterDate = request.UserRecord.RegisterDate;
                            if (request.UserRecord.RoleID > 0)
                            {
                                commonUser.RoleID = request.UserRecord.RoleID;

                            }



                            commonUser.FullName = request.UserRecord.FullName.Trim();
                            commonUser.LastUpdateDate = DateTime.Now;

                            commonUser.Notes = request.UserRecord.Notes;
                            if (request.UserRecord.RoleID > 0)
                                commonUser.RoleID = request.UserRecord.RoleID;

                            //var file = request.UserRecord.ImageFile;

                            //if (file != null && file.ContentLength > 0)
                            //{
                            //    commonUser.ImageUrl = ImageHelper.ImageResizeAndSave(file, ConfigManager.GetInstance(request.ApplicationID).DefaultImageWidth, ConfigManager.GetInstance(request.ApplicationID).DefaultImageHeight);
                            //}
                            //else if (!string.IsNullOrWhiteSpace(request.UserRecord.ProfileImageBase64))
                            //{
                            //    commonUser.ImageUrl = ImageHelper.ImageResizeAndSave(request.UserRecord.ProfileImageBase64, ConfigManager.GetInstance(request.ApplicationID).DefaultImageWidth, ConfigManager.GetInstance(request.ApplicationID).DefaultImageHeight);
                            //}
                            userRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            UserRepository.RemoveUserFromCache(commonUser.ID);
                            response.UserRecord = new UserRecord(commonUser);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);
                        }
                        else
                        {
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UserNotExist);

                        }
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidUserObject);
                    }
                    return response;
                });
            }
            return res;
        }

        public static List<long?> RecursiveChildren(Area[] items, long toplevelid, bool getParent = true)
        {
            List<long?> inner = new List<long?>();
            if (getParent)
            {
                inner.Add(items.FirstOrDefault(item => item.ID == toplevelid).ID);
            }
            foreach (var t in items.Where(item => item.ParentID == toplevelid))
            {
                inner.Add(t.ID);
                inner = inner.Union(RecursiveChildren(items, t.ID)).ToList();
            }

            return inner;
        }
    }
}