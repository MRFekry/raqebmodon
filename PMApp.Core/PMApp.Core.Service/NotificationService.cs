using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Interfaces;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class NotificationService : BaseService, INotification
    {
        private const string serviceName = "NotificationService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region Notification
        public const string operationName_DoNotificationList = "DoNotificationList";
        public const string operationName_DoNotificationSeen = "DoNotificationSeen";
        private const string operationName_DoNotificationDelete = "DoNotificationDelete";
        private const string operationName_DoNotificationInsert = "DoNotificationInsert";
        private const string operationName_DoNotificationUpdate = "DoNotificationUpdate";

        public NotificationResponse DoNotificationList(NotificationRequest req)
        {
            var res = new NotificationResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNotificationList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNotificationList, false, false, (NotificationRequest request, NotificationResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var notificationRepository = new NotificationRepository(CurrentContextEntities, request.ApplicationID);
                    var query = notificationRepository.All()
                        .Where(q => q.ToUserID == request.UserID)
                        .Select(p => new NotificationRecord
                        {
                            ID = p.ID,
                            FromUserID = p.FromUserID,
                            ToUserID = p.ToUserID,
                            IsSent = p.IsSent,
                            IsReceived = p.IsReceived,
                            IsSeen = p.IsSeen,
                            NotificationMessage = p.NotificationMessage,
                            NotificationData = p.NotificationData,
                            NotificationTypeID = p.NotificationTypeID,
                            DBObjectID = p.NotificationType.DBObjectID ?? 0,
                            CreationDate = p.CreationDate,
                            CreatedBy = p.CreatedBy,
                            ModifiedBy = p.ModifiedBy,
                            LastUpdateDate = p.LastUpdateDate,
                            IsDeleted = p.IsDeleted,
                            ApplicationID = p.ApplicationID,
                        });

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter == null)
                        request.Filter.DisplayEmailsNotifications = false;
                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var notifications = ApplyPaging(request, query, out count, false);
                    response.NotificationRecords = notifications.ToArray();
                    response.TotalCount = count;
                    //if (request.MakeAsSeen)
                    //{
                    //    var notSeenIDs = new List<long>();

                    //    for (int i = 0; i < notifications.Count; i++)
                    //    {
                    //        if (!notifications[i].IsSeen)
                    //            notSeenIDs.Add(notifications[i].ID);
                    //    }
                    //    if (notSeenIDs.Count > 0)
                    //    {
                    //        var notSeenNotifications = notificationRepository.Where(p => notSeenIDs.Contains(p.ID)).ToList();
                    //        for (int i = 0; i < notSeenNotifications.Count; i++)
                    //        {
                    //            notSeenNotifications[i].IsSeen = true;
                    //        }
                    //        notificationRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                    //    }
                    //}
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public NotificationResponse DoNotificationDelete(NotificationRequest req)
        {
            var res = new NotificationResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNotificationDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNotificationDelete, true, false, (NotificationRequest request, NotificationResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NotificationRecord != null && request.NotificationRecord.ID > 0)
                    {
                        var notificationRepository = new NotificationRepository(CurrentContextEntities, request.ApplicationID);
                        var notification = notificationRepository.FirstOrDefault(p => p.ID == request.NotificationRecord.ID);

                        if (notification != null)
                        {
                            notification.IsDeleted = true;
                            notificationRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNotificationTypeObject);
                    }
                    return response;
                });
            }
            return res;
        }

        public NotificationResponse DoNotificationInsert(NotificationRequest req)
        {
            var res = new NotificationResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNotificationInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNotificationInsert, true, false, (NotificationRequest request, NotificationResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NotificationRecord != null)
                    {
                        var notificationRepository = new NotificationRepository(CurrentContextEntities, request.ApplicationID);
                        var notification = NotificationServiceManager.CreateOrUpdateNotification(request.ApplicationID, request.UserID, request.NotificationRecord);
                        notificationRepository.Add(notification);
                        notificationRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.NotificationRecord.ID = notification.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNotificationTypeObject);
                    }
                    return response;
                });
            }
            return res;
        }

        public NotificationResponse DoNotificationUpdate(NotificationRequest req)
        {
            var res = new NotificationResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNotificationUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNotificationUpdate, true, false, (NotificationRequest request, NotificationResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NotificationRecord != null)
                    {
                        var notificationRepository = new NotificationRepository(CurrentContextEntities, request.ApplicationID);
                        var notification = notificationRepository.FirstOrDefault(p => p.ID == request.NotificationRecord.ID);

                        if (notification != null)
                        {
                            notification = NotificationServiceManager.CreateOrUpdateNotification(request.ApplicationID, request.UserID, request.NotificationRecord, notification);

                            notificationRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNotificationTypeObject);
                    }
                    return response;
                });
            }
            return res;
        }


        public NotificationResponse DoNotificationSeen(NotificationRequest req)
        {
            var res = new NotificationResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoNotificationSeen, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoNotificationSeen, true, false, (NotificationRequest request, NotificationResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.NotificationRecord != null)
                    {
                        var notificationRepository = new NotificationRepository(CurrentContextEntities, request.ApplicationID);
                        var notification = notificationRepository.FirstOrDefault(p => p.ID == request.NotificationRecord.ID);

                        if (notification != null)
                        {
                            notification.IsSeen = true;
                            notificationRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidNotificationTypeObject);
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion


    }
}