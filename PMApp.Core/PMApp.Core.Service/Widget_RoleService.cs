using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Interfaces;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class PMCore_Widget_RoleService : BaseService, IPMCore_Widget_Role
    {
        private const string serviceName = "PMCore_Widget_RoleService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region PMCore_Widget_RoleServices
        private const string operationName_DoPMCore_Widget_RoleList = "DoPMCore_Widget_RoleList";
        private const string operationName_DoPMCore_Widget_RoleDelete = "DoPMCore_Widget_RoleDelete";
        private const string operationName_DoPMCore_Widget_RoleInsert = "DoPMCore_Widget_RoleInsert";
        private const string operationName_DoPMCore_Widget_RoleUpdate = "DoPMCore_Widget_RoleUpdate";

        public PMCore_Widget_RoleResponse DoPMCore_Widget_RoleList(PMCore_Widget_RoleRequest req)
        {
            var res = new PMCore_Widget_RoleResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoPMCore_Widget_RoleList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoPMCore_Widget_RoleList, false, false, (PMCore_Widget_RoleRequest request, PMCore_Widget_RoleResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new PMCore_Widget_RoleRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new PMCore_Widget_RoleRecord
                    {
                        RoleID = p.RoleID,
                        WidgetID = p.WidgetID,
                        IsAllowed = p.IsAllowed,
                        CreationDate = p.CreationDate,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        IsDeleted = p.IsDeleted,
                        CreatedBy = p.CreatedBy,
                        ApplicationID = p.ApplicationID
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var pMCore_Widget_Roles = ApplyPaging(request, query, out count);

                    response.PMCore_Widget_RoleRecords = pMCore_Widget_Roles.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);

                    return response;
                });
            }
            return res;
        }

        public PMCore_Widget_RoleResponse DoPMCore_Widget_RoleDelete(PMCore_Widget_RoleRequest req)
        {
            var res = new PMCore_Widget_RoleResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoPMCore_Widget_RoleDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoPMCore_Widget_RoleDelete, true, false, (PMCore_Widget_RoleRequest request, PMCore_Widget_RoleResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.PMCore_Widget_RoleRecord != null && request.PMCore_Widget_RoleRecord.RoleID > 0)
                    {
                        var pMCore_Widget_RoleRepository = new PMCore_Widget_RoleRepository(CurrentContextEntities, request.ApplicationID);
                        var pMCore_Widget_Role = pMCore_Widget_RoleRepository.FirstOrDefault(p => p.RoleID == request.PMCore_Widget_RoleRecord.RoleID && p.WidgetID == request.PMCore_Widget_RoleRecord.WidgetID);

                        if (pMCore_Widget_Role != null)
                        {
                            pMCore_Widget_RoleRepository.Delete(pMCore_Widget_Role);
                            pMCore_Widget_RoleRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidPMCore_Widget_RoleTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public PMCore_Widget_RoleResponse DoPMCore_Widget_RoleInsert(PMCore_Widget_RoleRequest req)
        {
            var res = new PMCore_Widget_RoleResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoPMCore_Widget_RoleInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoPMCore_Widget_RoleInsert, true, false, (PMCore_Widget_RoleRequest request, PMCore_Widget_RoleResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.PMCore_Widget_RoleRecord != null)
                    {
                        var pMCore_Widget_RoleRepository = new PMCore_Widget_RoleRepository(CurrentContextEntities, request.ApplicationID);
                        var pMCore_Widget_Role = PMCore_Widget_RoleServiceManager.CreateOrUpdatePMCore_Widget_Role(request.UserID, request.PMCore_Widget_RoleRecord);
                        pMCore_Widget_RoleRepository.Add(pMCore_Widget_Role);
                        pMCore_Widget_RoleRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidPMCore_Widget_RoleTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public PMCore_Widget_RoleResponse DoPMCore_Widget_RoleUpdate(PMCore_Widget_RoleRequest req)
        {
            var res = new PMCore_Widget_RoleResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoPMCore_Widget_RoleUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoPMCore_Widget_RoleUpdate, true, false, (PMCore_Widget_RoleRequest request, PMCore_Widget_RoleResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.PMCore_Widget_RoleRecord != null)
                    {
                        var pMCore_Widget_RoleRepository = new PMCore_Widget_RoleRepository(CurrentContextEntities, request.ApplicationID);
                        var pMCore_Widget_Role = pMCore_Widget_RoleRepository.FirstOrDefault(p => p.RoleID == request.PMCore_Widget_RoleRecord.RoleID);

                        if (pMCore_Widget_Role != null)
                        {
                            pMCore_Widget_Role = PMCore_Widget_RoleServiceManager.CreateOrUpdatePMCore_Widget_Role(request.UserID, request.PMCore_Widget_RoleRecord, pMCore_Widget_Role);

                            pMCore_Widget_RoleRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidPMCore_Widget_RoleTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}