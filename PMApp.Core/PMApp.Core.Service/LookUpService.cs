using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Interfaces;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{

    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class LookupService : BaseService, ILookup
    {
        private const string serviceName = "LookupService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region Lookup
        public const string operationName_DoLookupListInquiry = "DoLookupListInquiry";
        public const string operationName_DoLookupInsertOperation = "DoLookupInsertOperation";
        public const string operationName_DoLookupUpdateOperation = "DoLookupUpdateOperation";
        public const string operationName_DoLookupDeleteOperation = "DoLookupDeleteOperation";

        public LookupResponse DoLookupListInquiry(LookupRequest req)
        {
            var res = new LookupResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLookupListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLookupListInquiry, false, false, (LookupRequest request, LookupResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    List<PMCore_Lookup> posLookupRecords = null;

                    var query = new PMCore_LookupRepository(CurrentContextEntities, request.ApplicationID).All();
                    query = query.Where(p => p.Enable == true);

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    posLookupRecords = ApplyPaging(request, query, out count);

                    var LookupRecords = LookupServiceManager.ConvertFromPOSLookup(request.Filter == null, posLookupRecords, request.Lang);

                    response.LookupRecords = LookupRecords;
                    response.TotalCount = count;
                    response.Success = true;

                    return response;
                });
            }
            return res;
        }

        public LookupResponse DoLookupInsertOperation(LookupRequest req)
        {
            var res = new LookupResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLookupInsertOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLookupInsertOperation, true, false, (LookupRequest request, LookupResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                 {
                     if (request.LookupRecord != null)
                     {
                         var PMCore_LookupRepository = new PMCore_LookupRepository(CurrentContextEntities, request.ApplicationID);

                         long nextKey = 0;
                         if (request.LookupRecord.LookupKey == "0")
                         {
                             var maxKey = PMCore_LookupRepository.Where(p => p.LookupName == request.LookupRecord.LookupName).Max(p => p.LookupKey);
                             if (!long.TryParse(maxKey, out nextKey))
                             {
                                 nextKey = PMCore_LookupRepository.Where(p => p.LookupName == request.LookupRecord.LookupName).Count();
                             }
                             nextKey++;
                             request.LookupRecord.LookupKey = nextKey.ToString();
                         }
                         var newLookup = new PMCore_Lookup();
                         LookupServiceManager.ConvertToPOSLookup(ref newLookup, nextKey, request.LookupRecord);
                         PMCore_LookupRepository.Add(newLookup);

                         PMCore_LookupRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                         response.Success = true;
                     }
                     else
                     {
                         response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidLookupTypeObject);
                     };

                     return response;
                 });
            }
            return res;
        }

        public LookupResponse DoLookupUpdateOperation(LookupRequest req)
        {
            var res = new LookupResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLookupUpdateOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLookupUpdateOperation, true, false, (LookupRequest request, LookupResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.LookupRecord != null)
                    {
                        var PMCore_LookupRepository = new PMCore_LookupRepository(CurrentContextEntities, request.ApplicationID);

                        var Lookup = PMCore_LookupRepository.FirstOrDefault(p => p.Enable == true && p.ID == request.LookupRecord.ID);
                        if (Lookup == null)
                        {
                            response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidLookupTypeObject);
                        }
                        else
                        {
                            LookupServiceManager.ConvertToPOSLookup(ref Lookup, 0, request.LookupRecord);

                            PMCore_LookupRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.Success = true;
                        }
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidLookupTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }

        public LookupResponse DoLookupDeleteOperation(LookupRequest req)
        {
            var res = new LookupResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLookupDeleteOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLookupDeleteOperation, true, false, (LookupRequest request, LookupResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.LookupRecord != null && request.LookupRecord.ID > 0)
                    {
                        var PMCore_LookupRepository = new PMCore_LookupRepository(CurrentContextEntities, request.ApplicationID);
                        var Lookup = PMCore_LookupRepository.FirstOrDefault(p => p.ID == request.LookupRecord.ID);
                        Lookup.Enable = false;
                        PMCore_LookupRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        response.Success = true;
                    }
                    else
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidLookupTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }
        #endregion

        #region Resource
        public const string operationName_DoResourceList = "DoResourceList";
        public const string operationName_DoResourceDelete = "DoResourceDelete";
        public const string operationName_DoResourceInsert = "DoResourceInsert";
        public const string operationName_DoResourceUpdate = "DoResourceUpdate";

        public ResourceResponse DoResourceList(ResourceRequest req)
        {
            var res = new ResourceResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoResourceList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoResourceList, false, false, (ResourceRequest request, ResourceResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new Common_ResourceRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new ResourceRecord
                    {
                        ID = p.ID,
                        ResourceKey = p.ResourceKey,
                        ResourceValue = p.ResourceValue,
                        ResourceLang = p.ResourceLang,
                        CreatedBy = p.CreatedBy,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        CreationDate = p.CreationDate,
                        IsDeleted = p.IsDeleted,
                        ApplicationID = p.ApplicationID
                    });

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var resources = ApplyPaging(request, query, out count);

                    response.ResourceRecords = resources.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public ResourceResponse DoResourceDelete(ResourceRequest req)
        {
            var res = new ResourceResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoResourceDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoResourceDelete, true, false, (ResourceRequest request, ResourceResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.ResourceRecord != null && request.ResourceRecord.ID > 0)
                    {
                        var Common_ResourceRepository = new Common_ResourceRepository(CurrentContextEntities, request.ApplicationID);
                        var resource = Common_ResourceRepository.FirstOrDefault(p => p.ID == request.ResourceRecord.ID);

                        if (resource != null)
                        {
                            resource.IsDeleted = true;
                            Common_ResourceRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidResourceTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }


        public ResourceResponse DoResourceInsert(ResourceRequest req)
        {
            var res = new ResourceResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoResourceInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoResourceInsert, true, false, (ResourceRequest request, ResourceResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.ResourceRecord != null)
                    {
                        var Common_ResourceRepository = new Common_ResourceRepository(CurrentContextEntities, request.ApplicationID);
                        var Resource = LookupServiceManager.CreateOrUpdateCommon_Resource(request.UserID, request.ResourceRecord);
                        Common_ResourceRepository.Add(Resource);
                        Common_ResourceRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.ResourceRecord.ID = Resource.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidResourceTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }

        public ResourceResponse DoResourceUpdate(ResourceRequest req)
        {
            var res = new ResourceResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoResourceUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoResourceUpdate, true, false, (ResourceRequest request, ResourceResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.ResourceRecord != null)
                    {
                        var Common_ResourceRepository = new Common_ResourceRepository(CurrentContextEntities, request.ApplicationID);
                        var Resource = Common_ResourceRepository.FirstOrDefault(p => p.ID == request.ResourceRecord.ID);

                        if (Resource != null)
                        {
                            Resource = LookupServiceManager.CreateOrUpdateCommon_Resource(request.UserID, request.ResourceRecord, Resource);

                            Common_ResourceRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidResourceTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }
        #endregion

        #region Configs
        public const string operationName_DoConfigsList = "DoConfigsList";
        public const string operationName_DoConfigsDelete = "DoConfigsDelete";
        public const string operationName_DoConfigsInsert = "DoConfigsInsert";
        public const string operationName_DoConfigsUpdate = "DoConfigsUpdate";

        public ConfigsResponse DoConfigsList(ConfigsRequest req)
        {
            var res = new ConfigsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoConfigsList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoConfigsList, false, false, (ConfigsRequest request, ConfigsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var repository = new PMCore_ConfigsRepository(CurrentContextEntities, request.ApplicationID);
                    var query = repository.All().Select(p => new ConfigRecord
                    {
                        ID = p.ID,
                        ConfKey = p.ConfKey,
                        ConfValue = p.ConfValue,
                        Decription = p.Decription,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        CreationDate = p.CreationDate,
                        IsDeleted = p.IsDeleted,
                        CreatedBy = p.CreatedBy,
                        ChannelIDs = p.ChannelIDs,
                        IsUIEditable = p.IsUIEditable,
                        ApplicationID = p.ApplicationID
                    });

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }
                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ChannelID, query);

                    int count = 0;
                    var pMCore_Configss = ApplyPaging(request, query, out count);

                    if (request.Filter != null && request.Filter.EnumTablesQueries)
                    {
                        var list = ConfigManager.LoadEnumTablesConfig(request.ApplicationID);
                        pMCore_Configss.AddRange(list.Select(p => new ConfigRecord
                        {
                            ConfKey = p.ConfKey,
                            ConfValue = p.ConfValue,
                            ApplicationID = p.ApplicationID
                        }));
                    }

                    response.ConfigsRecords = pMCore_Configss.Where(p => !p.ConfKey.StartsWith(ConfigManager.ConfigEnumStr)).ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);

                    return response;
                });
            }
            return res;
        }

        public ConfigsResponse DoConfigsDelete(ConfigsRequest req)
        {
            var res = new ConfigsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoConfigsDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoConfigsDelete, true, false, (ConfigsRequest request, ConfigsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.ConfigsRecord != null && request.ConfigsRecord.ID > 0)
                    {
                        var pMCore_PMCore_ConfigsRepository = new PMCore_ConfigsRepository(CurrentContextEntities, request.ApplicationID);
                        var pMCore_Configs = pMCore_PMCore_ConfigsRepository.FirstOrDefault(p => p.ID == request.ConfigsRecord.ID);

                        if (pMCore_Configs != null)
                        {
                            pMCore_Configs.IsDeleted = true;
                            pMCore_PMCore_ConfigsRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidConfigsTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }

        public ConfigsResponse DoConfigsInsert(ConfigsRequest req)
        {
            var res = new ConfigsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoConfigsInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoConfigsInsert, true, false, (ConfigsRequest request, ConfigsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.ConfigsRecord != null)
                    {
                        var pMCore_PMCore_ConfigsRepository = new PMCore_ConfigsRepository(CurrentContextEntities, request.ApplicationID);
                        var pMCore_Configs = LookupServiceManager.CreateOrUpdatePMCore_Configs(request.UserID, request.ConfigsRecord);
                        pMCore_PMCore_ConfigsRepository.Add(pMCore_Configs);
                        pMCore_PMCore_ConfigsRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.ConfigsRecord.ID = pMCore_Configs.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidConfigsTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }

        public ConfigsResponse DoConfigsUpdate(ConfigsRequest req)
        {
            var res = new ConfigsResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoConfigsUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoConfigsUpdate, true, false, (ConfigsRequest request, ConfigsResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.ConfigsRecord != null)
                    {
                        var pMCore_PMCore_ConfigsRepository = new PMCore_ConfigsRepository(CurrentContextEntities, request.ApplicationID);
                        var pMCore_Configs = pMCore_PMCore_ConfigsRepository.FirstOrDefault(p => p.ID == request.ConfigsRecord.ID && p.IsUIEditable);

                        if (pMCore_Configs != null)
                        {
                            pMCore_Configs = LookupServiceManager.CreateOrUpdatePMCore_Configs(request.UserID, request.ConfigsRecord, pMCore_Configs);

                            pMCore_PMCore_ConfigsRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidConfigsTypeObject);
                    }

                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}