using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Interfaces;
using PMApp.Modon.DAL6;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class SharedService : BaseService, IShared
    {
        private const string serviceName = "SharedService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region Status
        public const string operationName_DoStatusList = "DoStatusList";
        public const string operationName_DoStatusDelete = "DoStatusDelete";
        public const string operationName_DoStatusInsert = "DoStatusInsert";
        public const string operationName_DoStatusUpdate = "DoStatusUpdate";

        public StatusResponse DoStatusList(StatusRequest req)
        {
            var res = new StatusResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusList, false, false, (StatusRequest request, StatusResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new StatusRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new StatusRecord
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Description = p.Description,
                        ObjectID = p.ObjectID,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        IsDeleted = p.IsDeleted,
                        CreatedBy = p.CreatedBy,
                        CreationDate = p.CreationDate,
                        ApplicationID = p.ApplicationID
                    });

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }
                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(request.ApplicationID, query);

                    int count = 0;
                    var statuss = ApplyPaging(request, query, out count, true, StatusRecord.DisplayColumnName);

                    response.StatusRecords = statuss.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public StatusResponse DoStatusDelete(StatusRequest req)
        {
            var res = new StatusResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusDelete, true, false, (StatusRequest request, StatusResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.StatusRecord != null && request.StatusRecord.ID > 0)
                    {
                        var statusRepository = new StatusRepository(CurrentContextEntities, request.ApplicationID);
                        var status = statusRepository.FirstOrDefault(p => p.ID == request.StatusRecord.ID);

                        if (status != null)
                        {
                            status.IsDeleted = true;
                            statusRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidStatusTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public StatusResponse DoStatusInsert(StatusRequest req)
        {
            var res = new StatusResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusInsert, true, false, (StatusRequest request, StatusResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.StatusRecord != null)
                    {
                        var statusRepository = new StatusRepository(CurrentContextEntities, request.ApplicationID);
                        var status = SharedServiceManager.CreateOrUpdateStatus(request.UserID, request.ApplicationID, request.StatusRecord);
                        statusRepository.Add(status);
                        statusRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.StatusRecord.ID = status.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidStatusTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public StatusResponse DoStatusUpdate(StatusRequest req)
        {
            var res = new StatusResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusUpdate, true, false, (StatusRequest request, StatusResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.StatusRecord != null)
                    {
                        var statusRepository = new StatusRepository(CurrentContextEntities, request.ApplicationID);
                        var status = statusRepository.FirstOrDefault(p => p.ID == request.StatusRecord.ID);

                        if (status != null)
                        {
                            status = SharedServiceManager.CreateOrUpdateStatus(request.UserID, request.ApplicationID, request.StatusRecord, status);

                            statusRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidStatusTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion

        #region StatusHistory
        public const string operationName_DoStatusHistoryList = "DoStatusHistoryList";
        public const string operationName_DoStatusHistoryDelete = "DoStatusHistoryDelete";
        public const string operationName_DoStatusHistoryInsert = "DoStatusHistoryInsert";
        public const string operationName_DoStatusHistoryUpdate = "DoStatusHistoryUpdate";

        public StatusHistoryResponse DoStatusHistoryList(StatusHistoryRequest req)
        {
            var res = new StatusHistoryResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusHistoryList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusHistoryList, false, false, (StatusHistoryRequest request, StatusHistoryResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new StatusHistoryRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new StatusHistoryRecord
                    {
                        ID = p.ID,
                        FromStatusID = p.FromStatusID,
                        ToStatusID = p.ToStatusID,
                        TargetObjectID = p.TargetObjectID,
                        CreatedBy = p.CreatedBy,
                        CreationDate = p.CreationDate,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        IsDeleted = p.IsDeleted,
                        ApplicationID = p.ApplicationID
                    });

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var statusHistorys = ApplyPaging(request, query, out count);

                    response.StatusHistoryRecords = statusHistorys.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public StatusHistoryResponse DoStatusHistoryDelete(StatusHistoryRequest req)
        {
            var res = new StatusHistoryResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusHistoryDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusHistoryDelete, true, false, (StatusHistoryRequest request, StatusHistoryResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.StatusHistoryRecord != null && request.StatusHistoryRecord.ID > 0)
                    {
                        var statusHistoryRepository = new StatusHistoryRepository(CurrentContextEntities, request.ApplicationID);
                        var statusHistory = statusHistoryRepository.FirstOrDefault(p => p.ID == request.StatusHistoryRecord.ID);

                        if (statusHistory != null)
                        {
                            statusHistory.IsDeleted = true;
                            statusHistoryRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidStatusHistoryTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public StatusHistoryResponse DoStatusHistoryInsert(StatusHistoryRequest req)
        {
            var res = new StatusHistoryResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusHistoryInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusHistoryInsert, true, false, (StatusHistoryRequest request, StatusHistoryResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.StatusHistoryRecord != null)
                    {
                        var statusHistoryRepository = new StatusHistoryRepository(CurrentContextEntities, request.ApplicationID);
                        var statusHistory = SharedServiceManager.CreateOrUpdateStatusHistory(request.UserID, request.ApplicationID, request.StatusHistoryRecord);
                        statusHistoryRepository.Add(statusHistory);
                        statusHistoryRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.StatusHistoryRecord.ID = statusHistory.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidStatusHistoryTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public StatusHistoryResponse DoStatusHistoryUpdate(StatusHistoryRequest req)
        {
            var res = new StatusHistoryResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoStatusHistoryUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoStatusHistoryUpdate, true, false, (StatusHistoryRequest request, StatusHistoryResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.StatusHistoryRecord != null)
                    {
                        var statusHistoryRepository = new StatusHistoryRepository(CurrentContextEntities, request.ApplicationID);
                        var statusHistory = statusHistoryRepository.FirstOrDefault(p => p.ID == request.StatusHistoryRecord.ID);

                        if (statusHistory != null)
                        {
                            statusHistory = SharedServiceManager.CreateOrUpdateStatusHistory(request.UserID, request.ApplicationID, request.StatusHistoryRecord, statusHistory);

                            statusHistoryRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidStatusHistoryTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion

        #region DBObject
        public const string operationName_DoDBObjectList = "DoDBObjectList";
        public const string operationName_DoDBObjectDelete = "DoDBObjectDelete";
        public const string operationName_DoDBObjectInsert = "DoDBObjectInsert";
        public const string operationName_DoDBObjectUpdate = "DoDBObjectUpdate";

        public DBObjectResponse DoDBObjectList(DBObjectRequest req)
        {
            var res = new DBObjectResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoDBObjectList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoDBObjectList, false, false, (DBObjectRequest request, DBObjectResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new DBObjectRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new DBObjectRecord
                    {
                        ID = p.ID,
                        Name = p.Name,
                        CreationDate = p.CreationDate,
                        IsDeleted = p.IsDeleted,
                        CreatedBy = p.CreatedBy,
                        ModifiedBy = p.ModifiedBy,
                        LastUpdateDate = p.LastUpdateDate,
                        ApplicationID = p.ApplicationID
                    });

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var dBObjects = ApplyPaging(request, query, out count, true, DBObjectRecord.DisplayColumnName);

                    response.DBObjectRecords = dBObjects.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        public DBObjectResponse DoDBObjectDelete(DBObjectRequest req)
        {
            var res = new DBObjectResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoDBObjectDelete, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoDBObjectDelete, true, false, (DBObjectRequest request, DBObjectResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.DBObjectRecord != null && request.DBObjectRecord.ID > 0)
                    {
                        var dBObjectRepository = new DBObjectRepository(CurrentContextEntities, request.ApplicationID);
                        var dBObject = dBObjectRepository.FirstOrDefault(p => p.ID == request.DBObjectRecord.ID);

                        if (dBObject != null)
                        {
                            dBObject.IsDeleted = true;
                            dBObjectRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);

                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.DeletedSuccessfully);
                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidDBObjectTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public DBObjectResponse DoDBObjectInsert(DBObjectRequest req)
        {
            var res = new DBObjectResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoDBObjectInsert, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoDBObjectInsert, true, false, (DBObjectRequest request, DBObjectResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.DBObjectRecord != null)
                    {
                        var dBObjectRepository = new DBObjectRepository(CurrentContextEntities, request.ApplicationID);
                        var dBObject = SharedServiceManager.CreateOrUpdateDBObject(request.UserID, request.ApplicationID, request.DBObjectRecord);
                        dBObjectRepository.Add(dBObject);
                        dBObjectRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.DBObjectRecord.ID = dBObject.ID;

                        response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.AddedSuccessfully);
                        isInValidObject = false;
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidDBObjectTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }

        public DBObjectResponse DoDBObjectUpdate(DBObjectRequest req)
        {
            var res = new DBObjectResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoDBObjectUpdate, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoDBObjectUpdate, true, false, (DBObjectRequest request, DBObjectResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    bool isInValidObject = true;

                    if (request.DBObjectRecord != null)
                    {
                        var dBObjectRepository = new DBObjectRepository(CurrentContextEntities, request.ApplicationID);
                        var dBObject = dBObjectRepository.FirstOrDefault(p => p.ID == request.DBObjectRecord.ID);

                        if (dBObject != null)
                        {
                            dBObject = SharedServiceManager.CreateOrUpdateDBObject(request.UserID, request.ApplicationID, request.DBObjectRecord, dBObject);

                            dBObjectRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.UpdatedSuccessfully);

                            isInValidObject = false;
                        }
                    }

                    if (isInValidObject)
                    {
                        response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidDBObjectTypeObject.ToString());
                    }
                    return response;
                });
            }
            return res;
        }
        #endregion
    }
}