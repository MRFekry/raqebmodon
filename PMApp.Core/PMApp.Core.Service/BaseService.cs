using PMApp.Core.CommonDefinitions;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;

namespace  PMApp.Core.Services
{
    public delegate RS InvokeService<RQ, RS>(RQ request, RS response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr)
        where RQ : IAbBaseRequest
        where RS : BaseResponse;

    public abstract class BaseService
    {
        private ModonDBEntities _currentUnitOfWork;
        protected ModonDBEntities CurrentContextEntities
        {
            get
            {
                if (_currentUnitOfWork == null)
                {
                    var key = string.Format("BaseService.Invoke-{0}", this.GetType().FullName);
                    _currentUnitOfWork = EFUnitOfWork.NewUnitOfWork(key);
                }
                return _currentUnitOfWork;
            }
        }
        protected const string IDColumn = "ID";
        protected const string OrderByCommand = "OrderBy";
        protected const string OrderByDescCommand = "OrderByDescending";
        private static Dictionary<string, object> orderByDynamicDictionary = null;

        private static Dictionary<string, object> OrderByDynamicDictionary
        {
            get
            {
                if (orderByDynamicDictionary == null)
                    orderByDynamicDictionary = new Dictionary<string, object>();
                return orderByDynamicDictionary;
            }
        }

        protected abstract string ServiceName { get; }

        protected void LogTrack(IAbBaseRequest request, bool isSuccess, bool isOnline, string entityName, long recordID, string entityListStr, string trackingEntityListStr, string operationName, long exceptionID)
        {
            if (!isSuccess)
                return;
            if (!string.IsNullOrWhiteSpace(entityListStr) || !string.IsNullOrWhiteSpace(trackingEntityListStr))
            {
                if (TrackingLogger.GetInstance(request.ApplicationID).IsTrackingEnabled(ServiceName, operationName))
                {
                    var serviceOperation = ServiceOperationManager.GetInstance(request.ApplicationID).GetServiceOperation(ServiceName, operationName);
                    if (serviceOperation != null && serviceOperation.EnableTracking)
                    {
                        string currentIP = SessionHelper.CurrentIP;
                        string currentSessionID = SessionHelper.CurrentSessionID;
                        string channel = serviceOperation.IsAnonymous ? null : request.ChannelName;
                        long? userID = null;
                        if (!serviceOperation.IsAnonymous)
                            userID = request.UserID;

                        TrackingLogger.GetInstance(request.ApplicationID).Log(isSuccess, isOnline, currentIP, serviceOperation.ID, entityName, entityName, recordID, entityListStr, trackingEntityListStr, userID, currentSessionID, channel, exceptionID);
                    }
                }
            }
        }

        private void Log(IAbBaseRequest request, BaseResponse response, double durationInSeconds, long exceptionId, string operationName)
        {
            if (HostCommunicationLogger.GetInstance(request.ApplicationID).IsLoggingEnabled() || response.Success != true)
            {
                string currentIP = SessionHelper.CurrentIP;
                string currentSessionID = SessionHelper.CurrentSessionID;

                var serviceOperation = ServiceOperationManager.GetInstance(request.ApplicationID).GetServiceOperation(ServiceName, operationName);
                if (serviceOperation.EnableLog)
                {
                    var appName = request.AppCode;
                    long? userID = null;
                    if (!serviceOperation.IsAnonymous)
                        userID = request.UserID;

                    HostCommunicationLogger.GetInstance(request.ApplicationID).Log(appName,
                        0, durationInSeconds,
                        response.Success,
                        request.IsOnline,
                        request,
                        response,
                        userID,
                        currentIP,
                        currentSessionID,
                        serviceOperation.IsAnonymous ? null : request.ChannelName,
                        exceptionId,
                        operationName,
                        ServiceName);
                }
            }
        }

        protected long LogException(IAbBaseRequest request, string message, string stackTrace)
        {
            long? userid = null;
            if (request.UserID != 0)
                userid = request.UserID;

            return ExceptionLogger.GetInstance(request.ApplicationID).Log(message, stackTrace, userid == 0 ? null : userid, null, request.ChannelName);
        }

        protected List<Q> ApplyPaging<Q>(IAbBaseRequest request, IQueryable<Q> query, out int count, bool isASC = true, string orderByColumn = null)
        {
            count = query.Count();
            var skipPages = request.PageIndex * request.PageSize;
            return OrderByDynamic<Q>(request, query, orderByColumn, isASC).Skip(skipPages)
                .Take(request.PageSize).ToList();
        }

        private IQueryable<Q> OrderByDynamic<Q>(IAbBaseRequest request, IQueryable<Q> query, string orderByColumn, bool isASC)
        {
            var QType = typeof(Q);
            var parameter = Expression.Parameter(QType, "p");
            Expression resultExpression = null;
            var property = typeof(Q).GetProperty(orderByColumn ?? IDColumn);
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            resultExpression = Expression.Call(typeof(Queryable), isASC ? OrderByCommand : OrderByDescCommand, new Type[] { typeof(Q), property.PropertyType }, query.Expression, Expression.Quote(orderByExpression));

            return query.Provider.CreateQuery<Q>(resultExpression);
        }

        private long GetCacheNumber(long applicationID, string operationName)
        {
            long cacheNumber = -1;
            if (applicationID > 0)
            {
                var serviceOperation = ServiceOperationManager.GetInstance(applicationID).GetServiceOperation(ServiceName, operationName);
                if (serviceOperation != null && serviceOperation.IsCacheable.HasValue && serviceOperation.IsCacheable.Value)
                {
                    var pmCoreServiceOperationRepository = new PMCore_ServiceOperationRepository(CurrentContextEntities, applicationID);
                    var cacheNumberValue = pmCoreServiceOperationRepository.All().Where(p => p.OperationName == operationName && p.ServiceName == ServiceName).Select(p => p.CacheVersion).FirstOrDefault();
                    if (cacheNumberValue.HasValue)
                        cacheNumber = cacheNumberValue.Value;
                }
            }
            return cacheNumber;
        }


        private long UpdateCacheNumber(long applicationID, string operationName)
        {
            long cacheNumber = -1;
            if (applicationID > 0)
            {
                var serviceOperation = ServiceOperationManager.GetInstance(applicationID).GetServiceOperation(ServiceName, operationName);
                if (serviceOperation == null)
                    return cacheNumber;
                if (serviceOperation.IsCacheable.HasValue && serviceOperation.IsCacheable.Value)
                {
                    cacheNumber = serviceOperation.CacheVersion ?? -1;
                }
                else
                {
                    var serviceOperationList = ServiceOperationManager.GetInstance(applicationID).GetRelatedServiceOperations(ServiceName, operationName);
                    if (serviceOperationList != null && serviceOperationList.Count > 0)
                    {
                        if (!(serviceOperationList.Count == 1 && serviceOperation.ID == serviceOperationList[0].ID))
                        {
                            var pmCoreServiceOperationRepository = new PMCore_ServiceOperationRepository(CurrentContextEntities, applicationID);
                            for (int i = 0; i < serviceOperationList.Count; i++)
                            {
                                if (serviceOperationList[i].CacheVersion.HasValue)
                                    cacheNumber = serviceOperationList[i].CacheVersion.Value;
                                cacheNumber++;

                                var serviceOperationID = serviceOperationList[i].ID;
                                var service = pmCoreServiceOperationRepository.FirstOrDefault(p => p.ID == serviceOperationID);
                                service.CacheVersion = cacheNumber;
                                serviceOperationList[i].CacheVersion = cacheNumber;
                            }
                            pmCoreServiceOperationRepository.Commit();
                        }
                    }
                    cacheNumber = -1;
                }
            }
            return cacheNumber;
        }

        private RS Invoke<RQ, RS>(ref RQ request, ref RS response, string operationName, bool useTransactionScope, InvokeService<RQ, RS> invokeService)
            where RQ : IAbBaseRequest
            where RS : BaseResponse
        {
            long exceptionId = 0;
            var startDateTime = DateTime.Now;
            string entityListStr = "", trackingEntityListStr = "";
            try
            {
                UpdateDeviceLanguage(request);

                var cashNumber = GetCacheNumber(request.ApplicationID, operationName);
                if (cashNumber <= 0 || request.CacheVersion != cashNumber)
                {
                    if (invokeService != null)
                    {
                        bool enableTracking = TrackingLogger.GetInstance(request.ApplicationID).IsTrackingEnabled(ServiceName, operationName);

                        if (!useTransactionScope)
                        {
                            try
                            {
                                response = invokeService(request, response, enableTracking, ref entityListStr, ref trackingEntityListStr);
                            }
                            catch (Exception)
                            {
                                response.Success = false;
                                throw;
                            }
                        }
                        else
                        {
                            using (var scope = new TransactionScope(TransactionScopeOption.Suppress, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadUncommitted }))
                            {
                                try
                                {
                                    response = invokeService(request, response, enableTracking, ref entityListStr, ref trackingEntityListStr);
                                    if (response.Success.HasValue && response.Success.Value)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        scope.Dispose();
                                    }
                                }
                                catch (DbEntityValidationException e)
                                {
                                    string msg = "";
                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        msg += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors: \r\n", eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            msg += string.Format("- Property: \"{0}\", Error: \"{1}\"\r\n", ve.PropertyName, ve.ErrorMessage);
                                        }
                                    }
                                    Exception tmpEx = e;
                                    while (tmpEx.InnerException != null)
                                    {
                                        msg += tmpEx.Message + Environment.NewLine;
                                        tmpEx = tmpEx.InnerException;
                                        msg += tmpEx.Message + Environment.NewLine;
                                    }
                                    response.Success = false;
                                    scope.Dispose();
                                    throw new Exception(msg);
                                }
                                catch (DbUpdateException e)
                                {
                                    string msg = "";
                                    foreach (var eve in e.Entries)
                                    {
                                        msg += string.Format("Type: {0} was part of the problem. Values: {1}\r\n", eve.Entity.GetType().Name, eve.Entity.ToString());
                                    }
                                    Exception tmpEx = e;
                                    while (tmpEx.InnerException != null)
                                    {
                                        msg += tmpEx.Message + Environment.NewLine;
                                        tmpEx = tmpEx.InnerException;
                                        msg += tmpEx.Message + Environment.NewLine;
                                    }
                                    response.Success = false;
                                    scope.Dispose();
                                    throw new Exception(msg);
                                }
                                catch (Exception)
                                {
                                    response.Success = false;
                                    scope.Dispose();
                                    throw;
                                }
                            }
                        }
                    }
                }
                else
                {
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                exceptionId = response.SetBaseResponse(ex, request);
            }
            finally
            {

                if (response.Success.HasValue && response.Success.Value)
                    response.CacheVersion = UpdateCacheNumber(request.ApplicationID, operationName);
                else
                    response.CacheVersion = request.CacheVersion;

                var durationInSeconds = DateTime.Now.Subtract(startDateTime).TotalSeconds;

                //if (durationInSeconds > ConfigManager.GetInstance(request.ApplicationID).LogMinSeconds)
                //{
                //    Log(request, response, durationInSeconds, exceptionId, operationName);
                //    LogTrack(request, response.Success ?? false, request.IsOnline, operationName, request.GetCurrentRecordID(), entityListStr, trackingEntityListStr, operationName, exceptionId);
                //}

                if (_currentUnitOfWork != null /* &&!request.InnerCall */)
                    _currentUnitOfWork.Dispose();

                SessionHelper.ClearPerRequest();
            }
            return response;
        }

        private void UpdateDeviceLanguage(IAbBaseRequest request)
        {
            var deviceID = request.DeviceID;
            if (deviceID > 0 && request.ApplicationID > 0)
            {
                var lang = request.Lang.ToString();
                var deviceRepository = new Common_UserDeviceRepository(CurrentContextEntities, request.ApplicationID);
                var device = deviceRepository.FirstOrDefault(p => p.ID == deviceID && p.Lang != lang);
                if (device != null)
                {
                    device.Lang = lang;
                    deviceRepository.Commit();
                }
            }
        }

        protected RS Invoke<RQ, RS>(ref RQ request, ref RS response, string operationName, bool useTransactionScope, bool useLock, InvokeService<RQ, RS> invokeService)
            where RQ : IAbBaseRequest
            where RS : BaseResponse
        {
            RS res = null;
            if (!useLock)
            {
                res = Invoke<RQ, RS>(ref request, ref response, operationName, useTransactionScope, invokeService);
            }
            else
            {
                lock (operationName)
                {
                    res = Invoke<RQ, RS>(ref request, ref response, operationName, useTransactionScope, invokeService);
                }
            }
            if (!res.Success.HasValue)
                res.SetBaseResponse(false, ResponseStatus.Exception, MessageKey.EmptyResponse);
            return res;
        }


        public static bool? GetIsDeleted(DeleteSelect deleteSelect)
        {
            bool? isDeleted = null;
            switch (deleteSelect)
            {
                case DeleteSelect.IsNotDeleted:
                    isDeleted = false;
                    break;
                case DeleteSelect.All:
                    isDeleted = null;
                    break;
                case DeleteSelect.IsDeleted:
                    isDeleted = true;
                    break;
            }
            return isDeleted;
        }

        public static bool? GetIsActive(ActiveSelect activeSelect)
        {
            bool? isActive = null;
            switch (activeSelect)
            {
                case ActiveSelect.IsNotActive:
                    isActive = false;
                    break;
                case ActiveSelect.All:
                    isActive = null;
                    break;
                case ActiveSelect.IsActive:
                    isActive = true;
                    break;
            }
            return isActive;
        }
    }
}