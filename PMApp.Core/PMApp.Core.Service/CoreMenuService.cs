using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Interfaces;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class CoreMenuService : BaseService, ICoreMenu
    {
        private const string serviceName = "CoreMenuService";
        public const string operationName_DoCoreMenuListInquiry = "DoCoreMenuListInquiry";
        public const string operationName_DoRoleMenuListInquiry = "DoRoleMenuListInquiry";
        public const string operationName_DoUpdateRoleMenu = "DoUpdateRoleMenu";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        public CoreMenuResponse DoCoreMenuListInquiry(CoreMenuRequest req)
        {
            var res = new CoreMenuResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoCoreMenuListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoCoreMenuListInquiry, false, false, (CoreMenuRequest request, CoreMenuResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    List<PMCore_Menu> coreMenuRecords = null;
                    var query = new PMCore_MenuRepository(CurrentContextEntities, 1).All().AsQueryable();
                    long roleID = 0;
                    if (!request.LoadAll)
                    {
                        roleID = (request.RoleID.HasValue) ? request.RoleID.Value : request.UserRoleID;
                        query = query.Where(p => p.PMCore_Role_Menu.Any(r => r.RoleID == roleID && r.ApplicationID == request.ApplicationID && r.IsAllowed == true));
                    }
                    switch (request.ActiveSelectOption)
                    {
                        case ActiveSelect.IsActive:
                            query = query.Where(p => p.Enable == true);
                            break;
                        case ActiveSelect.IsNotActive:
                            query = query.Where(p => p.Enable == false);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);



                    int count = 0;
                    coreMenuRecords = ApplyPaging(request, query, out count, true, "OrderID");

                    if (request.LoadAll)
                    {
                        for (int i = 0; i < coreMenuRecords.Count(); i++)
                        {
                            coreMenuRecords[i].IsDeleted = coreMenuRecords[i].PMCore_Role_Menu.Where(r => r.RoleID == request.RoleID && r.IsAllowed && r.ApplicationID == req.ApplicationID).Count() > 0;
                        }
                    }
                    var menuRecords = CoreMenuServiceManager.ConvertFromCoreMenu(coreMenuRecords, request.Lang, roleID);

                    response.CoreMenuRecords = CoreMenuServiceManager.RecursiveChildren(menuRecords, null);


                    response.TotalCount = count;
                    response.Success = true;

                    return response;
                });
            }
            return res;
        }


        public CoreMenuResponse DoRoleMenuListInquiry(CoreMenuRequest req)
        {
            var res = new CoreMenuResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoRoleMenuListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoRoleMenuListInquiry, false, false, (CoreMenuRequest request, CoreMenuResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var menuRepository = new PMCore_MenuRepository(CurrentContextEntities, req.ApplicationID);
                    var query = menuRepository.All().Select(p => new CoreMenuRecord
                    {
                        ID = p.ID,
                        IsAllowed = p.PMCore_Role_Menu.Where(r => r.RoleID == request.RoleID && r.IsAllowed && r.ApplicationID == req.ApplicationID).Count() > 0,
                        Name = p.Name,
                        ParentId = p.ParentID,
                        OrderID = p.OrderID ?? 0,
                        ApplicationID = p.ApplicationID

                    }).OrderBy(p => new { p.OrderID, p.ID }).ToList();
                    response.CoreMenuRecords = query;
                    response.Success = true;

                    return response;
                });
            }
            return res;
        }

        public CoreMenuResponse DoUpdateRoleMenu(CoreMenuRequest req)
        {
            var res = new CoreMenuResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoUpdateRoleMenu, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoUpdateRoleMenu, false, false, (CoreMenuRequest request, CoreMenuResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var requestRecord = request.CoreMenuRecord;
                    if (requestRecord != null)
                    {
                        var menuRoleRepository = new PMCore_Role_MenuRepository(CurrentContextEntities, request.ApplicationID);
                        var roleMenuRecord = menuRoleRepository.All().FirstOrDefault(r => r.RoleID == requestRecord.RoleId && r.MenuID == requestRecord.ID);
                        if (roleMenuRecord != null)
                        {
                            roleMenuRecord.IsAllowed = requestRecord.IsAllowed;
                        }
                        menuRoleRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        response.Success = true;
                    }

                    return response;
                });
            }
            return res;
        }
    }
}