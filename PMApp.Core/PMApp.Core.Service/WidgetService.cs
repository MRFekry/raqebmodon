using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Core.Interfaces;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class WidgetService : BaseService, IWidgetService
    {
        #region constants
        private const string serviceName = "WidgetService";
        private const string operationName_DoWidgetList = "DoWidgetList";
        public const string operationName_DoWidgetInquiry = "DoWidgetInquiry";
        protected override string ServiceName
        {
            get { return serviceName; }
        }
        #endregion constants

        public WidgetResponse DoWidgetInquiry(WidgetRequest req, ISBSService sbsService = null)
        {
            var res = new WidgetResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoWidgetInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoWidgetInquiry, false, false, (WidgetRequest request, WidgetResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var widget_RoleRepository = new PMCore_Widget_RoleRepository(CurrentContextEntities, request.ApplicationID);
                    var widgetRole = widget_RoleRepository.Where(p => p.RoleID == request.UserRoleID && p.WidgetID == request.WidgetID).FirstOrDefault();
                    //if (widgetRole == null)
                    //{
                    //    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidFilters);
                    //}
                    //else
                    //{
                        var widgetRepository = new PMCore_WidgetRepository(CurrentContextEntities, request.ApplicationID);
                        var widget = widgetRepository.FirstOrDefault(p => p.ID == request.WidgetID);
                        if (widget == null)
                        {
                            response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidFilters);
                        }
                        else
                        {
                            var widgetDataRecords = new List<WidgetDataRecord>();

                            if (!string.IsNullOrWhiteSpace(widget.SPName))
                            {
                                var spParameters = WidgetServiceManager.GetSPParameters(request.UserID, widget.SPName, request.Filter.Params);
                                var widgetDT = DBHelper.ExecuteProcedureForWidget(CurrentContextEntities.Database.Connection.ConnectionString, widget.SPName, request.ApplicationID, spParameters);
                                WidgetServiceManager.FillWidgetDataRecord(CurrentContextEntities, request.UserID, request.ApplicationID, widget, widgetDataRecords, widgetDT);
                            }
                            else if (widget.ServiceOperationID.HasValue && widget.ServiceOperationID.Value != 0 && sbsService != null)
                            {
                                var sbsRequest = new SBSRequest();
                                sbsRequest.TargetOperation = new PMCore_ServiceOperationRepository(CurrentContextEntities, request.ApplicationID).FirstOrDefault(p => p.ID == widget.ServiceOperationID.Value).OperationName;
                                sbsRequest.EncryptedRequest = GetEncryptedRequest(request);
                                var sbsResponse = sbsService.DoInvoke(sbsRequest);
                                WidgetServiceManager.FillWidgetDataRecord(CurrentContextEntities, widget, request.ApplicationID, widgetDataRecords, sbsResponse);
                            }

                            var widgetResponse = DoWidgetList(new WidgetRequest
                            {
                                InnerCall = true,
                                AuthToken = request.AuthToken,
                                WidgetID = request.WidgetID,
                            });
                            response.WidgetRecords = widgetResponse.WidgetRecords;
                            response.WidgetDataRecords = widgetDataRecords.ToArray();
                            response.Success = true;
                        }
                    //}
                    return response;
                });
            }
            return res;
        }

        private string GetEncryptedRequest(WidgetRequest request)
        {
            var str = "";
            var filters = "";
            for (int i = 0; i < request.Filter.Params.Length; i++)
            {
                str += string.Format("'{0}':'{1}',", request.Filter.Params[i].Name, request.Filter.Params[i].Value.ToString());

                if (!string.IsNullOrWhiteSpace(filters))
                    filters += ",";
                filters += string.Format("'{0}':'{1}'", request.Filter.Params[i].Name, request.Filter.Params[i].Value.ToString());
            }
            str += string.Format("'AuthToken':'{0}'", request.AuthToken);
            str += string.Format("'AppCode':'{0}'", request.AppCode);
            str += string.Format("'ChannelName':'{0}'", request.ChannelName);
            str += string.Format("'IP':'{0}'", request.IP);
            str += string.Format("'Lang':'{0}'", request.Lang);
            str += string.Format("'Filter':'{0}'", filters);

            return "{" + str + "}";
        }

        public WidgetResponse DoWidgetList(WidgetRequest req)
        {
            var res = new WidgetResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoWidgetList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoWidgetList, false, false, (WidgetRequest request, WidgetResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var widgetRepository = new PMCore_WidgetRepository(CurrentContextEntities, request.ApplicationID);

                    var widgetList = widgetRepository.Where(p => !p.IsDeleted && (!request.WidgetID.HasValue || request.WidgetID == p.ID))
                                                .Select(p => new WidgetRecord
                                                {
                                                    ID = p.ID,
                                                    Name = p.Name,
                                                    SPName = p.SPName,
                                                    Description = p.Description,
                                                    ChartType = p.ChartType,
                                                    XKeys = p.XKeys,
                                                    YKeys = p.YKeys,
                                                    YKeysSPName = p.YKeysSPName,
                                                    Colors = p.Colors,
                                                    UserID = request.UserID,
                                                    PMCore_ParameterList = p.PMCore_Widget_Parameter.Select(pr => pr.PMCore_Parameter).AsQueryable(),
                                                    CategoryID = p.CategoryID,
                                                    IsAllowed = p.PMCore_Widget_Role.Where(r => r.RoleID == request.UserRoleID && r.IsAllowed).Count() > 0 ? true : false,
                                                    ApplicationID = p.ApplicationID
                                                }).ToList();

                    for (int i = 0; i < widgetList.Count; i++)
                    {
                        var prmsDT = DBHelper.GetSPParameters(CurrentContextEntities.Database.Connection.ConnectionString, widgetList[i].SPName, request.ApplicationID);
                        var a = widgetList[i].PMCore_ParameterList.ToArray();
                        widgetList[i].Params = new List<ReportParam>();
                        for (int p = 0; p < a.Length; p++)
                        {
                            var defaultValue = WidgetServiceManager.GetParameterValue(CurrentContextEntities, request.UserID, request.ApplicationID, a[p].DefaultStaticValue, a[p].DefaultSessionValue, a[p].DefaultQueryValue);

                            var prm = prmsDT.Select(string.Format(" PARAMETER_NAME = '{0}' ", a[p].Name)).FirstOrDefault();
                            widgetList[i].Params.Add(new ReportParam
                            {
                                Name = a[p].Name,
                                DataType = prm == null ? string.Empty : prm["DATA_TYPE"].ToString(),
                                LookUpSPName = a[p].LookUpSPName,
                                ControlType = a[p].ControlType,
                                IsReadOnly = a[p].IsReadOnly,
                                Description = a[p].Description,
                                DefaultValue = defaultValue
                            });
                        }
                    }
                    response.WidgetRecords = widgetList.ToArray();
                    response.TotalCount = response.WidgetRecords.Length;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }



    }
}