using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Interfaces;
using PMApp.Modon.DAL6;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class PMCore_WidgetCategoryService : BaseService, IPMCore_WidgetCategory
    {
        private const string serviceName = "PMCore_WidgetCategoryService";

        protected override string ServiceName
        {
            get { return serviceName; }
        }

        #region PMCore_WidgetCategoryServices
        private const string operationName_DoPMCore_WidgetCategoryList = "DoPMCore_WidgetCategoryList";

        public PMCore_WidgetCategoryResponse DoPMCore_WidgetCategoryList(PMCore_WidgetCategoryRequest req)
        {
            var res = new PMCore_WidgetCategoryResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoPMCore_WidgetCategoryList, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoPMCore_WidgetCategoryList, false, false, (PMCore_WidgetCategoryRequest request, PMCore_WidgetCategoryResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var query = new PMCore_WidgetCategoryRepository(CurrentContextEntities, request.ApplicationID).All().Select(p => new PMCore_WidgetCategoryRecord
                    {

                        ID = p.ID,
                        Name = p.Name,
                        Description = p.Description,
                        IconClass = p.IconClass,

                        ApplicationID = p.ApplicationID
                    });

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var pMCore_WidgetCategorys = ApplyPaging(request, query, out count, true, PMCore_WidgetCategoryRecord.DisplayColumnName);

                    response.PMCore_WidgetCategoryRecords = pMCore_WidgetCategorys.ToArray();
                    response.TotalCount = count;
                    response.SetBaseResponse(true, ResponseStatus.Success, MessageKey.ListedSuccessfully);
                    return response;
                });
            }
            return res;
        }

        #endregion
    }
}