using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.DAL;
using System.Collections.Generic;
using System.Linq;

namespace PMApp.Core.Services.Helpers
{
    public class UtilityServiceHelper
    {
        public static CoreMenuRecord[] ConvertFromPMCoreMenuToCoreMenu(List<PMCore_Role_Menu> coreRole_MenuList, Language language)
        {
            var roleMenuList = new List<CoreMenuRecord>();
            for (int i = 0; i < coreRole_MenuList.Count; i++)
            {
                roleMenuList.Add(new CoreMenuRecord()
                {
                    ID = coreRole_MenuList[i].MenuID,
                    IsAllowed = coreRole_MenuList[i].IsAllowed.HasValue && coreRole_MenuList[i].IsAllowed.Value
                });
            }
            return roleMenuList.ToArray();
        }

        public static ServiceOperationRoleAccess[] ConvertFromPOSServiceOperationRoleAccess(List<PMCore_ServiceOperationRoleAccess> pmCoreServiceOperationRoleAccess)
        {
            var lst = new List<ServiceOperationRoleAccess>();
            for (int i = 0; i < pmCoreServiceOperationRoleAccess.Count(); i++)
            {
                var serviceOperationRoleAccess = new ServiceOperationRoleAccess();
                serviceOperationRoleAccess.ID = pmCoreServiceOperationRoleAccess[i].ID;
                serviceOperationRoleAccess.IsAllowed = pmCoreServiceOperationRoleAccess[i].IsAllowed;
                serviceOperationRoleAccess.RoleID = pmCoreServiceOperationRoleAccess[i].RoleId;
                serviceOperationRoleAccess.ServiceOperationID = pmCoreServiceOperationRoleAccess[i].ServiceOperationId;
                lst.Add(serviceOperationRoleAccess);
            }
            return lst.ToArray();
        }

        public static PMCore_ServiceOperationRoleAccess[] ConvertToPMCoreServiceOperationRoleAccess(ApplicationRole applicationRole)
        {
            var pmCoreServiceOperationRoleAccesses = new List<PMCore_ServiceOperationRoleAccess>();
            PMCore_ServiceOperationRoleAccess pmCoreServiceOperationRoleAccess;
            if (applicationRole.ServiceOperationRoleAccesses.Any())
            {
                foreach (var coreServiceOperationRoleAccess in applicationRole.ServiceOperationRoleAccesses)
                {
                    pmCoreServiceOperationRoleAccess = new PMCore_ServiceOperationRoleAccess();
                    pmCoreServiceOperationRoleAccess.IsAllowed = coreServiceOperationRoleAccess.IsAllowed;
                    pmCoreServiceOperationRoleAccess.ServiceOperationId = coreServiceOperationRoleAccess.ServiceOperationID;

                    pmCoreServiceOperationRoleAccesses.Add(pmCoreServiceOperationRoleAccess);
                }
            }
            return pmCoreServiceOperationRoleAccesses.ToArray();
        }

        public static PMCore_Role ConvertToPMCoreRoleRecord(ApplicationRole applicationRole, long applicationId)
        {
            var pmCoreRole = new PMCore_Role();
            pmCoreRole.Name = applicationRole.Name;
            pmCoreRole.Notes = applicationRole.Notes;
            pmCoreRole.ParentID = applicationRole.ParentId;
            pmCoreRole.IsAdmin = applicationRole.IsAdmin;
            pmCoreRole.IsActive = applicationRole.IsActive;
            pmCoreRole.IsDeleted = false;
            pmCoreRole.ApplicationID = applicationId;

            return pmCoreRole;
        }

        public static List<ServiceOperation> ConvertFromPMCoreServiceOperation(List<PMCore_ServiceOperation> seviceOperations, Language language)
        {
            ServiceOperation serviceOperation;
            var serviceOperations = new List<ServiceOperation>();
            foreach (var pmCoreServiceOperation in seviceOperations)
            {
                serviceOperation = new ServiceOperation();
                serviceOperation.ID = pmCoreServiceOperation.ID;
                serviceOperation.IsAnonymous = pmCoreServiceOperation.IsAnonymous;
                serviceOperation.OperationName = pmCoreServiceOperation.OperationName;
                serviceOperation.ServiceName = pmCoreServiceOperation.ServiceName;
                serviceOperation.OperationTitle = Localization.GetLocalizedText(language, pmCoreServiceOperation.OperationTitle, pmCoreServiceOperation.OperationTitle_LS);
                serviceOperations.Add(serviceOperation);
            }
            return serviceOperations;
        }

    }
}
