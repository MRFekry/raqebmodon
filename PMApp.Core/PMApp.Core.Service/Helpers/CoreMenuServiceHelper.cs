using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.DAL;
using System.Collections.Generic;

namespace PMApp.Core.Services.Helpers
{
    public class CoreMenuServiceHelper
    {
        public static CoreMenuRecord[] ConvertFromCoreMenu(List<PMCore_Menu> coreMenuRecords, Language language, long roleID)
        {
            CoreMenuRecord menuRecord;
            var coreMenus = new List<CoreMenuRecord>();
            foreach (var coreMenu in coreMenuRecords)
            {
                menuRecord = new CoreMenuRecord();
                menuRecord.ID = coreMenu.ID;
                menuRecord.Name = Localization.GetLocalizedText(language, coreMenu.Name, coreMenu.Name_LS);
                menuRecord.Tip = Localization.GetLocalizedText(language, coreMenu.Tip, coreMenu.Tip_LS);
                menuRecord.ParentId = coreMenu.ParentID;
                menuRecord.RoleId = roleID;
                menuRecord.InitialValues = coreMenu.InitialValues;
                menuRecord.SenchaMenuId = coreMenu.SenchaMenuId;
                menuRecord.IsAddNewMenu = coreMenu.IsAddNewMenu.HasValue && coreMenu.IsAddNewMenu.Value;
                coreMenus.Add(menuRecord);
            }
            return coreMenus.ToArray();
        }

    }
}
