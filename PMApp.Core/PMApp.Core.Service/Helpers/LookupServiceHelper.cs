using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.DAL;
using System.Collections.Generic;

namespace PMApp.Core.Services.Helpers
{
    public class LookupServiceHelper
    {
        public static void ConvertToPOSLookup(ref PMCore_Lookup Lookup, long applicationID, long nextID, LookupRecord LookupRecord)
        {
            Lookup.LookupText = !string.IsNullOrWhiteSpace(LookupRecord.LookupText) ? LookupRecord.LookupText : LookupRecord.LookupText_LS;
            Lookup.LookupText_LS = !string.IsNullOrWhiteSpace(LookupRecord.LookupText_LS) ? LookupRecord.LookupText_LS : LookupRecord.LookupText;

            if (nextID != 0)
            {
                Lookup.LookupName = LookupRecord.LookupName;
                Lookup.ApplicationID = applicationID;
                Lookup.Enable = true;
                Lookup.LookupKey = nextID.ToString();
            }
        }

        public static LookupRecord[] ConvertFromPOSLookup(bool showLSText, List<PMCore_Lookup> posLookupRecords, Language language)
        {
            LookupRecord LookupRecord;
            var Lookups = new List<LookupRecord>();
            foreach (var posLookupRecord in posLookupRecords)
            {
                LookupRecord = new LookupRecord();
                LookupRecord.ID = posLookupRecord.ID;
                LookupRecord.LookupKey = posLookupRecord.LookupKey;
                LookupRecord.LookupName = posLookupRecord.LookupName;
                if (showLSText)
                {
                    LookupRecord.LookupText = posLookupRecord.LookupText;
                    LookupRecord.LookupText_LS = posLookupRecord.LookupText_LS;
                }
                else {
                    LookupRecord.LookupText = Localization.GetLocalizedText(language, posLookupRecord.LookupText, posLookupRecord.LookupText_LS);
                }
                Lookups.Add(LookupRecord);
            }
            return Lookups.ToArray();
        }

    }
}
