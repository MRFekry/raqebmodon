using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.DAL;
using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;

namespace PMApp.Core.Services.Helpers
{
    public class UserServiceHelper
    {
        public static UserRecord[] ConvertFromCommonUser(List<Common_User> commonUsers, Language language)
        {
            UserRecord userRecord;
            var userRecords = new List<UserRecord>();
            foreach (var commonUser in commonUsers)
            {
                userRecord = new UserRecord();
                userRecord.ID = commonUser.ID;
                userRecord.IsActive = commonUser.IsActive;
                userRecord.IsDeleted = commonUser.IsDeleted;
                userRecord.FullName = commonUser.FullName;
                userRecord.RegisterDate = DateTimeHelper.ToStringDD_MM_YYYY(commonUser.RegisterDate);
                userRecord.UserName = commonUser.UserName;
                if (commonUser.Birthdate.HasValue)
                    userRecord.Birthdate = DateTimeHelper.ToStringDD_MM_YYYY(commonUser.Birthdate.Value);
                userRecord.RoleId = commonUser.RoleId;
                userRecord.AllowedIPs = commonUser.App_AllowedIPs;
                userRecord.FullName = commonUser.FullName;
                userRecord.Notes = commonUser.Notes;
                //userRecord.ManagerID = commonUser.ManagerID ?? 0;
                //userRecord.ContactInfo = GetContactInfoForStock(commonUser.App_ContactInfoID, language);

                var commonContactInfoRepository = new Common_ContactInfoRepository();
                var commonContactInfo =
                    commonContactInfoRepository.FirstOrDefault(p => p.ID == commonUser.App_ContactInfoID);
                if (commonContactInfo != null)
                {
                    userRecord.AddressLine1 = commonContactInfo.AdressLine1;
                    userRecord.AddressLine2 = commonContactInfo.AdressLine2;
                    userRecord.AddressLine3 = commonContactInfo.AdressLine3;
                    userRecord.ContactInfoNotes = commonContactInfo.Notes;
                    userRecord.Phone1 = commonContactInfo.Phone1;
                    userRecord.Phone2 = commonContactInfo.Phone2;
                    userRecord.Phone3 = commonContactInfo.Phone3;
                    userRecord.WebSite = commonContactInfo.WebSite;
                    userRecord.Email = commonContactInfo.Email;
                }
                userRecords.Add(userRecord);
            }
            return userRecords.ToArray();
        }

        public static Common_ContactInfo ConvertToCommonContactInfo(UserRecord contactInformation)
        {
            var commonContactInfo = new Common_ContactInfo();
            commonContactInfo.AdressLine1 = contactInformation.AddressLine1;
            commonContactInfo.AdressLine2 = contactInformation.AddressLine2;
            commonContactInfo.AdressLine3 = contactInformation.AddressLine3;
            commonContactInfo.Notes = contactInformation.ContactInfoNotes;
            commonContactInfo.Phone1 = contactInformation.Phone1;
            commonContactInfo.Phone2 = contactInformation.Phone2;
            commonContactInfo.Phone3 = contactInformation.Phone3;
            commonContactInfo.WebSite = contactInformation.WebSite;
            commonContactInfo.Email = contactInformation.Email;
            return commonContactInfo;
        }

        public static Common_User ConvertToCommonUserRecord(UserRecord userRecord, long applicationId)
        {
            var commonUser = new Common_User();
            commonUser.ApplicationID = applicationId;
            commonUser.RegisterDate = DateTime.Now;
            commonUser.UserName = userRecord.UserName;
            commonUser.FullName = userRecord.FullName;
            commonUser.Password = PasswordHelper.CreateHash(userRecord.Password);
            commonUser.Birthdate = DateTimeHelper.ToDateDD_MM_YYYY(userRecord.Birthdate);
            commonUser.RoleId = userRecord.RoleId;
            commonUser.App_AllowedIPs = userRecord.AllowedIPs;
            commonUser.IsActive = userRecord.IsActive;
            commonUser.IsDeleted = userRecord.IsDeleted;
            commonUser.Notes = userRecord.Notes;
            //commonUser.ManagerID = userRecord.ManagerID;

            return commonUser;
        }

    }
}