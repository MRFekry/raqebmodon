using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Core.Interfaces;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Services
{

    [AspNetCompatibilityRequirements(RequirementsMode = DefaultAttributes.AspNetCompatibilityRequirementsAttribute)]
    public class UtilityService : BaseService, IUtility
    {
        private const string serviceName = "UtilityService";
        public const string operationName_DoLanguageListInquiry = "DoLanguageListInquiry";
        public const string operationName_DoWorkingDayListInquiry = "DoWorkingDayListInquiry";
        public const string operationName_DoServiceOperationListInquiry = "DoServiceOperationListInquiry";
        public const string operationName_DoServiceRoleAccessInsertOperation = "DoServiceRoleAccessInsertOperation";
        public const string operationName_DoServiceRoleAccessUpdateOperation = "DoServiceRoleAccessUpdateOperation";
        public const string operationName_DoServiceRoleAccessDeleteOperation = "DoServiceRoleAccessDeleteOperation";
        public const string operationName_DoServiceRoleAccessListInquiry = "DoServiceRoleAccessListInquiry";
        public const string operationName_DoRoleListInquiry = "DoRoleListInquiry";
        public const string operationName_DoLogException = "DoLogException";


        protected override string ServiceName
        {
            get { return serviceName; }
        }

        public WorkingDayResponse DoWorkingDayListInquiry(WorkingDayRequest req)
        {
            var res = new WorkingDayResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoWorkingDayListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoWorkingDayListInquiry, false, false, (WorkingDayRequest request, WorkingDayResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                   
                    return response;
                });
            }
            return res;
        }

        private WorkingDayResponse Invoke(ref WorkingDayRequest req, ref WorkingDayResponse res, string operationName_DoWorkingDayListInquiry, bool p1, bool p2, InvokeService<WorkingDayRequest, WorkingDayResponse> invokeService)
        {
            throw new System.NotImplementedException();
        }


        public LanguageResponse DoLanguageListInquiry(LanguageRequest req)
        {
            var res = new LanguageResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLanguageListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLanguageListInquiry, false, false, (LanguageRequest request, LanguageResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var languageRecords = new List<LanguageRecord>();

                    var languageRecord = new LanguageRecord();
                    languageRecord.ID = Language.Ar.GetHashCode();
                    languageRecord.Name = ResourceManager.GetInstance(request.ApplicationID).GetResource(Language.Ar.ToString(), request.Lang.ToString());
                    languageRecord.ApplicationID = request.ApplicationID;
                    languageRecords.Add(languageRecord);

                    languageRecord = new LanguageRecord();
                    languageRecord.ID = Language.En.GetHashCode();
                    languageRecord.Name = ResourceManager.GetInstance(request.ApplicationID).GetResource(Language.En.ToString(), request.Lang.ToString());
                    languageRecord.ApplicationID = request.ApplicationID;
                    languageRecords.Add(languageRecord);

                    response.LanguageRecords = languageRecords.ToArray();
                    response.Success = true;
                    return response;
                });
            }
            return res;
        }

        public ServiceOperationResponse DoServiceOperationListInquiry(ServiceOperationRequest req)
        {
            var res = new ServiceOperationResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoServiceOperationListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoServiceOperationListInquiry, false, false, (ServiceOperationRequest request, ServiceOperationResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    List<PMCore_ServiceOperation> posServiceOperation = null;

                    var query = new PMCore_ServiceOperationRepository(CurrentContextEntities, request.ApplicationID).All();

                    if (request.NoAnonymous)
                        query = query.Where(p => !p.IsAnonymous);

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    posServiceOperation = ApplyPaging(request, query, out count);

                    var seviceOperations = UtilityServiceManager.ConvertFromPMCoreServiceOperation(posServiceOperation, request.Lang);

                    response.ServiceOperations = seviceOperations.OrderBy(p => p.OperationTitle).ToArray();
                    response.TotalCount = count;
                    response.Success = true;
                    return response;
                });
            }
            return res;
        }

        public ServiceOperationRoleAccessResponse DoServiceRoleAccessInsertOperation(ServiceOperationRoleAccessRequest req)
        {
            var res = new ServiceOperationRoleAccessResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoServiceRoleAccessInsertOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoServiceRoleAccessInsertOperation, true, false, (ServiceOperationRoleAccessRequest request, ServiceOperationRoleAccessResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.ApplicationRole != null)
                    {
                        var coreRoleRepository = new PMCore_RoleRepository(CurrentContextEntities, request.ApplicationID);
                        var serviceOperationRoleAccessRepository = new PMCore_ServiceOperationRoleAccessRepository(CurrentContextEntities, request.ApplicationID);
                        var pmCoreRole = UtilityServiceManager.ConvertToPMCoreRoleRecord(request.ApplicationRole);
                        var pmCoreServiceOperationRoleAccesses = UtilityServiceManager.ConvertToPMCoreServiceOperationRoleAccess(request.ApplicationRole);
                        coreRoleRepository.Add(pmCoreRole);
                        if (request.ApplicationRole.RoleMenu != null && request.ApplicationRole.RoleMenu.Length > 0)
                        {
                            var coreRoleMenuRepository = new PMCore_Role_MenuRepository(CurrentContextEntities, request.ApplicationID);
                            for (int i = 0; i < request.ApplicationRole.RoleMenu.Length; i++)
                            {
                                coreRoleMenuRepository.Add(new PMCore_Role_Menu
                                {
                                    RoleID = request.ApplicationRole.ID,
                                    MenuID = request.ApplicationRole.RoleMenu[i].ID,
                                    IsAllowed = request.ApplicationRole.RoleMenu[i].IsAllowed
                                });
                            }
                        }

                        coreRoleRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        request.ApplicationRole.ID = pmCoreRole.ID;

                        foreach (var pmCoreServiceOperationRoleAccess in pmCoreServiceOperationRoleAccesses)
                        {
                            pmCoreServiceOperationRoleAccess.RoleId = pmCoreRole.ID;
                            serviceOperationRoleAccessRepository.Add(pmCoreServiceOperationRoleAccess);
                        }

                        serviceOperationRoleAccessRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                    }
                    response.Success = true;
                    return response;
                });
            }
            return res;
        }

        public ServiceOperationRoleAccessResponse DoServiceRoleAccessUpdateOperation(ServiceOperationRoleAccessRequest req)
        {
            ServiceOperationRoleAccessResponse res = new ServiceOperationRoleAccessResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoServiceRoleAccessUpdateOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoServiceRoleAccessUpdateOperation, true, false, (ServiceOperationRoleAccessRequest request, ServiceOperationRoleAccessResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var serviceOperationRoleAccessRecord = request.ServiceOperationRoleAccessRecord;
                    if (serviceOperationRoleAccessRecord != null)
                    {
                        var serviceOperationRoleAccessRepository = new PMCore_ServiceOperationRoleAccessRepository(CurrentContextEntities, request.ApplicationID);
                        var serviceOperationRoleAccess = serviceOperationRoleAccessRepository.All().FirstOrDefault(s => s.ID == serviceOperationRoleAccessRecord.ID);
                        if (serviceOperationRoleAccess != null)
                        {
                            serviceOperationRoleAccess.IsAllowed = serviceOperationRoleAccessRecord.IsAllowed;
                            serviceOperationRoleAccessRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                        }

                        response.Success = true;
                    }

                    return response;
                });
            }
            return res;
        }

        public ServiceOperationRoleAccessResponse DoServiceRoleAccessDeleteOperation(ServiceOperationRoleAccessRequest req)
        {
            var res = new ServiceOperationRoleAccessResponse(req.Lang, req.ApplicationID);

            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoServiceRoleAccessDeleteOperation, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoServiceRoleAccessDeleteOperation, true, false, (ServiceOperationRoleAccessRequest request, ServiceOperationRoleAccessResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    if (request.ApplicationRole != null && request.ApplicationRole.ID > 0)
                    {
                        var userRepository = new Common_UserRepository(CurrentContextEntities, request.ApplicationID);
                        var hasUsers = userRepository.FirstOrDefault(p => p.RoleID == request.ApplicationRole.ID) != null;

                        if (!hasUsers)
                        {
                            var pmCoreRoleRepository = new PMCore_RoleRepository(CurrentContextEntities, request.ApplicationID);
                            var pmCoreRole = pmCoreRoleRepository.FirstOrDefault(p => p.ID == request.ApplicationRole.ID);

                            pmCoreRole.IsDeleted = true;
                            pmCoreRoleRepository.Commit(entableTracking, ref entityListStr, ref trackingEntityListStr);
                            response.Success = true;
                        }
                        else
                        {
                            response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.RoleCantDeleteBecauseItHasUsers);
                        }
                    }
                    return response;
                });
            }
            return res;
        }

        public ServiceOperationRoleAccessResponse DoServiceRoleAccessListInquiry(ServiceOperationRoleAccessRequest req)
        {
            var res = new ServiceOperationRoleAccessResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoServiceRoleAccessListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoServiceRoleAccessListInquiry, false, false, (ServiceOperationRoleAccessRequest request, ServiceOperationRoleAccessResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var pmCoreRoleRepository = new PMCore_RoleRepository(CurrentContextEntities, request.ApplicationID);
                    var pmCoreServiceOperationRoleAccessRepository = new PMCore_ServiceOperationRoleAccessRepository(CurrentContextEntities, request.ApplicationID);

                    var filterRoleId = request.Filter != null ? request.Filter.FilterRoleID : 0;

                    var query = (from pmCoreRole in pmCoreRoleRepository.All()
                                 where
                                 (filterRoleId != 0 && pmCoreRole.ID == filterRoleId)
                                 select new SelectApplicationRole
                                 {
                                     ID = pmCoreRole.ID,
                                     Name = pmCoreRole.Name,
                                     PMCoreRole = pmCoreRole,
                                     PMServiceOperations = pmCoreRole.PMCore_ServiceOperationRoleAccess.Where(s => !s.PMCore_ServiceOperation.IsAnonymous),
                                     IsActive = pmCoreRole.IsActive,
                                     IsAdmin = pmCoreRole.IsAdmin,
                                     IsDeleted = pmCoreRole.IsDeleted
                                 });

                    switch (request.ActiveSelectOption)
                    {
                        case ActiveSelect.IsActive:
                            query = query.Where(p => p.IsActive);
                            break;
                        case ActiveSelect.IsNotActive:
                            query = query.Where(p => !p.IsActive);
                            break;
                    }

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var pmCoreSeviceOperationRoleAccessList = ApplyPaging(request, query, out count);

                    var seviceOperationRoleAccessList = new List<ApplicationRoleRecord>();
                    for (int i = 0; i < pmCoreSeviceOperationRoleAccessList.Count; i++)
                    {
                        seviceOperationRoleAccessList.Add(new ApplicationRoleRecord()
                        {
                            ID = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.ID,
                            IsActive = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.IsActive,
                            IsAdmin = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.IsAdmin.HasValue && pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.IsAdmin.Value,
                            Name = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.Name,
                            Notes = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.Notes,
                            ParentId = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.ParentID,
                            ServiceOperationRoleAccesses = UtilityServiceManager.ConvertFromServiceOperationRoleAccess(request.ApplicationID, pmCoreSeviceOperationRoleAccessList[i].PMServiceOperations.ToList()),
                            ApplicationID = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.ApplicationID
                        });
                    }
                    response.ApplicationRoles = seviceOperationRoleAccessList.ToArray();
                    response.TotalCount = count;
                    response.Success = true;
                    return response;
                });
            }
            return res;
        }

        public ServiceOperationRoleAccessResponse DoRoleListInquiry(ServiceOperationRoleAccessRequest req)
        {
            var res = new ServiceOperationRoleAccessResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoRoleListInquiry, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoRoleListInquiry, false, false, (ServiceOperationRoleAccessRequest request, ServiceOperationRoleAccessResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    var pmCoreRoleRepository = new PMCore_RoleRepository(CurrentContextEntities, request.ApplicationID);

                    var query = (from pmCoreRole in pmCoreRoleRepository.All()
                                 select new SelectApplicationRole
                                 {
                                     ID = pmCoreRole.ID,
                                     PMCoreRole = pmCoreRole,
                                     IsActive = pmCoreRole.IsActive,
                                     IsAdmin = pmCoreRole.IsAdmin,
                                     IsDeleted = pmCoreRole.IsDeleted,
                                     Name = pmCoreRole.Notes
                                 });
                    switch (request.ActiveSelectOption)
                    {
                        case ActiveSelect.IsActive:
                            query = query.Where(p => p.IsActive);
                            break;
                        case ActiveSelect.IsNotActive:
                            query = query.Where(p => !p.IsActive);
                            break;
                    }

                    switch (request.DeleteSelectOption)
                    {
                        case DeleteSelect.IsDeleted:
                            query = query.Where(p => p.IsDeleted);
                            break;
                        case DeleteSelect.IsNotDeleted:
                            query = query.Where(p => !p.IsDeleted);
                            break;
                    }

                    if (request.Filter != null)
                        query = request.Filter.ApplyFilter(query);

                    int count = 0;
                    var pmCoreSeviceOperationRoleAccessList = ApplyPaging(request, query, out count);

                    var seviceOperationRoleAccessList = new List<ApplicationRoleRecord>();
                    for (int i = 0; i < pmCoreSeviceOperationRoleAccessList.Count; i++)
                    {
                        seviceOperationRoleAccessList.Add(new ApplicationRoleRecord()
                        {
                            ID = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.ID,
                            IsActive = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.IsActive,
                            IsAdmin = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.IsAdmin.HasValue && pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.IsAdmin.Value,
                            Name = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.Name,
                            Notes = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.Notes,
                            ParentId = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.ParentID,
                            ApplicationID = pmCoreSeviceOperationRoleAccessList[i].PMCoreRole.ApplicationID
                        });
                    }
                    response.ApplicationRoles = seviceOperationRoleAccessList.ToArray();
                    response.TotalCount = count;
                    response.Success = true;
                    return response;
                });
            }
            return res;
        }

        public LogExceptionResponse DoLogException(LogExceptionRequest req)
        {
            var res = new LogExceptionResponse(req.Lang, req.ApplicationID);
            if (req.IsValid(CurrentContextEntities, ref res, operationName_DoLogException, ServiceName))
            {
                res = Invoke(ref req, ref res, operationName_DoLogException, false, false, (LogExceptionRequest request, LogExceptionResponse response, bool entableTracking, ref string entityListStr, ref string trackingEntityListStr) =>
                {
                    response.ExceptionID = base.LogException(request, request.Message, request.StackTrace);
                    response.Success = true;
                    return response;
                });
            }
            return res;
        }



    }
}