using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.Helpers;
using PMApp.Core.Interfaces;
using PMApp.Core.Services.Managers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;

namespace  PMApp.Core.Services
{
    public delegate RS InvokeSBSService<RQ, RS>(RQ request)
        where RQ : IAbBaseRequest
        where RS : BaseResponse;

    public abstract class BaseSBSService : ISBSService
    {
        protected void Log(long applicationID, SBSRequest request, SBSResponse response, string appName, double durationInSeconds, long exceptionId, string operationName)
        {
            if (applicationID > 0 && HostCommunicationLogger.GetInstance(applicationID).IsLoggingEnabled())
            {
                HostCommunicationLogger.GetInstance(applicationID).Log(appName,
                0, durationInSeconds,
                response.IsSuccess, request.IsOnline, request, response,
                response.UserID, SessionHelper.CurrentIP,
                SessionHelper.CurrentSessionID, response.ChannelName,
                exceptionId,
                operationName, null);
            }
            SessionHelper.ClearPerRequest();
        }

        protected void DoInvokeService<RQ, RS>(SBSRequest sbsRequest, ref SBSResponse sbsResponse, InvokeSBSService<RQ, RS> invokeService)
            where RQ : IAbBaseRequest
            where RS : BaseResponse
        {
            long performanceLoggerID = 0;
            RQ request = (RQ)JsonConvert.DeserializeObject(sbsRequest.EncryptedRequest, typeof(RQ));

            request.SetApplicationName(sbsRequest.ApplicationName);
            var applicationID = request.ApplicationID;
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromAppCode(sbsRequest.ApplicationName);
                request.SetApplicationID(applicationID);
            }

            RS response = null;
            if (applicationID > 0)
            {
                var serviceOperation = ServiceOperationManager.GetInstance(applicationID).GetServiceOperation(string.Empty, sbsRequest.TargetOperation);

                if (serviceOperation == null || serviceOperation.EnableLog == true)
                    performanceLoggerID = PerformanceLogger.GetInstance(applicationID).LogStartTime(Channel.Service, sbsRequest.TargetOperation);

                request.IsOnline = sbsRequest.IsOnline;

                response = invokeService(request);
            }

            sbsResponse.EncryptedResponse = PMExtentions.TrySerialize(response);
            if (string.IsNullOrWhiteSpace(sbsResponse.EncryptedResponse))
            {
                response = (RS)Activator.CreateInstance(typeof(RS));
                if (applicationID == 0)
                {
                    response.Message = ApplicationManager.EMPTY_APPLICATIONID_ERROR;
                }
                sbsResponse.EncryptedResponse = PMExtentions.TrySerialize(response);
                //TODO LogException
            }

            sbsResponse.IsSuccess = response.Success ?? false;
            sbsResponse.AppCode = request.AppCode;
            sbsResponse.ChannelName = request.ChannelName;
            sbsResponse.UserID = request.UserID;
            sbsResponse.ApplicationID = applicationID;

            if (applicationID > 0 && request.UserID != 0 && request.UserLocationRecord != null)
            {
                var userLocation = UtilityServiceManager.CreateOrUpdateUserLocation(request.UserID, request.ChannelName, request.UserLocationRecord);
                UserLocationLogger.GetInstance(applicationID).Log(userLocation);
            }

            if (applicationID > 0 && performanceLoggerID != 0)
            {
                PerformanceLogger.GetInstance(applicationID).LogEndTime(performanceLoggerID);
            }
        }

        public abstract SBSResponse DoInvoke(SBSRequest sbsRequest);
    }
}