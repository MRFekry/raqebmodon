using Newtonsoft.Json;
using PMApp.Modon.DAL6;
using System.Web.Script.Serialization;
using PMApp.Core.Helpers;

namespace  PMApp.Core.CommonDefinitions
{
    [System.Runtime.Serialization.DataContract]
    public abstract class BaseResponse
    {
        private string currentLanguage = "En";

        public BaseResponse(PMApp.Core.Helpers.Language lang, long applicationID)
        {
            currentLanguage = lang.ToString();
            this.ApplicationID = applicationID;
        }

        [JsonProperty(PropertyName = "S", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Status { get; set; }

        [ScriptIgnore]
        public long ApplicationID { get; private set; }

        [JsonProperty(PropertyName = "E", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Error Error { get; set; }

        [JsonProperty(PropertyName = "M", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "MK", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string MessageKey { get; set; }

        [JsonProperty(PropertyName = "IS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? Success { get; set; }

        [JsonProperty(PropertyName = "CV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long CacheVersion { get; set; }


        [JsonProperty(PropertyName = "TC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public int TotalCount { get; set; }

        [System.Runtime.Serialization.OnDeserialized]
        private void SetValuesOnDeserialized(System.Runtime.Serialization.StreamingContext context)
        {
            if (!Success.HasValue)
            {
                Success = true;
            }
        }

        public long SetBaseResponse(bool success, ResponseStatus status, string messageKey, params string[] parms)
        {
            this.Status = status.ToString();

            if (ApplicationID > 0)
            {
                if (!success && this.Error == null)
                {
                    this.Error = new Error();
                    this.Error.Message = ResourceManager.GetInstance(ApplicationID).GetResource(messageKey, currentLanguage);
                    this.Error.Code = status.ToString();
                }
                this.Success = success;
                this.Message = ResourceManager.GetInstance(ApplicationID).GetResource(messageKey, currentLanguage);
                this.MessageKey = messageKey;
                if (parms != null && parms.Length > 0)
                {
                    this.Message = string.Format(this.Message, parms);
                }
            }
            else
            {
                this.Success = false;
                this.MessageKey = this.Message = PMApp.Core.CommonDefinitions.MessageKey.InvalidApplicationID.ToString();
            }
            return 0;
        }

        /// <summary>
        /// Set Response
        /// </summary>
        /// <param name="success"></param>
        /// <param name="status">Active = 1 | Inactive = 2 | RequestDataInvalid = 3</param>
        /// <param name="messageKey"></param>
        public long SetBaseResponse(bool success, ResponseStatus status, MessageKey messageKey)
        {
            return SetBaseResponse(success, status, messageKey.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public long SetBaseResponse(System.Exception exception, IAbBaseRequest request)
        {
            long? userid = null;
            if (request.UserID != 0)
                userid = request.UserID;

            long id = 0;
            this.Error = new Error();
            this.Error.Message = exception.InnerException == null ? exception.Message : exception.InnerException.Message;// +" - " + exception.StackTrace;
            if (ApplicationID > 0 && !IsKnownErrors())
            {
                var channelName = request.ChannelName;
                id = ExceptionLogger.GetInstance(ApplicationID).Log(exception, userid == 0 ? null : userid, null, channelName);
                this.Error.Code = "EX" + id.ToString();
            }

            //if (exception.InnerException != null)
            //{
            //    this.Error.Message += " -- " + exception.InnerException.Message + " - " + exception.InnerException.StackTrace;
            //}
            this.SetBaseResponse(false, ResponseStatus.Exception, string.Format("({0}) {1}", this.Error.Code, this.Error.Message));
            return id;
        }

        private const string ThisRecordCantBeDeleted = "The DELETE statement conflicted with the REFERENCE constraint";

        private bool IsKnownErrors()
        {
            if (ApplicationID > 0 && this.Error.Message.Contains(ThisRecordCantBeDeleted))
            {
                this.Error.Message = ResourceManager.GetInstance(ApplicationID).GetResource("ThisRecordCantBeDeleted", currentLanguage);
                this.Error.Code = "Ex";
                return true;
            }
            return false;
        }
    }

}
