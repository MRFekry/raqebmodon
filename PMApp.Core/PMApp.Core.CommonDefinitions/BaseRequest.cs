﻿using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;

namespace  PMApp.Core.CommonDefinitions
{
    [System.Runtime.Serialization.DataContract]
    public abstract class BaseRequest<T, F> : AbBaseRequest<F>
        where T : BaseResponse
        where F : AbRequestFilter
    {
        public virtual bool IsValid(ModonDBEntities  contextEntities, ref T response, string operationName, string serviceName)
        {
            try
            {
                this.CurrentContextEntities = contextEntities;
                var serviceOperation = GetCurrentServiceOperation(operationName, serviceName);
                
                if (serviceOperation == null ||serviceOperation.IsAnonymous )
                {
                    return true;

                }
              return  CheckUserRoleAuthenticatedWithOperation(serviceOperation, ref response);

                if (!response.Success.HasValue)
                    return true;
                return response.Success.Value;
            }
            catch (System.Exception ex)
            {
                response.SetBaseResponse(ex, this);
                return false;
            }
        }


        private void UpdateUserDeviceLanguage()
        {
            var userDeviceRepository = new Common_UserDeviceRepository(CurrentContextEntities, ApplicationID);
            if (DeviceID > 0)
            {
                var requestLang = Lang.ToString();
                var userDevice = userDeviceRepository.FirstOrDefault(p => p.ID == DeviceID && p.Lang != requestLang);
                if (userDevice != null)
                {
                    userDevice.Lang = requestLang;
                    string entityListStr = "", trackingEntityListStr = "";
                    userDeviceRepository.Commit(false, ref entityListStr, ref trackingEntityListStr);
                }
            }
        }

        private bool CheckUserRoleAuthenticatedWithOperation(PMCore_ServiceOperation serviceOperation, ref T response)
        {
            if (this.UserRoleID == 0)
            {
                return true;
            }
            var isAllowed = ServiceOperationManager.GetInstance(this.ApplicationID).ServiceOperationIsAllowed(this.UserRoleID, serviceOperation.OperationName);

            if (!isAllowed)
            {
                response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, "غير مسموح بهذا الإجراء");
            }

            if (!response.Success.HasValue)
                return true;
            return response.Success.Value;
        }

        private PMCore_ServiceOperation GetCurrentServiceOperation(string operationName, string serviceName)
        {
            if (this.ApplicationID > 0)
            {
                //string operationName = "", serviceName = "";
                //int s = 0;
                //var callee = new StackTrace(s).GetFrame(0).GetMethod();
                //while (callee.DeclaringType != null)
                //{
                //    s++;
                //    callee = new StackTrace(s).GetFrame(0).GetMethod();
                //}
                //callee = new StackTrace(s - 1).GetFrame(0).GetMethod();
                //operationName = callee.Name;
                //serviceName = callee.DeclaringType.Name;

                return ServiceOperationManager.GetInstance(this.ApplicationID).GetServiceOperation(serviceName, operationName);
            }
            return null;
        }
    }
}
