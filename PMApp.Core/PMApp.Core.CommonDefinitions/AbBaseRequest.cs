using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using PMApp.Modon.DAL6.EntitiesExtention;

namespace  PMApp.Core.CommonDefinitions
{

    [System.Runtime.Serialization.DataContract]
    public abstract class AbBaseRequest<F> : AbFilter<F>, IAbBaseRequest
        where F : AbRequestFilter
    {
        private bool innerCall;

        [ScriptIgnore]
        public bool InnerCall
        {
            get
            {
                return innerCall;
            }
            set
            {
                this.innerCall = value;
                if (this.innerCall)
                    IsOnline = value;
            }
        }

        [ScriptIgnore]
        public bool IsOnline { get; set; }

        [JsonProperty(PropertyName = "DSO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public DeleteSelect DeleteSelectOption { get; set; }

        [JsonProperty(PropertyName = "ASO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ActiveSelect ActiveSelectOption { get; set; }

        public virtual long GetCurrentRecordID()
        {
            return 0;
        }

        [JsonProperty(PropertyName = "AT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AuthToken { get; set; }

        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string IP { get; set; }

        [JsonProperty(PropertyName = "L", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public PMApp.Core.Helpers.Language Lang { get; set; }

        private long deviceID;
        [ScriptIgnore]
        public long DeviceID
        {
            get
            {
                if (this.deviceID == 0)
                {
                    this.deviceID = SecurityHelper.GetDeviceIDFromAuthenticationToken(this.AuthToken);
                }
                return this.deviceID;
            }
        }

        protected long userRoleID;
        [ScriptIgnore]
        public long UserRoleID
        {
            get
            {
                if (this.userRoleID == 0 && this.UserID != 0)
                {
                    this.userRoleID = SecurityHelper.GetUserRoleIDFromAuthenticationToken(this.AuthToken);
                }
                return this.userRoleID;
            }
            set
            {
                this.userRoleID = value;
            }
        }

        private long userID;
        [ScriptIgnore]
        public long UserID
        {
            get
            {
                if (this.userID == 0)
                {
                    this.userID = SecurityHelper.GetUserIDFromAuthenticationToken(this.AuthToken);
                }
                return this.userID;
            }
            set { this.userID = value; }
        }

        private string channelName;
        [ScriptIgnore]
        public string ChannelName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.channelName))
                {
                    this.channelName = SecurityHelper.GetChannelFromAuthenticationToken(this.AuthToken);
                }
                return this.channelName;
            }
        }

        private long channelID = 0;
        [ScriptIgnore]
        public long ChannelID
        {
            get
            {
                if (channelID == 0 && !string.IsNullOrWhiteSpace(ChannelName))
                {
                    var unitOfWork = EFUnitOfWork.NewUnitOfWork("ChannelID");
                    var channel = new PMCore_ChannelRepository(unitOfWork, 0).FirstOrDefault(p => p.Name == ChannelName);
                    if (channel != null)
                        channelID = channel.ID;
                    unitOfWork.Dispose();
                }
                return channelID;
            }
        }
        
        private string appCode = null;
        [ScriptIgnore]
        public string AppCode
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.appCode) && !string.IsNullOrWhiteSpace(this.AuthToken))
                {
                    this.appCode = SecurityHelper.GetApplicationCodeFromAuthenticationToken(this.AuthToken);
                }
                if (string.IsNullOrWhiteSpace(this.appCode))
                {
                    this.appCode = ApplicationManager.GetApplicationFromRouteData();
                }
                if (string.IsNullOrWhiteSpace(this.appCode) && this.applicationID != 0)
                {
                    this.appCode = ApplicationManager.GetApplicationFromApplicationID(this.applicationID);
                }
                return this.appCode;
            }
        }

        private long applicationID = 0;
        [ScriptIgnore]
        public long ApplicationID
        {
            get
            {
                if (applicationID == 0)
                {
                    SetApplicationID(ApplicationManager.GetApplicationIDFromAppCode(this.AppCode));
                }
                return applicationID;
            }
        }

        public void SetApplicationID(long appID)
        {
            if (appID != 0 && this.applicationID == 0)
                this.applicationID = appID;
        }

        public void SetUserID(long uID)
        {
            if (uID != 0 && this.userID == 0)
                this.userID = uID;
        }

        public void SetApplicationName(string appName)
        {
            if (string.IsNullOrWhiteSpace(appCode) && !string.IsNullOrWhiteSpace(appName))
                this.appCode = appName;
        }

        [JsonProperty(PropertyName = "PI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public int PageIndex { get; set; }

        public int pageSize;
        [JsonProperty(PropertyName = "PS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public int PageSize
        {
            get
            {
                if (pageSize == 0)
                    pageSize = int.MaxValue;
                return pageSize;
            }
            set
            {
                pageSize = value;
            }
        }

        [JsonProperty(PropertyName = "CV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long CacheVersion { get; set; }

        [JsonProperty(PropertyName = "ULR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public UserLocationRecord UserLocationRecord { get; set; }
    }

    public interface IAbBaseRequest
    {
        bool InnerCall { get; set; }

        bool IsOnline { get; set; }

        DeleteSelect DeleteSelectOption { get; set; }

        ActiveSelect ActiveSelectOption { get; set; }

        string AuthToken { get; set; }

        string IP { get; set; }

        PMApp.Core.Helpers.Language Lang { get; set; }

        long UserRoleID { get; set; }
        
        long DeviceID { get; }

        long UserID { get; }

        string ChannelName { get; }
        long ChannelID { get; }

        string AppCode { get; }

        long ApplicationID { get; }

        int PageIndex { get; set; }

        int PageSize { get; set; }

        long CacheVersion { get; set; }

        UserLocationRecord UserLocationRecord { get; set; }

        long GetCurrentRecordID();
        void SetApplicationID(long applicationID);
        void SetApplicationName(string appName);
    }
}
