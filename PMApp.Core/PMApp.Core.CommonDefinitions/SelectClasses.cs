using PMApp.Modon.DAL6;
using System.Collections.Generic;

namespace  PMApp.Core.CommonDefinitions
{
    public class SelectApplicationRole
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public PMCore_Role PMCoreRole { get; set; }

        public IEnumerable<PMCore_ServiceOperationRoleAccess> PMServiceOperations { get; set; }

        public bool IsActive { get; set; }

        public bool? IsAdmin { get; set; }

        public bool IsDeleted { get; set; }

        public List<PMCore_Menu> RoleMenu { get; set; }
    }
}