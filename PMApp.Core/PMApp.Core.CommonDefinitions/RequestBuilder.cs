using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;

namespace  PMApp.Core.CommonDefinitions
{
    public static class RequestBuilder
    {
        public static T CreateRequest<T>(string authToken, PMApp.Core.Helpers.Language? lang = null, bool innerCall = true, bool isOnline = true)
            where T : IAbBaseRequest
        {
            T request = Activator.CreateInstance<T>();
            request.InnerCall = innerCall;
            request.AuthToken = authToken;

            request.IsOnline = isOnline;
            if (lang.HasValue)
                request.Lang = lang.Value;
            else
                request.Lang = UIHelper.GetCookieLanguageEnum();
            request.DeleteSelectOption = DeleteSelect.IsNotDeleted;
            request.ActiveSelectOption = ActiveSelect.IsActive;
            return request;
        }

        public static T CreateRequest<T, F>(string authToken, F filter, bool innerCall = true, bool isOnline = true, PMApp.Core.Helpers.Language? lang = null)
            where F : AbRequestFilter
            where T : AbBaseRequest<F>
        {
            T request = Activator.CreateInstance<T>();

            request.InnerCall = innerCall;
            request.IsOnline = isOnline;
            request.AuthToken = authToken;
            if (lang.HasValue)
                request.Lang = lang.Value;
            else
                request.Lang = UIHelper.GetCookieLanguageEnum();
            request.Filter = filter;
            request.DeleteSelectOption = DeleteSelect.IsNotDeleted;
            request.ActiveSelectOption = ActiveSelect.IsActive;
            return request;
        }

    }
}