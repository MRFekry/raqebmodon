
namespace  PMApp.Core.CommonDefinitions
{
    public enum ResponseStatus : int
    {
        Success = 0,
        Active = 1,
        Inactive = 2,
        RequestDataInvalid = 3,
        Exception = 4
    }

}