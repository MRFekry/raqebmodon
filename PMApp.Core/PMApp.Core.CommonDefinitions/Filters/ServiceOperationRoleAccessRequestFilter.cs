using System.Collections.Generic;
using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using System.Linq;
namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class ServiceOperationRoleAccessRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "FRI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long FilterRoleID { get; set; }

        [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> IDList { get; set; }

        public System.Linq.IQueryable<SelectApplicationRole> ApplyFilter(System.Linq.IQueryable<SelectApplicationRole> query)
        {
            if (IDList != null && IDList.Count > 0)
            {
                query = query.Where(c => IDList.Contains(c.ID));
            }
            return query;
        }
    }
}