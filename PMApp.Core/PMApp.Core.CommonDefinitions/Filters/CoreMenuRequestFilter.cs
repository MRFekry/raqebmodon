using Newtonsoft.Json;
using System.Linq;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class CoreMenuRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "CI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ChannelID { get; set; }

        public System.Linq.IQueryable<Modon.DAL6.PMCore_Menu> ApplyFilter(System.Linq.IQueryable<Modon.DAL6.PMCore_Menu> query)
        {
            
            return query;
        }
    }
}
