
using PMApp.Modon.DAL6;
using System;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public abstract class AbFilter<F>
        where F : AbRequestFilter
    {
        private F filter;

        [JsonProperty(PropertyName = "F", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public F Filter
        {
            get
            {
                if (filter == null)
                    filter = (F)Activator.CreateInstance(typeof(F), new object[] { });
                return filter;
            }
            set { filter = value; }
        }

        private ModonDBEntities contextEntities = null;
        [ScriptIgnore]
        protected ModonDBEntities CurrentContextEntities
        {
            get
            {
                return contextEntities;
            }
            set
            {
                this.contextEntities = value;
                if (Filter != null)
                {
                    Filter.ContextEntities = this.contextEntities;
                }
            }
        }
    }
}
