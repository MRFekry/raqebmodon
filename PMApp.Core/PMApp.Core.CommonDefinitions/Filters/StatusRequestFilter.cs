using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class StatusRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> IDList { get; set; }

        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "OIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ObjectIDList { get; set; }

        [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ModifiedByList { get; set; }

        [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> CreatedByList { get; set; }

        [JsonProperty(PropertyName = "SSOCl", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfClient { get; set; }

        [JsonProperty(PropertyName = "SSOC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfCycle { get; set; }

        [JsonProperty(PropertyName = "SSOCP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfCyclePlan { get; set; }

        [JsonProperty(PropertyName = "SSOO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfOrder { get; set; }

        [JsonProperty(PropertyName = "SSOPV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfPersonalVacation { get; set; }

        [JsonProperty(PropertyName = "SSOP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfProduct { get; set; }

        [JsonProperty(PropertyName = "SSOR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfRequest { get; set; }

        [JsonProperty(PropertyName = "SSOU", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfUser { get; set; }

        [JsonProperty(PropertyName = "SSOV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowStatusOfVisit { get; set; }


        public IQueryable<StatusRecord> ApplyFilter(long applicationID, IQueryable<StatusRecord> query)
        {
            if (ObjectIDList != null && ObjectIDList.Count > 0)
            {
                query = query.Where(p => ObjectIDList.Contains(p.ObjectID));
            }
            return query;
        }
    }
}