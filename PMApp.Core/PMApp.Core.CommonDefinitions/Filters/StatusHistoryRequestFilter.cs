using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class StatusHistoryRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> IDList { get; set; }

        [JsonProperty(PropertyName = "FSIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> FromStatusIDList { get; set; }

        [JsonProperty(PropertyName = "TSIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ToStatusIDList { get; set; }

        [JsonProperty(PropertyName = "TOIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> TargetObjectIDList { get; set; }

        [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> CreatedByList { get; set; }

        [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ModifiedByList { get; set; }



        public IQueryable<StatusHistoryRecord> ApplyFilter(IQueryable<StatusHistoryRecord> query)
        {
            if (IDList != null && IDList.Count > 0)
            {
                query = query.Where(p => IDList.Contains(p.ID));
            }

            if (FromStatusIDList != null && FromStatusIDList.Count > 0)
            {
                query = query.Where(p => FromStatusIDList.Contains(p.FromStatusID ?? 0));
            }

            if (ToStatusIDList != null && ToStatusIDList.Count > 0)
            {
                query = query.Where(p => ToStatusIDList.Contains(p.ToStatusID));
            }

            if (TargetObjectIDList != null && TargetObjectIDList.Count > 0)
            {
                query = query.Where(p => TargetObjectIDList.Contains(p.TargetObjectID));
            }

            if (CreatedByList != null && CreatedByList.Count > 0)
            {
                query = query.Where(p => CreatedByList.Contains(p.CreatedBy ?? 0));
            }

            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
                query = query.Where(p => ModifiedByList.Contains(p.ModifiedBy ?? 0));
            }


            return query;
        }
    }
}