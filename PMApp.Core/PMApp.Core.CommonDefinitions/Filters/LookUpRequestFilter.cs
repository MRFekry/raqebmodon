using PMApp.Modon.DAL6;
using System.Linq;
using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class LookupRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "LN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LookupName
        {
            get;
            set;
        }

        public System.Linq.IQueryable<PMCore_Lookup> ApplyFilter(System.Linq.IQueryable<PMCore_Lookup> query)
        {
            if (!string.IsNullOrWhiteSpace(LookupName))
                query = query.Where(p => p.LookupName == LookupName);
            return query;
        }
    }
}