using Newtonsoft.Json;
using PMApp.Modon.DAL6;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class WidgetRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "P", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ReportParam[] Params = null;


    }

    [System.Runtime.Serialization.DataContract]
    public class ReportParam : ReportField
    {
        public ReportParam()
        {
        }

        public ReportParam(string name, object value, string dataType)
            : base(name, value, dataType)
        {
        }

        [JsonProperty(PropertyName = "LUSN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LookUpSPName { get; set; }

        [JsonProperty(PropertyName = "CT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ControlType { get; set; }

        [JsonProperty(PropertyName = "IRO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsReadOnly { get; set; }

        [JsonProperty(PropertyName = "CI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long CategoryID { get; set; }

        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "DV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DefaultValue { get; set; }
    }

    [System.Runtime.Serialization.DataContract]
    public class ReportField
    {
        public ReportField()
        {
        }

        public ReportField(string name, object value, string dataType)
        {
            this.Name = name;
            this.Value = value;
            this.DataType = dataType;
        }

        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "DT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DataType { get; set; }

        [JsonProperty(PropertyName = "V", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public object Value { get; set; }
    }
}
