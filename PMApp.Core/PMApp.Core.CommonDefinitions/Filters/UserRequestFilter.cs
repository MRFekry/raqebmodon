using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web.UI.WebControls;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class UserRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "MIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> EduLevelIDList { get; set; }

        [JsonProperty(PropertyName = "UIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> UserIDList { get; set; }

        [JsonProperty(PropertyName = "AIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> CountryIDList { get; set; }

        [JsonProperty(PropertyName = "RIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> RoleIDList { get; set; }

        [JsonProperty(PropertyName = "LIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> LineIDList { get; set; }

        [JsonProperty(PropertyName = "SMMs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ShowMyManagers { get; set; }

        [JsonProperty(PropertyName = "IMT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? IsManageTeam { get; set; }

        [JsonProperty(PropertyName = "EC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string EmployeeCode { get; set; }

        [JsonProperty(PropertyName = "SUI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string FacebookID { get; set; }

        [JsonProperty(PropertyName = "EM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "FN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string FullName { get; set; }

        [JsonProperty(PropertyName = "SE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string SecondEmail { get; set; }

        public IQueryable<UserRecord> ApplyFilter(long userID, long roleID, ModonDBEntities context, IQueryable<UserRecord> query)
        {
            bool ManagerAndUserFilter = false;
            if ((EduLevelIDList != null && EduLevelIDList.Count > 0) && (UserIDList != null && UserIDList.Count > 0))
            {
                query = query.Where(p => EduLevelIDList.Contains(p.EduLevelID) || UserIDList.Contains(p.ID));
                ManagerAndUserFilter = true;
            }
            if (!ManagerAndUserFilter && EduLevelIDList != null && EduLevelIDList.Count > 0)
            {
                query = query.Where(p => EduLevelIDList.Contains(p.EduLevelID));
            }
            if (!ManagerAndUserFilter && UserIDList != null && UserIDList.Count > 0)
            {
                query = query.Where(p => UserIDList.Contains(p.ID));
            }
            if (CountryIDList != null && CountryIDList.Count > 0)
            {
                query = query.Where(p => CountryIDList.Contains(p.CountryID));
            }

            if (RoleIDList != null && RoleIDList.Count > 0)
            {
                query = query.Where(p => RoleIDList.Contains(p.RoleID));
            }
            //if (ShowMyManagers)
            //{
            //    var managersDT = DBHelper.ExecuteProcedure(context.Database.Connection.ConnectionString, "GetAllMyManagersWithMyTeam", new Parameter
            //    {
            //        Name = "@userID",
            //        DefaultValue = userID.ToString()
            //    });
            //    var EduLevelIDs = new List<long>();
            //    for (int i = 0; i < managersDT.Rows.Count; i++)
            //    {
            //        EduLevelIDs.Add(long.Parse(managersDT.Rows[i][0].ToString()));
            //    }
            //    query = query.Where(p => EduLevelIDs.Contains(p.ID));
            //}
            if (IsManageTeam.HasValue)
            {
                query = query.Where(p => p.DBRole.AllowManageTeam == IsManageTeam);
            }
            if (!string.IsNullOrWhiteSpace(EmployeeCode))
            {
                query = query.Where(p => p.Code == EmployeeCode);
            }

            if (!string.IsNullOrWhiteSpace(FacebookID))
            {
                query = query.Where(p => p.FacebookUserID == FacebookID);
            }
            if (!string.IsNullOrWhiteSpace(Email))
            {
                query = query.Where(p => p.Email == Email);
            }
            if (!string.IsNullOrWhiteSpace(SecondEmail))
            {
                query = query.Where(p => p.SecondEmail == SecondEmail);
            }
            if (!string.IsNullOrWhiteSpace(FullName))
            {
                query = query.Where(p => p.FullName.Contains(FullName) || p.NickName.Contains(FullName));
            }
            return query;
        }



    }
}