using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class NotificationRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> IDList { get; set; }

        [JsonProperty(PropertyName = "FUIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> FromUserIDList { get; set; }

        [JsonProperty(PropertyName = "TUIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ToUserIDList { get; set; }

        [JsonProperty(PropertyName = "NM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string NotificationMessage { get; set; }

        [JsonProperty(PropertyName = "ND", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string NotificationData { get; set; }

        [JsonProperty(PropertyName = "NTDL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> NotificationTypeIDList { get; set; }

        [JsonProperty(PropertyName = "DOL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> DBObjectList { get; set; }

        [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> CreatedByList { get; set; }

        [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ModifiedByList { get; set; }

        [JsonProperty(PropertyName = "DENs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool DisplayEmailsNotifications { get; set; }

        [JsonProperty(PropertyName = "ISS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? IsSeen { get; set; }

        public IQueryable<NotificationRecord> ApplyFilter(IQueryable<NotificationRecord> query)
        {
            if (IDList != null && IDList.Count > 0)
            {
                query = query.Where(p => IDList.Contains(p.ID));
            }

            if (FromUserIDList != null && FromUserIDList.Count > 0)
            {
                query = query.Where(p => FromUserIDList.Contains(p.FromUserID));
            }

            if (ToUserIDList != null && ToUserIDList.Count > 0)
            {
                query = query.Where(p => ToUserIDList.Contains(p.ToUserID));
            }

            if (!string.IsNullOrWhiteSpace(NotificationMessage))
            {
                query = query.Where(p => p.NotificationMessage.Contains(NotificationMessage));
            }

            if (!string.IsNullOrWhiteSpace(NotificationData))
            {
                query = query.Where(p => p.NotificationData.Contains(NotificationData));
            }

            if (NotificationTypeIDList != null && NotificationTypeIDList.Count > 0)
            {
                query = query.Where(p => NotificationTypeIDList.Contains(p.NotificationTypeID));
            }

            if (DBObjectList != null && DBObjectList.Count > 0)
            {
                query = query.Where(p => DBObjectList.Contains(p.DBObjectID));
            }

            if (CreatedByList != null && CreatedByList.Count > 0)
            {
                query = query.Where(p => CreatedByList.Contains(p.CreatedBy ?? 0));
            }

            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
                query = query.Where(p => ModifiedByList.Contains(p.ModifiedBy ?? 0));
            }

            //if (!DisplayEmailsNotifications)
            //{
            //    query = query.Where(p => p.ToEmail == "");
            //}
            if (IsSeen.HasValue)
            {
                query = query.Where(p => p.IsSeen==IsSeen);
            }

            return query;
        }
    }
}