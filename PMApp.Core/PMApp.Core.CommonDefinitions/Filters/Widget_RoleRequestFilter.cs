using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class PMCore_Widget_RoleRequestFilter : AbRequestFilter
    {

        [JsonProperty(PropertyName = "RIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<Int64> RoleIDList { get; set; }

        [JsonProperty(PropertyName = "WIL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<Int64> WidgetIDList { get; set; }

        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Boolean? IsAllowed { get; set; }

        [ScriptIgnore]
        public DateTime? CreationDate { get; set; }

        [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<Int64> ModifiedByList { get; set; }

        [ScriptIgnore]
        public DateTime? LastUpdateDate { get; set; }

        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Boolean? IsDeleted { get; set; }

        [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<Int64> CreatedByList { get; set; }



        public IQueryable<PMCore_Widget_RoleRecord> ApplyFilter(IQueryable<PMCore_Widget_RoleRecord> query)
        {

            if (RoleIDList != null && RoleIDList.Count > 0)
            {
                query = query.Where(p => RoleIDList.Contains(p.RoleID));
            }

            if (WidgetIDList != null && WidgetIDList.Count > 0)
            {
                query = query.Where(p => WidgetIDList.Contains(p.WidgetID));
            }

            if (IsAllowed.HasValue)
            {
                query = query.Where(p => p.IsAllowed == IsAllowed);
            }
            if (CreationDate.HasValue)
            {
                query = query.Where(p => p.CreationDate == CreationDate);
            }
            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
                query = query.Where(p => ModifiedByList.Contains(p.ModifiedBy ?? 0));
            }

            if (LastUpdateDate.HasValue)
            {
                query = query.Where(p => p.LastUpdateDate == LastUpdateDate);
            }
            if (IsDeleted.HasValue)
            {
                query = query.Where(p => p.IsDeleted == IsDeleted);
            }
            if (CreatedByList != null && CreatedByList.Count > 0)
            {
                query = query.Where(p => CreatedByList.Contains(p.CreatedBy ?? 0));
            }

            return query;
        }
    }
}