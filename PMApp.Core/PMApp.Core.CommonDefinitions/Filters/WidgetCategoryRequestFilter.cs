
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class PMCore_WidgetCategoryRequestFilter : AbRequestFilter
    {

        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Int32? ID { get; set; }

        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public String Name { get; set; }

        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public String Description { get; set; }

        [JsonProperty(PropertyName = "IC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public String IconClass { get; set; }



        public IQueryable<PMCore_WidgetCategoryRecord> ApplyFilter(IQueryable<PMCore_WidgetCategoryRecord> query)
        {

            if (ID.HasValue)
            {
                query = query.Where(p => p.ID == ID);
            }
            if (!string.IsNullOrWhiteSpace(this.Name))
            {
                query = query.Where(p => p.Name != null && p.Name.Contains(this.Name));
            }
            if (!string.IsNullOrWhiteSpace(this.Description))
            {
                query = query.Where(p => p.Description != null && p.Description.Contains(this.Description));
            }
            if (!string.IsNullOrWhiteSpace(this.IconClass))
            {
                query = query.Where(p => p.IconClass != null && p.IconClass.Contains(this.IconClass));
            }
            return query;
        }
    }
}