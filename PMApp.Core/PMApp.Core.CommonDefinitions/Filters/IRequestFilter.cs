using Newtonsoft.Json;
using PMApp.Modon.DAL6;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    public interface IRequestFilter
    {
        //IQueryable<Q> ApplyFilter<Q>(IQueryable<Q> query);
        [ScriptIgnore]
        ModonDBEntities ContextEntities { get; set; }
    }

    public abstract class AbRequestFilter : IRequestFilter
    {
        [ScriptIgnore]
        public ModonDBEntities ContextEntities { get; set; }
    }
}
