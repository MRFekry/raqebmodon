using System.Collections.Generic;
using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using System.Linq;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class ResourceRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> IDList { get; set; }

        [JsonProperty(PropertyName = "RK", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ResourceKey { get; set; }

        [JsonProperty(PropertyName = "RV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ResourceValue { get; set; }

        [JsonProperty(PropertyName = "RL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ResourceLang { get; set; }

        [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> CreatedByList { get; set; }

        [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ModifiedByList { get; set; }




        public List<long> filterIDList
        {
            get
            {
                if (IDList != null && IDList.Count > 0)
                    return IDList;
                return null;
            }
        }

        public string filterResourceKey
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ResourceKey))
                    return ResourceKey;
                return null;
            }
        }

        public string filterResourceValue
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ResourceValue))
                    return ResourceValue;
                return null;
            }
        }

        public string filterResourceLang
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(ResourceLang))
                    return ResourceLang;
                return null;
            }
        }

        public List<long> filterCreatedByList
        {
            get
            {
                if (CreatedByList != null && CreatedByList.Count > 0)
                    return CreatedByList;
                return null;
            }
        }

        public List<long> filterModifiedByList
        {
            get
            {
                if (ModifiedByList != null && ModifiedByList.Count > 0)
                    return ModifiedByList;
                return null;
            }
        }


        public IQueryable<ResourceRecord> ApplyFilter(IQueryable<ResourceRecord> query)
        {
            if (IDList != null && IDList.Count > 0)
            {
                query = query.Where(p => IDList.Contains(p.ID));
            }
            return query;
        }


    }
}