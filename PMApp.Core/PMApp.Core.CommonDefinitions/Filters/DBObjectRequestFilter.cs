using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class DBObjectRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "IL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> IDList { get; set; }

        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "CBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> CreatedByList { get; set; }

        [JsonProperty(PropertyName = "MBL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<long> ModifiedByList { get; set; }

        public IQueryable<DBObjectRecord> ApplyFilter(IQueryable<DBObjectRecord> query)
        {
            if (IDList != null && IDList.Count > 0)
            {
                query = query.Where(p => IDList.Contains(p.ID));
            }

            if (!string.IsNullOrWhiteSpace(Name))
            {
                query = query.Where(p => p.Name != null && p.Name.Contains(Name));
            }

            if (CreatedByList != null && CreatedByList.Count > 0)
            {
                query = query.Where(p => CreatedByList.Contains(p.CreatedBy ?? 0));
            }

            if (ModifiedByList != null && ModifiedByList.Count > 0)
            {
                query = query.Where(p => ModifiedByList.Contains(p.ModifiedBy ?? 0));
            }


            return query;
        }
    }
}