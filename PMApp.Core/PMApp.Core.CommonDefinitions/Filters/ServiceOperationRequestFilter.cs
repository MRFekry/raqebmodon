using Newtonsoft.Json;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class ServiceOperationRequestFilter : AbRequestFilter
    {
        public System.Linq.IQueryable<PMCore_ServiceOperation> ApplyFilter(System.Linq.IQueryable<PMCore_ServiceOperation> query)
        {
            return query;
        }
    }
}