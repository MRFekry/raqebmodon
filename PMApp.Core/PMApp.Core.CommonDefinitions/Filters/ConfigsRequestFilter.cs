using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using System.Linq;
using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.Filters
{
    [System.Runtime.Serialization.DataContract]
    public class ConfigsRequestFilter : AbRequestFilter
    {
        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }

        [JsonProperty(PropertyName = "CK", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ConfKey { get; set; }

        [JsonProperty(PropertyName = "CV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ConfValue { get; set; }

        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ModifiedBy { get; set; }

        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long CreatedBy { get; set; }

        [JsonProperty(PropertyName = "CI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ChannelID { get; set; }

        [JsonProperty(PropertyName = "IUE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? IsUIEditable { get; set; }

        [JsonProperty(PropertyName = "SOMC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? ShowOnlyMyChannel { get; set; }

        [JsonProperty(PropertyName = "ETQs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool EnumTablesQueries { get; set; }

        public IQueryable<ConfigRecord> ApplyFilter(long channelID, IQueryable<ConfigRecord> query)
        {
            if (ID > 0)
            {
                query = query.Where(p => p.ID == ID);
            }

            if (!string.IsNullOrWhiteSpace(ConfKey))
            {
                query = query.Where(p => p.ConfKey == ConfKey);
            }

            if (!string.IsNullOrWhiteSpace(ConfValue))
            {
                query = query.Where(p => p.ConfValue == ConfValue);
            }
            
            if (ModifiedBy > 0)
            {
                query = query.Where(p => p.ModifiedBy == ModifiedBy);
            }

            if (CreatedBy > 0)
            {
                query = query.Where(p => p.CreatedBy == CreatedBy);
            }

            if (ChannelID > 0)
            {
                var channelIDStr = ChannelID.ToString();
                query = query.Where(p => p.ChannelIDs.Contains(channelIDStr));
            }
            if (IsUIEditable.HasValue)
            {
                query = query.Where(p => p.IsUIEditable == IsUIEditable);
            }
            if (ShowOnlyMyChannel.HasValue && ShowOnlyMyChannel.Value && channelID > 0)
            {
                var channelIDStr = channelID.ToString();
                query = query.Where(p => p.ChannelIDs == "" || p.ChannelIDs == null || p.ChannelIDs.Contains(channelIDStr));
            }
            return query;
        }
    }
}