using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;
using System.ComponentModel.DataAnnotations;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ChangePasswordRequest : BaseRequest<ChangePasswordResponse, ChangePasswordRequestFilter>
    {
        [JsonProperty(PropertyName = "OP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [SBSRequiredAttribute("RequiredField")]
        public string OldPassword { get; set; }

        [JsonProperty(PropertyName = "NP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [SBSRequiredAttribute("RequiredField")]
        [SBSRegularExpressionAttribute(@"((?=.*\d)(?=.*[a-zA-Z]).{8,20})", errorMessageKey: "PasswordMustBe(8-20LengthWithA-a-z,number,symbol)")]
        public string NewPassword { get; set; }

        [SBSCompareAttribute("NewPassword", "NotMatched")]
        [SBSRequiredAttribute("RequiredField")]
        public string ConfirmPassword { get; set; }
    }

}