using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [DataContract]
    public class SBSResponse
    {
        [DataMember]
        public string EncryptedResponse { get; set; }

        // Without Data Member
        [ScriptIgnore]
        public bool IsSuccess { get; set; }

        [ScriptIgnore]
        public long UserID { get; set; }

        [ScriptIgnore]
        public string ChannelName { get; set; }

        [ScriptIgnore]
        public string AppCode { get; set; }

        [ScriptIgnore]
        public long ApplicationID { get; set; }
    }
}