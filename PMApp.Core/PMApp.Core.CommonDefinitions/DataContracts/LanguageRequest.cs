using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class LanguageRequest : BaseRequest<LanguageResponse, LanguageRequestFilter>
    {

    }
}