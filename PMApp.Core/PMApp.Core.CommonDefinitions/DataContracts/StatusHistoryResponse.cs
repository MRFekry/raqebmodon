using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class StatusHistoryResponse : BaseResponse
    {

        public StatusHistoryResponse(Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName ="SHRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public StatusHistoryRecord[] StatusHistoryRecords { get; set; }
    }
}