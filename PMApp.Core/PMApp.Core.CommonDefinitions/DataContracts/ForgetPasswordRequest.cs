using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ForgetPasswordRequest : BaseRequest<ForgetPasswordResponse, ForgetPasswordRequestFilter>
    {
        [JsonProperty(PropertyName = "E", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Email { get; set; }

        public override bool IsValid(ModonDBEntities contextEntities, ref ForgetPasswordResponse response, string operationName, string serviceName)
        {
            if (base.IsValid(contextEntities, ref response, operationName, serviceName))
            {
                if (string.IsNullOrWhiteSpace(this.Email))
                {
                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.EmailEmpty);
                }
            }
            if (response.Success.HasValue)
                return response.Success.Value;
            return true;
        }

        [JsonProperty(PropertyName = "FPC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ForgetPasswordChoice ForgetPasswordChoice { get; set; }
    }

}