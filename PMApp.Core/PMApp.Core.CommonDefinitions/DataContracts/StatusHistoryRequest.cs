using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class StatusHistoryRequest : BaseRequest<StatusHistoryResponse, StatusHistoryRequestFilter>
    {
        [JsonProperty(PropertyName ="SHR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public StatusHistoryRecord StatusHistoryRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (StatusHistoryRecord != null)
                return StatusHistoryRecord.ID;
            return base.GetCurrentRecordID();
        }

        public override bool IsValid(ModonDBEntities contextEntities, ref StatusHistoryResponse response, string operationName, string serviceName)
        {
            if (base.IsValid(contextEntities, ref response, operationName, serviceName))
            {
                if (this.Filter == null || this.Filter.TargetObjectIDList == null || this.Filter.TargetObjectIDList.Count == 0)
                {
                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.FilterEmpty);
                }
            }
            if (response.Success.HasValue)
                return response.Success.Value;
            return true;
        }
    }
}