using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class NotificationRequest : BaseRequest<NotificationResponse, NotificationRequestFilter>
    {
        [JsonProperty(PropertyName = "NR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public NotificationRecord NotificationRecord { get; set; }

        [JsonProperty(PropertyName = "MSA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool MakeAsSeen { get; set; }

        public override long GetCurrentRecordID()
        {
            if (NotificationRecord != null)
                return NotificationRecord.ID;
            return base.GetCurrentRecordID();
        }
    }
}