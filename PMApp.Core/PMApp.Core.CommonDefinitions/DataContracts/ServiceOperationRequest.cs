using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ServiceOperationRequest : BaseRequest<ServiceOperationResponse, ServiceOperationRequestFilter>
    {
        [JsonProperty(PropertyName = "NA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool NoAnonymous { get; set; }
    }
}