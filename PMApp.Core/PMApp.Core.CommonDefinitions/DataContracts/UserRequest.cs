using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class UserRequest : BaseRequest<UserResponse, UserRequestFilter>
    {
        [JsonProperty(PropertyName = "UR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public UserRecord UserRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (UserRecord != null)
                return UserRecord.ID;
            return base.GetCurrentRecordID();
        }

        public override bool IsValid(ModonDBEntities contextEntities, ref UserResponse response, string operationName, string serviceName)
        {
            if (base.IsValid(contextEntities, ref response, operationName, serviceName))
            {
                
            }
            if (response.Success.HasValue)
                return response.Success.Value;
            return true;
        }
    }
}