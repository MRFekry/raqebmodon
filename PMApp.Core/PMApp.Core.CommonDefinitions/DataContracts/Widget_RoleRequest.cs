using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;


namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class PMCore_Widget_RoleRequest : BaseRequest<PMCore_Widget_RoleResponse, PMCore_Widget_RoleRequestFilter>
    {
        [JsonProperty(PropertyName = "PWRR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public PMCore_Widget_RoleRecord PMCore_Widget_RoleRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (PMCore_Widget_RoleRecord != null)
                return PMCore_Widget_RoleRecord.WidgetID;
            return base.GetCurrentRecordID();
        }
    }
}