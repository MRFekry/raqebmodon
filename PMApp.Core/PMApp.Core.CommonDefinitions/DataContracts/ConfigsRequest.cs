using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ConfigsRequest : BaseRequest<ConfigsResponse, ConfigsRequestFilter>
    {
        [JsonProperty(PropertyName = "CR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ConfigRecord ConfigsRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (ConfigsRecord != null)
                return ConfigsRecord.ID;
            return base.GetCurrentRecordID();
        }

    }
}