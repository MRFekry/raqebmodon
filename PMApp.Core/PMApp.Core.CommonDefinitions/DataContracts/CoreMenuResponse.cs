using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using System.Collections.Generic;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class CoreMenuResponse : BaseResponse
    {
        public CoreMenuResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "CMRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<CoreMenuRecord> CoreMenuRecords { get; set; }
    }

}