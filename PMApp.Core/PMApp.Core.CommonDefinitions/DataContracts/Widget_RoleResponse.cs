using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;


namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class PMCore_Widget_RoleResponse : BaseResponse
    {

        public PMCore_Widget_RoleResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "PWRRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public PMCore_Widget_RoleRecord[] PMCore_Widget_RoleRecords { get; set; }
    }
}