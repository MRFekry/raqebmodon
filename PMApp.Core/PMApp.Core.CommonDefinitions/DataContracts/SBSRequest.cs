using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [DataContract]
    public class SBSRequest
    {
        [DataMember]
        public bool IsOnline { get; set; }

        [DataMember]
        public string EncryptedRequest { get; set; }

        [DataMember]
        public string ApplicationName { get; set; }

        [DataMember]
        public string TargetOperation { get; set; }
    }
}