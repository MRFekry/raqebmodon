using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ResourceResponse : BaseResponse
    {

        public ResourceResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName ="RRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ResourceRecord[] ResourceRecords { get; set; }
    }
}