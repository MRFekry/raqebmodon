using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class LoginRequest : BaseRequest<LoginResponse, LoginRequestFilter>
    {
        [JsonProperty(PropertyName = "FUI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string FacebookUserID { get; set; }

        [JsonProperty(PropertyName = "LCN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LoginChannelName { get; set; }

        [JsonProperty(PropertyName = "EM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "P", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Password { get; set; }

        [JsonProperty(PropertyName = "DR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public DeviceRecord DeviceRecord { get; set; }

        [JsonProperty(PropertyName = "AC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ApplicationCode { get; set; }

        public override long GetCurrentRecordID()
        {
            if (DeviceRecord != null)
                return DeviceRecord.ID;
            return base.GetCurrentRecordID();
        }

        public override bool IsValid(ModonDBEntities contextEntities, ref LoginResponse response, string operationName, string serviceName)
        {
            if (base.IsValid(contextEntities, ref response, operationName, serviceName))
            {
                if (string.IsNullOrWhiteSpace(this.Password))
                {
                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.PasswordEmpty);
                }
                else if (string.IsNullOrWhiteSpace(this.Email))
                {
                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.EmailEmpty);
                }
                else if (string.IsNullOrWhiteSpace(this.LoginChannelName))
                {
                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.ChannelEmpty);
                }
                else if (this.DeviceRecord == null)
                {
                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.DeviceRecordEmpty);
                }
            }
            if (response.Success.HasValue)
                return response.Success.Value;
            return true;
        }


    }
}