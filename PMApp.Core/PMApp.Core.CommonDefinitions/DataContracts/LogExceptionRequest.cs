using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class LogExceptionRequest : BaseRequest<LogExceptionResponse, LogExceptionRequestFilter>
    {
        [JsonProperty(PropertyName ="M", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Message { get; set; }

        [JsonProperty(PropertyName ="ST", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string StackTrace { get; set; }
    }
}