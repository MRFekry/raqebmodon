using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ServiceOperationResponse : BaseResponse
    {
        public ServiceOperationResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "SOs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ServiceOperationRecord[] ServiceOperations { get; set; }
    }
}