using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class LanguageResponse : BaseResponse
    {
        public LanguageResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "LRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public LanguageRecord[] LanguageRecords { get; set; }
    }

}
