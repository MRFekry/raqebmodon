using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class CacheVersionListResponse : BaseResponse
    {

        public CacheVersionListResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "CVRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public CacheVersionRecord[] CacheVersionRecords { get; set; }
    }

}