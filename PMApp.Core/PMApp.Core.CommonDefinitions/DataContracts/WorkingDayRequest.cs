using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class WorkingDayRequest : BaseRequest<WorkingDayResponse, WorkingDayFilter>
    {

    }
}