﻿using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [DataContract]
    public class ProfileResponse : BaseResponse
    {
        public ProfileResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "UR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public UserRecord UserRecord { get; set; }
        
        [JsonProperty(PropertyName = "IdrU", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsBlockedUser { get; set; }

        [JsonProperty(PropertyName = "IBrU", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsBlockerUser { get; set; }
    }
}