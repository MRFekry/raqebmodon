using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class StatusResponse : BaseResponse
    {

        public StatusResponse(Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName ="SRs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public StatusRecord[] StatusRecords { get; set; }
    }

}