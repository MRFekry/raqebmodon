using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class LookupRequest : BaseRequest<LookupResponse, LookupRequestFilter>
    {
        [JsonProperty(PropertyName = "LR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public LookupRecord LookupRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (LookupRecord != null)
                return LookupRecord.ID;
            return base.GetCurrentRecordID();
        }

        public override bool IsValid(ModonDBEntities contextEntities, ref LookupResponse response, string operationName, string serviceName)
        {
            if (base.IsValid(contextEntities, ref response, operationName, serviceName))
            {
                //if (this.LookupRecord == null)
                //{
                //    response.SetBaseResponse(false, RespnseStatus.RequestDataInvalid, MessageKey.LookupEmpty);
                //}
            }
            if (response.Success.HasValue)
                return response.Success.Value;
            return true;
        }
    }
}