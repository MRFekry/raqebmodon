using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.DataContracts.Record;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class PMCore_WidgetCategoryRequest : BaseRequest<PMCore_WidgetCategoryResponse, PMCore_WidgetCategoryRequestFilter>
    {
        [JsonProperty(PropertyName = "PWCR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public PMCore_WidgetCategoryRecord PMCore_WidgetCategoryRecord { get; set; }
    }
}