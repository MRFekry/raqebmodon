using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ResourceRequest : BaseRequest<ResourceResponse, ResourceRequestFilter>
    {
        [JsonProperty(PropertyName ="RR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ResourceRecord ResourceRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (ResourceRecord != null)
                return ResourceRecord.ID;
            return base.GetCurrentRecordID();
        }
    }
}