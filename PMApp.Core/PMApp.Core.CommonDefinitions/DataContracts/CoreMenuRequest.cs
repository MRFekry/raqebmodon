using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class CoreMenuRequest : BaseRequest<CoreMenuResponse, CoreMenuRequestFilter>
    {
        [JsonProperty(PropertyName = "RI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? RoleID { get; set; }

        [JsonProperty(PropertyName = "LA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool LoadAll { get; set; }

        [JsonProperty(PropertyName = "CMR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public CoreMenuRecord CoreMenuRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (CoreMenuRecord != null)
                return CoreMenuRecord.ID;
            return base.GetCurrentRecordID();
        }
    }
}