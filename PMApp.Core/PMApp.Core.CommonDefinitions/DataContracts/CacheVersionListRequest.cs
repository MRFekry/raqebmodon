using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class CacheVersionListRequest : BaseRequest<CacheVersionListResponse, CacheVersionListRequestFilter>
    {
        [JsonProperty(PropertyName = "CVR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public CacheVersionRecord CacheVersionRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            return base.GetCurrentRecordID();
        }
    }
}