using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class LogoutRequest : BaseRequest<LogoutResponse, LogoutRequestFilter>
    {

        [JsonProperty(PropertyName ="DR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public DeviceRecord DeviceRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (DeviceRecord != null)
                return DeviceRecord.ID;
            return base.GetCurrentRecordID();
        }
    }
}