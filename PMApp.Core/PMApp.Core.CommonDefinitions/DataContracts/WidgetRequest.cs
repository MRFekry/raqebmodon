using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class WidgetRequest : BaseRequest<WidgetResponse, WidgetRequestFilter>
    {
        [JsonProperty(PropertyName = "WI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? WidgetID { get; set; }

        public override long GetCurrentRecordID()
        {
            if (WidgetID.HasValue)
                return WidgetID.Value;
            return base.GetCurrentRecordID();
        }

        public override bool IsValid(ModonDBEntities contextEntities, ref WidgetResponse response, string operationName, string serviceName)
        {
            if (base.IsValid(contextEntities, ref response, operationName, serviceName))
            {
                if (!WidgetID.HasValue)
                {
                    return true;
                }
                if (this.WidgetID == 0)
                {
                    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidFilters);
                    return false;
                }
                //if (this.Filter == null || this.Filter.Params == null)
                //{
                //    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.InvalidFilters);
                //    return false;
                //}
            }
            if (response.Success.HasValue)
                return response.Success.Value;
            return true;
        }
    }
}