﻿using System.Runtime.Serialization;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [DataContract]
    public class ProfileRequest : BaseRequest<ProfileResponse, ProfileRequestFilter>
    {

        [JsonProperty(PropertyName = "UR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public UserRecord UserRecord { get; set; }

        [JsonProperty(PropertyName = "PUI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ProfileUserID { get; set; }

       
    }
}