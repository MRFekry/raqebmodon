using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class NotificationTypeRecord : BaseRecord
    {
        public NotificationTypeRecord()
        { }

        public NotificationTypeRecord(NotificationType notificationType)
        {
            if (notificationType == null)
                return;
            ID = notificationType.ID;
            DBObjectID = notificationType.DBObjectID ?? 0;
            Name = notificationType.Name;
            Description = notificationType.Description;
            IsDeleted = notificationType.IsDeleted;
            ModifiedBy = notificationType.ModifiedBy;
            LastUpdateDate = notificationType.LastUpdateDate;
            CreatedBy = notificationType.CreatedBy;
            CreationDate = notificationType.CreationDate;
            ApplicationID = notificationType.ApplicationID;
        }

        [JsonProperty(PropertyName = "DOI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long DBObjectID { get; set; }

        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Description { get; set; }

    }
}
