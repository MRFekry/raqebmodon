﻿using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{

    [System.Runtime.Serialization.DataContract]
    public class StatusHistoryRecord : BaseRecord
    {
        public StatusHistoryRecord()
        { }

        public StatusHistoryRecord(StatusHistory statusHistory)
        {
            if (statusHistory == null)
                return;
            ID = statusHistory.ID;
            FromStatusID = statusHistory.FromStatusID;
            ToStatusID = statusHistory.ToStatusID;
            TargetObjectID = statusHistory.TargetObjectID;
            CreatedBy = statusHistory.CreatedBy;
            CreationDate = statusHistory.CreationDate;
            ModifiedBy = statusHistory.ModifiedBy;
            LastUpdateDate = statusHistory.LastUpdateDate;
            IsDeleted = statusHistory.IsDeleted;
            ApplicationID = statusHistory.ApplicationID;
        }

        [JsonProperty(PropertyName = "FSI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? FromStatusID { get; set; }

        [JsonProperty(PropertyName = "TSI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ToStatusID { get; set; }

        [JsonProperty(PropertyName = "OI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ObjectID { get; set; }

        [JsonProperty(PropertyName = "TOI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long TargetObjectID { get; set; }
    }
}