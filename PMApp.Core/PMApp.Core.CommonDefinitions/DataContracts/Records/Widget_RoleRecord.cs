using Newtonsoft.Json;
using PMApp.Modon.DAL6;
using System;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class PMCore_Widget_RoleRecord : BaseRecord
    {
        public PMCore_Widget_RoleRecord()
        { }

        public PMCore_Widget_RoleRecord(PMCore_Widget_Role pMCore_Widget_Role)
        {
            if (pMCore_Widget_Role == null)
                return;

            RoleID = pMCore_Widget_Role.RoleID;
            WidgetID = pMCore_Widget_Role.WidgetID;
            IsAllowed = pMCore_Widget_Role.IsAllowed;
            CreationDate = pMCore_Widget_Role.CreationDate;
            ModifiedBy = pMCore_Widget_Role.ModifiedBy;
            LastUpdateDate = pMCore_Widget_Role.LastUpdateDate;
            IsDeleted = pMCore_Widget_Role.IsDeleted;
            CreatedBy = pMCore_Widget_Role.CreatedBy;
            ApplicationID = pMCore_Widget_Role.ApplicationID;
        }



        [JsonProperty(PropertyName = "RI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Int64 RoleID { get; set; }


        [JsonProperty(PropertyName = "WI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Int64 WidgetID { get; set; }


        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Boolean IsAllowed { get; set; }
    }
}
