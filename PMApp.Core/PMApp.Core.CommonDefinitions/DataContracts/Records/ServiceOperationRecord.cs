using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class ServiceOperationRecord : BaseRecord
    {
        [JsonProperty(PropertyName = "SN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ServiceName
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "ON", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OperationName
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "OT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OperationTitle
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "OTL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OperationTitle_LS
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsAnonymous
        {
            get;
            set;
        }
    }
}
