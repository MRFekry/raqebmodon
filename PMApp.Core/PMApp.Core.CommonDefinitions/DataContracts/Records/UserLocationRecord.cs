using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class UserLocationRecord : BaseRecord
    {
        public UserLocationRecord()
        { }

        public UserLocationRecord(UserLocation userLocation)
        {
            if (userLocation == null)
                return;
            ID = userLocation.ID;
            UserID = userLocation.UserID;
            DeviceID = userLocation.DeviceID;
            CreationDateTime = userLocation.CreationDateTime;
            SubmitDateTime = userLocation.SubmitDateTime;
            BataryStatus = userLocation.BataryStatus;
            NetworkStatus = userLocation.NetworkStatus;
            Address = userLocation.Address;
            Longitude = userLocation.Longitude;
            Latitude = userLocation.Latitude;
            IsOnline = userLocation.IsOnline;
            DirectionAngle = userLocation.DirectionAngle;
            AquirityDistance = userLocation.AquirityDistance;
            Speed = userLocation.Speed;
            ApplicationID = userLocation.ApplicationID;
        }

        [JsonProperty(PropertyName = "UI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long UserID { get; set; }

        [ScriptIgnore]
        public long DeviceID { get; set; }

        [JsonProperty(PropertyName = "BS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public int? BataryStatus { get; set; }

        [JsonProperty(PropertyName = "NS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public int? NetworkStatus { get; set; }

        [JsonProperty(PropertyName = "A", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "Lo", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public decimal Longitude { get; set; }

        [JsonProperty(PropertyName = "La", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public decimal Latitude { get; set; }

        [JsonProperty(PropertyName = "IO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsOnline { get; set; }

        [JsonProperty(PropertyName = "DA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double? DirectionAngle { get; set; }

        [JsonProperty(PropertyName = "AD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double? AquirityDistance { get; set; }

        [JsonProperty(PropertyName = "S", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double? Speed { get; set; }

        [ScriptIgnore]
        public DateTime? CreationDateTime { get; set; }

        [JsonProperty(PropertyName = "CDTS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateTimeStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(CreationDateTime);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateTimeDD_MM_YYYY_TIME(value, out dt))
                    CreationDateTime = dt;
            }
        }

        [ScriptIgnore]
        public DateTime SubmitDateTime { get; set; }

        [JsonProperty(PropertyName = "SDTS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string SubmitDateTimeStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(SubmitDateTime);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    SubmitDateTime = dt;
            }
        }

        [JsonProperty(PropertyName = "UN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "UIm", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string UserImage { get; set; }

        [JsonProperty(PropertyName = "RN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string RoleName { get; set; }
    }
}
