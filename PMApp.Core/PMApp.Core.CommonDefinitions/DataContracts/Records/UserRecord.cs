using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class UserRecord : BaseRecord
    {
        public UserRecord() { }

        public UserRecord(Common_User commonUser)
        {
            this.ApplicationID = commonUser.ApplicationID;
            this.Birthdate = commonUser.Birthdate;
            this.CreatedBy = commonUser.CreatedBy;
            this.phone1 = commonUser.Phone1;

            this.CreationDate = commonUser.CreationDate;
            this.Email = commonUser.Email;
            this.FullName = commonUser.FullName;
            this.ID = commonUser.ID;
            this.RoleID = commonUser.RoleID;
            this.RegisterDate = commonUser.RegisterDate;
            this.Notes = commonUser.Notes;
            this.ModifiedBy = commonUser.ModifiedBy;
            this.IsActive = commonUser.IsActive;
            this.IsDeleted = commonUser.IsDeleted;
        }


        [JsonProperty(PropertyName = "FUI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string FacebookUserID { get; set; }

        [JsonProperty(PropertyName = "RM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool RememberMe { get; set; }

        private ContactInformationRecord[] contactInfo;

        [JsonProperty(PropertyName = "CIR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ContactInformationRecord[] ContactInfo
        {
            get
            {
                if (ContactInfoList != null)
                    return ContactInfoList.ToArray();
                return contactInfo;
            }
            set
            {
                contactInfo = value;
            }
        }

        [ScriptIgnore]
        public DateTime RegisterDate { get; set; }


        [JsonProperty(PropertyName = "RDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string RegisterDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(RegisterDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    RegisterDate = dt;
            }
        }

        private string email;

        [JsonProperty(PropertyName = "E", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [SBSRequiredAttribute("EnterEmail")]
        public string Email
        {
            get { return email; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                    this.email = value.ToLower();
            }
        }

        [JsonProperty(PropertyName = "FN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [SBSRequiredAttribute("EnterName")]
        public string FullName
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "P", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [SBSRequiredAttribute("EnterPassword")]
        public string Password
        {
            get;
            set;
        }

        [ScriptIgnore]
        [SBSCompareAttribute("Password", "NotMatched")]
        public string VerifiedPassword
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "BD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public DateTime? Birthdate { get; set; }

        [JsonProperty(PropertyName = "BS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string BirthdateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(Birthdate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    Birthdate = dt;
            }
        }

        [JsonProperty(PropertyName = "RI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [SBSRequiredAttribute("ChooseRole")]
        public long RoleID
        {
            get;
            set;
        }

        [ScriptIgnore]
        public PMCore_Role DBRole { get; set; }

        [JsonProperty(PropertyName = "RN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string RoleName { get; set; }

        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsActive
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "Ns", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Notes
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "PPI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ProfilePictureID
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "PIB64", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ProfileImageBase64
        {
            get;
            set;
        }

        private string userimageUrl;

        [JsonProperty(PropertyName = "PPU", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ProfilePictureUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(userimageUrl))
                {
                    var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                    if (applicationID != 0)
                        userimageUrl = ConfigManager.GetInstance(applicationID).UserDefaultPictureUrl;
                }
                return UIHelper.ResolveServerUrl(userimageUrl);

            }
            set { userimageUrl = value; }
        }


        [JsonProperty(PropertyName = "EL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long EduLevelID
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "CI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long CountryID { get; set; }

        [JsonProperty(PropertyName = "CN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CountryName { get; set; }

        [JsonProperty(PropertyName = "NI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long NationalityID { get; set; }



        [JsonProperty(PropertyName = "ELN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string EduLevelName { get; set; }

        //[JsonProperty(PropertyName = "ELI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        //public string EduLevelIcon { get; set; }

        [JsonProperty(PropertyName = "ST", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? StatusID { get; set; }


        [ScriptIgnore]
        public HttpPostedFileBase ImageFile { get; set; }

        [ScriptIgnore]
        public string ReturnUrl { get; set; }

        [ScriptIgnore]
        public string Code { get; set; }

        [ScriptIgnore]
        public IEnumerable<ContactInformationRecord> ContactInfoList { get; set; }


        private string mobile;
        [DataType(DataType.PhoneNumber)]
        [SBSRegularExpression(pattern: @"^\d{9,18}$", errorMessageKey: "InvalidPhoneNumber")]
        [JsonProperty(PropertyName = "M", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Mobile
        {
            get
            {

                return mobile;
            }
            set
            {
                mobile = value;

            }
        }

        [ScriptIgnore]
        //[SBSRequiredAttribute("EnterPassword")]
        public string ApplicationCode { get; set; }


        [JsonProperty(PropertyName = "EPT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long EearningPoints { get; set; }

        [JsonProperty(PropertyName = "PPT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long PaidPoints { get; set; }

        [JsonProperty(PropertyName = "CMID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]

        public long? CurrentMemberShipID { get; set; }

        [JsonProperty(PropertyName = "MA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]

        public long? MyAnswers { get; set; }

        [JsonProperty(PropertyName = "AWQ", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? MyAwardedAnswers { get; set; }

        [JsonProperty(PropertyName = "CQ", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? MyClosedQuestions { get; set; }

        [JsonProperty(PropertyName = "OQ", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? MyOpenQuestions { get; set; }

        [JsonProperty(PropertyName = "RA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long Rank { get; set; }

        [ScriptIgnore]
        public DateTime? LastRedeem { get; set; }

        [JsonProperty(PropertyName = "LRS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastRedeemStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastRedeem);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastRedeem = dt;
            }
        }

        [JsonProperty(PropertyName = "LA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public DateTime? LastActivity { get; set; }

        [JsonProperty(PropertyName = "AHP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long AnswerOnHoldPoints { get; set; }

        [JsonProperty(PropertyName = "QHP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long QuestionOnHoldPoints { get; set; }

        private string nickName;
        [JsonProperty(PropertyName = "NN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string NickName
        {
            get { return nickName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    nickName = FullName;
                else nickName = value;
            }
        }

        [JsonProperty(PropertyName = "FPT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long FreePoints { get; set; }

        [JsonProperty(PropertyName = "PSC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double PointSchemaPrice { get; set; }

        [JsonProperty(PropertyName = "CISO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CurrencyISO { get; set; }

        [JsonProperty(PropertyName = "AURD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? AutoRedemption { get; set; }

        [JsonProperty(PropertyName = "AURN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? AutoRenewal { get; set; }

        [JsonProperty(PropertyName = "DEC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? DeactivateCount { get; set; }

        [JsonProperty(PropertyName = "DEL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? DeactivateLimit { get; set; }

        [JsonProperty(PropertyName = "SMR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? ShowMyRanking { get; set; }

        [JsonProperty(PropertyName = "RE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? Review { get; set; }


        [JsonProperty(PropertyName = "RO", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long RankOrder { get; set; }

        [ScriptIgnore]
        public string UserLanguage { get; set; }

        [JsonProperty(PropertyName = "NRR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long NextLevelRatio { get; set; }

        [JsonProperty(PropertyName = "SE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        [SBSEmailAddress("InvalidEmail")]
        public string SecondEmail { get; set; }

        public ForgetPasswordChoice ForgetPasswordChoice { get; set; }

        [JsonProperty(PropertyName = "DSE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? DefaultSourceIsEearning { get; set; }

        [JsonProperty(PropertyName = "NaN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string NationalityName { get; set; }

        [JsonProperty(PropertyName = "TRP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long TotalRankPoints { get; set; }

        private long balance;
        [JsonProperty(PropertyName = "B", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long Balance
        {
            get
            {
                balance = EearningPoints + FreePoints + PaidPoints;
                return balance;
            }
            set { value = balance; }
        }

        [JsonProperty(PropertyName = "PC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string PromoCode { get; set; }

        [JsonProperty(PropertyName = "IM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? IsMale { get; set; }

        [JsonProperty(PropertyName = "NLP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long NextLevelPoints { get; set; }

        [JsonProperty(PropertyName = "ARDA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool? AutoRedemptionDisabled { get; set; }
        public long? phone1 { get; set; }
        public long? AreaID { get; set; }
        public string AreaName { get; set; }
        public string ParentAreaName { get; set; }
        public long? ParentAreaID { get; set; }
    }
}
