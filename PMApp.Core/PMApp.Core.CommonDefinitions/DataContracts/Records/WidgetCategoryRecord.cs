using Newtonsoft.Json;
using PMApp.Modon.DAL6;
using System;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class PMCore_WidgetCategoryRecord : BaseRecord
    {
        public const string DisplayColumnName = "Name";

        public PMCore_WidgetCategoryRecord()
        { }

        public PMCore_WidgetCategoryRecord(PMCore_WidgetCategory pMCore_WidgetCategory)
        {
            if (pMCore_WidgetCategory == null)
                return;

            ID = pMCore_WidgetCategory.ID;
            Name = pMCore_WidgetCategory.Name;
            Description = pMCore_WidgetCategory.Description;
            IconClass = pMCore_WidgetCategory.IconClass;
            ApplicationID = pMCore_WidgetCategory.ApplicationID;
        }


        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public String Name { get; set; }


        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public String Description { get; set; }


        [JsonProperty(PropertyName = "IC", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public String IconClass { get; set; }


    }

}
