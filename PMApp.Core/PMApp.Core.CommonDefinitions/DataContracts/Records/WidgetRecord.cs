using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class WidgetRecord : BaseRecord
    {
        public WidgetRecord()
        {
        }

        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }

        [ScriptIgnore]
        public long UserID { get; set; }

        [JsonProperty(PropertyName = "CI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? CategoryID { get; set; }

        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "SN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string SPName { get; set; }

        [JsonProperty(PropertyName = "Fs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<ReportField> Fields { get; set; }

        [JsonProperty(PropertyName = "Ps", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<ReportParam> Params { get; set; }

        [ScriptIgnore]
        public IQueryable<PMCore_Parameter> PMCore_ParameterList { get; set; }

        [JsonProperty(PropertyName = "CT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ChartType { get; set; }

        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsAllowed { get; set; }

        [JsonProperty(PropertyName = "XKL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string[] XKeysList
        {
            get
            {
                if (XKeys != null)
                    return XKeys.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                return null;
            }
            set
            {
                // do No thing
            }
        }

        string[] xKeysLabelList = null;
        [JsonProperty(PropertyName = "XKLL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string[] XKeysLabelList
        {
            get
            {
                if (xKeysLabelList == null)
                {
                    var xList = XKeysList;
                    if (xList != null)
                    {
                        xKeysLabelList = new string[xList.Length];
                        for (int i = 0; i < xList.Length; i++)
                        {
                            xKeysLabelList[i] = ResourceManager.GetInstance(ApplicationID).GetResource(xList[i]);
                        }
                    }
                }
                return xKeysLabelList;
            }
            set
            {
                // do No thing
            }
        }


        [ScriptIgnore]
        public string XKeys { get; set; }

        private string[] ykeysList = null;
        [JsonProperty(PropertyName = "YKL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string[] YKeysList
        {
            get
            {
                if (ykeysList == null)
                {
                    if (!string.IsNullOrWhiteSpace(YKeysSPName))
                    {
                        var context = EFUnitOfWork.NewUnitOfWork("YKeysList");
                        lock (YKeysSPName)
                        {
                            YKeys = "";
                            var spName = YKeysSPName;
                            List<Parameter> parms = new List<Parameter>();
                            if (YKeysSPName.ToLower().Contains("@userid"))
                            {
                                spName = YKeysSPName.Replace("@userid", "").Trim();
                                parms.Add(new Parameter { Name = "@userid", DefaultValue = this.UserID.ToString() });
                            }
                            var ykeysDT = DBHelper.ExecuteProcedure(context.Database.Connection.ConnectionString, spName, parms.ToArray());
                            for (int i = 0; i < ykeysDT.Rows.Count; i++)
                            {
                                YKeys += ykeysDT.Rows[i][1].ToString() + ","; // "Data"
                            }
                            ykeysList = YKeys.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        }
                    }
                    else if (YKeys != null)
                        ykeysList = YKeys.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }
                return ykeysList;
            }
            set
            {
                ykeysList = value;
            }
        }

        string[] yKeysLabelList = null;
        [JsonProperty(PropertyName = "YKLL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string[] YKeysLabelList
        {
            get
            {
                if (yKeysLabelList == null)
                {
                    var yList = YKeysList;
                    if (yList != null)
                    {
                        yKeysLabelList = new string[yList.Length];
                        for (int i = 0; i < yList.Length; i++)
                        {
                            yKeysLabelList[i] = ResourceManager.GetInstance(ApplicationID).GetResource(yList[i]);
                        }
                    }
                }
                return yKeysLabelList;
            }
            set
            {
                // do No thing
            }
        }

        [ScriptIgnore]
        public string YKeys { get; set; }

        [ScriptIgnore]
        public string YKeysSPName { get; set; }

        [JsonProperty(PropertyName = "CL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string[] ColorsList
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Colors))
                    return Colors.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                //return ThemeManager.GetInstance(ApplicationID).GetChartColors();
                else
                    return null;
            }
            set
            {
                // do No thing
            }
        }

        [ScriptIgnore]
        public string Colors { get; set; }

    }
}
