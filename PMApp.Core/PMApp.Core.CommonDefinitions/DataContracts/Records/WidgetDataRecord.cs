using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.Filters;
using System.Collections.Generic;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class WidgetDataRecord : BaseRecord
    {
        public WidgetDataRecord(List<ReportField> data, long applicationID)
        {
            this.Data = data;
            this.ApplicationID = applicationID;
        }

        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public List<ReportField> Data { get; set; }
    }
}
