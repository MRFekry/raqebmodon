using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class ContactInformationRecord : BaseRecord
    {
        [JsonProperty(PropertyName = "V", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "TI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long TypeID { get; set; }

        [JsonProperty(PropertyName = "T", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Type { get; set; }
    }
}
