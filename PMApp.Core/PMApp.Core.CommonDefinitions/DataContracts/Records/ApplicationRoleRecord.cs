using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{

    [System.Runtime.Serialization.DataContract]
    public class ApplicationRoleRecord : BaseRecord
    {
        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "Ns", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Notes
        {
            get;
            set;
        }


        [JsonProperty(PropertyName = "PI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ParentId
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsActive
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "IAd", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsAdmin
        {
            get;
            set;
        }
        
        [JsonProperty(PropertyName = "SORA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ServiceOperationRoleAccessRecord[] ServiceOperationRoleAccesses { get; set; }

        [JsonProperty(PropertyName = "RM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public CoreMenuRecord[] RoleMenu { get; set; }
    }
}
