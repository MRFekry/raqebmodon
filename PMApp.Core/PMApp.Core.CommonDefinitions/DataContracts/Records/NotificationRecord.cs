using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{

    [System.Runtime.Serialization.DataContract]
    public class NotificationRecord : BaseRecord
    {

        public NotificationRecord()
        { }

        public NotificationRecord(Notification notification)
        {
            if (notification == null)
                return;
            ID = notification.ID;
            FromUserID = notification.FromUserID;
            ApplicationID = notification.ApplicationID;
            ToUserID = notification.ToUserID;
            IsSent = notification.IsSent;
            IsReceived = notification.IsReceived;
            IsSeen = notification.IsSeen;
            NotificationData = notification.NotificationData;
            NotificationTypeID = notification.NotificationTypeID;
            if (notification.NotificationType != null)
                DBObjectID = notification.NotificationType.DBObjectID ?? 0;
            CreationDate = notification.CreationDate;
            CreatedBy = notification.CreatedBy;
            ModifiedBy = notification.ModifiedBy;
            LastUpdateDate = notification.LastUpdateDate;
            IsDeleted = notification.IsDeleted;
            NotificationMessage = notification.NotificationMessage;
            ApplicationID = notification.ApplicationID;
            //ToEmail = notification.ToEmail;
        }

        [JsonProperty(PropertyName = "DOI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long DBObjectID { get; set; }

        [JsonProperty(PropertyName = "FUI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long FromUserID { get; set; }

        [JsonProperty(PropertyName = "TUI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ToUserID { get; set; }

        [JsonProperty(PropertyName = "ISt", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsSent { get; set; }

        [JsonProperty(PropertyName = "IR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsReceived { get; set; }

        [JsonProperty(PropertyName = "ISn", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsSeen { get; set; }

        [JsonProperty(PropertyName = "ND", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string NotificationData { get; set; }

        [JsonProperty(PropertyName = "NTI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long NotificationTypeID { get; set; }

        [JsonProperty(PropertyName = "NM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string NotificationMessage { get; set; }

        [ScriptIgnore]
        public string ExtraData { get; set; }

        [ScriptIgnore]
        public string ToEmail { get; set; }

        [ScriptIgnore]
        public DateTime? SubmitVisitDate
        {
            get
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateTimeDD_MM_YYYY_TIME(ExtraData, out dt))
                    return dt;
                return null;
            }
            set
            {
            }
        }

         [ScriptIgnore]
        public long? RecordID { get; set; }
    }
}
