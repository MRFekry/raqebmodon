﻿using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{

    [System.Runtime.Serialization.DataContract]
    public class DBObjectRecord : BaseRecord
    {
        public const string DisplayColumnName = "Name";

        public DBObjectRecord()
        { }

        public DBObjectRecord(DBObject dBObject)
        {
            if (dBObject == null)
                return;
            ID = dBObject.ID;
            Name = dBObject.Name;
            CreationDate = dBObject.CreationDate;
            IsDeleted = dBObject.IsDeleted;
            CreatedBy = dBObject.CreatedBy;
            ModifiedBy = dBObject.ModifiedBy;
            LastUpdateDate = dBObject.LastUpdateDate;
            ApplicationID = dBObject.ApplicationID;
        }


        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name { get; set; }

    }
}