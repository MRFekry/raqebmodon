using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{

    [System.Runtime.Serialization.DataContract]
    public class DeviceRecord : BaseRecord
    {
        public DeviceRecord()
        { }

        public DeviceRecord(Common_UserDevice common_UserDevice)
        {
            if (common_UserDevice == null)
                return;
            ID = common_UserDevice.ID;
            Common_UserID = common_UserDevice.Common_UserID;
            DeviceName = common_UserDevice.DeviceName;
            DeviceIMEI = common_UserDevice.DeviceIMEI;
            DeviceModel = common_UserDevice.DeviceType;
            DeviceOSVersion = common_UserDevice.DeviceOSVersion;
            DeviceToken = common_UserDevice.DeviceToken;
            DeviceEmail = common_UserDevice.DeviceEmail;
            EnableNotification = common_UserDevice.EnableNotification;
            AuthToken = common_UserDevice.AuthToken;
            AuthIP = common_UserDevice.AuthIP;
            AuthCreationDate = common_UserDevice.AuthCreationDate;
            AuthExpirationDate = common_UserDevice.AuthExpirationDate;
            CreatedBy = common_UserDevice.CreatedBy;
            ModifiedBy = common_UserDevice.ModifiedBy;
            LastUpdateDate = common_UserDevice.LastUpdateDate;
            CreationDate = common_UserDevice.CreationDate;
            IsDeleted = common_UserDevice.IsDeleted;
            ApplicationID = common_UserDevice.ApplicationID;
        }
                
        [ScriptIgnore]
        public long Common_UserID { get; set; }

        [JsonProperty(PropertyName = "DN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DeviceName { get; set; }

        [JsonProperty(PropertyName = "DI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DeviceIMEI { get; set; }

        [JsonProperty(PropertyName = "DM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DeviceModel { get; set; }

        [JsonProperty(PropertyName = "DOV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DeviceOSVersion { get; set; }

        [JsonProperty(PropertyName = "DT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DeviceToken { get; set; }

        [JsonProperty(PropertyName = "DE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DeviceEmail { get; set; }

        [JsonProperty(PropertyName = "EN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool EnableNotification { get; set; }

        [JsonProperty(PropertyName = "AT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AuthToken { get; set; }

        [JsonProperty(PropertyName = "AuP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AuthIP { get; set; }

        [ScriptIgnore]
        public DateTime? AuthCreationDate { get; set; }

        [JsonProperty(PropertyName = "ACDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AuthCreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(AuthCreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    AuthCreationDate = dt;
            }
        }

        [ScriptIgnore]
        public DateTime? AuthExpirationDate { get; set; }

        [JsonProperty(PropertyName = "AEDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AuthExpirationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(AuthExpirationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    AuthExpirationDate = dt;
            }
        }

        [JsonProperty(PropertyName = "DMN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DeviceMobileNumber { get; set; }
    }
}
