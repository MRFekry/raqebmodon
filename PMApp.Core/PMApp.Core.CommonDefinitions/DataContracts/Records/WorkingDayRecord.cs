using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{

    [System.Runtime.Serialization.DataContract]
    public class WorkingDayRecord : BaseRecord
    {
        [JsonProperty(PropertyName = "DOW", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DayOfWeek { get; set; }

        [JsonProperty(PropertyName = "IWD", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsWorkingDay { get; set; }
    }
}
