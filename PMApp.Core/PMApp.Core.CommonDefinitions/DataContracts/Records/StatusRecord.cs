﻿using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class StatusRecord : BaseRecord
    {
        public const string DisplayColumnName = "Name";

        public StatusRecord()
        { }

        public StatusRecord(Status status)
        {
            if (status == null)
                return;
            ID = status.ID;
            Name = status.Name;
            Description = status.Description;
            ObjectID = status.ObjectID;
            ModifiedBy = status.ModifiedBy;
            LastUpdateDate = status.LastUpdateDate;
            IsDeleted = status.IsDeleted;
            CreatedBy = status.CreatedBy;
            CreationDate = status.CreationDate;
            ApplicationID = status.ApplicationID;
        }
        private string name;
        [JsonProperty(PropertyName = "N", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Name
        {
            get
            {
                if (ApplicationID == 0)
                    return name;
                return ResourceManager.GetInstance(ApplicationID).GetResource(name);
            }
            set
            {
                name = value;
            }
        }

        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "OI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ObjectID { get; set; }
    }
}