using Newtonsoft.Json;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class ServiceOperationRoleAccessRecord : BaseRecord
    {
        [JsonProperty(PropertyName = "RI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long RoleID
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "SOI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ServiceOperationID
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "IA", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsAllowed
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "SON", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ServiceOperationName
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "SN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ServiceName { get; set; }

        [JsonProperty(PropertyName = "SOT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ServiceOperationTitle { get; set; }

        [JsonProperty(PropertyName = "OI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ObjectID { get; set; }

        [JsonProperty(PropertyName = "DON", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string DBObjectName { get; set; }
    }
}
