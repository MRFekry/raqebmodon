using Newtonsoft.Json;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{

    [System.Runtime.Serialization.DataContract]
    public class ConfigRecord : BaseRecord
    {
        public ConfigRecord()
        { }

        public ConfigRecord(PMCore_Configs configs)
        {
            if (configs == null)
                return;
            ID = configs.ID;
            ConfKey = configs.ConfKey;
            ConfValue = configs.ConfValue;
            Decription = configs.Decription;
            ModifiedBy = configs.ModifiedBy;
            LastUpdateDate = configs.LastUpdateDate;
            CreationDate = configs.CreationDate;
            IsDeleted = configs.IsDeleted;
            CreatedBy = configs.CreatedBy;
            ChannelIDs = configs.ChannelIDs;
            ApplicationID = configs.ApplicationID;
        }

        [JsonProperty(PropertyName = "CK", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ConfKey { get; set; }

        [JsonProperty(PropertyName = "CV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ConfValue { get; set; }

        [JsonProperty(PropertyName = "D", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Decription { get; set; }

        [JsonProperty(PropertyName = "CIs", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ChannelIDs { get; set; }

        [JsonProperty(PropertyName = "IUE", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsUIEditable { get; set; }
    }
}
