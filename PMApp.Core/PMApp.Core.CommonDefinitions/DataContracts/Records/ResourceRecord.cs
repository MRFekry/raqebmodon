using Newtonsoft.Json;
using PMApp.Modon.DAL6;
using System;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public class ResourceRecord : BaseRecord
    {
        public ResourceRecord() { }

        public ResourceRecord(Common_Resource resource)
        {
            if (resource == null)
                return;
            ID = resource.ID;
            ResourceKey = resource.ResourceKey;
            ResourceValue = resource.ResourceValue;
            ResourceLang = resource.ResourceLang;
            CreatedBy = resource.CreatedBy;
            ModifiedBy = resource.ModifiedBy;
            LastUpdateDate = resource.LastUpdateDate;
            CreationDate = resource.CreationDate;
            IsDeleted = resource.IsDeleted;
            ApplicationID = resource.ApplicationID;
        }

        [JsonProperty(PropertyName = "RK", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ResourceKey { get; set; }

        [JsonProperty(PropertyName = "RV", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ResourceValue { get; set; }

        [JsonProperty(PropertyName = "RL", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ResourceLang { get; set; }

    }
}
