using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace  PMApp.Core.CommonDefinitions.DataContracts.Record
{
    [System.Runtime.Serialization.DataContract]
    public abstract class BaseRecord
    {
        [JsonProperty(PropertyName = "OFI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string OfflineID { get; set; }

        [JsonProperty(PropertyName = "AI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ApplicationID { get; set; }

        [JsonProperty(PropertyName = "I", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long ID { get; set; }

        [JsonProperty(PropertyName = "ID", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsDeleted { get; set; }

        [ScriptIgnore]
        public long? CurrentUserID { get; set; }//not implemented in all services.may be null

        [ScriptIgnore]
        public long? CurrentUserRoleID { get; set; }//not implemented in all services.may be null

        [ScriptIgnore]
        public DateTime CreationDate { get; set; }

        [JsonProperty(PropertyName = "CDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreationDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY_TIME(CreationDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateTimeMM_DD_YYYY_TIME(value, out dt))
                    CreationDate = dt;
            }
        }

        [JsonProperty(PropertyName = "MB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? ModifiedBy { get; set; }

        [ScriptIgnore]
        public DateTime? LastUpdateDate { get; set; }

        [JsonProperty(PropertyName = "LUDS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string LastUpdateDateStr
        {
            get
            {
                return DateTimeHelper.ToStringDD_MM_YYYY(LastUpdateDate);
            }
            set
            {
                DateTime dt;
                if (DateTimeHelper.TryParseToDateDD_MM_YYYY(value, out dt))
                    LastUpdateDate = dt;
            }
        }

        [JsonProperty(PropertyName = "CB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? CreatedBy { get; set; }

        [JsonProperty(PropertyName = "CuI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long? CurrencyID { get; set; }

        private string createdByFullName = null;
        [JsonProperty(PropertyName = "CBFN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreatedByFullName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(createdByFullName))
                {
                    createdByFullName = UserRepository.GetUserFullName(CreatedBy ?? 0, ApplicationID);
                }
                return createdByFullName;
            }
            set { createdByFullName = value; }
        }

        private string createdByEmail = null;
        [ScriptIgnore]
        public string CreatedByEmail
        {
            get
            {
                if (string.IsNullOrWhiteSpace(createdByEmail))
                {
                    createdByEmail = UserRepository.GetUserEmail(CreatedBy ?? 0, ApplicationID);
                }
                return createdByEmail;
            }
            set { createdByEmail = value; }
        }

        private string createdByImageURL = null;
        [JsonProperty(PropertyName = "CBIU", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CreatedByImageURL
        {
            get
            {
                if (string.IsNullOrWhiteSpace(createdByImageURL))
                {
                    createdByImageURL = UserRepository.GetUserImageURL(CreatedBy ?? 0, ApplicationID);

                    if (!string.IsNullOrWhiteSpace(createdByImageURL))
                        createdByImageURL = UIHelper.ResolveServerUrl(createdByImageURL);
                }
                return createdByImageURL;
            }
            set { createdByImageURL = value; }
        }

        [ScriptIgnore]
        private Dictionary<NotificationFormatKey, string> notificationFormatDictionary = null;

        public Dictionary<NotificationFormatKey, string> InitializeNotificationFormater()
        {
            if (notificationFormatDictionary == null)
            {
                try
                {
                    notificationFormatDictionary = new Dictionary<NotificationFormatKey, string>();
                    notificationFormatDictionary.Add(NotificationFormatKey.CreatedByID, (CreatedBy ?? 0).ToString());
                    notificationFormatDictionary.Add(NotificationFormatKey.CreatedByFullName, CreatedByFullName);
                    notificationFormatDictionary.Add(NotificationFormatKey.CreatedByImageUrl, CreatedByImageURL);
                    notificationFormatDictionary.Add(NotificationFormatKey.CreatedByEmail, CreatedByEmail);
                    notificationFormatDictionary.Add(NotificationFormatKey.CreationDate, CreationDate.ToString());
                    if (ModifiedBy.HasValue)
                    {
                        var modifiedByUser = UserRepository.GetUser(ModifiedBy ?? 0, ApplicationID);
                        if (modifiedByUser != null)
                        {
                            notificationFormatDictionary.Add(NotificationFormatKey.ModifiedByID, (ModifiedBy ?? 0).ToString());
                            notificationFormatDictionary.Add(NotificationFormatKey.ModifiedByFullName, modifiedByUser.FullName);
                            //notificationFormatDictionary.Add(NotificationFormatKey.ModifiedByImageUrl, modifiedByUser.ImageUrl);
                            notificationFormatDictionary.Add(NotificationFormatKey.ModifiedByEmail, modifiedByUser.Email);
                        }

                        if (LastUpdateDate.HasValue)
                            notificationFormatDictionary.Add(NotificationFormatKey.LastUpdateDate, LastUpdateDate.ToString());
                    }

                    NotificationFormaterFill();
                }
                catch (Exception ex)
                {
                    ExceptionLogger.GetInstance(ApplicationID).Log(ex);
                }
            }
            return notificationFormatDictionary;
        }

        public void AddNotificationFormatedValue(NotificationFormatKey notificationFormatKey, string value)
        {
            InitializeNotificationFormater();
            if (notificationFormatDictionary.ContainsKey(notificationFormatKey))
            {
                notificationFormatDictionary[notificationFormatKey] = value;
            }
            else
            {
                notificationFormatDictionary.Add(notificationFormatKey, value);
            }
        }

        protected virtual void NotificationFormaterFill()
        {
        }
    }

    public enum NotificationFormatKey
    {
        CreatedByID,
        CreatedByEmail,
        CreatedByFullName,
        CreatedByImageUrl,
        QuestionID,
        QuestionLink,
        QuestionTitle,
        QuestionTitleTruncated,
        QuestionBody,
        AnswerBody,
        QuestionOwnerFullName,
        ActivityTypeName,
        QuestionStatusName,
        MessageReply,
        MessageText,
        MessageTypeName,
        CreationDate,
        LastUpdateDate,
        ModifiedByID,
        ModifiedByFullName,
        ModifiedByImageUrl,
        ModifiedByEmail,
        ReviewValue,
        AwarderFeesPoints,
        AwarderPoints,
    }
}
