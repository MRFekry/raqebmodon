using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;
using PMApp.Modon.DAL6;
using PMApp.Core.Helpers;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class StatusRequest : BaseRequest<StatusResponse, StatusRequestFilter>
    {
        [JsonProperty(PropertyName ="SR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public StatusRecord StatusRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (StatusRecord != null)
                return StatusRecord.ID;
            return base.GetCurrentRecordID();
        }

        public override bool IsValid(ModonDBEntities contextEntities, ref StatusResponse response, string operationName, string serviceName)
        {
            if (base.IsValid(contextEntities, ref response, operationName, serviceName))
            {
                //if (this.Filter == null || this.Filter.ObjectID == 0)
                //{
                //    response.SetBaseResponse(false, ResponseStatus.RequestDataInvalid, MessageKey.FilterEmpty);
                //}
            }
            if (response.Success.HasValue)
                return response.Success.Value;
            return true;
        }
    }
}