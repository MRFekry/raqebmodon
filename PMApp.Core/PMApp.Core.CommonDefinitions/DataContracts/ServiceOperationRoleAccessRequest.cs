using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ServiceOperationRoleAccessRequest : BaseRequest<ServiceOperationRoleAccessResponse, ServiceOperationRoleAccessRequestFilter>
    {
        [JsonProperty(PropertyName = "AR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ApplicationRoleRecord ApplicationRole { get; set; }

        [JsonProperty(PropertyName = "SORAR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public ServiceOperationRoleAccessRecord ServiceOperationRoleAccessRecord { get; set; }



        public override long GetCurrentRecordID()
        {
            if (ServiceOperationRoleAccessRecord != null)
                return ServiceOperationRoleAccessRecord.ID;
            return base.GetCurrentRecordID();
        }
    }
}