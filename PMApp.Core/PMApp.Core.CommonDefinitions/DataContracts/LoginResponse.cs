using Newtonsoft.Json;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class LoginResponse : BaseResponse
    {
        public LoginResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }

        [JsonProperty(PropertyName = "UI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long UserId { get; set; }

        [JsonProperty(PropertyName = "FN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string FullName { get; set; }

        [JsonProperty(PropertyName = "Ns", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Notes { get; set; }

        [JsonProperty(PropertyName = "RI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long RoleId { get; set; }

        [JsonProperty(PropertyName = "RN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string RoleName { get; set; }

        [JsonProperty(PropertyName = "EM", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "P", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "AT", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string AuthToken { get; set; }

        [JsonProperty(PropertyName = "AN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ApplicationName { get; set; }

        [JsonProperty(PropertyName = "ALU", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ApplicationLogoURL { get; set; }
        [JsonProperty(PropertyName = "IHFI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsHavingFormalityImage { get; set; }

        [JsonProperty(PropertyName = "UCI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long UserCountryID { get; set; }

        private string userimageUrl;

        [JsonProperty(PropertyName = "PPU", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string ProfilePictureUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(userimageUrl))
                {
                    var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                    userimageUrl = ConfigManager.GetInstance(applicationID).UserDefaultPictureUrl;
                }
                return UIHelper.ResolveServerUrl(userimageUrl);

            }
            set { userimageUrl = value; }
        }

        [JsonProperty(PropertyName = "NN", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string NickName { get; set; }

        [JsonProperty(PropertyName = "PSP", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public double PointSchemaPrice { get; set; }

        [JsonProperty(PropertyName = "CIS", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string CurrencyISO { get; set; }

        [JsonProperty(PropertyName = "UB", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long UserBalance { get; set; }

       [JsonProperty(PropertyName = "UNI", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public long NationalityID { get; set; }
    }
}