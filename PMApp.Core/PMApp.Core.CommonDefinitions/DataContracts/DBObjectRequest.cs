using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.CommonDefinitions.Filters;

namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class DBObjectRequest : BaseRequest<DBObjectResponse, DBObjectRequestFilter>
    {
        [JsonProperty(PropertyName = "DOR", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public DBObjectRecord DBObjectRecord { get; set; }

        public override long GetCurrentRecordID()
        {
            if (DBObjectRecord != null)
                return DBObjectRecord.ID;
            return base.GetCurrentRecordID();
        }
    }
}