
namespace  PMApp.Core.CommonDefinitions.DataContracts
{
    [System.Runtime.Serialization.DataContract]
    public class ChangePasswordResponse : BaseResponse
    {
        public ChangePasswordResponse(PMApp.Core.Helpers.Language lang, long applicationID)
            : base(lang, applicationID)
        {
        }
    }
}