using System.Collections.Generic;
using PMApp.Core.DAL;
using PMApp.Core.Helpers;

namespace PMApp.Core.CommonDefinitions
{
    public static class ApplicationHelper
    {
        private static Dictionary<long, PMCore_Application> applicationDictionary = null;
        private static Dictionary<long, PMCore_Application> ApplicationDictionary
        {
            get
            {
                if (applicationDictionary == null)
                    applicationDictionary = new Dictionary<long, PMCore_Application>();
                return applicationDictionary;
            }
        }

        private static Dictionary<long, PMCore_Role> roleDictionary = null;
        private static Dictionary<long, PMCore_Role> RoleDictionary
        {
            get
            {
                if (roleDictionary == null)
                    roleDictionary = new Dictionary<long, PMCore_Role>();
                return roleDictionary;
            }
        }

        public static Dictionary<string, PMCore_ServiceOperation> ServiceOperationDictionary
        {
            get
            {
                var serviceOperationDictionary = SessionHelper.Get("ServiceOperationDictionary") as Dictionary<string, PMCore_ServiceOperation>;
                if (serviceOperationDictionary == null)
                {
                    serviceOperationDictionary = new Dictionary<string, PMCore_ServiceOperation>();
                    SessionHelper.Set("ServiceOperationDictionary", serviceOperationDictionary);
                }
                return serviceOperationDictionary;
            }
        }

        public static PMCore_Application GetApplication(string applicationCode)
        {
            PMCore_Application application = null;
            if (!string.IsNullOrWhiteSpace(applicationCode))
            {
                applicationCode = applicationCode.ToLower();
                foreach (var app in ApplicationDictionary)
                {
                    if (app.Value == null)
                    {
                        ApplicationDictionary.Remove(app.Key);
                    }
                    else if (app.Value.CodeName.ToLower() == applicationCode)
                    {
                        application = app.Value;
                        break;
                    }
                }
                if (application == null)
                {
                    application = new PMCore_ApplicationRepository().FirstOrDefault(p => p.CodeName == applicationCode);
                    if (application != null && !ApplicationDictionary.ContainsKey(application.ID))
                        ApplicationDictionary.Add(application.ID, application);
                }
            }
            return application;
        }

        public static PMCore_Application GetApplication(long applicationID)
        {
            PMCore_Application application = null;
            if (applicationID != 0)
            {
                if (ApplicationDictionary.ContainsKey(applicationID))
                {
                    application = ApplicationDictionary[applicationID];
                }
                else
                {
                    application = new PMCore_ApplicationRepository().FirstOrDefault(p => p.ID == applicationID);
                    if (application != null && !ApplicationDictionary.ContainsKey(application.ID))
                        ApplicationDictionary.Add(applicationID, application);
                }
            }
            return application;
        }

        public static PMCore_Role GetRole(long roleId)
        {
            PMCore_Role role = null;
            if (roleId != 0)
            {
                if (RoleDictionary.ContainsKey(roleId))
                {
                    role = RoleDictionary[roleId];
                }
                else
                {
                    role = new PMCore_RoleRepository().FirstOrDefault(p => p.ID == roleId);
                    if (role != null && !RoleDictionary.ContainsKey(roleId))
                        RoleDictionary.Add(roleId, role);
                }
            }
            return role;
        }
    }
}
