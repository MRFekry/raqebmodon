using System;
using Newtonsoft.Json;
using PMApp.Modon.DAL6;

namespace  PMApp.Core.CommonDefinitions
{
    [System.Runtime.Serialization.DataContract]
    public class Error
    {
        [JsonProperty(PropertyName = "M", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "C", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public string Code { get; set; }
    }

    public class BusinessException : Exception
    {
        public static BusinessException CreateBusinessException(long applicationID, Exception innerException, string lang, string messageKey, params string[] values)
        {
            try
            {
                var message = ResourceManager.GetInstance(applicationID).GetResource(messageKey, lang);
                string msg;
                if (message != null && message.Length > 0)
                    msg = string.Format(message, values);
                else
                    msg = message;
                return new BusinessException(msg, innerException);
            }
            catch (Exception ex)
            {
                string msg = string.Format("{0} - {1}", lang, messageKey);
                for (int i = 0; i < values.Length; i++)
                {
                    msg += string.Format(" - {0}", values[i]);
                }
                return new BusinessException(msg, innerException ?? ex);
            }
        }
        private BusinessException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
