using System.ServiceModel;
using System.ServiceModel.Activation;

namespace  PMApp.Core.Interfaces
{
    public static class DefaultAttributes
    {
        public const SessionMode SessionModeAttribtute = SessionMode.Allowed;

        public const AspNetCompatibilityRequirementsMode AspNetCompatibilityRequirementsAttribute = AspNetCompatibilityRequirementsMode.Required;
    }
}
