using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace  PMApp.Core.Interfaces
{

    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IWidgetService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/WidgetInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        WidgetResponse DoWidgetInquiry(WidgetRequest request, ISBSService sbsService = null);
    }
}
