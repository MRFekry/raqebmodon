using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace  PMApp.Core.Interfaces
{

    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface ILookup
    {
        #region Lookup
        [OperationContract]
        [WebInvoke(UriTemplate = "/LookupInsertOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LookupResponse DoLookupInsertOperation(LookupRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/LookupUpdateOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LookupResponse DoLookupUpdateOperation(LookupRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/LookupDeleteOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LookupResponse DoLookupDeleteOperation(LookupRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/LookupListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LookupResponse DoLookupListInquiry(LookupRequest request);
        #endregion

        #region Configs
        [OperationContract]
        [WebInvoke(UriTemplate = "/ConfigsList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ConfigsResponse DoConfigsList(ConfigsRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ConfigsDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ConfigsResponse DoConfigsDelete(ConfigsRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ConfigsInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ConfigsResponse DoConfigsInsert(ConfigsRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ConfigsUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ConfigsResponse DoConfigsUpdate(ConfigsRequest request);
        #endregion Configs

        #region Resource
        [OperationContract]
        [WebInvoke(UriTemplate = "/ResourceList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ResourceResponse DoResourceList(ResourceRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ResourceDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ResourceResponse DoResourceDelete(ResourceRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ResourceInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ResourceResponse DoResourceInsert(ResourceRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ResourceUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ResourceResponse DoResourceUpdate(ResourceRequest request);
        #endregion Resource
    }
}
