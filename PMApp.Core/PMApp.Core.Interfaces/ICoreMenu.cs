using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface ICoreMenu
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/CoreMenuListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        CoreMenuResponse DoCoreMenuListInquiry(CoreMenuRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/RoleMenuListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        CoreMenuResponse DoRoleMenuListInquiry(CoreMenuRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UpdateRoleMenu", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        CoreMenuResponse DoUpdateRoleMenu(CoreMenuRequest request);
    }
}
