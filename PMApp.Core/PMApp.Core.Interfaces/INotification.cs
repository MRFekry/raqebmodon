using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface INotification
    {
        #region Notification
        [OperationContract]
        [WebInvoke(UriTemplate = "/NotificationList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NotificationResponse DoNotificationList(NotificationRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NotificationSeen", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NotificationResponse DoNotificationSeen(NotificationRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NotificationDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NotificationResponse DoNotificationDelete(NotificationRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NotificationInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NotificationResponse DoNotificationInsert(NotificationRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/NotificationUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        NotificationResponse DoNotificationUpdate(NotificationRequest request);
        #endregion Notification
    }
}