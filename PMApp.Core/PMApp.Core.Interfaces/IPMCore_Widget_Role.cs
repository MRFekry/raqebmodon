using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IPMCore_Widget_Role
    {
        #region PMCore_Widget_Role Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/PMCore_Widget_RoleList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        PMCore_Widget_RoleResponse DoPMCore_Widget_RoleList(PMCore_Widget_RoleRequest req);
                
        [OperationContract]
        [WebInvoke(UriTemplate = "/PMCore_Widget_RoleDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        PMCore_Widget_RoleResponse DoPMCore_Widget_RoleDelete(PMCore_Widget_RoleRequest req);

        [OperationContract]
        [WebInvoke(UriTemplate = "/PMCore_Widget_RoleInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        PMCore_Widget_RoleResponse DoPMCore_Widget_RoleInsert(PMCore_Widget_RoleRequest req);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/PMCore_Widget_RoleUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        PMCore_Widget_RoleResponse DoPMCore_Widget_RoleUpdate(PMCore_Widget_RoleRequest req);
        #endregion
    }
}