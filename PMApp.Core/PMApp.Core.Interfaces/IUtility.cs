using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IUtility
    {

        [OperationContract]
        [WebInvoke(UriTemplate = "/WorkingDayListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        WorkingDayResponse DoWorkingDayListInquiry(WorkingDayRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/LanguageListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LanguageResponse DoLanguageListInquiry(LanguageRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ServiceOperationListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ServiceOperationResponse DoServiceOperationListInquiry(ServiceOperationRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ServiceRoleAccessInsertOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ServiceOperationRoleAccessResponse DoServiceRoleAccessInsertOperation(ServiceOperationRoleAccessRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ServiceRoleAccessUpdateOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ServiceOperationRoleAccessResponse DoServiceRoleAccessUpdateOperation(ServiceOperationRoleAccessRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ServiceRoleAccessDeleteOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ServiceOperationRoleAccessResponse DoServiceRoleAccessDeleteOperation(ServiceOperationRoleAccessRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ServiceRoleAccessListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ServiceOperationRoleAccessResponse DoServiceRoleAccessListInquiry(ServiceOperationRoleAccessRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/RoleListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ServiceOperationRoleAccessResponse DoRoleListInquiry(ServiceOperationRoleAccessRequest request);


        [OperationContract]
        [WebInvoke(UriTemplate = "/LogException", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LogExceptionResponse DoLogException(LogExceptionRequest request);
    }
}
