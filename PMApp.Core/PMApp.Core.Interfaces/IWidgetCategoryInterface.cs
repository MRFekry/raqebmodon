using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IPMCore_WidgetCategory
    {
        #region PMCore_WidgetCategory Services
        
        [OperationContract]
        [WebInvoke(UriTemplate = "/PMCore_WidgetCategoryList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        PMCore_WidgetCategoryResponse DoPMCore_WidgetCategoryList(PMCore_WidgetCategoryRequest req);
        #endregion
    }
}