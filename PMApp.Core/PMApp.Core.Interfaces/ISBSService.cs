using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface ISBSService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/DoInvoke", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        SBSResponse DoInvoke(SBSRequest sbsRequest);

       
    }
}