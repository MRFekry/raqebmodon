using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IUser
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/Login", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LoginResponse DoLogin(LoginRequest request);


        [OperationContract]
        [WebInvoke(UriTemplate = "/DoSocialLogin", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LoginResponse DoSocialLogin(LoginRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Logout", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        LogoutResponse DoLogout(LogoutRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ForgetPasswordChoice", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ForgetPasswordResponse DoForgetPasswordChoice(ForgetPasswordRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ForgetPassword", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ForgetPasswordResponse DoForgetPassword(ForgetPasswordRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/ChangePassword", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        ChangePasswordResponse DoChangePassword(ChangePasswordRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UserInsertOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        UserResponse DoUserInsertOperation(UserRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UserUpdateOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        UserResponse DoUserUpdateOperation(UserRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UserDeleteOperation", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        UserResponse DoUserDeleteOperation(UserRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/UserListInquiry", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        UserResponse DoUserListInquiry(UserRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DoUserActivate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        UserResponse DoUserActivate(UserRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/CacheVersionList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        CacheVersionListResponse DoCacheVersionList(CacheVersionListRequest req);
    }
}
