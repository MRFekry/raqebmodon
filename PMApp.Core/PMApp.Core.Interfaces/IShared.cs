﻿using PMApp.Core.CommonDefinitions.DataContracts;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace  PMApp.Core.Interfaces
{
    [ServiceContract(SessionMode = DefaultAttributes.SessionModeAttribtute)]
    public interface IShared
    {
        #region DBObject
        [OperationContract]
        [WebInvoke(UriTemplate = "/DBObjectList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        DBObjectResponse DoDBObjectList(DBObjectRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DBObjectDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        DBObjectResponse DoDBObjectDelete(DBObjectRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DBObjectInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        DBObjectResponse DoDBObjectInsert(DBObjectRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/DBObjectUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        DBObjectResponse DoDBObjectUpdate(DBObjectRequest request);
        #endregion DBObject

        #region Status
        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusResponse DoStatusList(StatusRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusResponse DoStatusDelete(StatusRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusResponse DoStatusInsert(StatusRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusResponse DoStatusUpdate(StatusRequest request);
        #endregion Status

        #region StatusHistory
        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusHistoryList", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusHistoryResponse DoStatusHistoryList(StatusHistoryRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusHistoryDelete", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusHistoryResponse DoStatusHistoryDelete(StatusHistoryRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusHistoryInsert", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusHistoryResponse DoStatusHistoryInsert(StatusHistoryRequest request);

        [OperationContract]
        [WebInvoke(UriTemplate = "/StatusHistoryUpdate", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        StatusHistoryResponse DoStatusHistoryUpdate(StatusHistoryRequest request);
        #endregion StatusHistory

    }
}