using Newtonsoft.Json.Linq;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.Linq;

namespace  PMApp.Core.NotificationsSystem
{

    public delegate string LogPMPushHelperExceptionDelegate(Exception exception);
    public delegate string LogPMPushHelperMessageDelegate(string message);
    public delegate string LogPMPushHelperSuccessDelegate(string message);

    public class PMPushHelper
    {
        #region SingleTone
        private const string KeyFormat = "{0}-{1}";
        private static Object lockObj = new Object();
        private long applicationID;

        private PMPushHelper(long applicationID)
        {
            this.applicationID = applicationID;
        }

        private static Dictionary<string, PMPushHelper> PMPushHelperDic = new Dictionary<string, PMPushHelper>();
        public static PMPushHelper GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            var key = string.Format(KeyFormat, applicationID, "");
            if (!PMPushHelperDic.ContainsKey(key))
            {
                lock (lockObj)
                {
                    if (!PMPushHelperDic.ContainsKey(key))
                    {
                        PMPushHelperDic.Add(key, new PMPushHelper(applicationID));
                    }
                }
            }
            return PMPushHelperDic[key];
        }

        public static PMPushHelper GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private const int IPhoneTokenLength = 64 + 7;

        private event LogPMPushHelperExceptionDelegate OnLogException = null;
        private event LogPMPushHelperSuccessDelegate OnLogSuccessDelegate = null;

        private GcmServiceBroker androidBroker = null;
        private ApnsServiceBroker iosBroker = null;
        private Object initializeLocker = new Object();
        private bool isInitialized = false;

        public void Initialize()
        {
            if (!isInitialized)
            {
                lock (initializeLocker)
                {
                    EventLogger.GetInstance(applicationID).Log(string.Format("PMPushHelper Instance created"));

                    // call start
                    var configManager = ConfigManager.GetInstance(applicationID);
                    var pushAndroid_ServerKey = configManager.PushAndroid_ServerKey;
                    var pushAndroid_SenderID = configManager.PushAndroid_SenderID;
                    var p12FilePath = configManager.PushIOS_P12FilePath;
                    var p12FilePassword = configManager.PushIOS_P12FilePassword;

                    PMPushHelper.GetInstance(applicationID).Start(pushAndroid_ServerKey, pushAndroid_SenderID, p12FilePath, p12FilePassword, PMPushHelper_onLogPMPushHelperExceptionDelegate, PMPushHelper_onLogPMPushHelperSuccessDelegate);
                    isInitialized = true;
                }
            }
        }


        private string PMPushHelper_onLogPMPushHelperExceptionDelegate(Exception exception)
        {
            return ExceptionLogger.GetInstance(applicationID).Log(exception, ConfigManager.GetInstance(applicationID).WebChannelName).ToString();
        }

        private string PMPushHelper_onLogPMPushHelperSuccessDelegate(string message)
        {
            ModonDBEntities unitOfWork = null;
            try
            {
                dynamic msg = JObject.Parse(message);
                if (msg != null && msg.data != null && msg.data.payload != null && msg.data.mid != null)
                {
                    long msgID = long.Parse(msg.data.mid.ToString());
                    if (msgID > 0)
                    {
                        unitOfWork = EFUnitOfWork.NewUnitOfWork("PMPushHelper_onLogPMPushHelperSuccessDelegate");

                        var notificationRepository = new NotificationRepository(unitOfWork, applicationID);
                        var notification = notificationRepository.FirstOrDefault(n => n.ID == msgID && !n.IsSent);
                        if (notification != null)
                        {
                            notification.IsSent = true;
                            notification.LastUpdateDate = DateTime.Now;
                            notificationRepository.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex, ConfigManager.GetInstance(applicationID).WebChannelName);
            }
            finally
            {
                if (unitOfWork != null)
                    unitOfWork.Dispose();
            }

            EventLogger.GetInstance(applicationID).Log(message);
            return message;
        }

        public void Start(string pushAndroid_ServerKey, string PushAndroid_SenderID, string p12FilePath, string p12FilePassword, LogPMPushHelperExceptionDelegate onLogException = null, LogPMPushHelperSuccessDelegate onLogSuccessDelegate = null)
        {
            try
            {
                if (androidBroker == null && !string.IsNullOrWhiteSpace(pushAndroid_ServerKey))
                {
                    if (onLogException != null)
                        OnLogException += onLogException;

                    if (onLogSuccessDelegate != null)
                        OnLogSuccessDelegate += onLogSuccessDelegate;

                    //Create our push services broker
                    // Configuration
                    var configAndroid = new GcmConfiguration(PushAndroid_SenderID, pushAndroid_ServerKey, null);

                    // Create a new broker
                    androidBroker = new GcmServiceBroker(configAndroid);

                    //Wire up the events for all the services that the broker registers
                    androidBroker.OnNotificationFailed += androidBroker_OnNotificationFailed;
                    androidBroker.OnNotificationSucceeded += androidBroker_OnNotificationSucceeded;
                    androidBroker.Start();
                    EventLogger.GetInstance(applicationID).Log(string.Format("PMPushHelper AndroidBroker Started"));
                }

                if (iosBroker == null && !string.IsNullOrWhiteSpace(p12FilePath) && !p12FilePath.ToLower().Contains("p12"))
                {
                    var configIOS = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12FilePath, p12FilePassword);

                    // Create a new broker
                    iosBroker = new ApnsServiceBroker(configIOS);
                    iosBroker.OnNotificationFailed += iosBroker_OnNotificationFailed; ;
                    iosBroker.OnNotificationSucceeded += iosBroker_OnNotificationSucceeded; ;
                    iosBroker.Start();

                    EventLogger.GetInstance(applicationID).Log(string.Format("PMPushHelper IOSBroker Started"));
                }
            }
            catch (Exception ex)
            {
                if (OnLogException != null)
                    OnLogException(ex);
            }
        }

        private void iosBroker_OnNotificationSucceeded(ApnsNotification notification)
        {
            if (OnLogSuccessDelegate != null)
                OnLogSuccessDelegate(notification.ToString());
        }

        private void iosBroker_OnNotificationFailed(ApnsNotification notification, AggregateException aggregateEx)
        {
            aggregateEx.Handle(ex =>
            {

                // See what kind of exception it was to further diagnose
                if (ex is ApnsNotificationException)
                {
                    var notificationException = (GcmNotificationException)ex;
                    if (OnLogException != null)
                    {
                        OnLogException(notificationException);
                    }

                    // Deal with the failed notification
                    var gcmNotification = notificationException.Notification;
                    var description = notificationException.Description;
                    EventLogger.GetInstance(applicationID).Log("APNS Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");
                }
                else if (ex is DeviceSubscriptionExpiredException)
                {
                    HandlExpiredToken((DeviceSubscriptionExpiredException)ex);
                }
                else if (ex is RetryAfterException)
                {
                    var retryException = (RetryAfterException)ex;
                    if (OnLogException != null)
                    {
                        OnLogException(retryException);
                    }
                    // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                    EventLogger.GetInstance(applicationID).Log("APNS Rate Limited, don't send more until after {retryException.RetryAfterUtc}");
                }
                else
                {
                    EventLogger.GetInstance(applicationID).Log("APNS Notification Failed for some unknown reason");
                }

                // Mark it as handled
                return true;
            });
        }

        private void HandlExpiredToken(DeviceSubscriptionExpiredException expiredException)
        {
            var oldId = expiredException.OldSubscriptionId;
            var newId = expiredException.NewSubscriptionId;

            var common_UserDeviceRepository = new Common_UserDeviceRepository(EFUnitOfWork.NewUnitOfWork("Common_UserDeviceRepository"), applicationID);
            var userDevice = common_UserDeviceRepository.FirstOrDefault(p => p.DeviceToken == oldId);

            if (userDevice != null)
            {
                userDevice.DeviceToken = null;
                userDevice.IsLoggedIn = false;
                userDevice.AuthIP = null;
                userDevice.AuthToken = null;
                userDevice.ModifiedBy = ConfigManager.GetInstance(applicationID).SystemUserID;
                userDevice.LastUpdateDate = DateTime.Now;
            }
            if (!string.IsNullOrWhiteSpace(newId))
            {
                // Add it to DB (Upate the token)
                if (userDevice != null)
                    userDevice.DeviceToken = newId;
            }
            else
            {
                newId = "";
                // Remove it from DB (Upate the token)
            }
            common_UserDeviceRepository.Commit();

            if (OnLogException != null)
                OnLogException(new Exception(string.Format("Device RegistrationId Expired oldID: {0}, newID: {1}", oldId, newId)));
        }

        private void androidBroker_OnNotificationSucceeded(GcmNotification notification)
        {
            if (OnLogSuccessDelegate != null)
                OnLogSuccessDelegate(notification.ToString());
        }

        private void androidBroker_OnNotificationFailed(GcmNotification notification, AggregateException aggregateEx)
        {
            aggregateEx.Handle(ex =>
            {

                // See what kind of exception it was to further diagnose
                if (ex is GcmNotificationException)
                {
                    var notificationException = (GcmNotificationException)ex;
                    if (OnLogException != null)
                    {
                        OnLogException(notificationException);
                    }

                    // Deal with the failed notification
                    var gcmNotification = notificationException.Notification;
                    var description = notificationException.Description;
                    EventLogger.GetInstance(applicationID).Log("GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");
                }
                else if (ex is GcmMulticastResultException)
                {
                    var multicastException = (GcmMulticastResultException)ex;
                    if (OnLogException != null)
                    {
                        OnLogException(multicastException);
                    }

                    foreach (var succeededNotification in multicastException.Succeeded)
                    {
                        EventLogger.GetInstance(applicationID).Log("GCM Notification Failed: ID={succeededNotification.MessageId}");
                    }

                    foreach (var failedKvp in multicastException.Failed)
                    {
                        var n = failedKvp.Key;
                        var e = failedKvp.Value;

                        EventLogger.GetInstance(applicationID).Log("GCM Notification Failed: ID={n.MessageId}, Desc={e.Description}");
                    }
                }
                else if (ex is DeviceSubscriptionExpiredException)
                {
                    HandlExpiredToken((DeviceSubscriptionExpiredException)ex);
                }
                else if (ex is RetryAfterException)
                {
                    var retryException = (RetryAfterException)ex;
                    if (OnLogException != null)
                    {
                        OnLogException(retryException);
                    }
                    // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                    EventLogger.GetInstance(applicationID).Log("GCM Rate Limited, don't send more until after {retryException.RetryAfterUtc}");
                }
                else
                {
                    EventLogger.GetInstance(applicationID).Log("GCM Notification Failed for some unknown reason");
                }

                // Mark it as handled
                return true;
            });
        }

        public void PushBulk(long notificationID, Dictionary<string, string> deviceTokenDic, Dictionary<string, string> pushTextDic, Dictionary<string, string> additionalParametersDic = null)
        {
            try
            {
                if (deviceTokenDic == null || deviceTokenDic.Count == 0)
                {
                    ExceptionLogger.GetInstance(applicationID).Log("No Device token exists", "");
                    return;
                }

                if (pushTextDic == null || pushTextDic.Count == 0)
                    throw new Exception("Push Text Can't be null.");

                var token = new List<string>();
                int i = 0;
                foreach (var device in deviceTokenDic)
                {
                    if (!token.Contains(device.Key))
                    {
                        // Insert the notification into the DB
                        string pushText = "";
                        if (pushTextDic.ContainsKey(device.Value))
                            pushText = pushTextDic[device.Value];
                        else
                            pushText = pushTextDic.FirstOrDefault().Value;
                        Push(device.Key, notificationID, pushText, 1, additionalParametersDic);
                        token.Add(device.Key);
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                if (OnLogException != null)
                    OnLogException(ex);
            }
        }

        public void Push(string deviceToken, long notificationID, string text, int badge = 1, Dictionary<string, string> additionalParametersDic = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(deviceToken))
                    return;

                if (IsAndroidToken(deviceToken))
                {
                    PushAndroid(deviceToken, notificationID, text, badge, additionalParametersDic);
                }
                else if (IsIPhoneToken(deviceToken))
                {
                    PushIOS(deviceToken, notificationID, text, badge, additionalParametersDic);
                }
                else
                {
                    throw new Exception("Token is not recognized");
                }
            }
            catch (Exception ex)
            {
                if (OnLogException != null)
                    OnLogException(ex);
            }
        }

        public void Stop()
        {
            try
            {
                if (androidBroker != null)
                {
                    //Stop and wait for the queues to drains
                    androidBroker.Stop();
                    androidBroker = null;
                }
                isInitialized = false;
            }
            catch (Exception ex)
            {
                if (OnLogException != null)
                    OnLogException(ex);
            }
        }

        /// <summary>
        /// 
        /// //-------------------------
        /// // APPLE NOTIFICATIONS
        /// -------------------------
        /// Configure and start Apple APNS
        ///  IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
        ///    and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
        ///    app with!
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <param name="text"></param>
        private void PushIOS(string deviceToken, long notificationID, string text, int badge, Dictionary<string, string> additionalParametersDic)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(deviceToken) || string.IsNullOrWhiteSpace(text))
                    return;

                var jsonData = GetIOSPushMessage(notificationID, DateTime.Now, text, badge, additionalParametersDic);

                iosBroker.QueueNotification(new ApnsNotification
                {
                    DeviceToken = deviceToken,
                    Payload = JObject.Parse(jsonData)
                });
            }
            catch (Exception ex)
            {
                if (OnLogException != null)
                    OnLogException(ex);
            }
        }

        private const string pushMsgFormat1 = ", '{0}':'{1}'";
        private const string pushMsgFormat2 = "{{{0}}}";
        private const string iOSPushMsgFormat1 = "'aps':{{'alert':'{0}', 'badge':'{1}'}}, 'mid':'{2}', 'ndt':'{3}'";
        private const string androidPushMsgFormat1 = "'message':'{0}','badge':{1},'sound':'cheering.caf', 'icon':'appicon', 'I':'{2}', 'ndt':'{3}'";
        private const string androidPushMsgFormat2 = "{{'payload':{0}, 'mid':'{1}'}}";
        /// <summary>
        /// ---------------------------
        ///  ANDROID GCM NOTIFICATIONS
        /// ---------------------------
        /// Configure and start Android GCM
        /// IMPORTANT: The API KEY comes from your Google APIs Console App, under the API Access section, 
        ///   by choosing 'Create new Server key...'
        ///   You must ensure the 'Google Cloud Messaging for Android' service is enabled in your APIs Console
        /// </summary>
        /// <param name="PushAndroid_ServerKey"></param>
        /// <param name="deviceToken"></param>
        /// <param name="text"></param>
        private void PushAndroid(string deviceToken, long notificationID, string text, int badge, Dictionary<string, string> additionalParametersDic)
        {
            var jsonData = "";
            try
            {
                if (string.IsNullOrWhiteSpace(deviceToken))
                    return;

                jsonData = GetAndroidPushMessage(notificationID, DateTime.Now, text, badge, additionalParametersDic);

                jsonData = string.Format(androidPushMsgFormat2, jsonData, notificationID);

                androidBroker.QueueNotification(new GcmNotification()
                {
                    RegistrationIds = new List<string> { deviceToken },
                    Data = JObject.Parse(jsonData)
                });

            }
            catch (Exception ex)
            {
                if (OnLogException != null)
                    OnLogException(new Exception(jsonData, ex));
            }
        }

        private static string GetAndroidPushMessage(long notificationID, DateTime notificationDateTime, string text, int badge, Dictionary<string, string> additionalParametersDic)
        {
            var jsonData = string.Format(androidPushMsgFormat1, text, badge, notificationID, DateTimeHelper.ToStringDD_MM_YYYY_TIME(notificationDateTime));
            if (additionalParametersDic != null)
            {
                foreach (var par in additionalParametersDic)
                {
                    jsonData += string.Format(pushMsgFormat1, par.Key, par.Value);
                }
            }

            jsonData = string.Format(pushMsgFormat2, jsonData);
            //// use android
            //jsonData = "{'android':" + jsonData + "}";
            // use payload
            return jsonData;
        }

        private static string GetIOSPushMessage(long notificationID, DateTime notificationDateTime, string text, int badge, Dictionary<string, string> additionalParametersDic)
        {
            var jsonData = string.Format(iOSPushMsgFormat1, text, badge, notificationID, DateTimeHelper.ToStringDD_MM_YYYY_TIME(notificationDateTime));
            if (additionalParametersDic != null)
            {
                foreach (var par in additionalParametersDic)
                {
                    jsonData += string.Format(pushMsgFormat1, par.Key, par.Value);
                }
            }

            jsonData = string.Format(pushMsgFormat2, jsonData);
            return jsonData;
        }

        public static string GetPushMessage(string deviceToken, long notificationID, DateTime notificationDateTime, string text, int badge, Dictionary<string, string> additionalParametersDic)
        {
            if (IsAndroidToken(deviceToken))
                return GetAndroidPushMessage(notificationID, notificationDateTime, text, badge, additionalParametersDic);
            if (IsIPhoneToken(deviceToken))
                return GetIOSPushMessage(notificationID, notificationDateTime, text, badge, additionalParametersDic);
            return string.Empty;
        }

        /// <summary>
        /// Android Device length is longer than 160 char
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <returns></returns>
        private static bool IsAndroidToken(string deviceToken)
        {
            if (deviceToken != null && deviceToken.Length > IPhoneTokenLength)
                return true;
            return false;
        }

        /// <summary>
        /// IPhone Device length is exact 64 char
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <returns></returns>
        private static bool IsIPhoneToken(string deviceToken)
        {
            if (deviceToken != null && deviceToken.Length <= IPhoneTokenLength)
                return true;
            return false;
        }
    }
}
