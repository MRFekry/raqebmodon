using Newtonsoft.Json;
using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Core.Helpers;
using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace  PMApp.Core.NotificationsSystem
{
    public class NotificationManager
    {
        #region SingleTone
        private long applicationID;
        private static Object lockObj = new Object();
        private NotificationManager(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, NotificationManager> NotificationManagerDic = new Dictionary<long, NotificationManager>();
        private static Dictionary<long, NotificationType> NotificationTypeDic = new Dictionary<long, NotificationType>();
        public static NotificationManager GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!NotificationManagerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!NotificationManagerDic.ContainsKey(applicationID))
                    {
                        NotificationManagerDic.Add(applicationID, new NotificationManager(applicationID));

                        var unitOfWork = EFUnitOfWork.NewUnitOfWork("NotificationManager.GetInstance(request.ApplicationID).GetInstance");
                        var notificationTypeRepository = new NotificationTypeRepository(unitOfWork, applicationID);
                        var lst = notificationTypeRepository.Where(p => !p.IsDeleted).ToList();
                        for (int i = 0; i < lst.Count; i++)
                        {
                            NotificationTypeDic.Add(lst[i].ID, lst[i]);
                        }
                        unitOfWork.Dispose();
                    }
                }
            }
            return NotificationManagerDic[applicationID];
        }

        public static NotificationManager GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        public void Notify(long? userID, List<long> toUserIDList, EnumNotificationType enmNotificationType, long objectID, BaseRecord notificationRecord, string extraData = null, string secondEmail = null, bool alloweIncludeMeInToList = false)
        {
            if (notificationRecord != null)
            {
                var emailSubject = NotificationFormatter.GetNotificationEmailSubject(notificationRecord, enmNotificationType);
                var emailBody = NotificationFormatter.GetNotificationEmailBody(notificationRecord, enmNotificationType);
                var pushBody = NotificationFormatter.GetNotificationPushMessage(notificationRecord, enmNotificationType);

                Notify(userID, toUserIDList, enmNotificationType, emailSubject, emailBody, pushBody, objectID, notificationRecord, extraData, secondEmail, alloweIncludeMeInToList);
            }
        }

        public void Notify(long? userID, long toUserID, EnumNotificationType enmNotificationType, long objectID, BaseRecord notificationRecord, string extraData = null, string secondEmail = null, bool alloweIncludeMeInToList = false)
        {
            if (notificationRecord != null)
            {
                var emailSubject = NotificationFormatter.GetNotificationEmailSubject(notificationRecord, enmNotificationType);
                var emailBody = NotificationFormatter.GetNotificationEmailBody(notificationRecord, enmNotificationType);
                var pushBody = NotificationFormatter.GetNotificationPushMessage(notificationRecord, enmNotificationType);

                Notify(userID, toUserID, enmNotificationType, emailSubject, emailBody, pushBody, objectID, notificationRecord, extraData, secondEmail, alloweIncludeMeInToList);
            }
        }

        public void Notify(long? userID, long toUserID, EnumNotificationType enmNotificationType, long objectID, BaseRecord notificationRecord)
        {
            if (notificationRecord != null)
            {
                var emailSubject = NotificationFormatter.GetNotificationEmailSubject(notificationRecord, enmNotificationType);
                var emailBody = NotificationFormatter.GetNotificationEmailBody(notificationRecord, enmNotificationType);
                var pushBody = NotificationFormatter.GetNotificationPushMessage(notificationRecord, enmNotificationType);

                Notify(userID, toUserID, enmNotificationType, emailSubject, emailBody, pushBody, objectID, notificationRecord, null, null, false);
            }
        }

        public void Notify(long? userID, long toUserID, EnumNotificationType enmNotificationType, Dictionary<string, string> emailSubjectDic, Dictionary<string, string> emailBodyDic, Dictionary<string, string> pushBodyDic, long objectID, BaseRecord notificationRecord, string extraData = null, string secondEmail = null, bool alloweIncludeMeInToList = false)
        {
            if (toUserID > 0 || !string.IsNullOrWhiteSpace(secondEmail))
            {
                Notify(userID, new List<long> { toUserID }, enmNotificationType, emailSubjectDic, emailBodyDic, pushBodyDic, objectID, notificationRecord, extraData, secondEmail, alloweIncludeMeInToList);
            }
        }

        public void Notify(long? userID, List<long> toUserIDList, EnumNotificationType enmNotificationType, Dictionary<string, string> emailSubjectDic, Dictionary<string, string> emailBodyDic, Dictionary<string, string> pushBodyDic, long objectID, BaseRecord notificationRecord, string extraData = null, string secondEmail = null, bool alloweIncludeMeInToList = false)
        {
            Task.Run(() =>
            {
                if ((toUserIDList == null || toUserIDList.Count < 1) && string.IsNullOrWhiteSpace(secondEmail))
                    return;
                if (!userID.HasValue || userID == 0)
                {
                    userID = ConfigManager.GetInstance(applicationID).SystemUserID;
                }

                for (int i = toUserIDList.Count - 1; i >= 0; i--)
                {
                    if (toUserIDList[i] == 0 || (toUserIDList[i] == userID))
                        toUserIDList.RemoveAt(i);
                }
                var fromUserFullName = UserRepository.GetUserFullName(userID ?? 0, applicationID);

                pushBodyDic = pushBodyDic.ToDictionary(p => p.Key, p => p.Value.Replace("$FromUserFullName$", fromUserFullName));
                emailSubjectDic = emailSubjectDic.ToDictionary(p => p.Key, p => p.Value.Replace("$FromUserFullName$", fromUserFullName));
                emailBodyDic = emailBodyDic.ToDictionary(p => p.Key, p => p.Value.Replace("$FromUserFullName$", fromUserFullName));


                
                    PushNotify(userID, toUserIDList, enmNotificationType, pushBodyDic, objectID, notificationRecord, extraData);
                    EmailNotify(userID, toUserIDList, enmNotificationType, emailSubjectDic, emailBodyDic, objectID, notificationRecord, extraData, secondEmail);
            });
        }


        private void EmailNotify(long? userID, List<long> toUserIDList, EnumNotificationType enmNotificationType, Dictionary<string, string> emailSubjectDic, Dictionary<string, string> emailBodyDic, long objectID, BaseRecord notificationRecord, string extraData, string secondEmail = null)
        {
            ModonDBEntities unitOfWork = null;
            try
            {
                unitOfWork = EFUnitOfWork.NewUnitOfWork("NotificationManager.GetInstance(request.ApplicationID).EmailNotify");

                long notificationTypeID = enmNotificationType.GetCode(applicationID);

                var userRepository = new Common_UserRepository(unitOfWork, applicationID);
                var toUserNotificationContentList = new List<UserNotificationContent>();

                if (string.IsNullOrWhiteSpace(secondEmail))
                {
                    toUserNotificationContentList = userRepository.Where(p => !p.IsDeleted && toUserIDList.Contains(p.ID)
                        && (objectID == 0))
                                         .Select(p => new UserNotificationContent { Email = p.Email, FullName = p.FullName, Lang = p.Common_UserDevice.Select(c => c.Lang).FirstOrDefault() ?? Language.Ar.ToString().ToLower() }).Distinct().ToList();
                }
                else
                {
                    toUserNotificationContentList.Add(new UserNotificationContent { Email = secondEmail, FullName = secondEmail, Lang = Language.Ar.ToString().ToLower() });
                }

                foreach (var toUserNotificationContent in toUserNotificationContentList)
                {
                    var emailSubject = "";
                    if (emailSubjectDic.ContainsKey(toUserNotificationContent.Lang))
                        emailSubject = emailSubjectDic[toUserNotificationContent.Lang].Replace("$ToUserFullName$", toUserNotificationContent.FullName);
                    else
                        emailSubject = emailSubjectDic.FirstOrDefault().Value.Replace("$ToUserFullName$", toUserNotificationContent.FullName);

                    var emailBody = "";
                    if (emailBodyDic.ContainsKey(toUserNotificationContent.Lang))
                        emailBody = emailBodyDic[toUserNotificationContent.Lang].Replace("$ToUserFullName$", toUserNotificationContent.FullName);
                    else
                        emailBody = emailBodyDic.FirstOrDefault().Value.Replace("$ToUserFullName$", toUserNotificationContent.FullName);

                    var applicationTitle = ApplicationManager.GetApplicationFromApplicationID(applicationID);

                    PushEmailHelper.GetInstance(applicationID).NotifyByMail(toUserNotificationContent.Email, applicationTitle, NotificationTypeDic[notificationTypeID], emailSubject, emailBody);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex, ConfigManager.GetInstance(applicationID).WebChannelName);
            }
            finally
            {
                if (unitOfWork != null)
                    unitOfWork.Dispose();
            }
        }

        private void PushNotify(long? userID, List<long> toUserIDList, EnumNotificationType enmNotificationType, Dictionary<string, string> pushBodyDic, long objectID, BaseRecord notificationRecord, string extraData)
        {
            PMPushHelper.GetInstance(applicationID).Initialize();
            ModonDBEntities unitOfWork = null;

            try
            {
                if (toUserIDList == null || toUserIDList.Count < 1)
                    return;

                unitOfWork = EFUnitOfWork.NewUnitOfWork("NotificationManager.GetInstance(request.ApplicationID).PushNotify");

                var paramsDic = new Dictionary<string, string>();
                string notificationRecordStr = "{}";
                //if (notificationRecord != null)
                //{
                //    if (notificationRecord.GetType().Name.Contains("QuestionRecord"))
                //    {
                //        notificationRecordStr = "{" + "I:" + notificationRecord.ID.ToString() + "}";
                //    }
                //    else
                //    {
                //        notificationRecordStr = JsonConvert.SerializeObject(notificationRecord);
                //    }
                //}

                long notificationTypeID = enmNotificationType.GetCode(applicationID);

                paramsDic.Add("NR", notificationRecordStr);
                paramsDic.Add("OI", objectID.ToString());
                paramsDic.Add("NTI", notificationTypeID.ToString());
                paramsDic.Add("DO", NotificationTypeDic[notificationTypeID].DBObjectID.ToString());
                var deviceDic = new Dictionary<string, string>();

                var userDeviceRepository = new Common_UserDeviceRepository(unitOfWork, applicationID);
                var notificationRepository = new NotificationRepository(unitOfWork, applicationID);
                //var newsFeedsUserRepository = new NewsFeedsUserRepository(unitOfWork, applicationID);
                //
                var toTokensAll = userDeviceRepository.Where(p => toUserIDList.Contains(p.Common_UserID) && p.DeviceToken != null && p.IsLoggedIn && !p.IsDeleted
                    && (objectID == 0))
                                                        .OrderByDescending(p => p.LastActiveDate)
                                                        .Select(p => new { p.Common_UserID, p.DeviceToken, p.Lang })
                                                        .ToList().Distinct().ToList();

                for (var i = 0; i < toUserIDList.Count; i++)
                {
                    var toUserFullName = UserRepository.GetUserFullName(toUserIDList[i], applicationID);

                    pushBodyDic = pushBodyDic.ToDictionary(p => p.Key, p => p.Value.Replace("$ToUserFullName$", toUserFullName));


                    Notification notification = null;
                    var toTokens = toTokensAll.Where(p => p.Common_UserID == toUserIDList[i]).ToList();
                    var toToken = toTokens.FirstOrDefault(p => p.Common_UserID == toUserIDList[i]);
                    var lang = "en";
                    if (toToken != null)
                        lang = toToken.Lang.ToLower();
                    //if (NotificationTypeDic[enmNotificationType.GetCode(applicationID)].AddToNotificationTable)
                    {
                        notification = new Notification
                        {
                            FromUserID = userID ?? 0,
                            ToUserID = toUserIDList[i],
                            NotificationTypeID = enmNotificationType.GetHashCode(),
                            NotificationData = notificationRecordStr,
                            NotificationMessage = pushBodyDic[lang],
                            CreatedBy = userID ?? 0,
                            CreationDate = DateTime.Now,
                            //ExtraData = extraData,
                            ApplicationID = applicationID
                        };
                        //if (notificationRecord != null)
                        //    notification.RecordID = notificationRecord.ID;
                        //InjectExtraData(enmNotificationType, notification, notificationRecord);

                        notificationRepository.Add(notification);
                        notificationRepository.Commit();
                    }

                    deviceDic.Clear();
                    for (var j = 0; j < toTokens.Count; j++)
                    {
                        if (toTokens[j].DeviceToken != "SIMULATOR.DeviceToken")
                        {
                            if (!string.IsNullOrWhiteSpace(toTokens[j].DeviceToken) && !deviceDic.ContainsKey(toTokens[j].DeviceToken))
                                deviceDic.Add(toTokens[j].DeviceToken, toTokens[j].Lang.ToLower());
                        }
                    }

                    if (deviceDic.Count > 0)
                    {
                        long notificationID = notification == null ? 0 : notification.ID;
                        PMPushHelper.GetInstance(applicationID).PushBulk(notificationID, deviceDic, pushBodyDic, paramsDic);
                    }
                    notificationRepository.Commit();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex, ConfigManager.GetInstance(applicationID).WebChannelName);
            }
            finally
            {
                if (unitOfWork != null)
                    unitOfWork.Dispose();
            }
        }

        private void Initialize(long notificationTypeSuffix)
        {
            PMPushHelper.GetInstance(applicationID).Initialize();
        }
    }

    class UserNotificationContent
    {
        public string FullName { get; set; }
        public string Email { get; set; }

        string lang;
        public string Lang { get { return lang.ToLower(); } set { lang = value; } }
        public string DeviceToken { get; set; }
        public int BadgeCount { get; set; }
    }
}
