using PMApp.Modon.DAL6;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;

namespace  PMApp.Core.NotificationsSystem
{
    public class PushEmailHelper
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private PushEmailHelper(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, PushEmailHelper> PushEmailHelperDic = new Dictionary<long, PushEmailHelper>();
        public static PushEmailHelper GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!PushEmailHelperDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!PushEmailHelperDic.ContainsKey(applicationID))
                    {
                        PushEmailHelperDic.Add(applicationID, new PushEmailHelper(applicationID));
                    }
                }
            }
            return PushEmailHelperDic[applicationID];
        }
        public static PushEmailHelper GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private const string lineBreak = "<br/>";
        private const string valStringFormate = "<p><strong>{0}:</strong><br/>{1}</p>";

        public string StrigifyNotification(NotificationType notificationType, Notification notification)
        {
            var htmlContent = string.Empty;
            htmlContent = string.Format(valStringFormate, notificationType.Description, notification.NotificationMessage);
            return htmlContent;
        }

        public void NotifyByMail(string toEmail, string appName, NotificationType notificationType, string emailTitle, string emailBody)
        {
            try
            {
                var mailSubject = emailTitle;// string.Format(mailSubjectTemplate, appName + "-" + Environment.MachineName, notificationType.Description);
                var mailBody = emailBody;// StrigifyNotification(notificationType, notification);

                var logEmail = new LogMail(applicationID, mailSubject, toEmail, mailBody);
                logEmail.SendMail();
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex,ConfigManager.GetInstance(applicationID).WebChannelName);
            }
        }
    }
}