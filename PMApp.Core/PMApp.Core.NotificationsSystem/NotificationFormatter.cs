﻿using PMApp.Core.CommonDefinitions.DataContracts.Record;
using PMApp.Modon.DAL6;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace  PMApp.Core.NotificationsSystem
{
    public class NotificationFormatter
    {
        const string notificationKeyEmailSubject = "Notification.{0}.EmailSubject";
        const string notificationKeyEmailBody = "Notification.{0}.EmailBody";
        const string notificationKeyPushMessage = "Notification.{0}.PushMessage";

        public static Dictionary<string, string> GetNotificationEmailSubject(BaseRecord baseRecord, EnumNotificationType notificationKey)
        {
            var resourceString = ResourceManager.GetInstance(baseRecord.ApplicationID).GetResourcesForAllLanguages(string.Format(notificationKeyEmailSubject, notificationKey));

            return GetNotification(baseRecord, resourceString);
        }

        public static Dictionary<string, string> GetNotificationEmailBody(BaseRecord baseRecord, EnumNotificationType notificationKey)
        {
            var resourceString = ResourceManager.GetInstance(baseRecord.ApplicationID).GetResourcesForAllLanguages(string.Format(notificationKeyEmailBody, notificationKey));

            return GetNotification(baseRecord, resourceString);
        }

        public static Dictionary<string, string> GetNotificationPushMessage(BaseRecord baseRecord, EnumNotificationType notificationKey)
        {
            var resourceString = ResourceManager.GetInstance(baseRecord.ApplicationID).GetResourcesForAllLanguages(string.Format(notificationKeyPushMessage, notificationKey));

            return GetNotification(baseRecord, resourceString);
        }

        private static Regex notificationKeyRegEx = new Regex(@"\$[a-zA-Z]*\$");

        private static Dictionary<string, string> GetNotification(BaseRecord baseRecord, Dictionary<string, string> resourceStringDic)
        {
            if (resourceStringDic != null)
            {
                for (int i = 0; i < resourceStringDic.Count; i++)
                {
                    var resource = resourceStringDic.ElementAt(i);
                    var matches = notificationKeyRegEx.Matches(resource.Value);
                    if (matches.Count > 0)
                    {
                        var notificationFormatDictionary = baseRecord.InitializeNotificationFormater();
                        var resourceString = resource.Value;
                        foreach (var item in notificationFormatDictionary)
                        {
                            resourceString = resourceString.Replace(string.Format("${0}$", item.Key), item.Value);
                        }
                        resourceStringDic[resource.Key] = resourceString;
                    }
                }
            }
            return resourceStringDic;
        }

    }
}
