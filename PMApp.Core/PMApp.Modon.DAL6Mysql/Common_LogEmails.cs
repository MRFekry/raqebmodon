//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.Modon.DAL6
{
    using System;
    using System.Collections.Generic;
    
    public partial class Common_LogEmails
    {
        public long ID { get; set; }
        public string Channel { get; set; }
        public string LogType { get; set; }
        public string Emails { get; set; }
        public long ApplicationID { get; set; }
    }
}
