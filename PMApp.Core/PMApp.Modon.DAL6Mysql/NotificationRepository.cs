		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class NotificationRepository : EFRepository<Notification>
    {
        public NotificationRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Notification> All()
        {
            return ContextEntities.Notifications.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Notification> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Notification entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Notifications.Add(entity);
        }

        public override void Delete(Notification entity)
        {
            ContextEntities.Notifications.Remove(entity);
        }
    }
}