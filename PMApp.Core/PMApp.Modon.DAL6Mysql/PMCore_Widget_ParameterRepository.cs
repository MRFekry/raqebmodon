		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_Widget_ParameterRepository : EFRepository<PMCore_Widget_Parameter>
    {
        public PMCore_Widget_ParameterRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Widget_Parameter> All()
        {
            return ContextEntities.PMCore_Widget_Parameter.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Widget_Parameter> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Widget_Parameter entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Widget_Parameter.Add(entity);
        }

        public override void Delete(PMCore_Widget_Parameter entity)
        {
            ContextEntities.PMCore_Widget_Parameter.Remove(entity);
        }
    }
}