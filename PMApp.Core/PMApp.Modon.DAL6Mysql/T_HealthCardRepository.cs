		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class T_HealthCardRepository : EFRepository<T_HealthCard>
    {
        public T_HealthCardRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<T_HealthCard> All()
        {
            return ContextEntities.T_HealthCard.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<T_HealthCard> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(T_HealthCard entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.T_HealthCard.Add(entity);
        }

        public override void Delete(T_HealthCard entity)
        {
            ContextEntities.T_HealthCard.Remove(entity);
        }
    }
}