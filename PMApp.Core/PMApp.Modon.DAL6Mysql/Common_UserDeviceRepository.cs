		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_UserDeviceRepository : EFRepository<Common_UserDevice>
    {
        public Common_UserDeviceRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_UserDevice> All()
        {
            return ContextEntities.Common_UserDevice.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_UserDevice> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_UserDevice entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_UserDevice.Add(entity);
        }

        public override void Delete(Common_UserDevice entity)
        {
            ContextEntities.Common_UserDevice.Remove(entity);
        }
    }
}