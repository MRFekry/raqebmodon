		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_ResourceRepository : EFRepository<Common_Resource>
    {
        public Common_ResourceRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_Resource> All()
        {
            return ContextEntities.Common_Resource.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_Resource> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_Resource entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_Resource.Add(entity);
        }

        public override void Delete(Common_Resource entity)
        {
            ContextEntities.Common_Resource.Remove(entity);
        }
    }
}