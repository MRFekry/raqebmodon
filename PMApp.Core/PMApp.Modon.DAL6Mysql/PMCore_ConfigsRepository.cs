		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_ConfigsRepository : EFRepository<PMCore_Configs>
    {
        public PMCore_ConfigsRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Configs> All()
        {
            return ContextEntities.PMCore_Configs.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Configs> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Configs entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Configs.Add(entity);
        }

        public override void Delete(PMCore_Configs entity)
        {
            ContextEntities.PMCore_Configs.Remove(entity);
        }
    }
}