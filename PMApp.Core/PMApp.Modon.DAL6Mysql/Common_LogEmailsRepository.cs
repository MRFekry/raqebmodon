		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_LogEmailsRepository : EFRepository<Common_LogEmails>
    {
        public Common_LogEmailsRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_LogEmails> All()
        {
            return ContextEntities.Common_LogEmails.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_LogEmails> All(long userID)
        {
			return All();
        }

        public override void Add(Common_LogEmails entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_LogEmails.Add(entity);
        }

        public override void Delete(Common_LogEmails entity)
        {
            ContextEntities.Common_LogEmails.Remove(entity);
        }
    }
}