		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_LogExceptionRepository : EFRepository<Common_LogException>
    {
        public Common_LogExceptionRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_LogException> All()
        {
            return ContextEntities.Common_LogException.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_LogException> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_LogException entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_LogException.Add(entity);
        }

        public override void Delete(Common_LogException entity)
        {
            ContextEntities.Common_LogException.Remove(entity);
        }
    }
}