		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class OrderTypeRepository : EFRepository<OrderType>
    {
        public OrderTypeRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<OrderType> All()
        {
            return ContextEntities.OrderTypes.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<OrderType> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(OrderType entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.OrderTypes.Add(entity);
        }

        public override void Delete(OrderType entity)
        {
            ContextEntities.OrderTypes.Remove(entity);
        }
    }
}