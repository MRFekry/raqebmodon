//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.Modon.DAL6
{
    using System;
    using System.Collections.Generic;
    
    public partial class PMCore_Role
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PMCore_Role()
        {
            this.Common_User = new HashSet<Common_User>();
            this.PMCore_Role_Menu = new HashSet<PMCore_Role_Menu>();
            this.PMCore_Role1 = new HashSet<PMCore_Role>();
            this.PMCore_ServiceOperationRoleAccess = new HashSet<PMCore_ServiceOperationRoleAccess>();
            this.PMCore_Widget_Role = new HashSet<PMCore_Widget_Role>();
        }
    
        public long ID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public Nullable<long> ParentID { get; set; }
        public bool AllowMultiLogin { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<bool> IsAdmin { get; set; }
        public Nullable<bool> AllowManageTeam { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public System.DateTime CreationDate { get; set; }
        public long ApplicationID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_User> Common_User { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PMCore_Role_Menu> PMCore_Role_Menu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PMCore_Role> PMCore_Role1 { get; set; }
        public virtual PMCore_Role PMCore_Role2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PMCore_ServiceOperationRoleAccess> PMCore_ServiceOperationRoleAccess { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PMCore_Widget_Role> PMCore_Widget_Role { get; set; }
    }
}
