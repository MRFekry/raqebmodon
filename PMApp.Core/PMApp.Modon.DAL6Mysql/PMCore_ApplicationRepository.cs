		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_ApplicationRepository : EFRepository<PMCore_Application>
    {
        public PMCore_ApplicationRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Application> All()
        {
            return ContextEntities.PMCore_Application;
        }
		
        public override IQueryable<PMCore_Application> All(long userID)
        {
			return All();
        }

        public override void Add(PMCore_Application entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			
			ContextEntities.PMCore_Application.Add(entity);
        }

        public override void Delete(PMCore_Application entity)
        {
            ContextEntities.PMCore_Application.Remove(entity);
        }
    }
}