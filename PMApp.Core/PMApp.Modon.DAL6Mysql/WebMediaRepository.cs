		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class WebMediaRepository : EFRepository<WebMedia>
    {
        public WebMediaRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<WebMedia> All()
        {
            return ContextEntities.WebMedias.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<WebMedia> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(WebMedia entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.WebMedias.Add(entity);
        }

        public override void Delete(WebMedia entity)
        {
            ContextEntities.WebMedias.Remove(entity);
        }
    }
}