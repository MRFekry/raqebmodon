		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_RoleRepository : EFRepository<PMCore_Role>
    {
        public PMCore_RoleRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Role> All()
        {
            return ContextEntities.PMCore_Role.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Role> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Role entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Role.Add(entity);
        }

        public override void Delete(PMCore_Role entity)
        {
            ContextEntities.PMCore_Role.Remove(entity);
        }
    }
}