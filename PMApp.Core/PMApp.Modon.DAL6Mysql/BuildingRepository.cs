		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class BuildingRepository : EFRepository<Building>
    {
        public BuildingRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Building> All()
        {
            return ContextEntities.Buildings.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Building> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Building entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Buildings.Add(entity);
        }

        public override void Delete(Building entity)
        {
            ContextEntities.Buildings.Remove(entity);
        }
    }
}