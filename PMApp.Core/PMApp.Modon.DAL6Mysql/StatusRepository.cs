		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class StatusRepository : EFRepository<Status>
    {
        public StatusRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Status> All()
        {
            return ContextEntities.Status.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Status> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Status entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Status.Add(entity);
        }

        public override void Delete(Status entity)
        {
            ContextEntities.Status.Remove(entity);
        }
    }
}