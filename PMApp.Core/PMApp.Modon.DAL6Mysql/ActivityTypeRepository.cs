		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class ActivityTypeRepository : EFRepository<ActivityType>
    {
        public ActivityTypeRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<ActivityType> All()
        {
            return ContextEntities.ActivityTypes.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<ActivityType> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(ActivityType entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.ActivityTypes.Add(entity);
        }

        public override void Delete(ActivityType entity)
        {
            ContextEntities.ActivityTypes.Remove(entity);
        }
    }
}