		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class StatusHistoryRepository : EFRepository<StatusHistory>
    {
        public StatusHistoryRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<StatusHistory> All()
        {
            return ContextEntities.StatusHistories.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<StatusHistory> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(StatusHistory entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.StatusHistories.Add(entity);
        }

        public override void Delete(StatusHistory entity)
        {
            ContextEntities.StatusHistories.Remove(entity);
        }
    }
}