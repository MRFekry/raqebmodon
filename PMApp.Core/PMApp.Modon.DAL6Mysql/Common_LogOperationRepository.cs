		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_LogOperationRepository : EFRepository<Common_LogOperation>
    {
        public Common_LogOperationRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_LogOperation> All()
        {
            return ContextEntities.Common_LogOperation.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_LogOperation> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_LogOperation entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_LogOperation.Add(entity);
        }

        public override void Delete(Common_LogOperation entity)
        {
            ContextEntities.Common_LogOperation.Remove(entity);
        }
    }
}