		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class NotificationTypeRepository : EFRepository<NotificationType>
    {
        public NotificationTypeRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<NotificationType> All()
        {
            return ContextEntities.NotificationTypes.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<NotificationType> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(NotificationType entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.NotificationTypes.Add(entity);
        }

        public override void Delete(NotificationType entity)
        {
            ContextEntities.NotificationTypes.Remove(entity);
        }
    }
}