		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Order_CommentRepository : EFRepository<Order_Comment>
    {
        public Order_CommentRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Order_Comment> All()
        {
            return ContextEntities.Order_Comment.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Order_Comment> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Order_Comment entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Order_Comment.Add(entity);
        }

        public override void Delete(Order_Comment entity)
        {
            ContextEntities.Order_Comment.Remove(entity);
        }
    }
}