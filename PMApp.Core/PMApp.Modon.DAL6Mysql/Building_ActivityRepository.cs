		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Building_ActivityRepository : EFRepository<Building_Activity>
    {
        public Building_ActivityRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Building_Activity> All()
        {
            return ContextEntities.Building_Activity.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Building_Activity> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Building_Activity entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Building_Activity.Add(entity);
        }

        public override void Delete(Building_Activity entity)
        {
            ContextEntities.Building_Activity.Remove(entity);
        }
    }
}