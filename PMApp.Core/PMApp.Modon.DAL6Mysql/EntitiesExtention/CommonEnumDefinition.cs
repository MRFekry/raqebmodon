﻿namespace PMApp.Modon.DAL6
{
    public enum Channel : int
    {
        Web = 1,
        AndroidMobile = 2,
        AndroidTablet = 3,
        Service = 4,
        IOSMobile = 5,
        IOSTablet = 6
    }

    public enum DeleteSelect : int
    {
        IsNotDeleted = 0,
        All = 1,
        IsDeleted = 2
    }

    public enum ActiveSelect : int
    {
        IsActive = 0,
        All = 1,
        IsNotActive = 3
    }


    public enum EnumNotificationType
    {
        UnKnow = 0,
        CommentReply = 2,
        Comment = 1,
        EmailOnly = 5,
        NotificationOnly = 6,
        AddBuilding_User = 3,
        AddBuilding_Employee = 4,
        ApproveBuilding = 10,
        RejectBuilding = 7,
        InvestorEditBuilding = 8,
        PendingForEditBuilding = 9,
        AddComment,

    }
    public enum CurrencyType
    {
        USD,
        EGP
    }

    public enum EnumAudienceType : int
    {
        MyTeam = 0,
        Area = 1,
        Line = 2,
        Broadcast = 3,
        User = 4
    }

    public enum EnumCycleDurationType : int
    {
        Monthly = 1,
        Weekly = 2,
        BiWeekly = 3,
    }

    public enum LogType : int
    {
        Event = 0,
        Exception = 1,
        File = 2,
        HostCommunication = 4,
        PerformanceLogger = 5,
        UserLogger = 6,
        OperationLogger = 7
    }

    public enum ForgetPasswordChoice : int
    {
        Email = 1,
        SecondEmail = 2,
        Mobile = 3
    }

    public enum EnumPayPalState
    {
        created = 0,
        approved = 1,
        failed = 2,
        canceled = 3,
        expired = 4,
    }
    public enum EnumOrderType
    {
        AddBuilding = 1,
        ApprovedLicence = 2,
    }
    public static class EnumFromConfig
    {
        public static long GetCode(this EnumNotificationType enumNotificationType, long applicationID)
        {
            long enumValue = 0;
            var configManager = ConfigManager.GetInstance(applicationID);
            if (!long.TryParse(configManager.GetConfig(string.Format(ConfigManager.NotificationTypeEnum_XStr, enumNotificationType.ToString())), out enumValue))
                enumValue = enumNotificationType.GetHashCode();
            return enumValue;
        }
    }

}