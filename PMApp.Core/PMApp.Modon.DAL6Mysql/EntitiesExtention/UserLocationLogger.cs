using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PMApp.Modon.DAL6
{
    public class UserLocationLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private UserLocationLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, UserLocationLogger> UserLocationLoggerDic = new Dictionary<long, UserLocationLogger>();
        public static UserLocationLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!UserLocationLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!UserLocationLoggerDic.ContainsKey(applicationID))
                    {
                        UserLocationLoggerDic.Add(applicationID, new UserLocationLogger(applicationID));
                    }
                }
            }
            return UserLocationLoggerDic[applicationID];
        }
        public static UserLocationLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        public void Log(UserLocation userLocation)
        {
            try
            {
                if (userLocation != null)
                {
                    if (userLocation.Latitude != 0 && userLocation.Longitude != 0)
                    {
                        Task.Run(() =>
                        {
                            if (string.IsNullOrWhiteSpace(userLocation.Address))
                            {
                                userLocation.Address = DistanceManager.GetInstance(applicationID).GetHereMapPlaceSummeryRecord(userLocation.Latitude, userLocation.Longitude);
                            }

                            var context = EFUnitOfWork.NewUnitOfWork("UserLocationLogger");
                            var userLocationRepository = new UserLocationRepository(context, applicationID);
                            userLocationRepository.Add(new UserLocation
                            {
                                UserID = userLocation.UserID,
                                SubmitDateTime = userLocation.SubmitDateTime,
                                CreationDateTime = userLocation.CreationDateTime,
                                AquirityDistance = userLocation.AquirityDistance,
                                Address = userLocation.Address,
                                BataryStatus = userLocation.BataryStatus,
                                DeviceID = userLocation.DeviceID,
                                DirectionAngle = userLocation.DirectionAngle,
                                IsOnline = userLocation.IsOnline,
                                Latitude = userLocation.Latitude,
                                Longitude = userLocation.Longitude,
                                NetworkStatus = userLocation.NetworkStatus,
                                Speed = userLocation.Speed
                            });
                            userLocationRepository.Commit();
                            context.Dispose();
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
        }
    }
}