using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PMApp.Modon.DAL6
{
    public class ConfigManager
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private ConfigManager(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, ConfigManager> ConfigManagerDic = new Dictionary<long, ConfigManager>();
        public static ConfigManager GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!ConfigManagerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!ConfigManagerDic.ContainsKey(applicationID))
                    {
                        ConfigManagerDic.Add(applicationID, new ConfigManager(applicationID));
                    }
                }
            }
            return ConfigManagerDic[applicationID];
        }

        public static ConfigManager GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        #region constants
        public const string DefaultApplicationCode = "Modon";
        private const string SystemUserIDStr = "SystemUserID";
        private const string MonthlyBudgetStr = "MonthlyBudget";


        private const string SeeMessageReplyLinkStr = "SeeMessageReplyLink";
        private const string SeeQuestionLinkStr = "SeeQuestionLink";
        private const string DefaultPageSizeStr = "DefaultPageSize";
        private const string BeeTrustedIPAdressStr = "BeeTrustedIPAdress";
        private const string TopRankedPageSizeStr = "TopRankedPageSize";
        private const string rankSecondTabDaysStr = "rankSecondTabDays";
        private const string rankThirdTabDaysStr = "rankThirdTabDays";
        private const string DaysToTransferToEarningStr = "DaysToTransferToEarning";
        private const string QuestionWithNoAnswersDaysStr = "QuestionWithNoAnswersDays";
        private const string QuestionExpirationPeriodInDaysStr = "QuestionExpirationPeriodInDays";
        private const string PromoCodeExpirationPeriodInDaysStr = "PromoCodeExpirationPeriodInDays";

        private const string CacheVersionStr = "CacheVersion";

        private const string DefaultSocialPasswordStr = "DefaultSocialPassword";
        private const string DisableVisitsForTodayFilterStr = "DisableVisitsForTodayFilter";
        private const string NearByDistanceInMStr = "NearByDistanceInM";



        private const string PushAndroid_SenderIDStr = "PushAndroid_SenderID";
        private const string PushAndroid_ServerKeyStr = "PushAndroid_ServerKey";
        private const string PushIOS_P12FilePasswordStr = "PushIOS_P12FilePassword";
        private const string PushIOS_P12FilePathStr = "PushIOS_P12FilePath";
        private const string AuthExpirationPeriodInDaysStr = "AuthExpirationPeriodInDays";

        private const string WebChannelNameStr = "Web";
        private const string LogMinSecondsStr = "LogMinSeconds";

        //private const string MediaTypePictureIDStr = "MediaTypePictureID";

        private const string UserDefaultPictureUrlStr = "UserDefaultPictureUrl";

        private const string PerformanceCounterMinimumTimeInSecondsStr = "PerformanceCounterMinimumTimeInSeconds";

        private const string HereMap_appidStr = "HereMap_appid";
        private const string HereMap_appcodeStr = "HereMap_appcode";
        private const string GoogleMap_keyStr = "GoogleMap_key";

        private const string WorkingDaysCountPerMonthStr = "WorkingDaysCountPerMonth";

        private const string AllowMemberShipCheckAccessStr = "AllowMemberShipCheckAccess";


        private const string FaceBook_AppIDStr = "FaceBook_AppID";
        private const string FaceBook_AppSecretStr = "FaceBook_AppSecret";

        private const string MinimumInPlaceDistanceInMStr = "MinimumInPlaceDistanceInM";
        private const string QuestionDefaultDurationInMinsStr = "QuestionDefaultDurationInMins";
        private const string TrainingAssessmentDefaultDurationInMinsStr = " TrainingAssessmentDuartionInMins";
        private const string SalesEmailStr = "SalesEmail";
        private const string UpdateVisitDateValidTimesStr = "UpdateVisitDateValidTimes";
        private const string BlockDateDifferenceStr = "BlockDateDifference";

        private const string DBObjectEnum_XStr = "DBObjectEnum_{0}";
        private const string MembershipParameterTypeEnum_XStr = "MembershipParameterTypeEnum_{0}";

        private const string PaymentMethodEnum_XStr = "PaymentMethod_{0}";
        private const string StatusEnum_X_Y_Str = "StatusEnum_{0}_{1}";
        public const string NotificationTypeEnum_XStr = "NotificationTypeEnum_{0}";
        private const string RoleEnum_XStr = "RoleEnum_{0}";
        private const string FeatureParameterEnum_XStr = "FeatureParameterEnum_{0}";
        private const string ActionEnum_XStr = "ActionEnum_{0}";
        private const string ContactInfoTypeEnum_XStr = "ContactInfoTypeEnum_{0}";
        private const string ActivityTypeEnum_XStr = "ActivityTypeEnum_{0}";
        private const string MediaTypeEnum_XStr = "MediaTypeEnum_{0}";
        private const string AttachmentTypeEnum_XStr = "AttachmentTypeEnum_{0}";
        private const string MembershipFeaturesEnum_XStr = "MembershipFeaturesEnum_{0}";
        private const string PromoCodeTypeEnum_XStr = "PromoCodeTypeEnum_{0}";
        public const string ConfigEnumStr = "ConfigEnum_";

        private const string BasicMembershipIDStr = "BasicMembershipID";
        private const string Teacher2MembershipStr = "Teacher2MembershipID";


        private const string AwardedAnswerFeesPercentageStr = "AwardedAnswerFeesPercentage";
        private const string RedeemSchemaPerPointStr = "RedeemSchemaPerPoint";
        private const string MinRedeemPointsStr = "MinRedeemPoints";

        private const string MaximumPointsOfQuestionStr = "MaximumPointsOfQuestion";
        private const string MinimumPointsOfQuestionStr = "MinimumPointsOfQuestion";
        private const string MinimumPointsStepOfQuestionStr = "MinimumPointsStepOfQuestion";
        private const string AwardLimitationCountStr = "AwardLimitationCount";
        private const string FileLimitationSizeInMBStr = "FileLimitationSizeInMB";
        private const string FileLimitationCountStr = "FileLimitationCount";
        private const string GuestLimitOpenSearchStr = "GuestLimitOpenSearch";
        private const string GuestLimitClosedSearchStr = "GuestLimitClosedSearch";


        private const string DefaultImageHeightStr = "DefaultImageHeight";
        private const string DefaultImageWidthStr = "DefaultImageWidth";
        private const string DefaultEmailStr = "DefaultEmail";
        private const string SupportEmailStr = "SupportEmail";

        private const string DefaultSchemaPriceStr = "DefaultSchemaPrice";
        private const string DefaultCurrencyStr = "DefaultCurrency";
        private const string ActivationPromoCodeValueStr = "ActivationPromoCodeValue";
        private const string MenuWidgetReportUrlStr = "MenuWidgetReportUrl";
        #endregion

        //private DateTime LastDateTimeCheck;
        //private long CurrentCacheVersion = 0;
        private Dictionary<string, string> CashedConfigs = new Dictionary<string, string>();

        public static List<PMCore_Configs> LoadEnumTablesConfig(long applicationID)
        {
            var list = new List<PMCore_Configs>();
            // Load the EnumTablesQueries
            var context = EFUnitOfWork.NewUnitOfWork(ConfigEnumStr);
            var enumTablesQueriesConfigList = new PMCore_ConfigsRepository(context, applicationID).Where(p => p.ConfKey.StartsWith("ConfigEnum_") && p.ApplicationID == applicationID);
            if (enumTablesQueriesConfigList != null)
            {
                var queries = enumTablesQueriesConfigList.Select(p => p.ConfValue).ToList();
                for (int i = 0; i < queries.Count; i++)
                {
                    var dt = DBHelper.ExecuteQuery(context.Database.Connection.ConnectionString, queries[i], applicationID);
                    for (int r = 0; r < dt.Rows.Count; r++)
                    {
                        list.Add(new PMCore_Configs
                        {
                            ApplicationID = applicationID,
                            ConfKey = dt.Rows[r][1].ToString(),// Key
                            ConfValue = dt.Rows[r][0].ToString()// Value
                        });
                    }
                }
            }
            context.Dispose();
            return list;
        }

        private void InsertDefaultConfig(string confKey, string confValue)
        {
            try
            {
                if (confValue == null)
                    confValue = string.Empty;
                lock (lockObj)
                {
                    var context = EFUnitOfWork.NewUnitOfWork("GetConfig");
                    var configsRepository = new PMCore_ConfigsRepository(context, applicationID);
                    configsRepository.Add(new PMCore_Configs
                    {
                        ConfKey = confKey,
                        ConfValue = confValue,
                        CreatedBy = SystemUserID,
                        ApplicationID = applicationID,
                        Decription = confKey,
                        IsUIEditable = false,
                        IsDeleted = false,
                        CreationDate = DateTime.Now
                    });
                    configsRepository.Commit();

                    var key = string.Format("{0}-{1}", applicationID, confKey).ToLower();
                    if (!CashedConfigs.ContainsKey(key))
                        CashedConfigs.Add(key, confValue);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
        }

        public string GetConfig(string confKey, string defaultValue = null)
        {
            try
            {
                if (CashedConfigs == null || CashedConfigs.Count == 0)
                {
                    // 2.  get from the DB and cash it
                    lock (lockObj)
                    {
                        var context = EFUnitOfWork.NewUnitOfWork("GetConfig");
                        var configList = new PMCore_ConfigsRepository(context, applicationID).All().ToList();
                        if (configList != null)
                        {
                            var key = "";
                            for (int i = 0; i < configList.Count; i++)
                            {
                                key = string.Format("{0}-{1}", applicationID, configList[i].ConfKey).ToLower();
                                if (!CashedConfigs.ContainsKey(key))
                                    CashedConfigs.Add(key, configList[i].ConfValue);
                            }
                            var enumList = LoadEnumTablesConfig(applicationID);
                            for (int i = 0; i < enumList.Count; i++)
                            {
                                key = string.Format("{0}-{1}", applicationID, enumList[i].ConfKey).ToLower();
                                if (!CashedConfigs.ContainsKey(key))
                                    CashedConfigs.Add(key, enumList[i].ConfValue);
                            }
                            context.Dispose();
                        }
                    }
                }
                // 1. try get from the cashed configs
                var tmpConfKey = string.Format("{0}-{1}", applicationID, confKey).ToLower();
                if (CashedConfigs.ContainsKey(tmpConfKey))
                {
                    return CashedConfigs[tmpConfKey];
                }

                // 2. Insert the default value if not exists
                if (defaultValue != null)
                    InsertDefaultConfig(confKey, defaultValue);
            }
            catch (System.Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return confKey;
        }

        public long GetConfigLong(string confKey, long defaultValue)
        {
            long val = defaultValue;
            if (long.TryParse(GetConfig(confKey), out val))
                return val;
            return defaultValue;
        }
        public double GetConfigDouble(string confKey, double defaultValue)
        {
            double val = defaultValue;
            if (double.TryParse(GetConfig(confKey), out val))
                return val;
            return defaultValue;
        }
        public int GetConfigInt(string confKey, int defaultValue)
        {
            int val = defaultValue;
            if (int.TryParse(GetConfig(confKey), out val))
                return val;
            return defaultValue;
        }
        public bool GetConfigBoolean(string confKey, bool defaultValue)
        {
            bool val = defaultValue;
            if (bool.TryParse(GetConfig(confKey), out val))
                return val;
            return defaultValue;
        }

        public bool DisableVisitsForTodayFilter
        {
            get
            {
                return GetConfigBoolean(DisableVisitsForTodayFilterStr, false);
            }
        }

        public double NearByDistanceInM
        {
            get
            {
                return GetConfigDouble(NearByDistanceInMStr, 50);
            }
        }

        public int AuthExpirationPeriodInDays
        {
            get
            {
                return (int)GetConfigLong(AuthExpirationPeriodInDaysStr, 1);
            }
        }
        public string PushIOS_P12FilePath
        {
            get
            {
                return GetConfig(PushIOS_P12FilePathStr);
            }
        }
        public string PushIOS_P12FilePassword
        {
            get
            {
                return GetConfig(PushIOS_P12FilePasswordStr);
            }
        }
        public string PushAndroid_ServerKey
        {
            get
            {
                return GetConfig(PushAndroid_ServerKeyStr);
            }
        }
        public string PushAndroid_SenderID
        {
            get
            {
                return GetConfig(PushAndroid_SenderIDStr);
            }
        }

        public string WebChannelName
        {
            get
            {
                return WebChannelNameStr;
            }
        }


        public string BeeTrustedIPAdress
        {
            get
            {
                return GetConfig(BeeTrustedIPAdressStr, "1852665334946698");
            }
        }

        public string UserDefaultPictureUrl
        {
            get
            {
                return GetConfig(UserDefaultPictureUrlStr);
            }
        }

        public int PerformanceCounterMinimumTimeInSeconds
        {
            get
            {
                return (int)GetConfigLong(PerformanceCounterMinimumTimeInSecondsStr, 1);
            }
        }

        public string HereMap_appid
        {
            get
            {
                return GetConfig(HereMap_appidStr);
            }
        }

        public string HereMap_appcode
        {
            get
            {
                return GetConfig(HereMap_appcodeStr);
            }
        }

        public string GoogleMap_key
        {
            get
            {
                return GetConfig(GoogleMap_keyStr);
            }
        }

        public int WorkingDaysCountPerMonth
        {
            get
            {
                return (int)GetConfigLong(WorkingDaysCountPerMonthStr, 1);
            }
        }

        public bool AllowMemberShipCheckAccess
        {
            get
            {
                return GetConfigBoolean(AllowMemberShipCheckAccessStr, false);
            }
        }



        public string FaceBook_AppID
        {
            get
            {
                return GetConfig(FaceBook_AppIDStr, "1852665334946698");
            }
        }

        public string FaceBook_AppSecret
        {
            get
            {
                return GetConfig(FaceBook_AppSecretStr, "1f020a068c4245918468239580a63202");
            }
        }

        public string SalesEmail
        {
            get
            {
                return GetConfig(SalesEmailStr);
            }
        }

        public int MinimumInPlaceDistanceInM
        {
            get
            {
                return (int)GetConfigLong(MinimumInPlaceDistanceInMStr, 10);
            }
        }

        public int UpdateVisitDateValidTimes
        {
            get
            {
                return (int)GetConfigLong(UpdateVisitDateValidTimesStr, 0);
            }
        }

        public long BlockDateDifference
        {
            get
            {
                return GetConfigLong(BlockDateDifferenceStr, 2);
            }
        }

        private long workingDaysCount = 0;


        public string DefaultSocialPassword
        {
            get
            {
                return GetConfig(DefaultSocialPasswordStr);
            }
        }
        public long BasicMembershipID
        {
            get
            {
                return GetConfigLong(BasicMembershipIDStr, 4);
            }
        }
        public long MonthlyBudget
        {
            get
            {
                return GetConfigLong(MonthlyBudgetStr, 400);
            }
        }
        public long Teacher2MembershipID
        {
            get
            {
                return GetConfigLong(Teacher2MembershipStr, 3);
            }
        }
        public long AwardedAnswerFeesPercentage
        {
            get
            {
                return GetConfigLong(AwardedAnswerFeesPercentageStr, 2);
            }
        }
        public double RedeemSchemaPerPoint
        {
            get
            {
                return GetConfigDouble(RedeemSchemaPerPointStr, 1);
            }
        }
        public long MinRedeemPoints
        {
            get
            {
                return GetConfigLong(MinRedeemPointsStr, 1000);
            }
        }
        public long MinimumPointsOfQuestion
        {
            get
            {
                return GetConfigLong(MinimumPointsOfQuestionStr, 50);
            }
        }
        public long MaximumPointsOfQuestion
        {
            get
            {
                return GetConfigLong(MaximumPointsOfQuestionStr, 5000);
            }
        }
        public long MinimumPointsStepOfQuestion
        {
            get
            {
                return GetConfigLong(MinimumPointsStepOfQuestionStr, 5);
            }
        }
        public long AwardLimitationCount
        {
            get
            {
                return GetConfigLong(AwardLimitationCountStr, 3);
            }
        }
        public long FileLimitationSizeInMB
        {
            get
            {
                return GetConfigLong(FileLimitationSizeInMBStr, 2);
            }
        }
        public long FileLimitationCount
        {
            get
            {
                return GetConfigLong(FileLimitationCountStr, 5);
            }
        }
        public long GuestLimitOpenSearch
        {
            get
            {
                return GetConfigLong(GuestLimitOpenSearchStr, 10);
            }
        }
        public long GuestLimitClosedSearch
        {
            get
            {
                return GetConfigLong(GuestLimitClosedSearchStr, 10);
            }
        }



        #region DBObjectEnum
        public long UserDBObjectID
        {
            get
            {
                return GetConfigLong(string.Format(DBObjectEnum_XStr, "User"), 2);
            }
        }
        public long BuildingDBObjectID
        {
            get
            {
                return GetConfigLong(string.Format(DBObjectEnum_XStr, "Building"), 11);
            }
        }
        public long OrderDBObjectID
        {
            get
            {
                return GetConfigLong(string.Format(DBObjectEnum_XStr, "Order"), 1);
            }
        }

        public long T_HealthCardDBObjectID
        {
            get
            {
                return GetConfigLong(string.Format(DBObjectEnum_XStr, "Building"), 16);
            }
        }

        #endregion

        #region ContactInfoTypeIDEnum
        public long EmailContactInfoTypeID
        {
            get
            {
                return GetConfigLong(string.Format(ContactInfoTypeEnum_XStr, "Email"), 1);
            }
        }

        public long WebsiteContactInfoTypeID
        {
            get
            {
                return GetConfigLong(string.Format(ContactInfoTypeEnum_XStr, "Website"), 2);
            }
        }

        public long MobileContactInfoTypeID
        {
            get
            {
                return GetConfigLong(string.Format(ContactInfoTypeEnum_XStr, "Mobile"), 3);
            }
        }

        public long TelephoneContactInfoTypeID
        {
            get
            {
                return GetConfigLong(string.Format(ContactInfoTypeEnum_XStr, "Telephone"), 4);
            }
        }

        #endregion

        #region RoleEnum
        public long AdminRoleID
        {
            get
            {
                return GetConfigLong(string.Format(RoleEnum_XStr, "Admin"), 1);
            }
        }

        public long ModeratorRoleID
        {
            get
            {
                return GetConfigLong(string.Format(RoleEnum_XStr, "Moderator"), 2);
            }
        }

        public long EmployeeRoleID
        {
            get
            {
                return GetConfigLong(string.Format(RoleEnum_XStr, "Employee"), 3);
            }
        }
        public long SupervisorRoleID
        {
            get
            {
                return GetConfigLong(string.Format(RoleEnum_XStr, "Supervisor"), 4);
            }
        }
        public long InvestorRoleID
        {
            get
            {
                return GetConfigLong(string.Format(RoleEnum_XStr, "Investor"), 5);
            }
        }

        public long HospitalRoleID
        {
            get
            {
                return GetConfigLong(string.Format(RoleEnum_XStr, "Hospital"), 6);
            }
        }

        #endregion

        #region ActionEnum
        public long AddAnswerActionID
        {
            get
            {
                return GetConfigLong(string.Format(ActionEnum_XStr, "AddAnswer"), 1);
            }
        }

        public long AddQuestionActionID
        {
            get
            {
                return GetConfigLong(string.Format(ActionEnum_XStr, "AddQuestion"), 2);
            }
        }

        public long AnswerLikedActionID
        {
            get
            {
                return GetConfigLong(string.Format(ActionEnum_XStr, "AnswerLiked"), 3);
            }
        }

        public long AnswerAwardedActionID
        {
            get
            {
                return GetConfigLong(string.Format(ActionEnum_XStr, "AnswerAwarded"), 4);
            }
        }


        public long AnswerDislikedActionID
        {
            get
            {
                return GetConfigLong(string.Format(ActionEnum_XStr, "AnswerDisliked"), 5);
            }
        }
        #endregion

        #region FeatureParameterEnum
        public long MaxReturnedRecordsFeatureParameterID
        {
            get
            {
                return GetConfigLong(string.Format(FeatureParameterEnum_XStr, "MaxReturnedRecords"), 1);
            }
        }
        public long MaxDateFeatureParameterID
        {
            get
            {
                return GetConfigLong(string.Format(FeatureParameterEnum_XStr, "MaximumDate"), 6);
            }
        }
        public long DisplayAnswersFeatureParameterID
        {
            get
            {
                return GetConfigLong(string.Format(FeatureParameterEnum_XStr, "DisplayAnswers"), 3);
            }
        }
        public long NotificationManageFeatureParameterID
        {
            get
            {
                return GetConfigLong(string.Format(FeatureParameterEnum_XStr, "NotificationManage"), 8);
            }
        }
        public long ActivateDeactivateFeatureParameterID
        {
            get
            {
                return GetConfigLong(string.Format(FeatureParameterEnum_XStr, "MaximumNumber"), 5);
            }
        }
        public long MaxDailyLimitQuestionsParameterID
        {
            get
            {
                return GetConfigLong(string.Format(FeatureParameterEnum_XStr, "MaxDailyLimitQuestions"), 2);
            }
        }
        public long FeesPercentageParameterID
        {
            get
            {
                return GetConfigLong(string.Format(FeatureParameterEnum_XStr, "PointsFeesPercentage"), 9);
            }
        }
        #endregion

        #region MembershipFeaturesEnum

        public long SearchOpenClosedMembershipFeaturesID
        {
            get
            {
                return GetConfigLong(string.Format(MembershipFeaturesEnum_XStr, "SearchOpenClosed"), 1);
            }
        }

        public long SearchOpenMembershipFeaturesID
        {
            get
            {
                return GetConfigLong(string.Format(MembershipFeaturesEnum_XStr, "SearchOpen"), 2);
            }
        }

        public long SearchClosedMembershipFeaturesID
        {
            get
            {
                return GetConfigLong(string.Format(MembershipFeaturesEnum_XStr, "SearchClosed"), 3);
            }
        }
        public long NotificationMembershipFeaturesID
        {
            get
            {
                return GetConfigLong(string.Format(MembershipFeaturesEnum_XStr, "Notification"), 4);
            }
        }
        public long ActivateDeactivateMonthlyMembershipFeaturesID
        {
            get
            {
                return GetConfigLong(string.Format(MembershipFeaturesEnum_XStr, "ActivateDeactivateMonthly"), 5);
            }
        }
        public long FeesPercentageMembershipFeaturesID
        {
            get
            {
                return GetConfigLong(string.Format(MembershipFeaturesEnum_XStr, "PointsFeesPercentage"), 6);
            }
        }
        #endregion

        #region StatusEnum
        public long User_AccountActivateByAdminStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "User", "AccountActivateByAdmin"), 15);
            }
        }
        public long User_AccountActivateByUserStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "User", "AccountActivateByUser"), 17);
            }
        }
        public long User_AccountDeactivateByAdminStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "User", "AccountDeactivateByAdmin"), 14);
            }
        }
        public long User_AccountDeactivateByUserStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "User", "AccountDeactivateByUser"), 16);
            }
        }

        public long Order_NewStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "Order", "New"), 2);
            }
        }

        public long Order_PendingStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "Order", "Pending"), 4);
            }
        }
        public long Order_PendingForEditStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "Order", "PendingForEdit"), 5);
            }
        }
        public long Order_ApprovedStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "Order", "Approved"), 7);
            }
        }
        public long Order_RejectedStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "Order", "Rejected"), 5);
            }
        }
        public long Order_PendingForManagerStatusID
        {
            get
            {
                return GetConfigLong(string.Format(StatusEnum_X_Y_Str, "Order", "PendingForManager"), 9);
            }
        }
        #endregion

        #region MediaTypeEnum
        public long Picture_WebMediaTypeID
        {
            get
            {
                return GetConfigLong(string.Format(MediaTypeEnum_XStr, "UserIDLiecence"), 1);
            }
        }
        public long OwnerIDFile_WebMediaTypeID
        {
            get
            {
                return GetConfigLong(string.Format(MediaTypeEnum_XStr, "OwnerIDFile"), 2);
            }
        }

        public long STFileFile_WebMediaTypeID
        {
            get
            {
                return GetConfigLong(string.Format(MediaTypeEnum_XStr, "STFile"), 3);
            }
        }
        public long BuildingLayoutFile_WebMediaTypeID
        {
            get
            {
                return GetConfigLong(string.Format(MediaTypeEnum_XStr, "BuildingLayoutFile"), 4);
            }
        }
        public long OtherFile_WebMediaTypeID
        {
            get
            {
                return GetConfigLong(string.Format(MediaTypeEnum_XStr, "OtherFile"), 5);
            }
        }

        public long HRepFile_WebMediaTypeID
        {
            get
            {
                return GetConfigLong(string.Format(MediaTypeEnum_XStr, "HRepFile"), 6);
            }
        }


        #endregion

        #region PromoCodeTypeEnum
        public long PointsPromoCodeTypeID
        {
            get
            {
                return GetConfigLong(string.Format(PromoCodeTypeEnum_XStr, "Points"), 1);
            }
        }
        public long FixedDiscountPromoCodeTypeID
        {
            get
            {
                return GetConfigLong(string.Format(PromoCodeTypeEnum_XStr, "FixedDiscount"), 2);
            }
        }
        public long PercentageDiscountPromoCodeTypeID
        {
            get
            {
                return GetConfigLong(string.Format(PromoCodeTypeEnum_XStr, "PercentageDiscount"), 3);
            }
        }
        #endregion


        #region PaymentMethod
        public long PaymentMethod_PayPalID
        {
            get
            {
                return GetConfigLong(string.Format(PaymentMethodEnum_XStr, "PayPal"), 1);
            }
        }
        public long PaymentMethod_BeeID
        {
            get
            {
                return GetConfigLong(string.Format(PaymentMethodEnum_XStr, "Bee"), 4);
            }
        }
        public long PaymentMethod_CreditCardID
        {
            get
            {
                return GetConfigLong(string.Format(PaymentMethodEnum_XStr, "CreditCard"), 2);
            }
        }
        public long PaymentMethod_OfflineID
        {
            get
            {
                return GetConfigLong(string.Format(PaymentMethodEnum_XStr, "Offline"), 5);
            }
        }
        #endregion
        public double LogMinSeconds
        {
            get
            {
                return GetConfigDouble(LogMinSecondsStr, 10);
            }
        }

        public float QuestionDefaultDurationInMins
        {
            get
            {
                return (float)GetConfigDouble(QuestionDefaultDurationInMinsStr, 0.5);
            }
        }

        #region AttachmentTypeEnum
        public long AttachmentTypeEnum_AllFiles
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "AllFiles"), 1);
            }
        }
        public long AttachmentTypeEnum_Office
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "Word"), 5);
            }
        }
        public long AttachmentTypeEnum_Text
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "Text"), 2);
            }
        }
        public long AttachmentTypeEnum_PDF
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "PDF"), 3);
            }
        }
        public long AttachmentTypeEnum_Images
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "Images"), 4);
            }
        }
        public long AttachmentTypeEnum_Drawing
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "Drawing"), 6);
            }
        }
        #endregion

        #region MembershipParameterTypeEnum
        public long MembershipParameterTypeEnum_Number
        {
            get
            {
                return GetConfigLong(string.Format(MembershipParameterTypeEnum_XStr, "Number"), 1);
            }
        }
        public long MembershipParameterTypeEnum_Boolean
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "Boolean"), 2);
            }
        }
        public long MembershipParameterTypeEnum_Text
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "Text"), 3);
            }
        }
        public long MembershipParameterTypeEnum_Date
        {
            get
            {
                return GetConfigLong(string.Format(AttachmentTypeEnum_XStr, "Date"), 4);
            }
        }

        #endregion

        #region ActivityTypeEnum
        public long ActivityTypeEnum_Like
        {
            get
            {
                return GetConfigLong(string.Format(ActivityTypeEnum_XStr, "Like"), 1);
            }
        }
        public long ActivityTypeEnum_Dislike
        {
            get
            {
                return GetConfigLong(string.Format(ActivityTypeEnum_XStr, "Dislike"), 2);
            }
        }

        public long ActivityTypeEnum_Report
        {
            get
            {
                return GetConfigLong(string.Format(ActivityTypeEnum_XStr, "Report"), 3);
            }
        }

        #endregion

        public int DefaultImageHeight
        {
            get
            {
                return (int)GetConfigLong(DefaultImageHeightStr, 200);
            }
        }

        public int DefaultImageWidth
        {
            get
            {
                return (int)GetConfigLong(DefaultImageWidthStr, 400);
            }
        }
        public string DefaultEmail
        {
            get
            {
                return GetConfig(DefaultEmailStr).ToLower();
            }
        }
        public double DefaultSchemaPrice
        {
            get
            {
                return GetConfigDouble(DefaultSchemaPriceStr, 0);
            }
        }
        public string DefaultCurrency
        {
            get
            {
                return GetConfig(DefaultCurrencyStr);
            }
        }

        public long SystemUserID
        {
            get
            {
                return (long)GetConfigLong(SystemUserIDStr, 0);
            }
        }

        public string SeeMessageReplyLink
        {
            get
            {
                return GetConfig(SeeMessageReplyLinkStr);
            }
        }

        public int DefaultPageSize
        {
            get
            {
                return (int)GetConfigLong(DefaultPageSizeStr, 10);
            }
        }

        public int TopRankedPageSize
        {
            get
            {
                return (int)GetConfigLong(TopRankedPageSizeStr, 10);
            }
        }

        public string SeeQuestionLink
        {
            get
            {
                return GetConfig(SeeQuestionLinkStr);
            }
        }

        public string SupportEmail
        {
            get
            {
                return GetConfig(SupportEmailStr);
            }
        }

        public string rankSecondTabDays
        {
            get
            {
                return GetConfig(rankSecondTabDaysStr);
            }
        }

        public string rankThirdTabDays
        {
            get
            {
                return GetConfig(rankThirdTabDaysStr);
            }
        }

        public int DaysToTransferToEarning
        {
            get
            {
                return (int)GetConfigLong(DaysToTransferToEarningStr, 10);
            }
        }

        public long QuestionWithNoAnswersDays
        {
            get
            {
                return GetConfigLong(QuestionWithNoAnswersDaysStr, 1);
            }
        }

        public long QuestionExpirationPeriodInDays
        {
            get
            {
                return GetConfigLong(QuestionExpirationPeriodInDaysStr, 5);
            }
        }


        public int ActivationPromoCodeValue
        {
            get
            {
                return GetConfigInt(ActivationPromoCodeValueStr, 20);
            }
        }

        public long PromoCodeExpirationPeriodInDays
        {
            get
            {
                return GetConfigLong(PromoCodeExpirationPeriodInDaysStr, 360);
            }
        }

        public string MenuWidgetReportUrl
        {
            get
            {
                return GetConfig("MenuWidgetReportUrl", "~/Reports/InitReportFilters/{0}");
            }
        }

        public string Bee_UserName
        {
            get
            {
                return GetConfig("Bee_UserName", "eduLegend");
            }
        }
        public string Bee_UserPassword
        {
            get
            {
                return GetConfig("Bee_UserPassword", "3acd0be86de7dcccdbf91b20f94a68cea535922d");
            }
        }
        public string Bee_UserCode
        {
            get
            {
                return GetConfig("Bee_UserCode", "1971657708983828336822911572173815089583");
            }
        }
        public string Bee_EdulEgendMobile
        {
            get
            {
                return GetConfig("Bee_EdulEgendMobile", "01224088640");
            }
        }
        public string Bee_EdulEgendEmial
        {
            get
            {
                return GetConfig("Bee_EdulEgendEmial", "mnagy@sbs16.com");
            }
        }



        public void ClearConfigsCache()
        {
            CashedConfigs.Clear();
        }
    }
}