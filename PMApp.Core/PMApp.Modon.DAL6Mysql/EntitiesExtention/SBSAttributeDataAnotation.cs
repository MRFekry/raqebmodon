﻿using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace PMApp.Modon.DAL6

{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class SBSRequiredAttribute : RequiredAttribute, IClientValidatable
    {
        public SBSRequiredAttribute(string errorMessageKey)
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            this.ErrorMessage = ResourceManager.UIGetResource(errorMessageKey);
        }

        public IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules(System.Web.Mvc.ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "required"
            };
        }
    }


    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SBSCompareAttribute : System.ComponentModel.DataAnnotations.CompareAttribute, IClientValidatable
    {
        public SBSCompareAttribute(string otherProperty, string errorMessageKey)
            : base(otherProperty)
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            this.ErrorMessage = ResourceManager.UIGetResource(errorMessageKey);
        }


        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(System.Web.Mvc.ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule datecompareRule = new ModelClientValidationRule();
            datecompareRule.ErrorMessage = this.ErrorMessage;
            datecompareRule.ValidationType = "equalto";
            datecompareRule.ValidationParameters.Add("other", this.OtherProperty);

            yield return datecompareRule;
        }
    }
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public sealed class SBSEmailAddressAttribute : DataTypeAttribute, IClientValidatable
    {
        private static Regex _regex = new Regex(@"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public SBSEmailAddressAttribute(string errorMessageKey)
            : base(DataType.EmailAddress)
        {
            this.ErrorMessage = ResourceManager.UIGetResource(errorMessageKey);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(System.Web.Mvc.ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ValidationType = "email",
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName())
            };
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class SBSRegularExpressionAttribute : RegularExpressionAttribute, IClientValidatable
    {
        public SBSRegularExpressionAttribute(string pattern, string errorMessageKey)
            : base(pattern)
        {
            this.ErrorMessage = ResourceManager.UIGetResource(errorMessageKey);
        }

        public IEnumerable<System.Web.Mvc.ModelClientValidationRule> GetClientValidationRules(System.Web.Mvc.ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "regex",
                ErrorMessage = this.ErrorMessage,
            };
            rule.ValidationParameters.Add("pattern", this.Pattern);
            yield return rule;
        }
    }
}

