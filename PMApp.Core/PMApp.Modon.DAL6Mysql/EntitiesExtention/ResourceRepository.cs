using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PMApp.Modon.DAL6
{
    public class ResourceManager
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private ResourceManager(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, ResourceManager> ResourceManagerDic = new Dictionary<long, ResourceManager>();
        public static ResourceManager GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!ResourceManagerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!ResourceManagerDic.ContainsKey(applicationID))
                    {
                        ResourceManagerDic.Add(applicationID, new ResourceManager(applicationID));
                    }
                }
            }
            return ResourceManagerDic[applicationID];
        }
        public static ResourceManager GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private Dictionary<string, string> CashedResources = new Dictionary<string, string>();

        public static string AppCodeGetResource(string controlID, string applicationCode)
        {
            long applicationID = 0;
            if (!string.IsNullOrWhiteSpace(applicationCode))
                applicationID = ApplicationManager.GetApplicationIDFromAppCode(applicationCode);
            else
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();

            var resourceManager = ResourceManager.GetInstance(applicationID);
            if (resourceManager != null)
                return resourceManager.GetResource(controlID);
            return controlID;
        }

        public static string UIGetResource(string controlID, string lang = "")
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            if (applicationID == 0)
                applicationID = ApplicationManager.GetApplicationIDFromAppCode(ConfigManager.DefaultApplicationCode);
            var resourceManager = ResourceManager.GetInstance(applicationID);
            if (resourceManager != null)
                return resourceManager.GetResource(controlID, lang);
            return controlID;
        }

        //private  Common_ResourceRepository resourceRepository = new Common_ResourceRepository(CurrentContextEntities, request.ApplicationID);

        public string GetResource(string controlID)
        {
            //var currentCulture = Thread.CurrentThread.CurrentCulture;
            //var lang = (currentCulture.Name == "ar") ? "ar" : "en";
            var lang = UIHelper.GetCookieLanguage();
            return GetResource(controlID, lang);
        }

        public Dictionary<string, string> GetResourcesForAllLanguages(string controlID)
        {
            var resourcesDic = new Dictionary<string, string>();
            var langs = Enum.GetNames(typeof(Language));
            for (int i = 0; i < langs.Length; i++)
            {
                var lang = langs[i].ToLower();
                resourcesDic.Add(lang, GetResource(controlID, lang));
            }
            return resourcesDic;
        }

        public string GetResource(string resourceKey, string lang, string resourceDefaultValue = null)
        {
            try
            {
                if (resourceKey == null || resourceKey.StartsWith("(EX"))
                    return resourceKey;

                if (!string.IsNullOrWhiteSpace(lang))
                    lang = lang.ToLower();
                else
                    lang = UIHelper.GetCookieLanguage();

                var resourceUniueKey = string.Empty;

                if (CashedResources.Count == 0)
                {
                    var resourceRepository = new Common_ResourceRepository(EFUnitOfWork.NewUnitOfWork("GetResource"), applicationID);
                    var resourceList = resourceRepository.All().ToList();
                    for (int i = 0; i < resourceList.Count; i++)
                    {
                        resourceUniueKey = string.Format("{0}-{1}", resourceList[i].ResourceLang, resourceList[i].ResourceKey).ToLower();
                        if (!CashedResources.ContainsKey(resourceUniueKey))
                        {
                            CashedResources.Add(resourceUniueKey, resourceList[i].ResourceValue);
                        }
                    }
                }

                resourceUniueKey = string.Format("{0}-{1}", lang, resourceKey).ToLower();
                // 1. try get from the cashed resources
                if (CashedResources.ContainsKey(resourceUniueKey))
                    return CashedResources[resourceUniueKey];
                else
                {
                    var resourceValue = PMExtentions.GetLabel(resourceKey, "", resourceDefaultValue);
                    // 2.  create it and add to the cache
                    try
                    {
                        CashedResources.Add(resourceUniueKey, resourceValue);
                        lock (CashedResources)
                        {
                            var resourceRepository = new Common_ResourceRepository(EFUnitOfWork.NewUnitOfWork("GetResource"), applicationID);
                            // insert it into te DB with key resourceKey
                            var resource = new Common_Resource
                            {
                                ResourceKey = resourceKey,
                                ResourceValue = resourceValue,
                                ResourceLang = lang,
                                CreationDate = DateTime.Now
                            };
                            resourceRepository.Add(resource);
                            resourceRepository.Commit();
                        }
                    }
                    catch (Exception e)
                    {
                        // DoNothing
                    }
                    return resourceValue;
                }
            }
            catch (System.Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex, null, null, null);
            }
            return PMExtentions.GetLabel(resourceKey, "", resourceDefaultValue);
        }

        public void ClearResourcesCache()
        {
            CashedResources.Clear();
        }
    }
}