using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;

namespace PMApp.Modon.DAL6
{
    public class ExceptionLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private ExceptionLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, ExceptionLogger> ExceptionLoggerDic = new Dictionary<long, ExceptionLogger>();
        public static ExceptionLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!ExceptionLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!ExceptionLoggerDic.ContainsKey(applicationID))
                    {
                        ExceptionLoggerDic.Add(applicationID, new ExceptionLogger(applicationID));
                    }
                }
            }
            return ExceptionLoggerDic[applicationID];
        }
        public static ExceptionLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion


        private int QueueCount = 1;
        int count = 0;
        Common_LogExceptionRepository _repository = null;
        Common_LogExceptionRepository repository
        {
            get
            {
                lock (lockObj)
                {
                    if (_repository == null)
                        _repository = new Common_LogExceptionRepository(EFUnitOfWork.NewUnitOfWork("LogException"), applicationID);

                }
                return _repository;
            }
        }

        public long Log(Exception exception, string channel)
        {
            return Log(exception, null, string.Empty, channel);
        }

        public long Log(Exception exception)
        {
            return Log(exception, string.Empty);
        }

        public long Log(Exception exception, long? currentUserID, string sessionID, string channel)
        {
            try
            {
                var logExceptionID = Log(exception.Message, exception.StackTrace, currentUserID, sessionID, channel);
                if (exception.InnerException != null)
                {
                    Log(exception.InnerException, currentUserID, sessionID, channel);
                }
                return logExceptionID;
            }
            catch //(Exception ex)
            {
                EventLogger.GetInstance(applicationID).Log(string.Format("Exception: {0} - {1} - {2}", DateTime.Now, exception.Message, exception.StackTrace), System.Diagnostics.EventLogEntryType.Error);
            }
            return 0;
        }

        public long Log(string exceptionMessage, string exceptionStackTrace, long? currentUserID = null, string sessionID = null, string channel = null)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(sessionID))
                {
                    sessionID = SessionHelper.CurrentSessionID;
                }

                var logException = new Common_LogException();
                logException.Date = DateTime.Now;
                logException.UserID = currentUserID;
                logException.SessionID = sessionID;
                logException.Channel = channel;
                logException.ApplicationName = applicationID.ToString();
                logException.ExceptionMessage = exceptionMessage;
                logException.ExceptionStackTrace = exceptionStackTrace;

                repository.Add(logException);
                count++;
                if (count >= QueueCount)
                {
                    _repository.Commit();
                    count = 0;
                }
                var appCode = string.Format("Application {0}", applicationID);
                EmailLogger.GetInstance(applicationID).NotifyByMail(channel, LogType.Exception, appCode,
                    "ExceptionID", logException.ID.ToString(),
                    "Date", logException.Date.ToString(),
                    "UserID", currentUserID.ToString(),
                    "SessionID", sessionID,
                    "Channel", channel,
                    "ApplicationName", applicationID.ToString(),
                    "ExceptionMessage", exceptionMessage,
                    "ExceptionStackTrace", exceptionStackTrace);

                return logException.ID;
            }
            catch (Exception ex)
            {
                EventLogger.GetInstance(applicationID).Log(string.Format("Exception: {0} - {1} - {2}", DateTime.Now, ex.Message, ex.StackTrace), System.Diagnostics.EventLogEntryType.Error);
            }
            return 0;
        }
    }
}