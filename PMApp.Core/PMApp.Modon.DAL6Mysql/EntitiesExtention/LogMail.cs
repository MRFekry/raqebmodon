﻿using PMApp.Core.Helpers;
using System;

namespace PMApp.Modon.DAL6.EntitiesExtention
{
    public class LogMail
    {
        private const string mailSeprator = "<br/><hr/><br/>";
        private static MailSender mailSender = null;

        public long ApplicationID { get; private set; }
        public int Count { get; private set; }
        public string Subject { get; private set; }
        public string ToEmails { get; private set; }
        public string HtmlBody { get; private set; }

        public LogMail(long applicationID, string subject, string toEmails, string htmlBody)
        {
            this.Subject = subject;
            this.ToEmails = toEmails;
            this.HtmlBody = htmlBody;
            this.Count = 1;
            this.ApplicationID = applicationID;

            mailSender = new MailSender(ConfigManager.GetInstance(applicationID).GetConfig("SendMail.From"),
                ConfigManager.GetInstance(applicationID).GetConfig("SendMail.Password"),
                ConfigManager.GetInstance(applicationID).GetConfig("SendMail.Host"),
                ConfigManager.GetInstance(applicationID).GetConfig("SendMail.Port"),
                ConfigManager.GetInstance(applicationID).GetConfig("SendMail.EnableSsl"),
                ConfigManager.GetInstance(applicationID).GetConfig("SendMail.UseDefaultCredentials"));
        }

        public void AddToBody(string html)
        {
            this.HtmlBody += mailSeprator + html;
            this.Count++;
        }

        public void ResetCounter()
        {
            this.Count = 0;
        }

        public void SendMail()
        {
            try
            {
                this.Count = 0;
                mailSender.SendMail(this.ToEmails, this.Subject, this.HtmlBody, true);
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(this.ApplicationID).Log(ex);
            }
        }
    }
}
