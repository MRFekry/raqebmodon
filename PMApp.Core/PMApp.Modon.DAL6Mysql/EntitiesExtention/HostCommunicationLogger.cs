using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace PMApp.Modon.DAL6
{
    public class HostCommunicationLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private HostCommunicationLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, HostCommunicationLogger> HostCommunicationLoggerDic = new Dictionary<long, HostCommunicationLogger>();
        public static HostCommunicationLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!HostCommunicationLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!HostCommunicationLoggerDic.ContainsKey(applicationID))
                    {
                        HostCommunicationLoggerDic.Add(applicationID, new HostCommunicationLogger(applicationID));
                    }
                }
            }
            return HostCommunicationLoggerDic[applicationID];
        }
        public static HostCommunicationLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private bool? enableLog = null;

        public bool IsLoggingEnabled()
        {
            if (!enableLog.HasValue)
            {
                bool logCommunication = false;
                PMExtentions.TryParse(ConfigurationManager.AppSettings["LogCommunication"], out logCommunication);
                enableLog = logCommunication;
            }
            return enableLog.Value;
        }

        public void Log(string appName, long hostCommunicationId, double durationInSeconds, bool? isSuccess, bool isOnline, object request, object response, long? currentUserID, string clientIP, string sessionID, string channel, long? exceptionID, string operationName, string serviceName)
        {
            try
            {
                if ((durationInSeconds > ConfigManager.GetInstance(applicationID).LogMinSeconds && IsLoggingEnabled()) || isSuccess != true)
                {
                    try
                    {
                        var requestStr = PMExtentions.TrySerialize(request);
                        var responseStr = PMExtentions.TrySerialize(response);
                        LogCommunication(hostCommunicationId, appName, durationInSeconds, isSuccess, isOnline, requestStr, responseStr, currentUserID, clientIP, sessionID, channel, exceptionID, operationName, serviceName);
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.GetInstance(applicationID).Log(ex, currentUserID, sessionID, channel);
                    }
                }
            }
            catch (Exception exx)
            {
                ExceptionLogger.GetInstance(applicationID).Log(exx, currentUserID, sessionID, channel);
            }
        }

        private void LogCommunication(long hostCommunicationId, string appName, double durationInSeconds, bool? isSuccess, bool isOnline, string request, string response, long? currentUserID, string clientIP, string sessionID, string channel, long? exceptionID, string operationName, string serviceName)
        {
            try
            {
                if (IsLoggingEnabled())
                {
                    var serviceOperation = ServiceOperationManager.GetInstance(applicationID).GetServiceOperation(serviceName, operationName);

                    if (serviceOperation != null && serviceOperation.EnableLog)
                    {
                        Task.Run(() =>
                        {
                            var context = EFUnitOfWork.NewUnitOfWork("LogCommunication");
                            var repository = new Common_LogHostCommunicationRepository(context, applicationID);
                            if (string.IsNullOrWhiteSpace(sessionID))
                            {
                                sessionID = SessionHelper.CurrentSessionID;
                            }

                            Common_LogHostCommunication logHostCommunication = null;
                            if (hostCommunicationId != 0) // Upadte
                            {
                                logHostCommunication = repository.FirstOrDefault(p => p.ID == hostCommunicationId);
                                logHostCommunication.Date = DateTime.Now;
                                logHostCommunication.IsOnline = isOnline;
                                logHostCommunication.CreationDate = DateTime.Now;
                                //logHostCommunication.ClientIP = SessionHelper.CurrentIP;
                                logHostCommunication.UserID = currentUserID > 0 ? currentUserID : null;
                                logHostCommunication.Request = request;
                                logHostCommunication.Response = response;
                                //logHostCommunication.ServiceName = serviceName;
                                //logHostCommunication.OperationName = operationName;
                                logHostCommunication.IsSuccess = isSuccess ?? false;
                                logHostCommunication.Channel = channel;
                                logHostCommunication.ExceptionID = exceptionID;
                                logHostCommunication.SessionID = sessionID;
                                logHostCommunication.ApplicationName = appName;
                                logHostCommunication.DurationInSeconds = durationInSeconds;
                            }
                            else // Insert
                            {
                                logHostCommunication = new Common_LogHostCommunication();
                                logHostCommunication.Date = DateTime.Now;
                                logHostCommunication.UserID = currentUserID > 0 ? currentUserID : null;
                                logHostCommunication.ClientIP = clientIP;
                                logHostCommunication.Request = request;
                                logHostCommunication.Response = response;
                                logHostCommunication.ServiceName = serviceName;
                                logHostCommunication.OperationName = operationName;
                                logHostCommunication.IsSuccess = isSuccess ?? false;
                                logHostCommunication.Channel = channel;
                                logHostCommunication.ExceptionID = exceptionID;
                                logHostCommunication.SessionID = sessionID;
                                logHostCommunication.ApplicationName = appName;
                                logHostCommunication.DurationInSeconds = durationInSeconds;
                                repository.Add(logHostCommunication);
                            }
                            repository.Commit();
                            context.Dispose();
                            if (!isSuccess.HasValue || isSuccess == false)
                            {
                                EmailLogger.GetInstance(applicationID).NotifyByMail(channel, LogType.HostCommunication, appName,
                                    "HostCommunicationID", logHostCommunication.ID.ToString(),
                                    "Date", logHostCommunication.Date.ToString(),
                                    "UserID", currentUserID.ToString(),
                                    "SessionID", sessionID,
                                    "Channel", channel,
                                    "ApplicationName", appName,
                                    "ClientIP", clientIP,
                                    "ServiceName", serviceName,
                                    "OperationName", operationName,
                                    "IsSuccess", isSuccess.ToString(),
                                    "DurationInSeconds", durationInSeconds.ToString(),
                                    "Request", request,
                                    "Response", response);
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex, currentUserID, sessionID, channel);
            }
        }
    }
}