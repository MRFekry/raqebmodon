using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;

namespace PMApp.Modon.DAL6
{
    public class OperationLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private OperationLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, OperationLogger> OperationLoggerDic = new Dictionary<long, OperationLogger>();
        public static OperationLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!OperationLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!OperationLoggerDic.ContainsKey(applicationID))
                    {
                        OperationLoggerDic.Add(applicationID, new OperationLogger(applicationID));
                    }
                }
            }
            return OperationLoggerDic[applicationID];
        }
        public static OperationLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        int count = 0;
        List<Common_LogOperation> queue = new List<Common_LogOperation>();
        Common_LogOperationRepository _repository = null;
        Common_LogOperationRepository repository
        {
            get
            {
                lock (lockObj)
                {
                    if (_repository == null)
                        _repository = new Common_LogOperationRepository(EFUnitOfWork.NewUnitOfWork("OperationLogger"), applicationID);

                    if (count > 50)
                    {
                        _repository.Commit();
                        count = 0;
                    }
                    count++;
                }
                return _repository;
            }
        }


        public void Log(Channel channel, string message, params object[] parms)
        {
            try
            {
                if (parms != null && parms.Length > 0)
                {
                    message = string.Format(message, parms);
                }

                var logOperation = new Common_LogOperation();
                logOperation.CreationDate = DateTime.Now;
                logOperation.Notes = message;
                logOperation.IsActive = true;
                logOperation.Name = channel.ToString();
                repository.Add(logOperation);

                var appCode = string.Format("Application {0}", applicationID);
                EmailLogger.GetInstance(applicationID).NotifyByMail(channel, LogType.OperationLogger, appCode,
                    "LogOperationID", logOperation.ID.ToString(),
                    "Date", logOperation.CreationDate.ToString(),
                    "Name", channel.ToString(),
                    "Notes", message);

            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
        }
    }
}