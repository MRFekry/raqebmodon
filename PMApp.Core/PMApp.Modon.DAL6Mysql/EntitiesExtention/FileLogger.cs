﻿using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace PMApp.Modon.DAL6
{
    public class FileLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private FileLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, FileLogger> FileLoggerDic = new Dictionary<long, FileLogger>();
        public static FileLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!FileLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!FileLoggerDic.ContainsKey(applicationID))
                    {
                        FileLoggerDic.Add(applicationID, new FileLogger(applicationID));
                    }
                }
            }
            return FileLoggerDic[applicationID];
        }
        public static FileLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        Object simapor = new Object();

        public void Log(string messageFormat, params string[] formatParams)
        {
            Log(string.Format(messageFormat, formatParams));
        }

        public void Log(string message)
        {
            Task.Run(() =>
            {
                lock (simapor)
                {
                    using (StreamWriter w = File.AppendText("log.txt"))
                    {
                        var msg = new StringBuilder();
                        msg.AppendLine("\r\nLog Entry : ");
                        msg.AppendLine(string.Format("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString()));
                        msg.AppendLine("  :");
                        msg.AppendLine(string.Format("  :{0}", message));

                        msg.AppendLine("---------------------------------");
                        var msgStr = msg.ToString();
                        w.Write(msgStr);
                        var appCode = string.Format("Application {0}", applicationID);
                        EmailLogger.GetInstance(applicationID).NotifyByMail(Channel.Service, LogType.File, appCode, "Message", msgStr);
                    }
                }
            });
        }
    }
}
