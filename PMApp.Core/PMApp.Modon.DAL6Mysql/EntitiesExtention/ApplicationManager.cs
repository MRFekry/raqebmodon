﻿using PMApp.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace PMApp.Modon.DAL6.EntitiesExtention
{
    public static class ApplicationManager
    {
        private static Object lockOBJ = new Object();
        private static Dictionary<string, long> PMCore_ApplicationDic = new Dictionary<string, long>();

        public static string GetApplicationFromRouteData()
        {
            if (HttpContext.Current != null)
            {
                var routeTable = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));
                if (routeTable != null && routeTable.Values.ContainsKey("application"))
                {
                    return routeTable.Values["application"].ToString();
                }
            }
            return ConfigManager.DefaultApplicationCode;
        }


        private static bool ReloadApplications = false;

        public const string EMPTY_APPLICATIONID_ERROR = "EMPTY APPLICATIONID ERROR";

        public static long GetApplicationIDFromAppCode(string appName)
        {
            if (!string.IsNullOrWhiteSpace(appName))
            {
                if (ReloadApplications)
                {
                    PMCore_ApplicationDic.Clear();
                    ReloadApplications = false;
                }
                if (PMCore_ApplicationDic.Count == 0)
                {
                    lock (lockOBJ)
                    {
                        var context = EFUnitOfWork.NewUnitOfWork("ApplicationID");
                        var applicationRepository = new PMCore_ApplicationRepository(context, 0);
                        var appList = applicationRepository.Where(p => p.IsActive == true && p.Enable == true && p.IsDeleted == false).ToList();
                        for (int i = 0; i < appList.Count; i++)
                        {
                            var apCode = appList[i].CodeName.ToLower();
                            if (!PMCore_ApplicationDic.ContainsKey(apCode))
                                PMCore_ApplicationDic.Add(apCode, appList[i].ID);
                        }
                        context.Dispose();
                    }
                }
                appName = appName.ToLower();
                if (PMCore_ApplicationDic.ContainsKey(appName))
                    return PMCore_ApplicationDic[appName];

                var context1 = EFUnitOfWork.NewUnitOfWork("ApplicationID1");
                var applicationRepository1 = new PMCore_ApplicationRepository(context1, 0);
                if (PMCore_ApplicationDic.Count != applicationRepository1.Where(p => p.IsActive == true && p.Enable == true && p.IsDeleted == false).Count())
                {
                    ReloadApplications = true;
                }
                context1.Dispose();
            }
            return 0;
        }

        public static long GetApplicationIDFromAuthToken(string authToken)
        {
            var appCode = SecurityHelper.GetApplicationCodeFromAuthenticationToken(authToken);
            return GetApplicationIDFromAppCode(appCode);
        }

        public static long GetApplicationIDFromRouteData()
        {
            var appCode = GetApplicationFromRouteData();
            return GetApplicationIDFromAppCode(appCode);
        }

        public static string GetApplicationFromApplicationID(long applicationID)
        {
            if (applicationID != 0)
            {
                if (PMCore_ApplicationDic.Count == 0)
                {
                    lock (lockOBJ)
                    {
                        var context = EFUnitOfWork.NewUnitOfWork("ApplicationID");
                        var appList = new PMCore_ApplicationRepository(context, 0).Where(p => p.IsActive == true && p.Enable == true && p.IsDeleted == false).ToList();
                        for (int i = 0; i < appList.Count; i++)
                        {
                            var app = appList[i].CodeName.ToLower();
                            if (!PMCore_ApplicationDic.ContainsKey(app))
                                PMCore_ApplicationDic.Add(app, appList[i].ID);
                        }
                        context.Dispose();
                    }
                }
                foreach (var item in PMCore_ApplicationDic)
                {
                    if (item.Value == applicationID)
                        return item.Key;
                }
            }
            return string.Empty;
        }
    }
}