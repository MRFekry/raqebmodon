﻿using Newtonsoft.Json.Linq;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web;

namespace PMApp.Modon.DAL6
{
    public class DistanceManager
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private DistanceManager(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, DistanceManager> DistanceManagerDic = new Dictionary<long, DistanceManager>();
        public static DistanceManager GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!DistanceManagerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!DistanceManagerDic.ContainsKey(applicationID))
                    {
                        DistanceManagerDic.Add(applicationID, new DistanceManager(applicationID));
                    }
                }
            }
            return DistanceManagerDic[applicationID];
        }
        public static DistanceManager GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private const string webAddress = "https://route.cit.api.here.com/routing/7.2/calculateroute.json?";

        private const string webAddressV2 = "https://maps.googleapis.com/maps/api/distancematrix/json?";
        private const string featuresStringV2 = "units=meter&key={0}&";


        private const string webAddressForPlaceInfo = "https://places.demo.api.here.com/places/v1/discover/search?";

        private const string webAddressForImage = "https://image.maps.cit.api.here.com/mia/1.6/route?";

        private const string featuresStringForPlaceInfo = "q=street&app_id={0}&app_code={1}";

        private const string featuresString = "mode=fastest;car&departure=now&app_id={0}&app_code={1}";

        private const string featuresStringForImage = "&mode=fastest;car;traffic:disabled;motorway:-2&lc0=00ff00&&lw0=6&&&lw1=3&w=500&app_id={0}&app_code={1}";

        public HereMapRouteSummeryRecord GetRouteSummeryBetweenTwoLocations(double lat1, double lon1, double lat2, double lon2, DistanceUnit distanceUnit)
        {
            return GetRouteSummeryRecord(new List<GeoPoint>() { new GeoPoint((decimal)lat1, (decimal)lon1), new GeoPoint((decimal)lat2, (decimal)lon2) });
        }

        public double DistanceBetweenTwoLocations(double lat1, double lon1, double lat2, double lon2, DistanceUnit distanceUnit)
        {
            var routeSummeryRecord = GetRouteSummeryRecord(new List<GeoPoint>() { new GeoPoint((decimal)lat1, (decimal)lon1), new GeoPoint((decimal)lat2, (decimal)lon2) });
            switch (distanceUnit)
            {
                case DistanceUnit.Mile:
                    return (double)routeSummeryRecord.DistanceInKm * 0.621371;
                case DistanceUnit.KM:
                    return (double)routeSummeryRecord.DistanceInKm;
                case DistanceUnit.Meter:
                    return (double)routeSummeryRecord.DistanceInKm * 1000;
            }
            return (double)routeSummeryRecord.DistanceInKm;

            //double theta = lon1 - lon2;
            //double dist = Math.Sin(DEG2RAD(lat1)) * Math.Sin(DEG2RAD(lat2)) + Math.Cos(DEG2RAD(lat1)) * Math.Cos(DEG2RAD(lat2)) * Math.Cos(DEG2RAD(theta));
            //dist = Math.Acos(dist);
            //dist = RAD2DEG(dist);
            //dist = dist * 60 * 1.1515;
            //switch (distanceUnit)
            //{
            //    case DistanceUnit.K:
            //        dist = dist * 1.609344;
            //        break;
            //    case DistanceUnit.N:
            //        dist = dist * 0.8684;
            //        break;
            //    case DistanceUnit.Meter:
            //        dist = dist * 1.609344 * 1000;
            //        break;
            //}
            //return Math.Abs(dist);
        }

        //::  This function converts decimal degrees to radians   
        private static double DEG2RAD(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //::  This function converts radians to decimal degrees  
        private static double RAD2DEG(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        public enum DistanceUnit
        {
            Mile,// is statute miles (default)
            KM,//' is kilometers
            Meter// is meters
        }


        public string GetRouteImageURL(List<GeoPoint> geoPointList)
        {
            String urlWithEncode = string.Empty;
            try
            {
                if (geoPointList.Count > 1)
                {
                    var urlParamsStr = string.Empty;
                    var urlParamsStr1 = string.Empty;
                    var comma = HttpUtility.UrlEncode(",", Encoding.UTF8);
                    for (int i = 0; i < geoPointList.Count; i++)
                    {
                        urlParamsStr += string.Format("{0}{1}{2}{3}", geoPointList[i].Lat, comma, geoPointList[i].Long, comma);
                        // urlParamsStr1 += string.Format("{0}{1}{2}{3}", geoPointList[i].Lat, comma, geoPointList[i].Long, comma);
                    }
                    urlParamsStr1 += string.Format("{0}{1}{2}{3}", geoPointList[0].Lat, comma, geoPointList[0].Long, comma);
                    urlParamsStr1 += string.Format("{0}{1}{2}{3}", geoPointList[geoPointList.Count - 1].Lat, comma, geoPointList[geoPointList.Count - 1].Long, comma);
                    urlWithEncode = webAddressForImage + "r0=" + urlParamsStr.ToString() + "&m0=" + urlParamsStr1.ToString() + string.Format(featuresStringForImage, ConfigManager.GetInstance(applicationID).HereMap_appid, ConfigManager.GetInstance(applicationID).HereMap_appcode);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return urlWithEncode;
        }



        //public GoogleMapRouteSummeryRecord GetGoogleMapRouteSummeryRecord(List<GeoPoint> geoPointList)
        //{
        //    String urlWithEncode = string.Empty;
        //    int validPointsCount = 0;
        //    try
        //    {
        //        if (geoPointList.Count > 1)
        //        {
        //            var urlParamsStr = string.Empty;
        //            var urlParamsStr1 = string.Empty;
        //            var comma = ",";// HttpUtility.UrlEncode(",", Encoding.UTF8);
        //            var sperator = "|";// HttpUtility.UrlEncode("|", Encoding.UTF8);
        //            for (int i = 0; i < geoPointList.Count; i++)
        //            {
        //                urlParamsStr += string.Format("{0}{1}{2}{3}", sperator, geoPointList[i].Lat, comma, geoPointList[i].Long);
        //                validPointsCount++;
        //            }
        //            urlParamsStr1 += string.Format("{0}{1}{2}", geoPointList[0].Lat, comma, geoPointList[0].Long);
        //            var featuresStringV2WithKey = string.Format(featuresStringV2, ConfigManager.GoogleMap_key);
        //            urlWithEncode = webAddressV2 + featuresStringV2WithKey + "origins=" + urlParamsStr1.ToString() + "&destinations=" + urlParamsStr.ToString().TrimStart("|".ToCharArray());// + string.Format(featuresStringForImage, ConfigManager.HereMap_appid, ConfigManager.HereMap_appcode);

        //        }
        //        if (validPointsCount > 1)
        //        {
        //            GoogleMapRouteSummeryRecord googleMapRouteSummeryRecord;
        //            var startDateTime = DateTime.Now;

        //            using (var client = new WebClient())
        //            {
        //                byte[] responseBytes = client.DownloadData(urlWithEncode);
        //                String response = Encoding.UTF8.GetString(responseBytes);
        //                googleMapRouteSummeryRecord = CreateGoogleMapRouteSummeryRecord(response);

        //                PerformanceLogger.ConditionalLog(Channel.Service, startDateTime, DateTime.Now, "DistanceManager GetGoogleMapRouteSummeryRecord {0}", urlWithEncode);
        //                return googleMapRouteSummeryRecord;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogger.GetInstance(applicationID).Log(ex);
        //    }
        //    return new GoogleMapRouteSummeryRecord();
        //}

        public HereMapRouteSummeryRecord GetRouteSummeryRecord(List<GeoPoint> geoPointList)
        {
            if (geoPointList.Count > 1)
            {
                string urlWithEncode = string.Empty;
                try
                {
                    var urlParamsStr = string.Empty;
                    int validPointsCount = 0;
                    var comma = HttpUtility.UrlEncode(",", Encoding.UTF8);
                    for (int i = 0; i < geoPointList.Count; i++)
                    {
                        if (geoPointList[i].Lat != 0 && geoPointList[i].Long != 0)
                        {
                            urlParamsStr += string.Format("waypoint{0}={1}{2}{3}&", i, geoPointList[i].Lat, comma, geoPointList[i].Long);
                            validPointsCount++;
                        }
                    }
                    if (validPointsCount > 1)
                    {
                        var startDateTime = DateTime.Now;
                        HereMapRouteSummeryRecord hereMapRouteSummeryRecord = null;
                        using (var client = new WebClient())
                        {
                            urlWithEncode = webAddress + urlParamsStr.ToString() + string.Format(featuresString, ConfigManager.GetInstance(applicationID).HereMap_appid, ConfigManager.GetInstance(applicationID).HereMap_appcode);
                            byte[] responseBytes = client.DownloadData(urlWithEncode);
                            String response = Encoding.UTF8.GetString(responseBytes);
                            hereMapRouteSummeryRecord = CreateRouteSummeryRecord(response);
                        }
                        PerformanceLogger.GetInstance(applicationID).ConditionalLog(Channel.Service, startDateTime, DateTime.Now, "DistanceManager GetRouteSummeryRecord {0}", urlWithEncode);
                        return hereMapRouteSummeryRecord;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.GetInstance(applicationID).Log(ex.Message + " - " + urlWithEncode, ex.StackTrace);
                }
            }
            return new HereMapRouteSummeryRecord();
        }


        public HereMapPlaceSummeryRecord GetPlaceSummeryRecord(GeoPoint geoPoint)
        {
            HereMapPlaceSummeryRecord hereMapPlaceSummeryRecord = null;
            try
            {
                if (geoPoint != null && geoPoint.Lat != 0 && geoPoint.Long != 0)
                {
                    var urlParamsStr = string.Empty;
                    var comma = HttpUtility.UrlEncode(",", Encoding.UTF8);

                    urlParamsStr += string.Format("at={0}{1}{2}&", geoPoint.Lat, comma, geoPoint.Long);

                    string urlWithEncode = "";

                    var startDateTime = DateTime.Now;
                    using (var client = new WebClient())
                    {
                        urlWithEncode = webAddressForPlaceInfo + urlParamsStr.ToString() + string.Format(featuresStringForPlaceInfo, ConfigManager.GetInstance(applicationID).HereMap_appid, ConfigManager.GetInstance(applicationID).HereMap_appcode);
                        var responseBytes = client.DownloadData(urlWithEncode);
                        var response = Encoding.UTF8.GetString(responseBytes);
                        hereMapPlaceSummeryRecord = CreatePlaceSummeryRecord(response);
                    }
                    PerformanceLogger.GetInstance(applicationID).ConditionalLog(Channel.Service, startDateTime, DateTime.Now, "DistanceManager GetPlaceSummeryRecord {0}", urlWithEncode);
                    return hereMapPlaceSummeryRecord;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return new HereMapPlaceSummeryRecord();
        }


        private HereMapPlaceSummeryRecord CreatePlaceSummeryRecord(string jsonRes)
        {
            var hereMapPlaceSummeryRecord = new HereMapPlaceSummeryRecord();
            try
            {
                dynamic d = JObject.Parse(jsonRes);
                if (d != null && d.search != null && d.search.context != null && d.search.context.location != null && d.search.context.location.address != null)
                {
                    hereMapPlaceSummeryRecord.Text = d.search.context.location.address.text.ToString();
                    hereMapPlaceSummeryRecord.District = d.search.context.location.address.district.ToString();
                    hereMapPlaceSummeryRecord.City = d.search.context.location.address.city.ToString();
                    hereMapPlaceSummeryRecord.County = d.search.context.location.address.county.ToString();
                    hereMapPlaceSummeryRecord.Country = d.search.context.location.address.country.ToString();
                    hereMapPlaceSummeryRecord.CountryCode = d.search.context.location.address.countryCode.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return hereMapPlaceSummeryRecord;
        }

        private HereMapRouteSummeryRecord CreateRouteSummeryRecord(string jsonRes)
        {
            var hereMapRouteSummeryRecord = new HereMapRouteSummeryRecord();
            try
            {
                dynamic d = JObject.Parse(jsonRes);
                if (d != null && d.response != null && d.response.route != null && d.response.route.Count > 0)
                {
                    var distance = decimal.Parse(d.response.route[0].summary.distance.ToString());
                    hereMapRouteSummeryRecord.DistanceInKm = Math.Round(distance / (decimal)1000, 2);
                    var trafficTime = decimal.Parse(d.response.route[0].summary.trafficTime.ToString());
                    hereMapRouteSummeryRecord.TrafficTimeInMin = Math.Round(trafficTime / (decimal)60, 2);
                    var baseTime = decimal.Parse(d.response.route[0].summary.baseTime.ToString());
                    hereMapRouteSummeryRecord.BaseTimeInMin = Math.Round(baseTime / (decimal)60, 2);
                    hereMapRouteSummeryRecord.TextSummary = d.response.route[0].summary.text;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return hereMapRouteSummeryRecord;
        }

        //private GoogleMapRouteSummeryRecord CreateGoogleMapRouteSummeryRecord(string jsonRes)
        //{
        //    var googleMapRouteSummeryRecord = new GoogleMapRouteSummeryRecord();
        //    try
        //    {
        //        var arr = JObject.Parse(jsonRes);
        //        var arrDistance = arr["rows"][0]["elements"].Max(i => i["distance"]["value"]);
        //        var arrduration = arr["rows"][0]["elements"].Max(i => i["duration"]["value"]);
        //        var distance = decimal.Parse(arrDistance.ToString());
        //        googleMapRouteSummeryRecord.DistanceInKm = Math.Round(distance / (decimal)1000, 2);
        //        var trafficTime = decimal.Parse(arrduration.ToString());
        //        googleMapRouteSummeryRecord.TrafficTimeInMin = Math.Round(trafficTime / (decimal)60, 2);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogger.GetInstance(applicationID).Log(ex);
        //    }
        //    return googleMapRouteSummeryRecord;
        //}


        public string GetHereMapPlaceSummeryRecord(decimal latitude, decimal longitude)
        {
            //get the from place name using here map 
            if (latitude != 0 && longitude != 0)
            {
                DistanceManager.GeoPoint Gpoint = new DistanceManager.GeoPoint();
                Gpoint.Lat = (decimal)latitude;
                Gpoint.Long = (decimal)longitude;
                var HereMapPlaceSummeryRecord = DistanceManager.GetInstance(applicationID).GetPlaceSummeryRecord(Gpoint);
                return HereMapPlaceSummeryRecord.Text.Replace("<br/>", "-");
            }
            return string.Empty;
        }
        public class GeoPoint
        {
            public GeoPoint() { }

            public GeoPoint(decimal Lat, decimal Long)
            {
                this.Lat = Lat;
                this.Long = Long;
            }

            public decimal Lat { get; set; }
            public decimal Long { get; set; }
        }


        public class HereMapPlaceSummeryRecord
        {
            public HereMapPlaceSummeryRecord()
            {
                Text = District = City = County = Country = CountryCode = string.Empty;
            }

            public string Text { get; set; }
            public string District { get; set; }
            public string City { get; set; }
            public string County { get; set; }
            public string Country { get; set; }
            public string CountryCode { get; set; }

        }

        public class HereMapRouteSummeryRecord
        {
            public HereMapRouteSummeryRecord()
            {
                TextSummary = TollStations = string.Empty;
            }

            public decimal DistanceInKm { get; set; }
            public decimal TrafficTimeInMin { get; set; }
            public decimal BaseTimeInMin { get; set; }
            public string TextSummary { get; set; }
            public string TollStations { get; set; }

        }

        public class GoogleMapRouteSummeryRecord
        {
            public GoogleMapRouteSummeryRecord()
            {
                TextSummary = TollStations = string.Empty;
            }
            public decimal DistanceInKm { get; set; }
            public decimal TrafficTimeInMin { get; set; }
            public decimal BaseTimeInMin { get; set; }
            public string TextSummary { get; set; }
            public string TollStations { get; set; }
        }


    }
}
