﻿using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMApp.Modon.DAL6
{
    public class EmailLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private EmailLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, EmailLogger> EmailLoggerDic = new Dictionary<long, EmailLogger>();
        public static EmailLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!EmailLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!EmailLoggerDic.ContainsKey(applicationID))
                    {
                        EmailLoggerDic.Add(applicationID, new EmailLogger(applicationID));
                    }
                }
            }
            return EmailLoggerDic[applicationID];
        }
        public static EmailLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private int QueueCount = 50;
        private string mailSubjectTemplate = "{0} - {1} - {2}";
        private const string lineBreak = "<br/>";
        private const string valStringFormate = "<p><strong>{0}:</strong><br/>{1}</p>";
        const string all = "*";

        List<Common_LogEmails> logEmailsList = null;
        List<LogMail> LogMailQueue = new List<LogMail>();


        private List<Common_LogEmails> LogEmailsList
        {
            get
            {
                if (logEmailsList == null)
                {
                    try
                    {
                        var context = EFUnitOfWork.NewUnitOfWork("EmailLogger.Inizialize");
                        logEmailsList = new Common_LogEmailsRepository(context, applicationID).All().ToList();
                        context.Dispose();
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.GetInstance(applicationID).Log(ex);
                    }
                }
                return logEmailsList;
            }
        }

        public string StrigifyParams(params string[] vals)
        {
            var htmlContent = string.Empty;

            if (vals.Length % 2 == 0)
            {
                var length = vals.Length / 2;
                var val1 = "";
                var val2 = "";
                for (int i = 0; i < vals.Length; i++)
                {
                    val1 = vals[i];
                    val2 = vals[++i];
                    htmlContent += string.Format(valStringFormate, (val1 == null) ? "" : val1, (val2 == null) ? "" : val2.ReplaceLineBreaks(lineBreak));
                }
            }
            else
            {
                throw new Exception("Invalid Mail Parameters");
            }

            return htmlContent;
        }

        public void NotifyByMail(Channel channel, LogType logType, string appName, params string[] vals)
        {
            NotifyByMail(channel.ToString(), logType, appName, vals);
        }

        public void NotifyByMail(string channelStr, LogType logType, string appName, params string[] vals)
        {
            if (LogEmailsList != null)
            {
                var logTypeStr = logType.ToString();
                var logEmails = LogEmailsList.Where(p => (p.Channel == channelStr || p.Channel == all)
                                                               && (p.LogType == logTypeStr || p.LogType == all)).ToList();


                Task.Run(() =>
                {
                    try
                    {
                        for (int i = 0; i < logEmails.Count(); i++)
                        {
                            var mailSubject = string.Format(mailSubjectTemplate, appName + "-" + Environment.MachineName, channelStr, logTypeStr);
                            var mailBody = StrigifyParams(vals);

                            var logEmail = LogMailQueue.FirstOrDefault(p => p.Subject == mailSubject && p.ToEmails == logEmails[i].Emails);
                            if (logEmail == null)
                            {
                                logEmail = new LogMail(applicationID, mailSubject, logEmails[i].Emails, mailBody);
                                LogMailQueue.Add(logEmail);
                            }
                            else
                                logEmail.AddToBody(mailBody);

                            if (logEmail.Count > QueueCount)
                            {
                                logEmail.SendMail();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.GetInstance(applicationID).Log(ex);
                    }
                });
            }
        }

        public static bool TrySendMail(long applicationID, string to, string subject, string body, bool isBodyHtml = true)
        {
            try
            {
                var mailSender = new MailSender(ConfigManager.GetInstance(applicationID).GetConfig("SendMail.From"),
                    ConfigManager.GetInstance(applicationID).GetConfig("SendMail.Password"),
                    ConfigManager.GetInstance(applicationID).GetConfig("SendMail.Host"),
                    ConfigManager.GetInstance(applicationID).GetConfig("SendMail.Port"),
                    ConfigManager.GetInstance(applicationID).GetConfig("SendMail.EnableSsl"),
                    ConfigManager.GetInstance(applicationID).GetConfig("SendMail.UseDefaultCredentials"));

                return mailSender.SendMail(to, subject, body, isBodyHtml);
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
                return false;
            }
        }
    }
}
