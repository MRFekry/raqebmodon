﻿namespace PMApp.Modon.DAL6
{
    public interface IBaseEntity<T>
    {
        T ID { get; set; }
    }
}
