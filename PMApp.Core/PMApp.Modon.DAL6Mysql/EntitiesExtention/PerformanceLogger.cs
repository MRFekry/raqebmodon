using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace PMApp.Modon.DAL6
{
    public class PerformanceLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private PerformanceLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, PerformanceLogger> PerformanceLoggerDic = new Dictionary<long, PerformanceLogger>();
        public static PerformanceLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!PerformanceLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!PerformanceLoggerDic.ContainsKey(applicationID))
                    {
                        PerformanceLoggerDic.Add(applicationID, new PerformanceLogger(applicationID));
                    }
                }
            }
            return PerformanceLoggerDic[applicationID];
        }
        public static PerformanceLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        List<Common_LogPerformance> queue = new List<Common_LogPerformance>();
        int count = 0;
        Common_LogPerformanceRepository _repository = null;
        Common_LogPerformanceRepository repository
        {
            get
            {
                lock (lockObj)
                {
                    if (_repository == null)
                        _repository = new Common_LogPerformanceRepository(EFUnitOfWork.NewUnitOfWork("LogPerformance"), applicationID);

                    if (count > 50)
                    {
                        _repository.Commit();
                        count = 0;
                    }
                    count++;
                }
                return _repository;
            }
        }

        private bool IsLoggingPerformanceEnabled()
        {
            bool logCommunication = false;
            PMExtentions.TryParse(ConfigurationManager.AppSettings["LogPerformanceCommunication"], out logCommunication);
            return logCommunication;
        }

        public void ConditionalLog(Channel channel, DateTime startDate, DateTime endDate, string performanceMessage, params object[] parms)
        {
            try
            {
                if (IsLoggingPerformanceEnabled())
                {
                    var duration = (decimal)endDate.Subtract(startDate).TotalSeconds;

                    if (duration >= ConfigManager.GetInstance(applicationID).PerformanceCounterMinimumTimeInSeconds)
                    {
                        if (parms != null && parms.Length > 0)
                        {
                            performanceMessage = string.Format(performanceMessage, parms);
                        }

                        var logPerformance = new Common_LogPerformance();
                        logPerformance.CreationDate = DateTime.Now;
                        logPerformance.StartTime = startDate;
                        logPerformance.EndTime = endDate;
                        logPerformance.Duration = duration;
                        logPerformance.PerformanceMessage = performanceMessage;
                        logPerformance.ApplicationName = channel.ToString();
                        logPerformance.SessionID = SessionHelper.CurrentSessionID;
                        repository.Add(logPerformance);

                        var appCode = string.Format("Application {0}", applicationID);
                        EmailLogger.GetInstance(applicationID).NotifyByMail(channel, LogType.PerformanceLogger, appCode,
                            "LogPerformanceID", logPerformance.ID.ToString(),
                            "CreationDate", logPerformance.CreationDate.ToString(),
                            "StartTime", logPerformance.StartTime.ToString(),
                            "EndTime", logPerformance.EndTime.ToString(),
                            "Duration", logPerformance.Duration.ToString(),
                            "PerformanceMessage", logPerformance.PerformanceMessage.ToString(),
                            "ApplicationName", logPerformance.ApplicationName.ToString(),
                            "SessionID", logPerformance.SessionID.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
        }

        public long LogStartTime(Channel channel, string performanceMessage, params object[] parms)
        {
            try
            {
                if (IsLoggingPerformanceEnabled())
                {
                    if (parms != null && parms.Length > 0)
                        performanceMessage = string.Format(performanceMessage, parms);

                    var logPerformance = new Common_LogPerformance();
                    logPerformance.CreationDate = DateTime.Now;
                    logPerformance.StartTime = DateTime.Now;
                    logPerformance.PerformanceMessage = performanceMessage;
                    logPerformance.ApplicationName = channel.ToString();
                    logPerformance.SessionID = SessionHelper.CurrentSessionID;
                    logPerformance.ID = (int)DateTime.Now.ToOADate();
                    queue.Add(logPerformance);
                    return logPerformance.ID;
                }
                return 0;
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return 0;
        }

        public long LogEndTime(long id)
        {
            try
            {
                Task.Run(() =>
                {
                    if (IsLoggingPerformanceEnabled())
                    {
                        var logPerformance = queue.FirstOrDefault(p => p.ID == id);
                        if (logPerformance != null)
                        {
                            logPerformance.EndTime = DateTime.Now;
                            logPerformance.Duration = (decimal)logPerformance.EndTime.Value.Subtract((DateTime)logPerformance.StartTime).TotalSeconds;
                            var channel = (Channel)Enum.Parse(typeof(Channel), logPerformance.ApplicationName);
                            ConditionalLog(channel, logPerformance.StartTime.Value, logPerformance.EndTime.Value, logPerformance.PerformanceMessage);
                            queue.Remove(logPerformance);
                        }
                    }
                });
                return 0;
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return 0;
        }
    }
}