﻿using System;
using System.Collections.Generic;
using System.Linq;
using PMApp.Core.Helpers;

namespace PMApp.Modon.DAL6
{
    public partial class UserRepository
    {
        private const int maxCacheCount = 10000;
        private const string defaultUserFullNameStr = "User {0}";
        private static Common_UserRepository common_UserRepository = null;
        private static List<Common_User> Common_UserList = new List<Common_User>();
        private static Object GetUserLockObj = new Object();

        public static void RemoveUserFromCache(long id)
        {
            var usersList = Common_UserList.Where(p => p.ID == id).ToList();
            if (usersList != null)
            {
                lock (GetUserLockObj)
                {
                    for (int i = 0; i < usersList.Count; i++)
                    {
                        Common_UserList.Remove(usersList[i]);
                    }
                }
                common_UserRepository = null;
            }
        }

        public static Common_User GetUser(long id, long applicationID)
        {
            if (id > 0 && applicationID > 0)
            {
                var user = Common_UserList.FirstOrDefault(p => p.ID == id);
                if (user == null)
                {
                    lock (GetUserLockObj)
                    {
                        if (common_UserRepository == null)
                        {
                            common_UserRepository = new Common_UserRepository(EFUnitOfWork.NewUnitOfWork("UserRepository"), applicationID);
                        }
                        var nuser = common_UserRepository.FirstOrDefault(p => p.ID == id);
                        if (nuser != null)
                        {
                            if (Common_UserList.Count > maxCacheCount)
                                Common_UserList.Clear();
                            Common_UserList.Add(nuser);
                            user = nuser;
                        }
                    }
                }
                return user;
            }
            return null;
        }

        public static string GetUserFullName(long id, long applicationID)
        {
            var user = GetUser(id, applicationID);
            if (user != null && !string.IsNullOrWhiteSpace(user.FullName))
                return user.FullName;
            return string.Format(defaultUserFullNameStr, id);
        }

        public static string GetUserEmail(long id, long applicationID)
        {
            var user = GetUser(id, applicationID);
            if (user != null && !string.IsNullOrWhiteSpace(user.Email))
                return user.Email;
            return string.Empty;
        }

        public static string GetUserImageURL(long id, long applicationID)
        {
            //var user = GetUser(id, applicationID);
            //if (user != null && !string.IsNullOrWhiteSpace(user.ImageUrl))
            //    return user.ImageUrl;
            //return ConfigManager.GetInstance(applicationID).UserDefaultPictureUrl;
            return "";
        }


        public static long GetUserRoleID(long id, long applicationID)
        {
            var user = GetUser(id, applicationID);
            if (user != null)
                return user.RoleID;
            return 0;
        }

        public static long GetUserBalance(long userID, long applicationID, bool isTotal = true)
        {
            var context = EFUnitOfWork.NewUnitOfWork("UserRepository");
            var userExtension = new UserWalletRepository(context, applicationID)
                .Where(p => p.UserID == userID);

            long balance = 0;
           
                if (userExtension != null && userExtension.Count()>0)
                {
                
                        balance = userExtension.Sum(p=>p.Points);
                }
            context.Dispose();
            return balance;
        }
    }
}
