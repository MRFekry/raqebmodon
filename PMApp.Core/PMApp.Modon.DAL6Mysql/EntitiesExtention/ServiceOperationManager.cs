using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PMApp.Modon.DAL6
{
    public class ServiceOperationManager
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private ServiceOperationManager(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, ServiceOperationManager> ServiceOperationManagerDic = new Dictionary<long, ServiceOperationManager>();
        public static ServiceOperationManager GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!ServiceOperationManagerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!ServiceOperationManagerDic.ContainsKey(applicationID))
                    {
                        ServiceOperationManagerDic.Add(applicationID, new ServiceOperationManager(applicationID));
                    }
                }
            }
            return ServiceOperationManagerDic[applicationID];
        }
        public static ServiceOperationManager GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private Dictionary<string, PMCore_ServiceOperation> serviceOperationDictionary = null;

        public bool ReloadService { get; set; }

        public Dictionary<string, PMCore_ServiceOperation> ServiceOperationDictionary
        {
            get
            {
                try
                {
                    if (ReloadService)
                    {
                        serviceOperationDictionary = null;
                        ReloadService = false;
                    }

                    if (serviceOperationDictionary == null)
                    {
                        serviceOperationDictionary = new Dictionary<string, PMCore_ServiceOperation>();
                    }
                    if (serviceOperationDictionary.Count == 0)
                    {
                        var unitOfWork = EFUnitOfWork.NewUnitOfWork("ServiceOperationDictionary");
                        var pmCoreServiceOperationRepository = new PMCore_ServiceOperationRepository(unitOfWork, applicationID);
                        var serviceOperationList = pmCoreServiceOperationRepository.All().ToList();
                        unitOfWork.Dispose();
                        for (int i = 0; i < serviceOperationList.Count; i++)
                        {
                            string keyName = string.Format("{0}___{1}", string.Empty, serviceOperationList[i].OperationName);
                            if (keyName != null && !serviceOperationDictionary.ContainsKey(keyName) && serviceOperationList[i] != null)
                                serviceOperationDictionary.Add(keyName, serviceOperationList[i]);
                        }
                        //if (unitOfWork.Context.Connection.State != System.Data.ConnectionState.Closed)
                        //    unitOfWork.Context.Connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.GetInstance(applicationID).Log(ex);
                }
                return serviceOperationDictionary;
            }
        }

        public PMCore_ServiceOperation GetServiceOperation(long serviceOperationID)
        {
            PMCore_ServiceOperation serviceOperation = null;
            try
            {
                foreach (var operation in ServiceOperationDictionary)
                {
                    if (operation.Value.ID == serviceOperationID)
                    {
                        serviceOperation = operation.Value;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
            }
            return serviceOperation;
        }

        public PMCore_ServiceOperation GetServiceOperation(string serviceName, string operationName)
        {
            PMCore_ServiceOperation serviceOperation = null;
            try
            {
                string keyName = string.Format("{0}___{1}", string.Empty, operationName);
                if (ServiceOperationDictionary.ContainsKey(keyName))
                    serviceOperation = ServiceOperationDictionary[keyName];
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex);
                ExceptionLogger.GetInstance(applicationID).Log(string.Format("Service Operation not exists '{0}.{1}", serviceName, operationName), ex.StackTrace);
            }
            return serviceOperation;
        }

        public List<PMCore_ServiceOperation> GetRelatedServiceOperations(string serviceName, string operationName)
        {
            var serviceOperation = GetServiceOperation(serviceName, operationName);
            List<PMCore_ServiceOperation> result = null;
            if (!string.IsNullOrWhiteSpace(serviceOperation.RelatedServiceOperationsIDs))
            {
                var ids = serviceOperation.RelatedServiceOperationsIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var IDs = new List<long>();
                for (int i = 0; i < ids.Length; i++)
                    IDs.Add(long.Parse(ids[i]));
                result = ServiceOperationDictionary.Where(p => IDs.Contains(p.Value.ID)).Select(p => p.Value).ToList();
            }
            return result;
        }

        private List<PMCore_ServiceOperationRoleAccess> PMCore_ServiceOperationRoleAccessList = null;

        public bool ServiceOperationIsAllowed(long userRoleID, string operationName)
        {
            var operation = GetServiceOperation(string.Empty, operationName);
            if (operation != null)
            {
                if (PMCore_ServiceOperationRoleAccessList == null)
                {
                    var unitOfWork = EFUnitOfWork.NewUnitOfWork("CheckUserRoleAuthenticatedWithOperation");
                    var pmCoreServiceOperationRoleAccessRepository = new PMCore_ServiceOperationRoleAccessRepository(unitOfWork, applicationID);
                    PMCore_ServiceOperationRoleAccessList = (from coreServiceOperationRoleAccess in pmCoreServiceOperationRoleAccessRepository.All()
                                                             where coreServiceOperationRoleAccess.IsAllowed
                                                             select coreServiceOperationRoleAccess).ToList();
                    unitOfWork.Dispose();
                }
                var service = PMCore_ServiceOperationRoleAccessList.FirstOrDefault(p => p.ServiceOperationId == operation.ID && p.RoleId == userRoleID);
                if (service != null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}