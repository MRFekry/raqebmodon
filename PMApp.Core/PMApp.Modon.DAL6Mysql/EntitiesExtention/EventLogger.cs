﻿using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PMApp.Modon.DAL6
{
    public class EventLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private EventLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, EventLogger> EventLoggerDic = new Dictionary<long, EventLogger>();
        public static EventLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!EventLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!EventLoggerDic.ContainsKey(applicationID))
                    {
                        EventLoggerDic.Add(applicationID, new EventLogger(applicationID));
                    }
                }
            }
            return EventLoggerDic[applicationID];
        }
        public static EventLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        EventLog eventLog = null;

        public EventLog EventLog
        {
            get
            {
                if (eventLog == null)
                {
                    eventLog = new System.Diagnostics.EventLog();
                    eventLog.Log = eventLog.Source = string.Format("Application{0}Logs", applicationID);
                    ((System.ComponentModel.ISupportInitialize)(eventLog)).BeginInit();
                }
                return eventLog;
            }
        }

        public void Log(EventLogEntryType eventLogEntryType, string messageFormat, params string[] formatParams)
        {
            Log(string.Format(messageFormat, formatParams), eventLogEntryType);
        }

        public void Log(string messageFormat, params string[] formatParams)
        {
            Log(string.Format(messageFormat, formatParams));
        }

        public void Log(string message, EventLogEntryType eventLogEntryType = EventLogEntryType.Information)
        {
            try
            {
                if (ConfigurationManager.AppSettings["EnableWindowsLogging"].ToLower() == "true")
                {
                    message += "\r\n";
                    // In interactive mode ?
                    if (Environment.UserInteractive)
                    {
                        // In debug mode ?
                        if (System.Diagnostics.Debugger.IsAttached)
                        {
                            Console.WriteLine("{0}: {1}", eventLogEntryType, message);
                        }
                    }

                    Task.Run(() =>
                    {
                        EventLog.WriteEntry(message, eventLogEntryType);
                    });

                    var appCode = string.Format("Application {0}", applicationID);
                    EmailLogger.GetInstance(applicationID).NotifyByMail(Channel.Service, LogType.Event, appCode, "Message", message);
                }
            }
            catch (Exception)
            {
                //TODO Log the exception into a file
            }
        }
    }
}
