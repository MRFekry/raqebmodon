using PMApp.Core.Helpers;
using PMApp.Modon.DAL6.EntitiesExtention;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Script.Serialization;

namespace PMApp.Modon.DAL6
{
    public class TrackingLogger
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private TrackingLogger(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, TrackingLogger> TrackingLoggerDic = new Dictionary<long, TrackingLogger>();
        public static TrackingLogger GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!TrackingLoggerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!TrackingLoggerDic.ContainsKey(applicationID))
                    {
                        TrackingLoggerDic.Add(applicationID, new TrackingLogger(applicationID));
                    }
                }
            }
            return TrackingLoggerDic[applicationID];
        }
        public static TrackingLogger GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion


        //private  string defaultAppName = "SBS.LiveRep";
        private JavaScriptSerializer jsSerialaizer = new JavaScriptSerializer();

        private bool? enableTracking = null;

        public bool IsTrackingEnabled(string serviceName, string operationName)
        {
            if (IsTrackingEnabled())
            {
                var serviceOperation = ServiceOperationManager.GetInstance(applicationID).GetServiceOperation(serviceName, operationName);
                if (serviceOperation != null && serviceOperation.EnableTracking)
                    return true;
            }
            return false;
        }

        public bool IsTrackingEnabled()
        {
            if (!enableTracking.HasValue)
            {
                bool logTracking = false;
                PMExtentions.TryParse(ConfigurationManager.AppSettings["LogTracking"], out logTracking);
                enableTracking = logTracking;
            }
            return enableTracking.Value;
        }

        private const string emptyTrackStr = "[]";
        public void Log(bool? isSuccess, bool isOnline, string clientIP, long serviceOperationID, string entityName, string actionName, long recordID, string entityListStr, string trackingEntityListStr, long? currentUserID, string sessionID, string channel, long exceptionID)
        {
            try
            {
                if (IsTrackingEnabled())
                {
                    var context = EFUnitOfWork.NewUnitOfWork("LogTracking");
                    var repository = new Common_LogTrackRepository(context, applicationID);
                    if (string.IsNullOrWhiteSpace(sessionID))
                    {
                        sessionID = SessionHelper.CurrentSessionID;
                    }
                    if (entityListStr != null)
                    {
                        entityListStr = entityListStr.Replace(emptyTrackStr, "");
                    }
                    if (trackingEntityListStr != null)
                    {
                        trackingEntityListStr = trackingEntityListStr.Replace(emptyTrackStr, "");
                    }

                    var common_LogTrack = new Common_LogTrack();
                    common_LogTrack.ID = Guid.NewGuid();
                    common_LogTrack.IsOnline = isOnline;
                    common_LogTrack.OperationDate = DateTime.Now;
                    common_LogTrack.ActionName = actionName;
                    common_LogTrack.UserID = currentUserID ?? 0;
                    common_LogTrack.OperationID = serviceOperationID;
                    common_LogTrack.TableName = entityName;
                    common_LogTrack.EntityList = entityListStr;
                    common_LogTrack.TrackingEntityList = trackingEntityListStr;
                    common_LogTrack.RecordID = recordID.ToString();
                    common_LogTrack.OperationIsSuccess = isSuccess ?? false;
                    common_LogTrack.IP = clientIP;
                    common_LogTrack.ExceptionID = exceptionID;
                    common_LogTrack.HostCommunicationID = 0;
                    common_LogTrack.SessionID = sessionID;
                    repository.Add(common_LogTrack);
                    repository.Commit();
                    context.Dispose();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogger.GetInstance(applicationID).Log(ex, currentUserID, sessionID, channel);
            }
        }
    }
}