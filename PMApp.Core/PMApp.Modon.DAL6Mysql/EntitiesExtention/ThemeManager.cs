﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace PMApp.EduLegend.DAL6MySql.EntitiesExtention
{
    public class ThemeManager
    {
        #region SingleTone
        private static Object lockObj = new Object();
        private long applicationID;
        private ThemeManager(long applicationID)
        {
            this.applicationID = applicationID;
        }
        private static Dictionary<long, ThemeManager> ThemeManagerDic = new Dictionary<long, ThemeManager>();
        public static ThemeManager GetInstance(long applicationID)
        {
            if (applicationID == 0)
            {
                applicationID = ApplicationManager.GetApplicationIDFromRouteData();
                if (applicationID == 0)
                    return null;
            }
            if (!ThemeManagerDic.ContainsKey(applicationID))
            {
                lock (lockObj)
                {
                    if (!ThemeManagerDic.ContainsKey(applicationID))
                    {
                        ThemeManagerDic.Add(applicationID, new ThemeManager(applicationID));
                    }
                }
            }
            return ThemeManagerDic[applicationID];
        }
        public static ThemeManager GetUIInstance()
        {
            var applicationID = ApplicationManager.GetApplicationIDFromRouteData();
            return GetInstance(applicationID);
        }
        #endregion

        private const string chartDefaultColorsStr = "ChartDefaultColors";
        private static Dictionary<string, string> ColorDic = new Dictionary<string, string>();

        public string GetColor(string colorName)
        {
            if (ColorDic.Count == 0)
            {
                lock (lockObj)
                {
                    var colorRepository = new PMCore_ColorRepository(EFUnitOfWork.NewUnitOfWork("GetColor"), applicationID);
                    var colors = colorRepository.All().ToList();
                    for (int i = 0; i < colors.Count; i++)
                    {
                        colors[i].ColorName = colors[i].ColorName.ToLower();
                        if (!ColorDic.ContainsKey(colors[i].ColorName.ToLower()))
                        {
                            ColorDic.Add(colors[i].ColorName.ToLower(), colors[i].ColorHex);
                        }
                    }
                }
            }
            colorName = colorName.ToLower();
            if (ColorDic.ContainsKey(colorName))
                return ColorDic[colorName];
            return colorName;
        }

        public string[] GetChartColors()
        {
            var chartColorsListStr = ConfigManager.GetInstance(applicationID).GetConfig(chartDefaultColorsStr);
            var chartColorsList = chartColorsListStr.Trim().Split(new char[] { ',', '-' }, StringSplitOptions.RemoveEmptyEntries);
            var colors = new string[chartColorsList.Length];
            for (int i = 0; i < chartColorsList.Length; i++)
            {
                colors[i] = GetColor(chartColorsList[i].Trim());
            }
            return colors;
        }
    }
}
