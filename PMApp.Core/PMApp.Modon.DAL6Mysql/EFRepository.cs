using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using PMApp.Core.Helpers;
using System.Data;

namespace PMApp.Modon.DAL6
{
    public abstract class EFRepository<T> : IRepository<T> where T : class
    { 
		public long ApplicationID { get; private set; }

		public ModonDBEntities ContextEntities { get; set; }
		
        public EFRepository(ModonDBEntities contextEntities, long applicationID)
        {
            this.ContextEntities = contextEntities;
			this.ApplicationID = applicationID;
        }


        public abstract IQueryable<T> All();

        public abstract IQueryable<T> All(long userID);

        public abstract void Add(T entity);

        public abstract void Delete(T entity);

        public IQueryable<T> Where(Expression<Func<T, bool>> expression)
        {
            return All().Where(expression);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        private const string objectStr = "object";
        private const string entityStateFormat = "{0} - {1}";
        public void Commit(bool enableTracking, ref string entityListStr, ref string trackingEntityListStr)
        {
            if (enableTracking)
            {
                if (entityListStr == null)
                    entityListStr = string.Empty;
                if (trackingEntityListStr == null)
                    trackingEntityListStr = string.Empty;
                try
                {
                    var trackingEntityList = new List<TrackingEntity>();
                    var entityList = new List<string>();
                    var auditedEntities = this.ContextEntities.ChangeTracker.Entries()
                                  .Where(e => e.State != EntityState.Unchanged);

                    foreach (var changedEntity in auditedEntities)
                    {
                        var entityName = changedEntity.Entity.GetType().BaseType.Name;
                        var status = changedEntity.State.ToString();

                        if (entityName.ToLower() == objectStr)
                            entityName = changedEntity.Entity.GetType().Name;

                        var entityState = string.Format(entityStateFormat, entityName, status);
                        if (!entityList.Contains(entityState))
                            entityList.Add(entityState);

                        if (changedEntity.State == EntityState.Added)
                        {
                            foreach (string propertyName in changedEntity.CurrentValues.PropertyNames)
                            {
                                trackingEntityList.Add(new TrackingEntity
                                {
                                    Action = status,
                                    EntityName = entityName,
                                    PropertyName = propertyName,
                                    OldValue = null,
                                    NewValue = changedEntity.CurrentValues[propertyName]
                                });
                            }
                        }
                        else if (changedEntity.State == EntityState.Modified)
                        {
                            // Log Modified
                            var originalValues = changedEntity.OriginalValues;
                            var currentValues = changedEntity.CurrentValues;

                            foreach (string propertyName in originalValues.PropertyNames)
                            {
                                var original = originalValues[propertyName];
                                var current = currentValues[propertyName];

                                if (!Equals(original, current))
                                {
                                    trackingEntityList.Add(new TrackingEntity
                                    {
                                        Action = status,
                                        EntityName = entityName,
                                        PropertyName = propertyName,
                                        OldValue = original,
                                        NewValue = current
                                    });
                                }
                            }

                        }
                        else if (changedEntity.State == EntityState.Deleted)
                        {
                            // log deleted
                            var propertyName = changedEntity.CurrentValues.PropertyNames.FirstOrDefault();
                            trackingEntityList.Add(new TrackingEntity
                            {
                                Action = status,
                                EntityName = entityName,
                                PropertyName = propertyName,
                                NewValue = null,
                                OldValue = changedEntity.CurrentValues[propertyName]
                            });
                        }
                    }
                    entityListStr += PMExtentions.TrySerialize(entityList);
                    trackingEntityListStr += PMExtentions.TrySerialize(trackingEntityList);
                }
                catch (Exception ex)
                {

                }
            }

            this.Commit();
        }

        public void Commit()
        {
            this.ContextEntities.SaveChanges();
        }
    }

    class TrackingEntity
    {
        public string Action { get; set; }
        public string EntityName { get; set; }
        public string PropertyName { get; set; }
        public object OldValue { get; set; }
        public object NewValue { get; set; }
    }

}