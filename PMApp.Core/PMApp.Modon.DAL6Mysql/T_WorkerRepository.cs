		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class T_WorkerRepository : EFRepository<T_Worker>
    {
        public T_WorkerRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<T_Worker> All()
        {
            return ContextEntities.T_Worker.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<T_Worker> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(T_Worker entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.T_Worker.Add(entity);
        }

        public override void Delete(T_Worker entity)
        {
            ContextEntities.T_Worker.Remove(entity);
        }
    }
}