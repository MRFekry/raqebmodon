		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class UserLocationRepository : EFRepository<UserLocation>
    {
        public UserLocationRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<UserLocation> All()
        {
            return ContextEntities.UserLocations.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<UserLocation> All(long userID)
        {
			return All();
        }

        public override void Add(UserLocation entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.UserLocations.Add(entity);
        }

        public override void Delete(UserLocation entity)
        {
            ContextEntities.UserLocations.Remove(entity);
        }
    }
}