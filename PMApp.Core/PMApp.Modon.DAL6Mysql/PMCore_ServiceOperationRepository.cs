		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_ServiceOperationRepository : EFRepository<PMCore_ServiceOperation>
    {
        public PMCore_ServiceOperationRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_ServiceOperation> All()
        {
            return ContextEntities.PMCore_ServiceOperation.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_ServiceOperation> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_ServiceOperation entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_ServiceOperation.Add(entity);
        }

        public override void Delete(PMCore_ServiceOperation entity)
        {
            ContextEntities.PMCore_ServiceOperation.Remove(entity);
        }
    }
}