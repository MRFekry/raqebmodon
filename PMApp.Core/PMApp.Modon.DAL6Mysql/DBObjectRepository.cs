		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class DBObjectRepository : EFRepository<DBObject>
    {
        public DBObjectRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<DBObject> All()
        {
            return ContextEntities.DBObjects.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<DBObject> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(DBObject entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.DBObjects.Add(entity);
        }

        public override void Delete(DBObject entity)
        {
            ContextEntities.DBObjects.Remove(entity);
        }
    }
}