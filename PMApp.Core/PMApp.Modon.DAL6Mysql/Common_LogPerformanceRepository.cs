		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_LogPerformanceRepository : EFRepository<Common_LogPerformance>
    {
        public Common_LogPerformanceRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_LogPerformance> All()
        {
            return ContextEntities.Common_LogPerformance.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_LogPerformance> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_LogPerformance entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_LogPerformance.Add(entity);
        }

        public override void Delete(Common_LogPerformance entity)
        {
            ContextEntities.Common_LogPerformance.Remove(entity);
        }
    }
}