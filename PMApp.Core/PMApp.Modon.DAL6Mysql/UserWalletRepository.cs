		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class UserWalletRepository : EFRepository<UserWallet>
    {
        public UserWalletRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<UserWallet> All()
        {
            return ContextEntities.UserWallets.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<UserWallet> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(UserWallet entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.UserWallets.Add(entity);
        }

        public override void Delete(UserWallet entity)
        {
            ContextEntities.UserWallets.Remove(entity);
        }
    }
}