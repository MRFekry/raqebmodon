		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_LogHostCommunicationRepository : EFRepository<Common_LogHostCommunication>
    {
        public Common_LogHostCommunicationRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_LogHostCommunication> All()
        {
            return ContextEntities.Common_LogHostCommunication.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_LogHostCommunication> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_LogHostCommunication entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_LogHostCommunication.Add(entity);
        }

        public override void Delete(Common_LogHostCommunication entity)
        {
            ContextEntities.Common_LogHostCommunication.Remove(entity);
        }
    }
}