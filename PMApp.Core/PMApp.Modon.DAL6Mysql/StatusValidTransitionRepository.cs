		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class StatusValidTransitionRepository : EFRepository<StatusValidTransition>
    {
        public StatusValidTransitionRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<StatusValidTransition> All()
        {
            return ContextEntities.StatusValidTransitions.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<StatusValidTransition> All(long userID)
        {
			return All();
        }

        public override void Add(StatusValidTransition entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.StatusValidTransitions.Add(entity);
        }

        public override void Delete(StatusValidTransition entity)
        {
            ContextEntities.StatusValidTransitions.Remove(entity);
        }
    }
}