		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class NewsFeedRepository : EFRepository<NewsFeed>
    {
        public NewsFeedRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<NewsFeed> All()
        {
            return ContextEntities.NewsFeeds.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<NewsFeed> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(NewsFeed entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.NewsFeeds.Add(entity);
        }

        public override void Delete(NewsFeed entity)
        {
            ContextEntities.NewsFeeds.Remove(entity);
        }
    }
}