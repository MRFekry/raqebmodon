		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_Widget_RoleRepository : EFRepository<PMCore_Widget_Role>
    {
        public PMCore_Widget_RoleRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Widget_Role> All()
        {
            return ContextEntities.PMCore_Widget_Role.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Widget_Role> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Widget_Role entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Widget_Role.Add(entity);
        }

        public override void Delete(PMCore_Widget_Role entity)
        {
            ContextEntities.PMCore_Widget_Role.Remove(entity);
        }
    }
}