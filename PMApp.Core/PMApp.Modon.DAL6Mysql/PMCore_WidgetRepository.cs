		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_WidgetRepository : EFRepository<PMCore_Widget>
    {
        public PMCore_WidgetRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Widget> All()
        {
            return ContextEntities.PMCore_Widget.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Widget> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Widget entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Widget.Add(entity);
        }

        public override void Delete(PMCore_Widget entity)
        {
            ContextEntities.PMCore_Widget.Remove(entity);
        }
    }
}