		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_UserRepository : EFRepository<Common_User>
    {
        public Common_UserRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_User> All()
        {
            return ContextEntities.Common_User.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_User> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_User entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_User.Add(entity);
        }

        public override void Delete(Common_User entity)
        {
            ContextEntities.Common_User.Remove(entity);
        }
    }
}