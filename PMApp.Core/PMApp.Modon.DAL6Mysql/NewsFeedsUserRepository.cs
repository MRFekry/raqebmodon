		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class NewsFeedsUserRepository : EFRepository<NewsFeedsUser>
    {
        public NewsFeedsUserRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<NewsFeedsUser> All()
        {
            return ContextEntities.NewsFeedsUsers.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<NewsFeedsUser> All(long userID)
        {
			return All();
        }

        public override void Add(NewsFeedsUser entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.NewsFeedsUsers.Add(entity);
        }

        public override void Delete(NewsFeedsUser entity)
        {
            ContextEntities.NewsFeedsUsers.Remove(entity);
        }
    }
}