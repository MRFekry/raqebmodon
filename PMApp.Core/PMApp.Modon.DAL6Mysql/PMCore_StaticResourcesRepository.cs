		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_StaticResourcesRepository : EFRepository<PMCore_StaticResources>
    {
        public PMCore_StaticResourcesRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_StaticResources> All()
        {
            return ContextEntities.PMCore_StaticResources.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_StaticResources> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_StaticResources entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_StaticResources.Add(entity);
        }

        public override void Delete(PMCore_StaticResources entity)
        {
            ContextEntities.PMCore_StaticResources.Remove(entity);
        }
    }
}