﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace PMApp.Modon.DAL6
{ 
    public interface IRepository<T> where T : class
    {
        ModonDBEntities ContextEntities { get; set; }
		long ApplicationID { get; }

        IQueryable<T> All();
        IQueryable<T> All(long userID);
        void Add(T entity);
        void Delete(T entity);
        IQueryable<T> Where(Expression<Func<T, bool>> expression);
        T FirstOrDefault(Expression<Func<T, bool>> expression);
        void Commit(bool enableTracking, ref string entityListStr, ref string trackingEntityListStr);
        void Commit();
    }
}

