		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Comment_TypeRepository : EFRepository<Comment_Type>
    {
        public Comment_TypeRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Comment_Type> All()
        {
            return ContextEntities.Comment_Type.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Comment_Type> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Comment_Type entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Comment_Type.Add(entity);
        }

        public override void Delete(Comment_Type entity)
        {
            ContextEntities.Comment_Type.Remove(entity);
        }
    }
}