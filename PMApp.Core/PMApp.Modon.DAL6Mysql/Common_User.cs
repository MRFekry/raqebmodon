//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.Modon.DAL6
{
    using System;
    using System.Collections.Generic;
    
    public partial class Common_User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Common_User()
        {
            this.Common_LogException = new HashSet<Common_LogException>();
            this.Common_LogHostCommunication = new HashSet<Common_LogHostCommunication>();
            this.Common_UserDevice = new HashSet<Common_UserDevice>();
            this.NewsFeeds = new HashSet<NewsFeed>();
            this.NewsFeedsUsers = new HashSet<NewsFeedsUser>();
            this.T_HealthCard = new HashSet<T_HealthCard>();
            this.UserLocations = new HashSet<UserLocation>();
            this.UserWallets = new HashSet<UserWallet>();
        }
    
        public long ID { get; set; }
        public string EmployeeCode { get; set; }
        public System.DateTime RegisterDate { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> Birthdate { get; set; }
        public long RoleID { get; set; }
        public Nullable<long> ManagerID { get; set; }
        public bool IsActive { get; set; }
        public string Notes { get; set; }
        public Nullable<long> AreaID { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public long ApplicationID { get; set; }
        public Nullable<long> Phone1 { get; set; }
    
        public virtual Area Area { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_LogException> Common_LogException { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_LogHostCommunication> Common_LogHostCommunication { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_UserDevice> Common_UserDevice { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NewsFeed> NewsFeeds { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NewsFeedsUser> NewsFeedsUsers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_HealthCard> T_HealthCard { get; set; }
        public virtual PMCore_Role PMCore_Role { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserLocation> UserLocations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserWallet> UserWallets { get; set; }
    }
}
