		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_MenuRepository : EFRepository<PMCore_Menu>
    {
        public PMCore_MenuRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Menu> All()
        {
            return ContextEntities.PMCore_Menu.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Menu> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Menu entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Menu.Add(entity);
        }

        public override void Delete(PMCore_Menu entity)
        {
            ContextEntities.PMCore_Menu.Remove(entity);
        }
    }
}