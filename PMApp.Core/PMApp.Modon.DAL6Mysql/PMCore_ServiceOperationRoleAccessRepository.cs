		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_ServiceOperationRoleAccessRepository : EFRepository<PMCore_ServiceOperationRoleAccess>
    {
        public PMCore_ServiceOperationRoleAccessRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_ServiceOperationRoleAccess> All()
        {
            return ContextEntities.PMCore_ServiceOperationRoleAccess.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_ServiceOperationRoleAccess> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_ServiceOperationRoleAccess entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_ServiceOperationRoleAccess.Add(entity);
        }

        public override void Delete(PMCore_ServiceOperationRoleAccess entity)
        {
            ContextEntities.PMCore_ServiceOperationRoleAccess.Remove(entity);
        }
    }
}