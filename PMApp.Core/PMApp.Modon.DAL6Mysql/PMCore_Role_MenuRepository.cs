		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_Role_MenuRepository : EFRepository<PMCore_Role_Menu>
    {
        public PMCore_Role_MenuRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Role_Menu> All()
        {
            return ContextEntities.PMCore_Role_Menu.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Role_Menu> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Role_Menu entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Role_Menu.Add(entity);
        }

        public override void Delete(PMCore_Role_Menu entity)
        {
            ContextEntities.PMCore_Role_Menu.Remove(entity);
        }
    }
}