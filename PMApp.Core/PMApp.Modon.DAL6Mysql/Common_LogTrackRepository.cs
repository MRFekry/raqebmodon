		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Common_LogTrackRepository : EFRepository<Common_LogTrack>
    {
        public Common_LogTrackRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Common_LogTrack> All()
        {
            return ContextEntities.Common_LogTrack.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Common_LogTrack> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Common_LogTrack entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Common_LogTrack.Add(entity);
        }

        public override void Delete(Common_LogTrack entity)
        {
            ContextEntities.Common_LogTrack.Remove(entity);
        }
    }
}