		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class WebMediaTypeRepository : EFRepository<WebMediaType>
    {
        public WebMediaTypeRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<WebMediaType> All()
        {
            return ContextEntities.WebMediaTypes.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<WebMediaType> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(WebMediaType entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.WebMediaTypes.Add(entity);
        }

        public override void Delete(WebMediaType entity)
        {
            ContextEntities.WebMediaTypes.Remove(entity);
        }
    }
}