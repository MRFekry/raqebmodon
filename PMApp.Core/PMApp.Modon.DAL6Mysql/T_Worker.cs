//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.Modon.DAL6
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_Worker
    {
        public long ID { get; set; }
        public string W_Name { get; set; }
        public string W_NID_NO { get; set; }
        public Nullable<System.DateTime> W_NID_StartDate { get; set; }
        public Nullable<System.DateTime> W_NID_EndDate { get; set; }
        public string W_NID_imgURL { get; set; }
        public string W_imgURL { get; set; }
        public string W_Job { get; set; }
        public string W_Nationality { get; set; }
        public Nullable<System.DateTime> W_BirthDate { get; set; }
        public string W_Relagion { get; set; }
        public string W_Mobile { get; set; }
        public string W_KafeelName { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public long CreatedBy { get; set; }
        public System.DateTime CreationDate { get; set; }
        public bool IsDeleted { get; set; }
        public long ApplicationID { get; set; }
        public Nullable<long> BuildingID { get; set; }
    
        public virtual T_HealthCard T_HealthCard { get; set; }
    }
}
