		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class Activity_SubTypeRepository : EFRepository<Activity_SubType>
    {
        public Activity_SubTypeRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Activity_SubType> All()
        {
            return ContextEntities.Activity_SubType.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Activity_SubType> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Activity_SubType entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Activity_SubType.Add(entity);
        }

        public override void Delete(Activity_SubType entity)
        {
            ContextEntities.Activity_SubType.Remove(entity);
        }
    }
}