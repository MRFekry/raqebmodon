		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_ParameterRepository : EFRepository<PMCore_Parameter>
    {
        public PMCore_ParameterRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Parameter> All()
        {
            return ContextEntities.PMCore_Parameter.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Parameter> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Parameter entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Parameter.Add(entity);
        }

        public override void Delete(PMCore_Parameter entity)
        {
            ContextEntities.PMCore_Parameter.Remove(entity);
        }
    }
}