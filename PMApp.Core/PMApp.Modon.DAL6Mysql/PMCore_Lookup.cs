//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMApp.Modon.DAL6
{
    using System;
    using System.Collections.Generic;
    
    public partial class PMCore_Lookup
    {
        public long ID { get; set; }
        public string LookupName { get; set; }
        public string LookupKey { get; set; }
        public string LookupText { get; set; }
        public Nullable<bool> Enable { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public System.DateTime CreationDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> LastUpdateDate { get; set; }
        public long ApplicationID { get; set; }
    }
}
