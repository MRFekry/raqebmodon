		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_ChannelRepository : EFRepository<PMCore_Channel>
    {
        public PMCore_ChannelRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Channel> All()
        {
            return ContextEntities.PMCore_Channel.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Channel> All(long userID)
        {
			return All();
        }

        public override void Add(PMCore_Channel entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Channel.Add(entity);
        }

        public override void Delete(PMCore_Channel entity)
        {
            ContextEntities.PMCore_Channel.Remove(entity);
        }
    }
}