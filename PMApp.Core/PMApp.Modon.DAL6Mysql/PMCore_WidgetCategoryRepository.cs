		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_WidgetCategoryRepository : EFRepository<PMCore_WidgetCategory>
    {
        public PMCore_WidgetCategoryRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_WidgetCategory> All()
        {
            return ContextEntities.PMCore_WidgetCategory.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_WidgetCategory> All(long userID)
        {
			return All();
        }

        public override void Add(PMCore_WidgetCategory entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_WidgetCategory.Add(entity);
        }

        public override void Delete(PMCore_WidgetCategory entity)
        {
            ContextEntities.PMCore_WidgetCategory.Remove(entity);
        }
    }
}