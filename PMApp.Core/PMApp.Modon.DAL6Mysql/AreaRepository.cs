		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class AreaRepository : EFRepository<Area>
    {
        public AreaRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<Area> All()
        {
            return ContextEntities.Areas.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<Area> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(Area entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.Areas.Add(entity);
        }

        public override void Delete(Area entity)
        {
            ContextEntities.Areas.Remove(entity);
        }
    }
}