		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class ModonOrderRepository : EFRepository<ModonOrder>
    {
        public ModonOrderRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<ModonOrder> All()
        {
            return ContextEntities.ModonOrders.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<ModonOrder> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(ModonOrder entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.ModonOrders.Add(entity);
        }

        public override void Delete(ModonOrder entity)
        {
            ContextEntities.ModonOrders.Remove(entity);
        }
    }
}