		
using System;
using System.Linq;
    
namespace PMApp.Modon.DAL6
{   
    public partial class PMCore_LookupRepository : EFRepository<PMCore_Lookup>
    {
        public PMCore_LookupRepository(ModonDBEntities contextEntities, long applicationID)
            : base(contextEntities, applicationID)
        {
        }

        public override IQueryable<PMCore_Lookup> All()
        {
            return ContextEntities.PMCore_Lookup.Where(p => p.ApplicationID == ApplicationID);
        }
		
        public override IQueryable<PMCore_Lookup> All(long userID)
        {
			
			return All(); 
        }

        public override void Add(PMCore_Lookup entity)
        {
            if(ApplicationID == 0)
				throw new Exception("Added Entity should have valid applicationID");
			entity.ApplicationID = ApplicationID;
			ContextEntities.PMCore_Lookup.Add(entity);
        }

        public override void Delete(PMCore_Lookup entity)
        {
            ContextEntities.PMCore_Lookup.Remove(entity);
        }
    }
}