using System.Linq;

namespace  PMApp.Core.Helpers
{
    public static class FilterManager
    {
        public static IQueryable<T> Search<T>(this IQueryable<T> source, string propertyName, object value)
        {
            IQueryable<T> values = source.Where(t => t.GetType().GetProperty(propertyName).GetValue(t, null) == value);
            return values;
        }
    }
}
