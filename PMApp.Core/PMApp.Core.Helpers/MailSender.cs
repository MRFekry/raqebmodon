using System;
using System.Net.Mail;

namespace  PMApp.Core.Helpers
{
    public class MailSender
    {
        string host;
        int port;
        string password;
        string fromMail;
        bool enableSsl;
        bool useDefaultCredentials;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sendEmailFromMail">ResourceRepository.GetResource("SendMail.From", null)</param>
        /// <param name="sendEmailFromMailPassword">ResourceRepository.GetResource("SendMail.Password", null)</param>
        /// <param name="sendEmailHost">ResourceRepository.GetResource("SendMail.Host", null)</param>
        /// <param name="sendEmailPort">ResourceRepository.GetResource("SendMail.Port", null)</param>
        /// <param name="sendEmailEnableSsl">ResourceRepository.GetResource("SendMail.EnableSsl", null))</param>
        /// <param name="sendEmailUseDefaultCredentials">ResourceRepository.GetResource("SendMail.UseDefaultCredentials", null))</param>
        public MailSender(string sendEmailFromMail, string sendEmailFromMailPassword, string sendEmailHost, string sendEmailPort, string sendEmailEnableSsl, string sendEmailUseDefaultCredentials)
        {
            this.fromMail = sendEmailFromMail;// ResourceRepository.GetResource("SendMail.From", null)}
            this.password = sendEmailFromMailPassword;//  ResourceRepository.GetResource("SendMail.Password", null)
            this.host = sendEmailHost;// ResourceRepository.GetResource("SendMail.Host", null);
            this.port = int.Parse(sendEmailPort);//ResourceRepository.GetResource("SendMail.Port", null)
            this.enableSsl = bool.Parse(sendEmailEnableSsl); // ResourceRepository.GetResource("SendMail.EnableSsl", null));
            this.useDefaultCredentials = bool.Parse(sendEmailUseDefaultCredentials);// ResourceRepository.GetResource("SendMail.UseDefaultCredentials", null));
        }

        //Satrt Send Email Function
        public bool SendMail(string to, string subject, string body, bool isBodyHtml = true)
        {
            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string msg = string.Empty;

            MailAddress fromAddress = new MailAddress(this.fromMail);
            message.From = fromAddress;
            var toList = to.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < toList.Length; i++)
            {
                message.To.Add(toList[i]);
            }
            message.Subject = subject;
            message.IsBodyHtml = isBodyHtml;
            message.Body = body;
            smtpClient.Host = this.host;
            smtpClient.Port = this.port;
            smtpClient.EnableSsl = this.enableSsl;
            smtpClient.UseDefaultCredentials = this.useDefaultCredentials;
            smtpClient.Credentials = new System.Net.NetworkCredential(this.fromMail, this.password);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            smtpClient.Send(message);

            return true;
        }
        //End Send Email Function

    }
}
