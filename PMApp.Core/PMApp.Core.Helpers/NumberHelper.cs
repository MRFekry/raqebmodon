
using System;
namespace PMApp.Core.Helpers
{

    public static class NumberHelper
    {
        public static long? ConvertToNullable(this long number)
        {
            if (number.Equals(0))
                return null;
            return number;
        }

        public static int? ConvertToNullable(this int number)
        {
            if (number.Equals(0))
                return null;
            return number;
        }

        public static double RoundCurrrency(double value, string divider)
        {
            if (!string.IsNullOrEmpty(divider))
            {
                if (divider=="USD")
                {
                    var returenedVal = Math.Round(value * 2) / 2;
                    return returenedVal;   
                }
                else if(divider=="EGP")
                {
                    var returenedVal = Math.Ceiling(value);
                    return returenedVal; 
                }
                else
                    return value;
                
            }
            else
                return value;
        }

        public static decimal? ConvertToNullable(this decimal number)
        {
            if (number.Equals(0))
                return null;
            return number;
        }

        public static float? ConvertToNullable(this float number)
        {
            if (number.Equals(0))
                return null;
            return number;
        }
    }
}