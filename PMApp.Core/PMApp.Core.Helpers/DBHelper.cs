using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace  PMApp.Core.Helpers
{
    public static class DBHelper
    {
        public static object ExecuteScaler(string connectionString, string sql, params Parameter[] sqlParams)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                using (var cmd = (SqlCommand)conn.CreateCommand())
                {
                    cmd.CommandTimeout = 0;
                    if (sqlParams != null)
                    {
                        for (int i = 0; i < sqlParams.Length; i++)
                        {
                            AddSqlCmdParameter(sqlParams[i], cmd);
                        }
                    }
                    conn.Open();  // open connection if not already open
                    cmd.CommandText = sql;
                    return cmd.ExecuteScalar();
                }
            }
        }

        public static DataTable ExecuteProcedure(string connectionString, string procedureName, params Parameter[] sqlParams)
        {
            var dt = new DataTable();
            if (!string.IsNullOrWhiteSpace(procedureName))
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    using (var cmd = (SqlCommand)conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = procedureName;
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (sqlParams != null)
                        {
                            for (int i = 0; i < sqlParams.Length; i++)
                                AddSqlCmdParameter(sqlParams[i], cmd);
                        }
                        var da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            return dt;
        }

        public static DataTable ExecuteProcedureForWidget(string connectionString, string procedureName, long applicationID, params Parameter[] sqlParams)
        {
            var dt = new DataTable();
            if (!string.IsNullOrWhiteSpace(procedureName))
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    using (var cmd = (SqlCommand)conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = procedureName;
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (sqlParams != null && sqlParams.Count()>0)
                        {
                            for (int i = 0; i < sqlParams.Length; i++)
                            {
                                AddSqlCmdParameter(sqlParams[i], cmd);
                            }
                            //cmd.Parameters.AddWithValue("pApplicationID", applicationID.ToString());
                        }
                        var da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            return dt;
        }

        public static DataTable ExecuteQuery(string connectionString, string query, long applicationID, params Parameter[] sqlParams)
        {
            var dt = new DataTable();

            using (var conn = new SqlConnection(connectionString))
            {
                using (var cmd = (SqlCommand)conn.CreateCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = query;
                    if (sqlParams != null)
                    {
                        for (int i = 0; i < sqlParams.Length; i++)
                        {
                            AddSqlCmdParameter(sqlParams[i], cmd);
                        }
                    }
                    cmd.Parameters.AddWithValue("@ApplicationID", applicationID.ToString());
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable ExecuteQuery(string connectionString, string query, params Parameter[] sqlParams)
        {
            var dt = new DataTable();

            using (var conn = new SqlConnection(connectionString))
            {
                using (var cmd = (SqlCommand)conn.CreateCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = query;
                    if (sqlParams != null)
                    {
                        for (int i = 0; i < sqlParams.Length; i++)
                        {
                            AddSqlCmdParameter(sqlParams[i], cmd);
                        }
                    }
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }
            }
            return dt;
        }

        public static DataTable GetSPColumns(string connectionString, string sPName, long applicationId, DataTable prmsDT)
        {
            var paramsList = new List<Parameter>();
            for (int i = 0; i < prmsDT.Rows.Count; i++)
            {
                paramsList.Add(new Parameter { Name = prmsDT.Rows[i]["Parameter_Name"].ToString() });
            }
            var dataTable = DBHelper.ExecuteProcedure(connectionString, sPName, paramsList.ToArray());
            if (dataTable != null)
            {
                var columnsTable = new DataTable();
                columnsTable.Columns.Add("ColumnName");
                columnsTable.Columns.Add("ColumnDataType");

                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var row = columnsTable.NewRow();
                    row[0] = dataTable.Columns[i].ColumnName;
                    row[1] = dataTable.Columns[i].DataType.ToString();
                    columnsTable.Rows.Add(row);
                }

                return columnsTable;
            }
            return null;
        }

        public static DataTable GetSPParameters(string connectionString, string sPName, long applicationId)
        {
            return DBHelper.ExecuteProcedure(connectionString, "GetSPParameters", new Parameter { Name = "spName", DefaultValue = sPName });
        }


        private static void AddSqlCmdParameter(Parameter sqlParam, SqlCommand cmd)
        {
            if (sqlParam.Type == TypeCode.Object)
            {
                var table = new DataTable();

                var ids = sqlParam.DefaultValue.Split(new char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries).Select(p => long.Parse(p.Trim())).ToArray();
                table.Columns.Add("ID", typeof(long));
                foreach (long id in ids)
                {
                    table.Rows.Add(id);
                }
                cmd.Parameters.AddWithValue(sqlParam.Name, table);
            }
            else if ((sqlParam.Name == "@FromDate" || sqlParam.Name == "@ToDate") && !string.IsNullOrWhiteSpace( sqlParam.DefaultValue))
            {
                
                cmd.Parameters.AddWithValue(sqlParam.Name, Convert.ToDateTime(sqlParam.DefaultValue));
            }
            else
            {
                cmd.Parameters.AddWithValue(sqlParam.Name, sqlParam.DefaultValue);
            }
        }

    }
}