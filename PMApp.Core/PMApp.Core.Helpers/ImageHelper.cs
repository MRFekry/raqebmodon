using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace  PMApp.Core.Helpers
{
    public class ImageHelper
    {
        private const string ProfilePicturesPath = "~/UploadedImages/Profile/{0}-{1}.jpg";
        private const string UploadedImagesPath = "~/UploadedImages/{0}.jpg";

        private const double maxImageSize = 0.5 * 1024 * 1024; // 0.5 MB
        private const int JpegQuality = 60;

        public static string ImageResizeAndSave(HttpPostedFileBase file, int width, int height)
        {
            if (file != null && file.ContentLength > 0)
            {
                var imagePath = string.Format(UploadedImagesPath, Guid.NewGuid().ToString());
                var imagePhysicialPath = UIHelper.MapPath(imagePath);

                byte[] byteArrayIn;

                using (var reader = new BinaryReader(file.InputStream))
                {
                    byteArrayIn = reader.ReadBytes(file.ContentLength);
                    ResizeImageBytes(width, height, imagePhysicialPath, byteArrayIn);
                }
                return imagePath;
            }
            return string.Empty;
        }

        public static string ImageResizeAndSave(string imageBase64, int width, int height)
        {
            if (!string.IsNullOrWhiteSpace(imageBase64))
            {
                var imagePath = string.Format(UploadedImagesPath, Guid.NewGuid().ToString());
                var imagePhysicialPath = UIHelper.MapPath(imagePath);

                var byteArrayIn = Convert.FromBase64String(imageBase64);

                ResizeImageBytes(width, height, imagePhysicialPath, byteArrayIn);

                return imagePath;
            }
            return string.Empty;
        }

        private static void ResizeImageBytes(int width, int height, string imagePhysicialPath, byte[] byteArrayIn)
        {
            if (byteArrayIn.Length > maxImageSize)
            {
                var ms = new MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
                ms.Position = 0; // this is important
                var originalImage = new Bitmap(ms, true);

                var resized = ImageHelper.ResizeImage(originalImage, new Size(width, height), true);

                var jpegEncoder = ImageCodecInfo.GetImageEncoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.ColorDepth, JpegQuality);

                resized.Save(imagePhysicialPath, jpegEncoder, encoderParameters);
            }
            else
            {
                File.WriteAllBytes(imagePhysicialPath, byteArrayIn);
            }
        }

        private static Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true)
        {
            // C#
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }

            // C#
            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.Default;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        public static Bitmap CombineBitmap(int width, int height, params string[] files)
        {
            //read all images into memory
            List<System.Drawing.Bitmap> images = new List<System.Drawing.Bitmap>();
            System.Drawing.Bitmap finalImage = null;

            try
            {
                foreach (string image in files)
                {
                    //create a Bitmap from the file and add it to the list
                    System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(image);

                    images.Add(bitmap);
                }

                //create a bitmap to hold the combined image
                finalImage = new System.Drawing.Bitmap(width, height);

                //get a graphics object from the image so we can draw on it
                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(finalImage))
                {
                    //set background color
                    g.Clear(System.Drawing.Color.Transparent);

                    //go through each image and draw it on the final image
                    int offset = 0;
                    foreach (System.Drawing.Bitmap image in images)
                    {
                        g.DrawImage(image, new System.Drawing.Rectangle(offset, 0, image.Width, image.Height));
                        //offset += image.Width;
                    }
                }

                return finalImage;
            }
            catch (Exception ex)
            {
                if (finalImage != null)
                    finalImage.Dispose();

                throw ex;
            }
            finally
            {
                //clean up memory
                foreach (System.Drawing.Bitmap image in images)
                {
                    image.Dispose();
                }
            }
        }

    }
}
