﻿using System;
using System.Web.Mvc;

namespace  PMApp.Core.Helpers.MVCBinders
{
    public class EncryptDataBinder<T> : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueProviderResult != null)
            {
                if (valueProviderResult.RawValue is Array)
                    return SecurityHelper.DecryptQueryValue<T>((string)((Array)valueProviderResult.RawValue).GetValue(0));
                else
                    return SecurityHelper.DecryptQueryValue<T>((string)valueProviderResult.RawValue);
            }
            return base.BindModel(controllerContext, bindingContext);

        }
    }

}
