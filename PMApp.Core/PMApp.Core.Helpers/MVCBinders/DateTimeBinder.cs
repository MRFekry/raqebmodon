using System;
using System.Web.Mvc;

namespace  PMApp.Core.Helpers.MVCBinders
{
    public class DateTimeBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var dateStr =(bindingContext.ValueProvider.GetValue(bindingContext.ModelName)==null)?null: bindingContext.ValueProvider.GetValue(bindingContext.ModelName).AttemptedValue;

            if (String.IsNullOrEmpty(dateStr))
                return null;

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, bindingContext.ValueProvider.GetValue(bindingContext.ModelName));

            DateTime date;
            if (DateTimeHelper.TryParseToDateTimeDD_MM_YYYY_TIME(dateStr, out date))
                return date;
            return null;
        }
    }
}
